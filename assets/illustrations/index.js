import IconWishlist from "./ic-wishlist.svg";
import IconGrid from "./ic-grid.svg";
import IconList from "./ic-list.svg";
import IconDelete from "./delete.svg";
import IconBriefcase from "./briefcase-icon.svg";
import IconCheck from "./check-icon.svg";
import IconFailed from "./failed-icon.svg";

export { IconWishlist, IconGrid, IconList, IconDelete, IconBriefcase, IconCheck, IconFailed };

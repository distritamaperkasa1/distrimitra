import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import Withdraw from "./Withdraw";
import useUser from "../../lib/useUser";
import { requestGet } from "../../lib/axios";
import { moneyFormat } from "../../lib/helper";
import styles from "../../styles/Components/Account.module.css";

const Index = () => {
    const { user } = useUser();
    const router = useRouter();
    const [storeCredit, setStoreCredit] = useState("");
    const [withdrawMode, setWithdrawMode] = useState(false);
    const [page, setPage] = useState(1);
    const [size, setSize] = useState(10);
    const [loading, setLoading] = useState(false);
    const [creditHistory, setCreditHistory] = useState({});

    const getStoreCredit = async () => {
        try {
            const resp = await requestGet(
                "/rest/V1/carts/mine/aw-get-customer-store-credit",
                null,
                user?.token
            );
            setStoreCredit(resp);
        } catch (error) {}
    };

    const getCreditHistory = async () => {
        try {
            const resp = await requestGet(
                `/rest/V1/list-storecredit?pageSize=${size}&currentPage=${page}`,
                null,
                user?.token
            );
            setCreditHistory(resp);
        } catch (error) {}
    };

    useEffect(() => {
        getStoreCredit();
    }, [withdrawMode, user]);

    useEffect(() => {
        getCreditHistory();
    }, [page, size, user]);

    const Rows = () => {
        let rows = [];

        creditHistory?.items?.map((data, index) => {
            console.log("Contains -", data.balance.includes("-"));
            rows.push(
                <tr key={data.id}>
                    <td
                        data-header="Transaction ID"
                        className="xl:p-6 font-bold xl:font-normal"
                    >
                        {data.id}
                    </td>
                    <td
                        data-header="Credit Adjustment"
                        className={`xl:p-6 font-bold xl:font-normal ${
                            data.balance.includes("-")
                                ? "text-red-600"
                                : "text-green-600"
                        }`}
                    >
                        {data.balance}
                    </td>
                    <td
                        data-header="Credit Balance"
                        className="xl:p-6 font-bold xl:font-normal"
                    >
                        {data.current_balance}
                    </td>
                    <td
                        data-header="Comment"
                        className="xl:p-6 font-bold xl:font-normal"
                    >
                        <p>{data.comment}</p>
                    </td>
                    <td
                        data-header="Transaction Date"
                        className="xl:p-6 font-bold xl:font-normal"
                    >
                        {data.date}
                    </td>
                </tr>
            );
        });

        return rows;
    };

    return (
        <div
            className={`mt-2 xl:mt-0 text-secondary-dark-grey mb-10 text-base`}
        >
            <div className="flex items-center mb-5 relative xl:static">
                <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className={`h-5 w-5 cursor-pointer absolute top-1 xl:top-2 ${
                        withdrawMode ? "" : "hidden"
                    }`}
                    viewBox="0 0 20 20"
                    fill="currentColor"
                    onClick={() => setWithdrawMode(false)}
                >
                    <path
                        fillRule="evenodd"
                        d="M9.707 16.707a1 1 0 01-1.414 0l-6-6a1 1 0 010-1.414l6-6a1 1 0 011.414 1.414L5.414 9H17a1 1 0 110 2H5.414l4.293 4.293a1 1 0 010 1.414z"
                        clipRule="evenodd"
                    />
                </svg>
                <h1 className="font-bold text-lg flex-1 text-center text-black xl:hidden">
                    Store Credit & Refund
                </h1>
            </div>

            {withdrawMode ? (
                <Withdraw
                    balance={storeCredit.customer_store_credit_balance}
                    setWithdrawMode={setWithdrawMode}
                />
            ) : (
                <>
                    <div>
                        <div className="lg:mb-2">
                            <div className="flex flex-col lg:flex-row lg:items-center">
                                <span className="mr-2">Your current balance is</span>
                                <span className="text-2xl lg:text-xl">
                                    <b className="text-black">
                                        {moneyFormat(
                                            storeCredit.customer_store_credit_balance ||
                                                0
                                        )}
                                    </b>
                                </span>
                            </div>
                            <span className="">Fund transfer will be process every Thursday.<br/> Processing time could take one week periode.</span>
                        </div>
                        
                        <button
                            className="py-2 px-4 text-white bg-secondary rounded font-semibold my-2"
                            onClick={() => setWithdrawMode(true)}
                        >
                            Credit Withdrawal
                        </button>

                        <table
                            className={`mt-2 table-auto w-full text-left text-dark-grey xl:border rounded ${styles["table-order"]}`}
                        >
                            <thead>
                                <tr className="border-b">
                                    <th className="xl:p-6">Transaction ID</th>
                                    <th className="xl:p-6">
                                        Store Credit Adjustment
                                    </th>
                                    <th className="xl:p-6">
                                        Store Credit Balance
                                    </th>
                                    <th className="xl:p-6">Comment</th>
                                    <th className="xl:p-6">Transaction Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <Rows />
                            </tbody>
                        </table>

                        <div className="flex text-base justify-between mt-5 flex-wrap items-center">
                            <span>{`${creditHistory?.total_count} Item(s)`}</span>
                            <div className="flex">
                                <button
                                    className={`p-2 hover:bg-gray-100 disabled:opacity-50 disabled:cursor-auto`}
                                    disabled={page < 2}
                                    onClick={() => setPage(page - 1)}
                                >
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        className="h-5 w-5"
                                        viewBox="0 0 20 20"
                                        fill="currentColor"
                                    >
                                        <path
                                            fillRule="evenodd"
                                            d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
                                            clipRule="evenodd"
                                        />
                                    </svg>
                                </button>
                                <button className="p-2 hover:bg-gray-100">
                                    {page}
                                </button>
                                <button
                                    className={`p-2 hover:bg-gray-100 disabled:opacity-50 disabled:cursor-auto`}
                                    disabled={
                                        page >=
                                        creditHistory?.total_count / size
                                    }
                                    onClick={() => setPage(page + 1)}
                                >
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        className="h-5 w-5"
                                        viewBox="0 0 20 20"
                                        fill="currentColor"
                                    >
                                        <path
                                            fillRule="evenodd"
                                            d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                            clipRule="evenodd"
                                        />
                                    </svg>
                                </button>
                            </div>
                            <div>
                                Show
                                <select
                                    className="mx-1 text-base"
                                    name="size"
                                    value={size}
                                    onChange={(e) => setSize(e.target.value)}
                                >
                                    <option value={10}>10</option>
                                    <option value={20}>20</option>
                                    <option value={30}>30</option>
                                </select>
                                per page
                            </div>
                        </div>
                    </div>
                </>
            )}
        </div>
    );
};

export default Index;

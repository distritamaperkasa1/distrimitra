import React, { useState, useEffect } from 'react'
import { requestGet, requestPost } from '../../lib/axios'
import useUser from '../../lib/useUser'
import { moneyFormat } from '../../lib/helper'
import CurrencyFormat from 'react-currency-format';

const Withdraw = ({ balance, setWithdrawMode }) => {
    const { user } = useUser();
    const [dataReq, setDataReq] = useState("");
    const [form, setForm] = useState({});
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState("");

    const getDataReq = async () => {
        const resp = await requestGet("/rest/V1/requestwithdraw", null, user.token);
        setDataReq(resp[0].bank_info[0]);
        console.log("data req", resp[0].bank_info)
    }

    useEffect(() => {
        getDataReq();
    }, []);

    useEffect(() => {
        if (error !== "") {
            setTimeout(() => {
                setError("")
            }, 5000)
        }
    }, [error]);

    const handleChange = (e) => {
        setForm({
            ...form,
            [e.target.name] : e.target.value.replaceAll(".", "")
        })
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        setLoading(true);

        if (form.amount > balance) {
            setError("Credit withdrawal is failed! The amount for withdraw is too big than your store credit balance. Please adjust the amount.");
        } else {
            const data = {
                data: {
                    ...dataReq,
                    ...form
                }
            }
    
            try {
                const resp = await requestPost("/rest/V1/requestwithdraw", data, user.token);
    
                if (resp[0].error) {
                    setError(resp[0].message)
                } else {
                    setWithdrawMode(false)
                }
            } catch (error) {
                setError("Something went wrong. Please try again.")
            }
        }

        setLoading(false);
    }

    return (
        <div>
            <h2 className="text-black text-xl">Credit Withdrawal</h2>

            <form onSubmit={handleSubmit}>
                <h3 className="border-b pb-1 text-black mb-2 text-lg">Bank Information</h3>
                <div className="mb-2">
                    <label className="block">Bank Name *</label>
                    <input 
                        className="border border-black rounded w-full p-1 text-black mt-1"
                        name="bank_name"
                        defaultValue={dataReq.bank_name}
                        onChange={handleChange}
                        required
                    />
                </div>

                <div className="mb-2">
                    <label className="block">Branch *</label>
                    <input 
                        className="border border-black rounded w-full p-1 text-black mt-1"
                        name="cabang"
                        defaultValue={dataReq.cabang}
                        onChange={handleChange}
                        required
                    />
                </div>

                <div className="mb-2">
                    <label className="block">Account Number *</label>
                    <input 
                        className="border border-black rounded w-full p-1 text-black mt-1"
                        name="bank_number"
                        defaultValue={dataReq.bank_number}
                        onChange={handleChange}
                        required
                    />
                </div>

                <div className="mb-2">
                    <label className="block">Account Name *</label>
                    <input 
                        className="border border-black rounded w-full p-1 text-black mt-1"
                        name="nama_rek"
                        defaultValue={dataReq.nama_rek}
                        onChange={handleChange}
                        required
                    />
                </div>

                <span>*Withdrawal to other bank except BCA, will be deduct for the admin fee.</span>

                <div className="mt-2">
                    <h3 className="border-b pb-1 text-black mb-2 text-lg">Amount Withdrawal</h3>
                    <span>Your current balance is <b className="text-black">{ moneyFormat(balance) }</b></span>

                    <div className="mb-2">
                        <label className="block">Amount To Withdraw *</label>
                        {/* <input 
                            className="border border-black rounded p-1 text-black mt-1 xl:w-1/2"
                            name="amount"
                            type="number"
                            onChange={handleChange}
                            required
                        /> */}
                        <CurrencyFormat thousandSeparator={"."} decimalSeparator={","} className="w-1/2 rounded text-black font-semibold" name="amount" onChange={handleChange}/>
                        {/* {console.log(form)} */}
                    </div>
                </div>

                <div className={`mt-2 text-red-600 font-bold flex flex-row items-center ${!error ? 'hidden' : ''}`}>
                    <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 mr-1" viewBox="0 0 20 20" fill="currentColor">
                        <path fillRule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z" clipRule="evenodd" />
                    </svg>
                    {error}
                </div>

                <button 
                    className="py-2 px-4 bg-secondary rounded text-white mt-4 disabled:opacity-50 disabled:cursor-auto"
                    disabled={loading}
                >
                    Submit
                </button>
            </form>
        </div>
    )
}

export default Withdraw

import { useRouter } from "next/router";
import React from "react";
import Image from "next/image";

const Breadcrumb = (props) => {
    const { items, responsiveStyles } = props;
    const router = useRouter();

    return (
        <div
            className={`flex flex-row gap-2 items-center uppercase ${responsiveStyles}`}
        >
            <div className="h-full">
                <button
                    className="w-full text-2xl text-gray-500 font-semibold cursor-pointer hover:bg-gray-100 border-r-2 border-secondary mr-4 px-4 py-0"
                    onClick={() => router.back()}
                >
                    <Image
                        src="/trimitra-arrow-left.png"
                        alt="Logo Trimitra"
                        width={20}
                        height={20}
                    ></Image>
                </button>  
            </div>
            
            <div className="flex flex-wrap">
                {items.map((item, key) => {
                    return (
                        <div key={key}>
                            {item.path ? (
                                <span
                                    className="text-sm text-gray-500 font-normal cursor-pointer hover:text-black"
                                    onClick={() => router.push(item.path)}
                                >
                                    {item.name.replace(/_/g, " ")}
                                </span>
                            ) : (
                                <span className="text-sm text-gray-500 font-normal cursor-pointer">
                                    {item.name}
                                </span>
                            )}
                            {items.length - 1 !== key && (
                                <span className="text-sm text-gray-500 font-normal px-2">
                                    &gt;
                                </span>
                            )}
                        </div>
                    );
                })}
            </div>
            
        </div>
    );
};

export default Breadcrumb;

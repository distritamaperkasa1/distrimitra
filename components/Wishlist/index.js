import React, { useEffect, useContext, useState, useCallback } from "react";
import { requestDelete, requestGet } from "../../lib/axios";
import { useRouter } from 'next/router'
import Layout from "../../components/Theme/Layout";
import { MenuContext } from "../../contexts/MenuContext";
import useUser from "../../lib/useUser";
import { moneyFormat } from "../../lib/helper";
import styles from "../../styles/Pages/Wishlist.module.css"
import Link from "next/link"
import Spinner from "../Loader/Spinner";
import EmptyWishlist from "./emptyWishlist";

const Wishlist = (props) => {
    const { menu } = props;
    const [html, setHtml] = useState();
    const [wishlist, setWishlist] = useState();
    const [wishlistIdState, setWishlistIdState] = useState();
    const [deleteItem, setDeleteItem] = useState(false);
    const [modalOpenState, setModalOpenState] = useState(false);
    const { user, mutateUser } = useUser();
    const [ userIdState , setUserIdState ] = useState([user])
    const { setMenu } = useContext(MenuContext);
    const router = useRouter();

    const setUserId = (userId, wishlist) => {
        let userIdData = userId
        // try {
        //     await AsyncLocalStorage.setItem('userId', userIdData)
        // } catch(e) {
        //     // error
        // }
        console.log(userIdData+' asd')
        setUserIdState(userIdData)
    }

    // console.log(JSON.stringify(menu));
    const setWishlistData = async (userId) => {
        const wishlistData = await requestGet('/rest/V1/wishlist/items',{customerId:userId}, user.token)
        console.log(JSON.stringify(wishlistData))
        setWishlist(wishlistData);
    }

    const showDeleteConfirmation = (wishlistId) => {
        setDeleteItem(!deleteItem)
        setWishlistIdState(wishlistId)
        setModalOpenState(!modalOpenState)
        // alert(wishlistIdState)
    }

    const deleteWishlist = async (wishlistId) => {
        // alert(wishlistId)
        const deleteWishlist = await requestDelete('/rest/V1/wishlist/delete/'+wishlistId+'?customerId='+userIdState,{}, user.token)
        if(deleteWishlist){
            setWishlistData(userIdState)
            setWishlistIdState(0)
            setDeleteItem(!deleteItem)
            setModalOpenState(!modalOpenState)
        }
    }

    const dismissModal = () => {
        setDeleteItem("")
        setModalOpenState(!modalOpenState)
    }

    // const EmptyWishlist = () => {
    //     return (
    //         <div className="text-center xl:text-left">
    //             <h3>Your wishlist is empty.</h3>
    //             <p>Explore more and shortlist some items.</p>

    //             <Link href="/">
    //                 <a>
    //                     <button className="py-2 px-4 bg-secondary text-white rounded mt-2">
    //                         Explore
    //                     </button>
    //                 </a>
    //             </Link>
    //         </div>
    //     )
    // }

    // if(wishlist){
    //     console.log(JSON.stringify(wis))
    // }
    
     useEffect(() => {
        if(user){
            console.log(user.data.id)
            setUserId(user.data.id)
            setWishlistData(user.data.id)
        }   
        if(menu){
            setMenu(menu); 
        }
     }, [setMenu, menu, user])

    return(
        <div className={`max-width-1100 mx-auto lg:py-4 ${styles["wishlist-mobile-container"]}`} style={{ minHeight:400 }}>
            {/* <h1 className="text-lg font-bold xl:font-normal xl:text-xl text-center xl:text-left text-dark-grey xl:mb-5">Your Wishlist</h1> */}
            {
                wishlist
                ?
                    wishlist.map((data, key) => {
                        return (
                            <div key={data.wishlist_item_id} style={{ marginBottom:'3em', maxWidth:'100%' }}>
                                {/* <p>{JSON.stringify(data)}</p> */}
                                <div className="flex">
                                    <div className={`${styles["flex-0-25"]}`}>
                                        <img className={`${styles["img-wishlist"]}`} src={data.product.small_image}/>
                                    </div>
                                    <div className={`${styles["flex-0-75"]}`}>
                                        <div className={`${styles["vertical-center"]}`}>
                                                <div className={`${styles["vertical-center-responsive"]}`}>
                                                    <Link href={`/product/`+data.product.sku}>
                                                        <a className={`${styles["product-link"]}`}>
                                                            <p>{data.product.name}</p>
                                                        </a>
                                                    </Link>
                                                    <p>{data.product.sku}</p>
                                                    <p>{moneyFormat(data.product.final_price)}</p>
                                                </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div className={`${styles["vertical-center"]}`}>
                                        {/* <a className="flex" onClick={() => showDeleteConfirmation()}> */}
                                        <a className="flex" onClick={() => showDeleteConfirmation(data.wishlist_item_id)}>
                                        <svg style={{ height:'inherit', marginRight:5 }} xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" className={`bi bi-trash`} viewBox="0 0 16 16">
                                            <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                                            <path fillRule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                                        </svg>
                                        </a>
                                        </div>
                                    </div>
                                </div>
                                <div 
                                    className={`min-w-screen h-screen animated fadeIn faster fixed left-0 top-0 flex justify-center items-center inset-0 z-50 outline-none focus:outline-none bg-no-repeat bg-center bg-cover ${deleteItem == "" ? 'hidden' : ''}`}
                                    id="modal-id"
                                >
                                    <div className="absolute bg-black opacity-80 inset-0 z-0"></div>
                                    <div className="w-full  max-w-lg p-5 relative mx-auto my-auto rounded-xl shadow-lg  bg-white ">
                                        <div className="">
                                            <div className="text-center p-5 flex-auto justify-center">
                                                    <h2 className="text-xl font-bold py-4 ">Are you sure?</h2>
                                                    <p className="text-sm text-gray-500 px-8">Do you really want to delete this wishlist item?</p>    
                                            </div>
                                        
                                            <div className="p-3  mt-2 text-center space-x-4 md:block">
                                                <button 
                                                    className="mb-2 md:mb-0 bg-white px-5 py-2 text-sm shadow-sm font-medium tracking-wider border text-gray-600 rounded-full hover:shadow-lg hover:bg-gray-100"
                                                    onClick={() => dismissModal()}
                                                >
                                                    Cancel
                                                </button>
                                                <button 
                                                    className="mb-2 md:mb-0 bg-red-500 border border-red-500 px-5 py-2 text-sm shadow-sm font-medium tracking-wider text-white rounded-full hover:shadow-lg hover:bg-red-600"
                                                    onClick={() => deleteWishlist(wishlistIdState)}
                                                >
                                                    Delete
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )
                    })
                    // <p>{JSON.stringify(wishlist)}</p>
                :
                <Spinner />
            }
            {
                wishlist?.length < 1 && < EmptyWishlist />
            }
        </div>
    )
}

export default Wishlist;

export async function getStaticProps() {
    let resp = {};
 
    try {
        const [menu] = await Promise.all([
            requestGet("/rest/V1/products/all-categories")
        ]);
        resp = {
            menu: menu,
         }
    } catch (error) {
        console.log("error in wishlist page", error);
    }
    return {
        props: resp,
        revalidate: 10,
    };
 
}

// export async function getStaticPaths() {
//     const product = await requestGet("/rest/default/V1/products/", {
//        "searchCriteria[currentPage]": 0,
//        "searchCriteria[pageSize]": 300,
//     });
 
//     const paths = product.items.map((item) => ({
//        params: { id: item.sku },
//     }));
 
//     return {
//        paths: paths,
//        fallback: 'blocking', // for new pages, do SSR and show page only when html is generated completely
//     };
//  }

// export async function getStaticProps() {
//     const cms = await requestGet("/rest/V1/cmsPage/search/", {
//         "searchCriteria[sortOrders][0][field]": 'identifier',
//         "searchCriteria[sortOrders][0][direction]": 'asc',
//         "searchCriteria[filter_groups][0][filters][0][field]": 'identifier',
//         "searchCriteria[filter_groups][0][filters][0][value]": 'home',
//         "identifier": 'home',
//     });
//     return {
//        props: {
//           cms: 'cms',
//        },
//     };
//  }
/* eslint-disable react/no-unescaped-entities */
import React from "react";
import Image from "next/image";
import Link from "next/link";

const emptyWishlist = () => {
    return (
        <div className="flex flex-col justify-center items-center w-full">
            <div>
                <Image
                    src="/img/empty-wishlist.png"
                    alt="Logo Trimitra"
                    width={450}
                    height={400}
                ></Image>
            </div>

            <h1 className="text-xl font-bold tracking-wide text-primary my-4">
                Your Wishlist is empty!
            </h1>
            <p className="text-center text-lg text-gray-400 font-medium">
                Seems like you haven't added anything to your wishlist yet.{" "}
                <br />
                Start shopping now!
            </p>
            <Link href="/">
                <a>
                    <button className="py-2 px-4 bg-primary text-white font-semibold rounded my-4">
                        Explore
                    </button>
                </a>
            </Link>
        </div>
    );
};

export default emptyWishlist;

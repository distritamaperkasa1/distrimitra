import React, { useEffect, useState, useCallback } from "react";
import Image from "next/image";
import { moneyFormat } from "../../lib/helper";
import ReactStars from "react-rating-stars-component";
import styles from "../../styles/Components/Product.module.css";
import { useRouter } from "next/router";
import fetchJson from '../../lib/fetchJson';
import useUser from "../../lib/useUser";
import useCart from "../../lib/useCart";
import { requestPut, requestPost } from '../../lib/axios'
import AsyncLocalStorage from '@createnextapp/async-local-storage'

const Product = (props) => {
   const { data, categoryId } = props;
   const router = useRouter();
   const { user, mutateUser } = useUser();
   const { cart, mutateCart } = useCart(user);
   const [ cartState, setCartState ] = useState([]);
   const [ disableCart, setDisableCart ] = useState(false);
   const [showButton, setShowButton] = useState(false);
   const [dataAttr, setDataAttr] = useState([]);
    useEffect(() => {
        setCartState(cart?.items)
        // console.log(cartState+' AAA')
    }, [cart, setCartState])

   const addToCart = async (e) => {
    e.preventDefault();
    if (!user.isLoggedIn){
        router.push('/login')
    } else {
        // setLoading(true)
        setDisableCart(true)
        const quoteId = await fetchJson("/api/cart/quote", {
          method: "POST",
          headers: { "Content-Type": "application/json" },
         });

        const param = {
          cartItem: {
             sku: data.sku,
             qty: 1,
             quote_id: quoteId
          }
        }

       console.log(JSON.stringify(param))
       console.log(JSON.stringify(data))

       const resp = fetchJson("/api/cart/item", {
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify(param),
       })
       .catch((error) => {
           console.log(error)
            setDisableCart(true)
        });

        mutateUser(resp)
        mutateCart();
        setTimeout(
            function(){
                mutateUser(resp)
                mutateCart();
            }, 1000
        )

    //    mutateUser(resp)
        
    
    
    //    router.push('/')
    
    await AsyncLocalStorage.setItem('successAddToCart','true')
    if(resp){
        let addedToCart = await AsyncLocalStorage.getItem('successAddToCart')
        setDisableCart(false)
        // if(addedToCart === 'true'){
        //     setTimeout(
        //         async function(){
        //             try{
        //                 await AsyncLocalStorage.setItem('successAddToCart','false')
        //             } catch(e){
            
        //             }
        //         }, 3000    
        //     )
        // }
        //   setLoading(false)
    }
 
       console.log("resp add to cart", resp)
    }
 }
   const useMediaQuery = (width) => {
    const [targetReached, setTargetReached] = useState(false);
  
    const updateTarget = useCallback((e) => {
      if (e.matches) {
        setTargetReached(true);
      } else {
        setTargetReached(false);
      }
    }, []);
  
    useEffect(() => {
      const media = window.matchMedia(`(max-width: ${width}px)`);
      media.addListener(updateTarget);
  
      // Check on mount (callback is not called until a change occurs)
      if (media.matches) {
        setTargetReached(true);
      }
  
      return () => media.removeListener(updateTarget);
    }, []);
    return targetReached;
  };
  const isBreakpoint = useMediaQuery(1280);

   // useEffect(() => {
   //    let attribute = data?.custom_attributes;
   //    let arr = [];
   //    attribute.map((data, key) => {
   //       arr[data.attribute_code] = data.value;
   //    });

   //    setDataAttr(arr);
   // }, [data]);

   const ratingChanged = (newRating) => {
      console.log(newRating);
   };

   return (
      <div
         className={`flex flex-col h-auto xl:pt-5 xl:pb-3 xl:px-5 cursor-pointer hover:bg-white xl:my-4 rounded-md ${styles["product"]}`}
         onMouseOver={() => !isBreakpoint ? setShowButton(true) : null}
         onMouseOut={() => !isBreakpoint ? setShowButton(false) : null}
        //  onClick={() => router.push(`/product/${data.sku}`)}
      >
        <div className="relative">
            <Image
                src={data.image_url ? data.image_url : "/img/example-product.png"}
                objectFit='contain'
                width={160}
                height={160}
                onClick={() => router.push({pathname: `/product/${data.sku}`, query: {category_id : categoryId}}) }
            />
            {
                data.custom_is_preorder_active === "1"
                ?
                <p className="text-sm font-bold preorder-badge">Preorder</p>
                :
                null
            }
        </div>

         <div className='text-sm text-gray-500 font-normal mt-4 mb-2'>
            {data.sku}
         </div>
         <div style={{ lineHeight:'1.5em', maxHeight:'max-content' }} onClick={() => !isBreakpoint ? router.push(
                {
                    pathname: `/product/${data.sku}`,
                    query: {category_id : categoryId}
                })  
                : null}>
            {
                 data.name.length < 30
                 ?
                    <div onClick={() => isBreakpoint ? 
                        router.push(
                            {
                                pathname: `/product/${data.sku}`,
                                query: {category_id : categoryId}
                            })  
                        : null} 
                        className={`text-base font-semibold text-gray-800`}
                        // onClick={() => router.push(`/product/${data.sku}`)}
                    >
                        {/* truncate */}
                        {/* <p>{data.name.length}</p> */}
                            <h1 className={`${styles["product--name"]}`}>  
                                {data.name}
                            </h1>
                    </div>
                    :
                    data.name.length > 50
                    ?
                    <div onClick={() => isBreakpoint ? router.push(
                        {
                            pathname: `/product/${data.sku}`,
                            query: {category_id : categoryId}
                        })  
                        : null} 
                        className={`text-base font-semibold text-gray-800 ${styles["product-name-wrap"]}`}
                    // onClick={() => router.push(`/product/${data.sku}`)}
                    >
                        <h1 className={`${styles["product--name"]} ${styles["product--name-after"]}`}>  
                            {data.name.slice(0, 45)}...
                        </h1>
                    </div>
                    :
                    <div className={`text-base font-semibold text-gray-800 ${styles["product-name-wrap"]}`}
                    // onClick={() => router.push(`/product/${data.sku}`)}
                    >
                    <h1 className={`${styles["product--name"]} ${styles["product--name-after"]}`}>  
                        {data.name}
                    </h1>
                    </div>
            }

            <div className='flex flex-col 1xl:flex-row justify-between 1xl:items-center my-2'>
                <ReactStars
                    classNames='flex-1'
                    count={5}
                    // onChange={ratingChanged}
                    size={20}
                    value={parseInt(data?.rating_review_percentage)/20 || 0}
                    activeColor='#F2994A'
                    edit={false}
                    isHalf={true}
                />

                <div className='flex-1 1xl:text-right font-normal text-gray-600 text-xs'>
                    <div>
                        <span className='text-sm font-semibold text-gray-800'>
                            {/* 0.0 */}
                        </span>{" "}
                        ({data?.rating_review_count || 0} Reviews)
                    </div>
                </div>
            </div>

            {
                isBreakpoint
                ?
                <button
                    className={`bg-secondary text-white text-sm py-3 font-bold rounded 
                    ${styles["button-add-to-cart"] } ${styles["button-add-to-cart-mobile"] }
                    `}
                        style={{ bottom:0, position:'relative' }}
                        disabled={disableCart}
                        // onClick={() => router.push(`/product/${data.sku}`)}
                        onClick={(e) => addToCart(e)}
                        // onClick={() => alert('Tes')}
                    >
                        Add to cart
                </button>
                :
                null
            }

             { data.regular_price == data.final_price  ? 
                ( 
                    <div className='text-md font-bold text-gray-800 mt-4'>
                        {moneyFormat(data.final_price)}
                    </div> 
                ) 
                : (
                    <div className="flex flex-col gap-2 mt-4">
                        <div className='text-sm font-bold text-gray-500 line-through'>
                            {moneyFormat(data.regular_price)}
                        </div> <div className='text-md font-bold text-red-500'>
                            {moneyFormat(data.final_price)}
                        </div> 
                    </div>
                )
            }
         </div>
         {
             data.qty > 0 && !disableCart
             ?
             <button
             className={`bg-secondary text-white text-sm py-3 font-bold rounded mt-2.5 
             ${
             showButton ? "block" : "hidden"
             }
             ${styles["button-add-to-cart"]}
             `}
                style={{ zIndex:999, bottom:0, position:'relative' }}
                disabled={disableCart}
                // onClick={() => router.push(`/product/${data.sku}`)}
                onClick={(e) => addToCart(e)}
                // onClick={() => alert('Tes')}
            >
                Add to cart
            </button>
            :
            <button
                className={`text-white text-sm py-3 font-bold rounded mt-2.5 
                ${showButton ? "block" : "hidden"}
                ${styles["button-add-to-cart-disabled"]}
                `}
                style={{ zIndex:999 }}
                disabled={false}
                // onClick={() => router.push(`/product/${data.sku}`)}
                // onClick={() => alert('Product out of stock')}
                // onClick={() => alert('Tes')}
            >
                Add to cart
            </button>
         }
      </div>
   );
};

export default Product;
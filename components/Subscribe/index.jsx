import React, { useState } from "react";
import Image from "next/image";
import Link from "next/link";
import router from "next/router";
import useUser from "../../lib/useUser";
import { requestPut } from "../../lib/axios";
import LoaderBounce from "../../components/Loader/Bounce";
import styles from "../../styles/Theme/Footer.module.css";

export const Subscribe = (props) => {
    const { user, mutateUser } = useUser();
    const [email, setEmail] = useState("");
    const [loading, setLoading] = useState(false);

    const handleSubmit = async (e) => {
        e.preventDefault();
        setLoading(true);

        if (!user.isLoggedIn) {
            var loginPromt = confirm(
                "You must Login first to subscribe. Do you want to login?"
            );
            if (loginPromt) {
                router.push("/login");
            }
            setLoading(false);
        } else {
            if (!email) {
                alert("Please enter your email address");
                setLoading(false);
            } else {
                try {
                    if (email != user.data.email) {
                        alert(
                            "Your email address is not matched with your account. Please login again."
                        );
                        setLoading(false);
                    } else {
                        const subsResp = await requestPut(
                            "/rest/V1/customers/me",
                            null,
                            {
                                customer: {
                                    id: user.data.id,
                                    email: user.data.email,
                                    firstname: user.data.firstname,
                                    lastname: user.data.lastname,
                                    store_id: user.data.store_id,
                                    website_id: user.data.website_id,
                                    extension_attributes: {
                                        is_subscribed: true,
                                    },
                                },
                            },
                            user.token
                        );
                        setLoading(false);
                        var success = confirm(
                            "You have successfully subscribed to our newsletter."
                        );
                    }
                } catch (error) {
                    console.log(error);
                    setLoading(false);
                }
            }
        }
    };

    return (
        <div className="flex-1 justify-center items-center mt-5 xl:mt-0">
            <h3 className="font-bold text-sm text-center">
                SUBSCRIBE FOR THE PROMOTION INFO
            </h3>

            <div className={`${styles.newsletter} mx-auto xl:mx-0`}>
                <form onSubmit={(e) => handleSubmit(e)}>
                    <input
                        type="email"
                        className="border rounded border-gray text-base"
                        placeholder="Enter your email here"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                    />
                    <button type="submit" disabled={loading}>
                        <div
                            className={`flex flex-row items-center justify-center my-3 ${
                                !loading ? "hidden" : ""
                            }`}
                        >
                            <LoaderBounce />
                        </div>
                        <div
                            className={`grid items-center ${
                                loading ? "hidden" : ""
                            }`}
                        >
                            <Image
                                src="/send-icon.png"
                                alt="Trimitra Newsletter"
                                width={24}
                                height={24}
                                margin={"auto"}
                            />
                        </div>
                    </button>
                </form>
            </div>
        </div>
    );
};

export default Subscribe;

/* eslint-disable @next/next/no-img-element */
import React, { useState } from "react";
import { ChevronDown, ChevronUp } from "react-feather";

const Accordion = (props) => {
   const { title, children, defaultExpanded } = props;
   const [expanded, setExpanded] = useState(defaultExpanded || false);

   return (
      <div
         className={`flex flex-col py-1.5 px-3 border-b text-gray-light cursor-pointer  ${
            expanded ? "space-y-3" : "space-y-0"
         }`}
      >
         <div
            className='flex flex-row items-center'
            onClick={() => setExpanded(!expanded)}
         >
            <p
               className={`flex-auto text-gray-2 hover:text-dark-grey text-base ${
                  expanded ? "text-gray-dark font-black" : "font-bold"
               }`}
               style={{ textTransform:'uppercase' }}
            >
               {title}
            </p>
            {expanded ? (
               <ChevronUp width={20} height={20} color='#828282' />
            ) : (
               <ChevronDown width={20} height={20} color='#828282' />
            )}
         </div>
         <div
            className={`transition-all transform duration-700 ease-in-out overflow-hidden text-base text-gray-700 ${
               expanded ? "h-auto" : "h-0"
            }`}
         >
            {children}
         </div>
      </div>
   );
};

export default Accordion;

/* eslint-disable @next/next/no-img-element */
import React from "react";

const Slides = (props) => {
   const { images } = props;

   images.map((image, index) => (
      <img key={index} src={image.imageUrl} alt={image.placeHolder} />
   ));
};

export default Slides;

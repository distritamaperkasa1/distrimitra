import React from "react";

const Tab = (props) => {
   const { tabs, children, isActive, onChange, responsiveStyles, paddingStyles } = props;

   const TabItems = ({ title, isActive, index }) => {
      return (
         <div className='flex-col max-w-max' onClick={() => onChange(index)}>
            <div
               className={`text-md px-2 ${
                  isActive ? "font-semibold" : "cursor-pointer"
               }`}
            >
               {title}
            </div>
            {isActive && <div className='bg-secondary h-0.5 my-2' />}
         </div>
      );
   };

   return (
      <div className={`flex flex-col ${paddingStyles}`}>
         <div className={`flex-1 flex flex-row space-x-8 ${responsiveStyles}`}>
            {tabs.length > 0 &&
               tabs.map((item, key) => {
                  return (
                     <TabItems
                        key={key}
                        index={key}
                        title={item.title}
                        isActive={item.active || false}
                     />
                  );
               })}
         </div>

         <div className='flex-auto'>{children}</div>
      </div>
   );
};

export default Tab;

import React, { useState } from "react"
import useUser from "../../lib/useUser"
import useOrder from "../../lib/useOrder"
import { requestPost } from "../../lib/axios"

const RequestCancel = ({ data, setDetail, detailIndex }) => {
    const { user } = useUser({ redirectTo: "/login" });
    const { order, mutateOrder } = useOrder();
    const [showModal, setShowModal] = useState(false);
    const [reason, setReason] = useState("");
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState("");

    const handleSubmit = (e) => {
        e.preventDefault();
        
        setLoading(true);
        const param = {
            "orderId": data.entity_id,
            "reason": reason
        };
        
        try {
            const resp = requestPost("/rest/V1/requestocancel", param, user.token);
            setShowModal(false);

            let orderUpdate = [...order.items];
            orderUpdate[detailIndex].status = "refundprocess";

            
            mutateOrder({ ...order, items: orderUpdate }, false)
            setDetail("");
        } catch (error) {
            setError(error.message);
        }

        setLoading(false);
    }

    return (
        <>
            <button 
                className="text-white rounded py-2 px-3 bg-primary text-base font-semibold"
                onClick={() => setShowModal(true)}
            >
                Request To Cancel
            </button>
            {showModal ? (
            <>
                <div
                    className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none"
                    datatestid="modal-test"
                >
                    <div className="relative w-auto my-6 mx-auto max-w-xl w-screen">
                        {/*content*/}
                        <form 
                            className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none"
                            onSubmit={handleSubmit}
                        >
                            {/*header*/}
                            <div className="flex items-start justify-between p-5 border-b border-solid border-blueGray-200 rounded-t">
                                <h3 className="text-xl text-black">
                                    Request To Cancel Order #{data.increment_id}
                                </h3>
                            </div>
                            {/*body*/}
                            <div className="relative px-6 pb-4 flex-auto my-2 text-base text-black">
                                <label className="block">Reason</label>
                                <textarea 
                                    className="w-full h-28 text-base rounded"
                                    onChange={(e) => setReason(e.target.value)}
                                    defaultValue={reason}
                                />
                            </div>

                            <div className="flex justify-end px-6 pb-4">
                                <button 
                                    className="mr-2 text-base py-2 px-4"
                                    onClick={() => setShowModal(false)}
                                >
                                    Cancel
                                </button>

                                <button 
                                    className="text-base py-2 px-4 bg-secondary rounded text-white disabled:opacity-50 disabled:cursor-auto"
                                    disabled={loading}
                                >
                                    Submit
                                </button>
                            </div>
                            <div className={`mt-2 text-red-600 font-bold flex flex-row items-center justify-center ${!error ? 'hidden' : ''}`}>
                                <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 mr-1" viewBox="0 0 20 20" fill="currentColor">
                                    <path fillRule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z" clipRule="evenodd" />
                                </svg>
                                {error}
                            </div>
                        </form>
                    </div>
                </div>
                <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
            </>
            ) : null}
        </>
    );
}

export default RequestCancel
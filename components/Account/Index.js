import React, { useContext, useState, useEffect } from 'react';
import { MenuContext } from '../../contexts/MenuContext';
import useUser from "../../lib/useUser";
import fetchJson from "../../lib/fetchJson";
import { useRouter } from 'next/router'
import EditAccount from './EditAccount';
import ChangePassword from './ChangePassword';
import useCustomer from "../../lib/useCustomer";
import { searchDefaultAddress } from "../../lib/helper";
import styles from '../../styles/Pages/DetailAccount.module.css';

const Index = () => {
    const { mutateUser } = useUser();
    const router = useRouter();
    const { user } = useUser({ redirectTo: "/login" });
    // const { user } = useUser();
    let account = {};
    if (user) {
        account = user.data || {}
    }
    console.log(JSON.stringify(account));
    // const { account, setAccount } = useContext(MenuContext);
    const { customer, mutateCustomer } = useCustomer();
    console.log(customer);
    const { default_billing, default_shipping, addresses } = customer || {};
    const [showEditAccount, setShowEditAccount] = useState(false);
    const [showEditPass, setShowEditPass] = useState(false);
    const [success, setSuccess] = useState(false);
    const [successMessage, setSuccessMessage] = useState("");

    useEffect(() => {
        setTimeout(() => {
            setSuccess(false);
        }, 15000);
    }, [successMessage]);

    const signOut = async () => {
        mutateUser(
            await fetchJson("/api/logout", { method: "POST" }),
            false,
        );
        router.replace('/');
    };

    return (
        <div>
            <div className="relative xl:static">
                <svg 
                    xmlns="http://www.w3.org/2000/svg" 
                    className={`mb-2 h-5 w-5 cursor-pointer absolute top-1 xl:top-2 ${showEditAccount || showEditPass != "" ? '' : 'hidden'}`} 
                    viewBox="0 0 20 20" 
                    fill="currentColor" 
                    onClick={() => {
                        setShowEditAccount("");
                        setShowEditPass("");
                    }
                    }
                >
                    <path fillRule="evenodd" d="M9.707 16.707a1 1 0 01-1.414 0l-6-6a1 1 0 010-1.414l6-6a1 1 0 011.414 1.414L5.414 9H17a1 1 0 110 2H5.414l4.293 4.293a1 1 0 010 1.414z" clipRule="evenodd" />
                </svg>
                <h1 className="mb-2 font-bold text-lg flex-1 text-center text-black xl:hidden">My Account</h1>
            </div>

            {success && 
                <div className="bg-green-100 border-l-4 border-green-500 text-green-700 p-4 mb-4" role="alert">
                    <p className="font-bold">Success!</p>
                    <p>{successMessage}</p>
                </div>
            }

            {showEditAccount && <EditAccount customer={ customer } mutateCustomer={mutateCustomer} setShowEditAccount={setShowEditAccount} setShowEditPass={setShowEditPass} setSuccess={setSuccess} setSuccessMessage={setSuccessMessage}/>}

            {showEditPass && <ChangePassword setShowEditPass={setShowEditPass} setSuccess={setSuccess} setSuccessMessage={setSuccessMessage}/>}

            {!showEditAccount && !showEditPass ? 
                <div className="xl:flex xl:flex-col">
                    <div className="xl:flex">
                        <div className="border rounded p-4 text-sm xl:flex-1 h-auto xl:flex xl:flex-col xl:justify-between">
                            <div>
                                <h2 className="text-dark-grey font-bold mb-2">Contact Information</h2>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>Name</td>
                                            <td>:</td>
                                            {
                                                customer?.firstname !== undefined || customer?.lastname !== undefined
                                                ?
                                                <td className="pl-9 py-2">{`${customer?.firstname} ${customer?.lastname}`}</td>
                                                :
                                                <td className="pl-9 py-2">-</td>
                                            }
                                        </tr>
                                        <tr className="py-2">
                                            <td>Email</td>
                                            <td>:</td>
                                            <td className="pl-9 py-2">{customer?.email}</td>
                                        </tr>
                                        <tr className="py-2">
                                            <td>Phone</td>
                                            <td>:</td>
                                            <td className="pl-9 py-2">{customer?.custom_attributes?.find(o => o.attribute_code === 'phone').value || '-'}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            
                            <div className="flex items-center mt-10">
                                <button 
                                    className={`py-1 px-2 flex items-end ${styles['edit-btn']}`}
                                    onClick={() => {setShowEditAccount(true);setSuccess(false);}}
                                >
                                    <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z" />
                                    </svg>
                                    <span className="ml-1">Edit</span>
                                </button>

                                <button 
                                    className="ml-4 text-red-600"
                                    onClick={() => {setShowEditPass(true);setSuccess(false);}}
                                >
                                    Change Password
                                </button>
                                
                            </div>
                        </div>

                        <div style={{ display:'none' }} className="border rounded p-4 text-sm mt-8 xl:flex-1 h-auto xl:mt-0 xl:flex xl:flex-col xl:justify-between">
                            <div>
                                <h2 className="text-dark-grey font-bold mb-2">Newsletters</h2>
                                <p>You aren’t subscribed our newsletter</p>
                            </div>

                            <div className="flex items-center mt-10 items-end">
                                <button 
                                    className={`py-1 px-2 flex items-end ${styles['edit-btn']}`}
                                >
                                    <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z" />
                                    </svg>
                                    <span className="ml-1">Edit</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    
                    <div className="xl:flex xl:flex-col xl:mt-6">
                        <div className="xl:flex">
                            <div style={{ display:'none' }} className="border rounded p-4 text-sm mt-8 xl:flex-1 xl:mr-4 h-auto xl:mt-0 xl:flex xl:flex-col xl:justify-between">
                                <div>
                                    <h2 className="text-dark-grey font-bold mb-2">Default Billing Address</h2>
                                    {searchDefaultAddress(addresses, default_billing)}
                                </div>
                                <div className="flex items-center mt-10">
                                    <button className={`py-1 px-2 flex items-end ${styles['edit-btn']}`}>
                                        <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z" />
                                        </svg>
                                        <span className="ml-1">Edit Address</span>
                                    </button>
                                </div>
                            </div>

                            <div style={{ display:'none' }} className="border rounded p-4 text-sm mt-8 xl:flex-1 h-auto xl:mt-0 xl:flex xl:flex-col xl:justify-between">
                                <div>
                                    <h2 className="text-dark-grey font-bold mb-2">Default Shipping Address</h2>
                                    {searchDefaultAddress(addresses, default_shipping)}
                                </div>

                                <div className="flex items-center mt-10">
                                    <button className={`py-1 px-2 flex items-end ${styles['edit-btn']}`}>
                                        <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z" />
                                        </svg>
                                        <span className="ml-1">Edit Address</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='w-full flex justify-start mt-4'>
                        <button className='p-2 rounded bg-primary text-white font-semibold' onClick={(e)=>signOut(e)}>Sign Out</button>
                    </div>
                </div>
            : ""}
        </div>
    )
}

export default Index;
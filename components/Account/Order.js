import React, { useState, useEffect } from "react";
import OrderDetail from "./OrderDetail";
import Spinner from "../Loader/Spinner";
import useOrder from "../../lib/useOrder";
import fetchJson from "../../lib/fetchJson";
import { moneyFormat, dateFormat, detailDateFormat } from "../../lib/helper";
import { ArrowRight } from "../../assets/illustrations/index";
import styles from "../../styles/Components/Account.module.css";
import moment from "moment";

const OrderHistory = () => {
    const { order, loadingOrder, mutateOrder } = useOrder();
    const [ size, setSize ] = useState(10);
    const [ page, setPage ] = useState(1);
    const [ detail, setDetail ] = useState("");
    console.log("Order history: ", order)

    useEffect(async () => {
        mutateOrder(await fetchJson(`/api/order?pageSize=${size || 10}&currentPage=${page || 1}`, {
            method: "POST",
            headers: { "Content-Type": "application/json" },
        }), false);
    }, [page, size, mutateOrder]);

    const OrderRows = () => {
        let orderhistory = [];
        
        order?.items.map((data, index) => {
            orderhistory.push(
                <tr key={data.entity_id}>
                    <td data-header="Order" className="xl:p-6 font-bold xl:font-normal">#{data.increment_id}</td>
                    <td data-header="Date" className="xl:p-6 font-bold xl:font-normal">{moment(data.created_at).format("MM/DD/YYYY")}</td>
                    <td data-header="Ship To" className="xl:p-6 font-bold xl:font-normal">{`${data.customer_firstname} ${data.customer_lastname}`}</td>
                    <td data-header="Order Total" className="xl:p-6 font-bold xl:font-normal">{moneyFormat(data.grand_total)}</td>
                    <td data-header="Status" className="xl:p-6 font-bold xl:font-normal capitalize">{data.status == "refundprocess" ? "refund process" : data.status}</td>
                    <td className="xl:p-6">
                        <button className="underline mr-7" onClick={() => setDetail({ data: data, index: index })}>View Order</button>
                        {/* <button className="underline">Reorder</button> */}
                    </td>
                </tr>
            )
        })
        
        return orderhistory;
    }

    return (
        <div>
            <div className="flex items-center mb-5 relative xl:static">
                <svg xmlns="http://www.w3.org/2000/svg" className={`h-5 w-5 cursor-pointer absolute top-1 xl:top-2 ${detail != "" ? '' : 'hidden'}`} viewBox="0 0 20 20" fill="currentColor" onClick={() => setDetail("")}>
                    <path fillRule="evenodd" d="M9.707 16.707a1 1 0 01-1.414 0l-6-6a1 1 0 010-1.414l6-6a1 1 0 011.414 1.414L5.414 9H17a1 1 0 110 2H5.414l4.293 4.293a1 1 0 010 1.414z" clipRule="evenodd" />
                </svg>
                <h1 className="font-bold text-lg flex-1 text-center text-black xl:hidden">My Order</h1>
            </div>

            <div className="p-1 text-base xl:p-0">
                {detail != "" ? 
                    <OrderDetail detail={detail.data} detailIndex={detail.index} setDetail={setDetail} />
                :
                    
                    <>
                    
                    <table className={`table-auto w-full text-left text-dark-grey xl:border rounded ${styles['table-order']}`}>
                        <thead>
                            <tr className="border-b">
                                <th className="xl:p-6">Order #</th>
                                <th className="xl:p-6">Date</th>
                                <th className="xl:p-6">Ship To</th>
                                <th className="xl:p-6">Order Total</th>
                                <th className="xl:p-6">Status</th>
                                <th className="xl:p-6">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <OrderRows />
                        </tbody>
                    </table>
                    {
                        loadingOrder && (
                            <div className="flex justify-center mt-5">
                                <Spinner />
                            </div>
                        )
                    }
                    
                    <div className="flex text-base justify-between mt-5 flex-wrap items-center">
                        <span>{`${order?.total_count || 0} Item(s)`}</span>
                        <div className="flex">
                            <button 
                                className={`p-2 hover:bg-gray-100 disabled:opacity-50 disabled:cursor-auto`} 
                                disabled={page < 2}
                                onClick={() => setPage(page-1)}
                            >
                                <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                    <path fillRule="evenodd" d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z" clipRule="evenodd" />
                                </svg>
                            </button>
                            <button className="p-2 hover:bg-gray-100">{page}</button>
                            <button 
                                className={`p-2 hover:bg-gray-100 disabled:opacity-50 disabled:cursor-auto`} 
                                disabled={page >= order?.total_count/size}
                                onClick={() => setPage(page+1)}
                            >
                                <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                    <path fillRule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clipRule="evenodd" />
                                </svg>
                            </button>
                        </div>
                        <div>
                            Show 
                            <select className="mx-1 text-base" name="size" value={size} onChange={(e) => setSize(e.target.value)}>
                                <option value={10}>10</option>
                                <option value={20}>20</option>
                                <option value={30}>30</option>
                            </select> 
                            per page
                        </div>
                    </div>
                    </>
                }
            </div>
        </div>
    )
}

export default OrderHistory;
import React, { useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import EditAddress from './EditAddress'
import useCustomer from '../../lib/useCustomer'
import useUser from '../../lib/useUser'
import { searchAddress, searchDefaultAddress } from '../../lib/helper'
import styles from "../../styles/Components/Account.module.css";
import { requestPut } from '../../lib/axios'

const Address = () => {
    const { customer, mutateCustomer, loadingCustomer } = useCustomer();
    const { user } = useUser();
    const [ addressId, setAddressId ] = useState("");
    const [ deleteAddress, setDeleteAddress ] = useState("");
    const router = useRouter();
    const [ loading, setLoading ] = useState(false);

    const handleDeleteAddress = async () => {
        const deletedAddress = searchAddress(customer?.addresses, deleteAddress);
        let addressList = [...customer.addresses];

        addressList.splice(deletedAddress.index,1);

        const body = {"customer": {...customer, addresses: addressList}}

        setLoading(true);
        try {
            const updateResp = await requestPut('/rest/default/V1/customers/me', null, body, user.token);

            setDeleteAddress("");
            mutateCustomer(updateResp);
        } catch (error) {
            setDeleteAddress("");
        }
        setLoading(false);
    }

    const OtherAddress = (props) => {
        console.log("props", props)
        const { data } = props;
        const defaultValue = <p className="text-base">You have no other address entries in your address book.</p>;

        let otherAddress = [];
        data?.forEach((addr, index) => {
            if (!addr.default_billing && !addr.default_shipping){
                otherAddress.push(addr)
            }
        });

        if (otherAddress.length < 1) {
            return defaultValue;
        }

        return (
            <table className={`mt-3 border text-base table-auto w-full text-dark-grey ${styles['table-order']}`}>
                <thead>
                    <tr className="border-b text-left">
                        <th className="p-2 xl:p-4">Name</th>
                        <th className="p-2 xl:p-4">Street Address</th>
                        <th className="p-2 xl:p-4">City</th>
                        <th className="p-2 xl:p-4">Region</th>
                        <th className="p-2 xl:p-4">Zip/Postal Code</th>
                        <th className="p-2 xl:p-4">Phone</th>
                        <th className="p-2 xl:p-4"></th>
                    </tr>
                </thead>
                <tbody>
                    {otherAddress.map((addr, index) => {
                        return (
                            <tr className="text-left" key={addr.id}>
                                <td data-header="Name" className="p-2 xl:p-4 font-normal">{`${addr.firstname} ${addr.lastname}`}</td>
                                <td data-header="Street Address" className="p-2 xl:p-4 font-normal">{addr.street[0]}</td>
                                <td data-header="City" className="p-2 xl:p-4 font-normal">{addr.city}</td>
                                <td data-header="Region" className="p-2 xl:p-4 font-normal">{addr.region.region}</td>
                                <td data-header="Zip/Postal Code" className="p-2 xl:p-4 font-normal">{addr.postcode}</td>
                                <td data-header="Phone" className="p-2 xl:p-4 font-normal">{addr.telephone}</td>
                                <td className="p-2 xl:p-4 font-normal">
                                    <button className="mr-2" onClick={() => setAddressId(addr.id)}>
                                        Edit
                                    </button>
                                    <button onClick={() => setDeleteAddress(addr.id)}>
                                        Delete
                                    </button>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        )
    }

    return (
        <div className={`mt-2 xl:mt-0 text-secondary-dark-grey mb-10`}>
            {addressId != "" ? <EditAddress setAddressId={setAddressId} customerData={customer} addressId={addressId} mutateCustomer={mutateCustomer} /> : 
            <>
                <div className="flex items-center mb-5 xl:hidden">
                    <h1 className="font-bold text-lg flex-1 text-center text-black">Address Book</h1>
                </div>

                <div className="flex flex-col xl:flex-row xl:flex-wrap">
                    <div className="xl:w-2/4 py-2 xl:py-0 xl:pb-4 xl:pr-2 hidden">
                        <div className="border rounded p-4 text-base h-full">
                            <h2 className="text-dark-grey font-bold mb-2">Default Billing Address</h2>
                            {searchDefaultAddress(customer?.addresses, customer?.default_billing)}

                            <div className="flex items-center mt-10">
                                <button className={`py-1 px-2 flex items-end ${styles['edit-btn']}`} onClick={() => setAddressId(customer?.default_billing)}>
                                    <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z" />
                                    </svg>
                                    <span className="ml-1">Edit Address </span>
                                </button>
                            </div>
                        </div>
                    </div>

                    <div className="xl:w-2/4 py-2 xl:py-0 xl:pb-4 xl:pl-2">
                        <div className="border rounded p-4 text-base h-full">
                            <h2 className="text-dark-grey font-bold mb-2">Default Shipping Address</h2>
                            {searchDefaultAddress(customer?.addresses, customer?.default_shipping)}

                            <div className="flex items-center mt-10">
                                <button className={`py-1 px-2 flex items-end ${styles['edit-btn']}`} onClick={() => setAddressId(customer?.default_shipping)}>
                                    <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z" />
                                    </svg>
                                    <span className="ml-1">Edit Address </span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div>
                    <h2 className="text-base font-bold text-dark-grey">Other address</h2>
                    <OtherAddress data={customer?.addresses || []} />
                </div>

                <button className="bg-secondary text-sm text-white py-2 px-4 rounded mt-5" onClick={() => router.push("/account/address/add")}>
                    Add New Address
                </button>
                <div 
                    className={`min-w-screen h-screen animated fadeIn faster fixed left-0 top-0 flex justify-center items-center inset-0 z-50 outline-none focus:outline-none bg-no-repeat bg-center bg-cover ${deleteAddress == "" ? 'hidden' : ''}`}
                    id="modal-id"
                >
                    <div className="absolute bg-black opacity-80 inset-0 z-0"></div>
                    <div className="w-full  max-w-lg p-5 relative mx-auto my-auto rounded-xl shadow-lg  bg-white ">
                        <div className="">
                            <div className="text-center p-5 flex-auto justify-center">
                                    <h2 className="text-xl font-bold py-4 ">Are you sure?</h2>
                                    <p className="text-sm text-gray-500 px-8">Do you really want to delete this item?</p>    
                            </div>
                        
                            <div className="p-3  mt-2 text-center space-x-4 md:block">
                                <button 
                                    className="mb-2 md:mb-0 bg-white px-5 py-2 text-sm shadow-sm font-medium tracking-wider border text-gray-600 rounded-full hover:shadow-lg hover:bg-gray-100"
                                    onClick={() => setDeleteAddress("")}
                                >
                                    Cancel
                                </button>
                                <button 
                                    className="mb-2 md:mb-0 bg-red-500 border border-red-500 px-5 py-2 text-sm shadow-sm font-medium tracking-wider text-white rounded-full hover:shadow-lg hover:bg-red-600"
                                    onClick={handleDeleteAddress}
                                >
                                    Delete
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </>
            }

        </div>
    )
}

export default Address;
import React, { useState, useEffect } from 'react'
import Image from 'next/image'
import jsPDF from 'jspdf'
import RequestCancel from './RequestCancel'
import Spinner from '../Loader/Spinner'
import { detailDateFormat, formatAddress, moneyFormat } from '../../lib/helper'
import fetchJson from '../../lib/fetchJson'
import styles from "../../styles/Components/Account.module.css"

const OrderDetail = ({ detail, setDetail, detailIndex, token }) => {
    const [ orderItems, setOrderItems ] = useState([]);
    const [ loading, setLoading ] = useState(true);

    console.log("Detail", detail);

    useEffect(() => {
        const getOrderItems = async () => {
            const res = await fetchJson(`/api/order/items?orderId=${detail.entity_id}`, { method: "POST" });
            setOrderItems(res);
            setLoading(false);
        }
        
        getOrderItems();
    }, [])

    function demoFromHTML() {
        let source = document.getElementById('invoice-section');
        var width = source.clientWidth + 20;
        var height = source.clientHeight + 40;
        var pdf = new jsPDF(width < 400 ? 'p': 'l', 'pt', [width, height]);
        
        pdf.html(source, {
            callback: function (doc) {
                doc.setProperties({
                    title: 'PDF Title',
                });
                doc.output('dataurlnewwindow');
            },
            x: 10,
            y: 10,
         });
    }

    const OrderedItem = ({ item }) => {
        return (
            <tr>
                <td data-header="Product Name:" className="xl:w-2/5 xl:p-3">
                    <p className="w-3/5 xl:w-auto">
                        {item.name}<br />{item.preorder_is_active === "1" && <span className='font-semibold'>{`Pre order: ${item.preorder_duration} days`}</span>}
                    </p>
                </td>
                <td data-header="SKU:" className="xl:p-3">{item.sku}</td>
                <td data-header="Price:" className="xl:p-3">{moneyFormat(item.price)}</td>
                <td data-header="Qty:" className="xl:p-3">{item.qty_ordered}</td>
                <td data-header="Subtotal:" className="xl:p-3">{moneyFormat(item.row_total)}</td>
            </tr>
        )
    }

    return (
        <div id="invoice-section">
            <div className="flex items-center justify-between">
                <div className="flex flex items-center">
                    <h1 className="text-xl text-black mr-5">Order #{detail.increment_id}</h1>
                    <div className='grid text-center gap-2'>
                        <span className="text-black border-2 py-1 px-2 rounded capitalize">{detail.status == "refundprocess" ? "refund process" : detail.status}</span>
                        <div className='flex md:hidden'>
                            {detail.status === "processing" && <RequestCancel data={detail} setDetail={setDetail} detailIndex={detailIndex} />}
                        </div>
                    </div>
                </div>
                <div className='hidden md:flex'>
                    {detail.status === "processing" && <RequestCancel data={detail} setDetail={setDetail} detailIndex={detailIndex} />}
                </div>
            </div>
            {detailDateFormat(detail.created_at)}
            
            <div className="mt-5 text-black text-base">
                <div className="flex justify-between mb-2">
                    <h2 className="py-2">Items Ordered</h2>
                    <div className="flex gap-2">
                        <a 
                            className="pl-4 p-2 bg-white hover:bg-gray-200 border font-semibold border-gray-600 rounded-lg"
                            target="_blank"
                            href={`http://${window.location.host}/print/invoice/${detail.entity_id}`} rel="noreferrer"
                        >
                        <span className="relative top-1 right-2">
                            <Image
                                    src="/print-icon.png"
                                    alt="Trimitra Newsletter"
                                    width={20}
                                    height={20}
                                    margin={"auto"}
                                    />
                        </span>
                            Invoice
                        </a>
                        <a 
                            className="pl-4 p-2 bg-white hover:bg-gray-200 border font-semibold border-gray-600 rounded-lg"
                            target="_blank"
                            href={`http://${window.location.host}/print/order/${detail.entity_id}`} rel="noreferrer"
                        >
                        <span className="relative top-1 right-2">
                            <Image
                                    src="/print-icon.png"
                                    alt="Trimitra Newsletter"
                                    width={20}
                                    height={20}
                                    margin={"auto"}
                                    />
                        </span>
                            Order
                        </a>
                    </div>
                </div>
                {
                    loading ? <Spinner />
                    :
                    <table className={`table-auto w-full text-left border-b xl:border rounded ${styles['table-order']}`}>
                        <thead>
                            <tr>
                                <th className="xl:p-3">Product Name</th>
                                <th className="xl:p-3">SKU</th>
                                <th className="xl:p-3">Price</th>
                                <th className="xl:p-3">Qty</th>
                                <th className="xl:p-3">Subtotal</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                orderItems.map((item, index) => {
                                    return <OrderedItem item={item} key={`items-${index}`} />
                                })
                            }
                        </tbody>
                    </table>
                }
                
            </div>

            <div className="mt-5 text-black text-base">
                <h2 className="border-b py-2 mb-2">Order Information</h2>
                <div className="flex flex-col xl:flex-row">
                    <div className="xl:w-1/4 pr-2 my-2">
                        <h3 className="font-bold">Shipping Address</h3>
                        {formatAddress(detail.extension_attributes.shipping_assignments[0].shipping.address)}
                    </div>
                    <div className="xl:w-1/4 pr-2 my-2">
                        <h3 className="font-bold">Shipping Method</h3>
                        <p>{detail.shipping_description}</p>
                    </div>
                    <div className="xl:w-1/4 pr-2 my-2">
                        <h3 className="font-bold">Billing Address</h3>
                        {formatAddress(detail.billing_address)}
                    </div>
                    <div className="xl:w-1/4 pr-2 my-2">
                        <h3 className="font-bold">Payment Method</h3>
                        <p>{detail.payment.additional_information[0]}</p>
                        <div className="w-full flex gap-3 items-center">
                                            {/* <div style={{ display:'inline-flex', verticalAlign:'middle' }}>
                                            <span><img style={{ width:70 }} src="/img/Bca.png"/></span>
                                        </div> */}
                                            <div
                                                    dangerouslySetInnerHTML={{
                                                    //   __html: payment?.instructions?.replaceAll("\n", "<br>"),
                                                    __html: detail.payment.additional_information?.[1]?.replaceAll("\n", "<br>"),
                                           
                                                    }}
                                                    className="text-sm text-[14px] px-1 py-4 leading-5"
                                                  ></div>
                                       </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default OrderDetail

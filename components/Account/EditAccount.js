import { data } from "autoprefixer";
import { set } from "nprogress";
import React, { useState } from "react";
import LoaderBounce from "../../components/Loader/Bounce";
import { requestPut, requestPost } from "../../lib/axios";
import useUser from "../../lib/useUser";

const EditAccount = ({
    customer,
    mutateCustomer,
    setShowEditAccount,
    setShowEditPass,
    setSuccess,
    setSuccessMessage,
}) => {
    // console.log("Customer data", customer)
    const { user } = useUser();
    const [firstName, setFirstName] = useState(customer?.firstname || "");
    const [lastName, setlastName] = useState(customer?.lastname || "");
    const [email, setEmail] = useState(customer?.email || "");
    const [phone, setPhone] = useState((
        customer?.custom_attributes?.find((o) => o.attribute_code === "phone")
            .value.slice(1) || ""
    ));
    const [loading, setLoading] = useState(false);
    const [showConfirm, setShowConfirm] = useState(false);
    const [confirmation, setConfirmation] = useState("");
    const [errorPhone, setErrorPhone] = useState("");

    const [error, setError] = useState("");

    const passwordCheck = async (e) => {
        e.preventDefault();

        const data = {
            userid: customer.id,
            password: confirmation,
        };

        const param = JSON.stringify(data);

        setLoading(true);
        if (confirmation === "") {
            setError("Please enter your confirmation password");
            setLoading(false);
        } else {
            try {
                const response = await requestPost(
                    `/rest/V1/integration/customer/checkPassword`,
                    param,
                    ""
                );
                return response;
            } catch (error) {
                setLoading(false);
                setError(error.response.data.message);
            }
        }
    };

    const handleSubmit = async (e) => {
        e.preventDefault();

        const passConfirmed = await passwordCheck(e);

        setLoading(true);
        zeroSlice(phone);

        if (passConfirmed) {
            console.log("Password confirmed");

            let customAttributes = [];
            if (customer.custom_attributes) {
                customAttributes = [...customer.custom_attributes];

                const phoneAttributeIndex = customAttributes.findIndex(
                    (o) => o.attribute_code === "phone"
                );
                customAttributes[phoneAttributeIndex] = {
                    attribute_code: "phone",
                    value: 0+phone,
                };
                console.log("customAttributes", customAttributes);
            } else {
                customAttributes.push({
                    attribute_code: "phone",
                    value: 0+phone,
                });
                console.log("customAttributes", customAttributes);
            }

            const body = {
                customer: {
                    ...customer,
                    firstname: firstName,
                    lastname: lastName,
                    email: email,
                    custom_attributes: customAttributes,
                },
            };
            if(phone.length >= 8 && phone.length < 13){
                try {
                    const updateResp = await requestPut(
                        "/rest/default/V1/customers/me",
                        null,
                        body,
                        user.token
                    );
                    mutateCustomer(updateResp, false);
                    setShowEditAccount(false);
                    setLoading(false);
                    setSuccess(true);
                    setSuccessMessage("Your account has been updated.");
                } catch (error) {
                    setError(error.message);
                }
            } else {
                // setError('Your phone number must be started with 08xxxxxxx');
                phone.length <= 8 ? 
                setError("Phone number must be at least 8 characters") 
                : setError("Phone number must be less than 13 characters");
                    
            }

            setLoading(false);
        } else {
            setError("Make sure your password is correct.");
            setLoading(false);
        }

        setLoading(false);
    };

    function handleChangePassword () {
        setShowEditPass(true);
        setShowEditAccount(false);
    };

    function phoneCheck(num) {
        console.log(num.length);
        let newNum = zeroSlice(num)
        setPhone(newNum);
        num.length < 8 ? 
                setErrorPhone("Phone number must be at least 8 characters") 
                : num.length >= 13 ? 
                    setErrorPhone("Phone number must be less than 13 characters") 
                    : setErrorPhone("");
    }

    function zeroSlice(phone) {

        if (phone.substring(0,1) == 0) {
            return phone.slice(1);
        } 
    }

    return (
        <div className="rounded border p-4 text-black text-base">
            <h2 className="text-base font-bold ">Edit Account Information</h2>

            <form onSubmit={(e) => handleSubmit(e)}>
                <div className="mt-2">
                    <label className="block text-sm font-bold">Email *</label>
                    <input
                        className="w-full rounded mt-1 border-gray-400 bg-gray-200 text-base"
                        type="text"
                        placeholder="Email"
                        required
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        disabled
                    />
                </div>
                
                <div className="mt-2 xl:mt-3">
                    <label className="block text-sm font-bold">
                        First Name *
                    </label>
                    <input
                        className="w-full rounded mt-1 border-gray-400 text-base"
                        type="text"
                        placeholder="First Name"
                        required
                        value={firstName}
                        onChange={(e) => setFirstName(e.target.value)}
                    />
                </div>

                <div className="mt-2">
                    <label className="block text-sm font-bold">
                        Last Name *
                    </label>
                    <input
                        className="w-full rounded mt-1 border-gray-400 text-base"
                        type="text"
                        placeholder="Last Name"
                        required
                        value={lastName}
                        onChange={(e) => setlastName(e.target.value)}
                    />
                </div>

                

                <div className="mt-2">
                    <label className="block text-sm font-bold">Phone *</label>
                    <small className="block mb-1 text-gray-400">* Please fill the number start with 8xxxxxxx</small>
                    <div className="flex items-center">
                        
                        <input className="flex-0 w-16 text-center rounded mt-1 border-gray-400 bg-gray-200 text-base " type="text" placeholder="Phone" disabled
                            value="+62"
                        />
                        <p className="text-2xl font-bold text-center px-2">-</p>
                        <input className="w-full rounded mt-1 border-gray-400 text-base" style={{ appearance : "textfield" }} type="number" placeholder="Phone" reuired
                            value={phone}
                            onKeyUp={(e) => phoneCheck(e.target.value)}
                            onChange={(e) => {setPhone(e.target.value);}}
                        />
                    </div>
                        <span className="text-sm text-red-700">{errorPhone}</span>
                </div>

                {/* <div className="mt-2">
                    <label className="block text-sm font-bold">Current Password</label>
                    <input className="w-1/2 md:w-1/4 rounded mt-1 mr-1 bg-gray-200 border-gray-400 text-base" type="text" placeholder="Phone" disabled
                        value={phone} 
                    />
                    <button className='p-2 bg-primary text-white rounded font-semibold'>Change Password</button>
                </div> */}

                <div className="mt-2">
                    <label className="block text-sm font-bold">
                        Confirm Password *
                    </label>
                    <small className="text-gray-400">
                        To update your profile you need confirm your password.
                    </small>
                    <div className="relative w-full">
                        <input
                            type={!showConfirm ? "password" : "text"}
                            name="new-password"
                            id="new-password"
                            className="block w-full px-4 border rounded"
                            placeholder="Confirmation Password"
                            value={confirmation}
                            onChange={(e) => setConfirmation(e.target.value)}
                        />
                        <div className="absolute inset-y-0 right-0 pr-3 mt-1 mr-3 flex items-center text-lg text-gray-600">
                            <i
                                className={`fa fa-eye ${
                                    showConfirm ? "hidden" : "block"
                                }`}
                                onClick={() => setShowConfirm(!showConfirm)}
                                aria-hidden="true"
                            ></i>
                            <i
                                className={`fa fa-eye-slash ${
                                    !showConfirm ? "hidden" : "block"
                                }`}
                                onClick={() => setShowConfirm(!showConfirm)}
                                aria-hidden="true"
                            ></i>
                        </div>
                    </div>
                </div>

                <div className="mt-4">
                    <div className="flex items-center gap-4">
                        <button
                            className="bg-secondary text-white font-bold rounded py-2 px-4 disabled:opacity-50 disabled:cursor-auto"
                            type="submit"
                            disabled={loading}
                        >
                            <div
                                className={`flex flex-row items-center justify-center my-3 ${
                                    !loading ? "hidden" : ""
                                }`}
                            >
                                <LoaderBounce />
                            </div>
                            <span className={`${loading ? "hidden" : "text-base"}`}>
                                Save Account
                            </span>
                        </button>
                        <button onClick={() => handleChangePassword()} className="bg-primary text-white font-bold rounded py-2 px-4">
                                    Change Password
                        </button>
                    </div>
                    <div
                        className={`mt-4 py-4 text-red-600 font-bold flex flex-row items-center justify-center bg-red-100 border-l-4 border-red-600 ${
                            !error ? "hidden" : ""
                        }`}
                    >
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            className="h-5 w-5 mr-1"
                            viewBox="0 0 20 20"
                            fill="currentColor"
                        >
                            <path
                                fillRule="evenodd"
                                d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z"
                                clipRule="evenodd"
                            />
                        </svg>
                        {error}
                    </div>
                </div>
            </form>
        </div>
    );
};

export default EditAccount;

import React, { useState } from 'react'
import LoaderBounce from '../../components/Loader/Bounce'
import { requestPut } from '../../lib/axios'
import useUser from '../../lib/useUser'

const ChangePassword = ({ setShowEditPass, setSuccess, setSuccessMessage }) => {
    const { user } = useUser();
    const [currentPass, setCurrentPass] = useState("");
    const [newPass, setNewPass] = useState("");
    const [confirmPass, setConfirmPass] = useState("");
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState("");
    const [showCurrentPass, setShowCurrentPass] = useState(false);
    const [showNewPass, setShowNewPass] = useState(false);
    const [showConfirmPass, setShowConfirmPass] = useState(false);

    const handleSubmit = async (e) => {
        e.preventDefault();
        setLoading(true);

        const body = {
            "currentPassword": currentPass,
            "newPassword": newPass
        }

        if (newPass === confirmPass){
            try {
                const updateResp = await requestPut('/rest/default/V1/customers/me/password', null, body, user.token);
                setShowEditPass(false);
                setSuccess(true);
                setSuccessMessage("Your password has been successfully changed.");
            } catch (error) {
                let errorArgument = error.response.data.message;
                error.response.data.parameters?.forEach((element, index) => {
                    const regex = new RegExp('%'+(parseInt(index)+1),"g");
                    console.log(regex)
                    errorArgument = errorArgument.replace(regex, element)
                });
                setError(errorArgument);
            }
            
        } else {
            setError("New password and confirm password does not match, please try again.");
            setLoading(false);
        }

        setLoading(false);
    }

    return (
        <div className="rounded border p-4 text-black text-base">
            <h2 className="text-base font-bold ">Change Password</h2>

            <form onSubmit={(e) => handleSubmit(e)}>
                <div className="mt-2 xl:mt-3">
                    <label className="block text-sm font-bold">Current Password *</label>
                    <div className="relative">
                        <input className="w-full rounded mt-1 border-gray-400 text-base" type={!showCurrentPass ? "password" : "text"} required
                            value={currentPass} 
                            onChange={(e) => setCurrentPass(e.target.value)}
                        />
                        <div className="absolute inset-y-0 right-0 pr-3 mt-1 mr-3 flex items-center text-lg text-gray-600">
                            <i
                                className={`fa fa-eye ${
                                    showCurrentPass ? "hidden" : "block"
                                }`}
                                onClick={() => setShowCurrentPass(!showCurrentPass)}
                                aria-hidden="true"
                            ></i>
                            <i
                                className={`fa fa-eye-slash ${
                                    !showCurrentPass ? "hidden" : "block"
                                }`}
                                onClick={() => setShowCurrentPass(!showCurrentPass)}
                                aria-hidden="true"
                            ></i>
                        </div>
                    </div>
                </div>

                <div className="mt-2">
                    <label className="block text-sm font-bold">New Password *</label>
                    <div className='relative'>
                        <input className="w-full rounded mt-1 border-gray-400 text-base" type={!showNewPass ? "password" : "text"} required
                            value={newPass} 
                            onChange={(e) => setNewPass(e.target.value)}
                        />
                        <div className="absolute inset-y-0 right-0 pr-3 mt-1 mr-3 flex items-center text-lg text-gray-600">
                            <i
                                className={`fa fa-eye ${
                                    showNewPass ? "hidden" : "block"
                                }`}
                                onClick={() => setShowNewPass(!showNewPass)}
                                aria-hidden="true"
                            ></i>
                            <i
                                className={`fa fa-eye-slash ${
                                    !showNewPass ? "hidden" : "block"
                                }`}
                                onClick={() => setShowNewPass(!showNewPass)}
                                aria-hidden="true"
                            ></i>
                        </div>
                    </div>
                </div>

                <div className="mt-2">
                    <label className="block text-sm font-bold">Confirm New Password *</label>
                    <div className='relative'>
                        <input className="w-full rounded mt-1 border-gray-400 text-base" type={!showConfirmPass ? "password" : "text"} required
                            value={confirmPass} 
                            onChange={(e) => setConfirmPass(e.target.value)}
                        />
                        <div className="absolute inset-y-0 right-0 pr-3 mt-1 mr-3 flex items-center text-lg text-gray-600">
                                <i
                                    className={`fa fa-eye ${
                                        showConfirmPass ? "hidden" : "block"
                                    }`}
                                    onClick={() => setShowConfirmPass(!showConfirmPass)}
                                    aria-hidden="true"
                                ></i>
                                <i
                                    className={`fa fa-eye-slash ${
                                        !showConfirmPass ? "hidden" : "block"
                                    }`}
                                    onClick={() => setShowConfirmPass(!showConfirmPass)}
                                    aria-hidden="true"
                                ></i>
                            </div>
                        </div>
                </div>

                <div className="mt-4">
                    <button 
                        className="bg-secondary text-white font-bold rounded py-2 px-4 disabled:opacity-50 disabled:cursor-auto" 
                        type="submit"
                        disabled={loading}
                    >
                        <div className={`flex flex-row items-center justify-center my-3 ${!loading ? 'hidden' : ''}`}>
                            <LoaderBounce />
                        </div>
                        <span className={`${loading ? 'hidden' : 'text-base'}`}>Save Account</span>
                    </button>
                    <div className={`mt-2 text-red-600 font-bold flex flex-row items-center justify-center ${!error ? 'hidden' : ''}`}>
                        <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 mr-1" viewBox="0 0 20 20" fill="currentColor">
                            <path fillRule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z" clipRule="evenodd" />
                        </svg>
                        {error}
                    </div>
                </div>
            </form>
        </div>
    )
}

export default ChangePassword

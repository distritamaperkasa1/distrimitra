const Spinner = () => {
    return (
        <div 
            style={{ borderTopColor: "transparent" }}
            className="w-6 h-6 border-2 border-blue-400 border-solid rounded-full animate-spin"
        ></div>
    )
}

export default Spinner;
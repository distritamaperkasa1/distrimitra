/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import styles from "../../styles/Components/Checkbox.module.css";
import { useRouter } from "next/router";


const Checkbox = (props) => {
    const router = useRouter();
   const { variant, label, children, count, item, onChecked, ...options } =
      props;
   const [checked, setChecked] = useState(false);

   useEffect(() => {
      if (onChecked && item) {
         return onChecked(item, checked);
      }
   }, [checked]);

   return (
      <div className={`flex flex-row items-center space-x-3`}>
         {/* <input
            type='checkbox'
            className={`flex-initial focus:ring-transparent focus:bg-transparent ${styles["root"]}`}
            onChange={(e) => {
               setChecked(e.target.checked);
            }}
            checked={checked}
            {...options}
         /> */}

         {variant == "content" ? (
            children
         ) : (
            <span
               className={`text-base font-normal ${
                  !checked ? "text-dark-grey" : "text-black"
               }`}
               onClick={() => setChecked(!checked)}
            >
                <a onClick={() => router.push('/category/low-voltage-products-systems/mcb/mcb-box.html')}>{label}{" "}</a>
               {count && (
                  <span className='text-base font-normal text-dark-grey-2'>{`(${count})`}</span>
               )}
            </span>
         )}
      </div>
   );
};

export default Checkbox;

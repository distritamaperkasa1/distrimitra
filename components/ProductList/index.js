/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable @next/next/no-img-element */

import React, { useEffect, useState, useCallback } from "react";
import Image from "next/image";
import { moneyFormat } from "../../lib/helper";
import ReactStars from "react-rating-stars-component";
import styles from "../../styles/Components/Product.module.css";
import { useRouter } from "next/router";
import fetchJson from '../../lib/fetchJson';
import useUser from "../../lib/useUser";
import useCart from "../../lib/useCart";
import { requestPut, requestPost } from '../../lib/axios'
import AsyncLocalStorage from '@createnextapp/async-local-storage' 

const ProductList = (props) => {
   const { data } = props;
   const router = useRouter();
   const { user, mutateUser } = useUser();
   const { cart, mutateCart } = useCart(user);
   const [ cartState, setCartState ] = useState([]);
   const [ disableCart, setDisableCart ] = useState(false);
   const [showButton, setShowButton] = useState(false);
   const [ totalItemsAsync, setTotalItemsAsync ] = useState(0);
   const [ subTotalAsync, setSubTotalAsync ] = useState(0);
   const [dataAttr, setDataAttr] = useState([]);
   const readCurrentQtyCart = async () => {
       let currentQtyCart
       try {
         currentQtyCart = await AsyncLocalStorage.getItem('totalItems')
        //  console.log(currentQtyCart+' asdsa')
         setTotalItemsAsync(currentQtyCart)
       } catch(e) {

       }
    //    console.log(currentQtyCart)
   }

   const readCurrentSubTotal = async () => {
       let currentSubTotal
       try {
        currentSubTotal = await AsyncLocalStorage.getItem('subtotal')
        //  console.log(currentQtyCart+' asdsa')
         setSubTotalAsync(currentSubTotal)
       } catch(e) {

       }
    //    console.log(currentQtyCart)
   }
    useEffect(() => {
        setCartState(cart?.items)
        readCurrentQtyCart();
        readCurrentSubTotal();
        // console.log(cartState+' AAA')
    }, [cart, setCartState, readCurrentQtyCart, readCurrentSubTotal])



    const addToCartAsync = async (e) => {
        // e.preventDefault();
        // alert('ABCDEF')
        setDisableCart(true)
        let updatedQtyCart;
        let updatedSubTotal;
        var updateQtyCart = (parseInt(totalItemsAsync)+1).toString()
        try {
            // setTotalItemsAsync(updateQtyCart)
            // console.log(updateQtyCart)
            await AsyncLocalStorage.setItem('totalItems', updateQtyCart)
            updatedQtyCart = AsyncLocalStorage.getItem('totalItems')
            console.log(updatedQtyCart+' updated')
        } catch(e) {
            // error
        }
        // alert(updatedSubTotal)
        // setTotalItemsAsync(updateQtyCart)
        setTimeout(
            function(){
                setDisableCart(false)
            }, 1200
        )
        addToCart(e);
        // setTimeout(
        //     function(){
        //         readCurrentQtyCart();
        //     }, 1000
        // )
    }

    const addToCart = async (e) => {
        e.preventDefault();
        if (!user.isLoggedIn){
            router.push('/login')
        } else {
            // setLoading(true)
            setDisableCart(true)
            const quoteId = await fetchJson("/api/cart/quote", {
              method: "POST",
              headers: { "Content-Type": "application/json" },
             });
    
            const param = {
              cartItem: {
                 sku: data.sku,
                 qty: 1,
                 quote_id: quoteId
              }
            }
    
           console.log(JSON.stringify(param))
           console.log(JSON.stringify(data))
    
           const resp = fetchJson("/api/cart/item", {
              method: "POST",
              headers: { "Content-Type": "application/json" },
              body: JSON.stringify(param),
           })
           .catch((error) => {
               console.log(error)
                setDisableCart(true)
            });
    
            mutateUser(resp)
            mutateCart();
            setTimeout(
                function(){
                    mutateUser(resp)
                    mutateCart();
                }, 1000
            )
    
        //    mutateUser(resp)
            
        
        
        //    router.push('/')
        
        await AsyncLocalStorage.setItem('successAddToCart','true')
        if(resp){
            let addedToCart = await AsyncLocalStorage.getItem('successAddToCart')
            setDisableCart(false)
            // if(addedToCart === 'true'){
            //     setTimeout(
            //         async function(){
            //             try{
            //                 await AsyncLocalStorage.setItem('successAddToCart','false')
            //             } catch(e){
                
            //             }
            //         }, 3000    
            //     )
            // }
            //   setLoading(false)
        }
     
           console.log("resp add to cart", resp)
        }
     }
   const useMediaQuery = (width) => {
    const [targetReached, setTargetReached] = useState(false);
  
    const updateTarget = useCallback((e) => {
      if (e.matches) {
        setTargetReached(true);
      } else {
        setTargetReached(false);
      }
    }, []);
  
    useEffect(() => {
      const media = window.matchMedia(`(max-width: ${width}px)`);
      media.addListener(updateTarget);
  
      // Check on mount (callback is not called until a change occurs)
      if (media.matches) {
        setTargetReached(true);
      }
  
      return () => media.removeListener(updateTarget);
    }, []);
    return targetReached;
  };
  const isBreakpoint = useMediaQuery(768);

   // useEffect(() => {
   //    let attribute = data?.custom_attributes;
   //    let arr = [];
   //    attribute.map((data, key) => {
   //       arr[data.attribute_code] = data.value;
   //    });

   //    setDataAttr(arr);
   // }, [data]);

   const ratingChanged = (newRating) => {
      console.log(newRating);
   };

   return (
      <div
         className={`flex h-auto xl:pt-5 xl:pb-3 xl:px-5 cursor-pointer hover:bg-white xl:my-4 rounded-md ${styles["product-list"]} `}
        //  onMouseOver={() => !isBreakpoint ? setShowButton(true) : null}
        //  onMouseOut={() => !isBreakpoint ? setShowButton(false) : null}
        //  onClick={() => router.push(`/product/${data.sku}`)}
      >
          <div style={{ flex:0.3 }} className="list-product">
            <Image
                src={data.image_url ? data.image_url : "/img/example-product.png"}
                objectFit='contain'
                width={160}
                style={{ width:'100%' }}
                height={160}
                onClick={() => router.push(`/product/${data.sku}`)}
            />
          </div>

         <div style={{ flex:0.7 }}>
            <div className='text-sm text-gray-500 font-normal mt-4 mb-2'>
                SKU#: {data.sku}
            </div>
            <div className={`text-base font-semibold text-gray-800`}
                onClick={() => router.push(`/product/${data.sku}`)}
            >
                    <h1>  
                        {data.name}
                    </h1>
            </div>

            <div className='flex flex-col 1xl:flex-row justify-between 1xl:items-center my-2'>
                <ReactStars
                classNames='flex-1'
                count={5}
                onChange={ratingChanged}
                size={20}
                value={0}
                activeColor='#F2994A'
                />

                <div className='flex-1 1xl:text-right font-normal text-gray-600 text-xs'>
                <div>
                    <span className='text-sm font-semibold text-gray-800'>
                        {/* 0.0 */}
                    </span>{" "}
                    (0 Reviews)
                </div>
                </div>
            </div>
            <div className="flex">
                <div style={{ flex:0.5 }} className='text-md font-bold text-gray-800 my-4'>
                    {moneyFormat(data.final_price)}
                </div>
                <div style={{ flex:0.5, textAlign:"right" }}>
                    {
                        data.qty > 0 && !disableCart
                        ?
                        <button
                            className={`bg-secondary text-white text-sm py-3 font-bold rounded mt-4 
                            ${styles["button-add-to-cart"]}
                            `}
                            style={{ zIndex:999, padding:'1em' }}
                            disabled={disableCart}
                            // onClick={() => router.push(`/product/${data.sku}`)}
                            onClick={(e) => addToCart(e)}
                            // onClick={(e) => addToCartAsync(e)}
                            // onClick={() => alert('Tes')}
                        >
                            Add to cart
                        </button>
                        :
                        <button
                            className={`text-white text-sm py-3 font-bold rounded mt-4
                            ${styles["button-add-to-cart-disabled"]}
                            `}
                            style={{ zIndex:999, padding:'1em' }}
                            disabled={false}
                            // onClick={() => router.push(`/product/${data.sku}`)}
                            // onClick={() => alert('Product out of stock')}
                            // onClick={() => alert('Tes')}
                        >
                            Add to cart
                        </button>
                    }
                </div>
            </div>
         </div>
      </div>
   );
};

export default ProductList;

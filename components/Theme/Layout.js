import React, {
    useState,
    useEffect,
    useContext,
    useCallback,
    createRef,
    useRef,
} from "react";

import Head from "next/head";
import TopNav from "./TopNav";
import Header from "./Header";
import Navbar from "./Navbar/Index";
import Footer from "./Footer";
import Link from "next/link";
import responsiveStyle from "../../styles/Components/HeaderResponsive.module.css";
import { requestGet } from "../../lib/axios";
import * as gtag from "../../lib/gtag";

const Layout = ({ children, title = "Mall Listrik", menu, modalOpen }) => {
    const [buttonWa, setButtonWa] = useState(null);
    const [buttonLoaded, setButtonLoaded] = useState(false);

    const isDev = process.env.NEXT_PUBLIC_INDEXING === "true"

    useEffect(async () => {
        if (!buttonLoaded) {
            const wa = await requestGet("/rest/V1/home/whatsapp");
            setButtonWa(wa);
            console.log(title);
        }
        setButtonLoaded(true);
        console.log(JSON.stringify(buttonWa));
    }, [buttonWa, setButtonWa]);

    return (
        <div>
            <Head>
                <title>{`${title} - Mall Listrik`}</title>
                <meta name="description" content="Mall Listrik" />
                { isDev && (
                    <>
                        <meta key="robots" name="robots" content="NOINDEX,NOFOLLOW" />
                        <meta key="googlebot" name="googlebot" content="NOINDEX,NOFOLLOW" />
                    </>
                )}
                <link rel="icon" href="/img/favicon.png" />
                <link
                    rel="stylesheet"
                    href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
                    integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p"
                    crossOrigin="anonymous"
                />
                <script
                    async
                    src={`https://www.googletagmanager.com/gtag/js?id=${gtag.GA_TRACKING_ID}`}
                />
                <script
                    id="gtag-init"
                    strategy="afterInteractive"
                    dangerouslySetInnerHTML={{
                        __html: `
                            window.dataLayer = window.dataLayer || [];
                            function gtag(){dataLayer.push(arguments);}
                            gtag('js', new Date());
                            gtag('config', '${gtag.GA_TRACKING_ID}', {
                            page_path: window.location.pathname,
                            });
                        `,
                    }}
                />
            </Head>

            <header
                id="myHeader"
                style={modalOpen ? { zIndex: 0 } : null}
                className={`sticky ` + responsiveStyle.headerMobile}
            >
                <TopNav />
                <Header>
                    <Navbar menu={menu} />
                </Header>
            </header>

            <main className={responsiveStyle.contentMobile}>
                <div>{children}</div>
            </main>
            {buttonWa &&
            title !== "Checkout Shipping" &&
            buttonWa[0].is_visible === "1" ? (
                <Link href={buttonWa[0].link}>
                    <a target="_blank">
                        <img
                            className={`${responsiveStyle["img-contact-wa"]}`}
                            src={buttonWa[0].image_url}
                        />
                    </a>
                </Link>
            ) : null}
            <Footer />
        </div>
    );
};

export default Layout;

import React, { useState } from "react";
import styles from "../../../styles/Theme/Navbar.module.css";
import Link from "next/link";
import AsyncLocalStorage from '@createnextapp/async-local-storage'

function Menu(props) {
   const { data, menu } = props;

   const [active, setActive] = useState(false);

   const handleClick = () => {
      setActive(!active);
   };

   const handleMenuClick = () => {
    // alert('AAA')
         AsyncLocalStorage.setItem('menuState', 'false')
   }

   const generateMenu = () => {
      let div1 = [];
      let div2 = [];
      let div3 = [];
      data.children.map((child, index) => {
         if ((index + 1) % 3 == 1) {
            div1.push(parentMenu(child, index));
         } else if ((index + 1) % 3 == 2) {
            div2.push(parentMenu(child, index));
         } else {
            div3.push(parentMenu(child, index));
         }
      });

      return (
         <>
            {div1.length > 0 ? (
               <div className='flex flex-col flex-1'>{div1}</div>
            ) : (
               ""
            )}

            {div2.length > 0 ? (
               <div className='flex flex-col flex-1'>{div2}</div>
            ) : (
               ""
            )}

            {div3.length > 0 ? (
               <div className='flex flex-col flex-1'>{div3}</div>
            ) : (
               ""
            )}
         </>
      );
   };

   const parentMenu = (parentData, key) => {
      return (
         <ul className='text-dark-grey w-max h-min-content' key={key}>
            <h2 className='text-dark-grey mt-2 xl:mt-6 px-6'>
               {parentData.name}
            </h2>
            {parentData.children.map((childmenu, index) => {
               return childMenu(childmenu, index);
            })}
         </ul>
      );
   };

   const childMenu = (childData, key) => {
      const BASE_API_URL = process.env.NEXT_PUBLIC_API_ENDPOINT;
      const regexBaseUrl = new RegExp(BASE_API_URL, "gi");
      const path = childData.url_path.replace(regexBaseUrl, '');
      return (
         <li 
            className='my-1 w-max cursor-pointer' 
            key={key}
         >
            <Link 
               href={`/category/${path}`}
               prefetch={false}
            >
               <a className='font-normal' onClick={handleMenuClick}>{childData.name}</a>
            </Link>
         </li>
      );
   };

   return (
      <li
         className={"py-4 py-2-mobile flex flex-col xl:flex-row " + styles.childToogle}
         onClick={handleClick}
      >
         <span
            className={`flex flex-row justify-between pl-6 pl-0-mobile xl:px-6 ${
               active ? "text-secondary" : ""
            }`}
         >
            {data.name}

            <div>
               {active ? (
                  <svg
                     xmlns='http://www.w3.org/2000/svg'
                     className={`h-5 w-5 xl:hidden ${
                        data.children.length > 0 ? "" : "hidden"
                     }`}
                     fill='none'
                     viewBox='0 0 24 24'
                     stroke='currentColor'
                  >
                     <path
                        strokeLinecap='round'
                        strokeLinejoin='round'
                        strokeWidth='2'
                        d='M5 15l7-7 7 7'
                     />
                  </svg>
               ) : (
                  <svg
                     xmlns='http://www.w3.org/2000/svg'
                     className={`h-5 w-5 xl:hidden ${
                        data.children.length > 0 ? "" : "hidden"
                     }`}
                     fill='none'
                     viewBox='0 0 24 24'
                     stroke='currentColor'
                  >
                     <path
                        strokeLinecap='round'
                        strokeLinejoin='round'
                        strokeWidth='2'
                        d='M19 9l-7 7-7-7'
                     />
                  </svg>
               )}
            </div>
         </span>

         {data.children.length > 0 ? (
            <div
               className={`${
                  styles.child
               } ml-4 xl:ml-0 flex flex-col xl:flex-row ${
                  active ? "" : "hidden"
               }`}
               style={{ maxHeight:'75vh' }}
            >
               {generateMenu()}
            </div>
         ) : (
            ""
         )}
      </li>
   );
}

export default Menu;
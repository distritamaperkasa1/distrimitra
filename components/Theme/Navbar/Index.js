import React, { useContext, useEffect, useCallback } from 'react'
import styles from '../../../styles/Theme/Navbar.module.css'
import Link from 'next/link'
import Image from 'next/image'
import Menu from './Menu';
import { useState } from 'react';
import { MenuContext } from '../../../contexts/MenuContext';
import { requestGet } from '../../../lib/axios'
import { useRouter } from 'next/router'
import AsyncLocalStorage from '@createnextapp/async-local-storage'

// const { menu } = useContext(MenuContext);


function Index(props) {
    const { setMenu } = useContext(MenuContext);
    const [active, setActive] = useState(false);
    const [ searchInput, setSearchInput ] = useState();
    const [aboutActive, setAboutActive] = useState(false);
    const [menuActive, setMenuActive] = useState(true);
    const [menuBaru, setMenuBaru] = useState([]);
    const { menu } = useContext(MenuContext);
    const router = useRouter();

    const handleClick = () => {
        setActive(!active);
    };


    const useMediaQuery = (width) => {
        const [targetReached, setTargetReached] = useState(false);
      
        const updateTarget = useCallback((e) => {
          if (e.matches) {
            setTargetReached(true);
          } else {
            setTargetReached(false);
          }
        }, []);
      
        useEffect(() => {
          const media = window.matchMedia(`(max-width: ${width}px)`);
          media.addListener(updateTarget);
        //   console.log(JSON.stringify(promoProduct))
      
          // Check on mount (callback is not called until a change occurs)
          if (media.matches) {
            setTargetReached(true);
          }
      
          return () => media.removeListener(updateTarget);
        }, []);
        return targetReached;
      };
    const isBreakpointMenu = useMediaQuery(1279);

    const handleAboutClick = () => {
        setAboutActive(!aboutActive);
    };

    const handleMenuClick = () => {
        setMenuActive(!menuActive);
        setTimeout(
            function(){
                setMenuActive(true);
            }, 1000
        )
    };
    
    const toggleMenuClick = () => {
        setMenuActive(!menuActive);
    };

    const handleMenuCategoryClick = () => {
        setMenuCategoryActive(!menuCategoryActive);
        setTimeout(
            function(){
                setMenuCategoryActive(true);
            }, 1000
        )
    };

    const searchProduct = (e) => {
        e.preventDefault();
        if(searchInput){
            AsyncLocalStorage.setItem('searchInput',searchInput)
            AsyncLocalStorage.setItem('isSearch','true')
            handleClick();
            router.push('/search?page=1')
        }
    }

    const getMenuState = async () => {
        let menuState
        try{
            menuState = await AsyncLocalStorage.getItem('menuState')
            if(menuState === 'false'){
                setActive(false)
                AsyncLocalStorage.setItem('menuState', 'true')
            }
        } catch(e) {

        }
        // console.log(menuState)
    }

    useEffect(() => {
        setMenu(menu)
        setInterval(
            function(){
                getMenuState();
            }, 500
        )
    }, [setMenu, menu])

    return (
        <nav className={`bg-white xl:bg-primary xl:text-white font-bold text-base ${active ? 'w-screen absolute left-0 top-0 p-2 z-50 ' : ''}`}>
            <button 
                className={`ml-4 xl:hidden text-black ${active ? 'hidden' : ''}`}
                onClick={handleClick}
            >
                <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 6h16M4 12h16M4 18h16" />
                </svg>
            </button>
            <div className={`mx-5 mx-1-mobile overflowy-mobile overflowxhidden-mobile xl:mx-auto max-width-1136 relative xl:block ${active ? '' : 'hidden'}`}>
                <div className="xl:hidden mt-2 pb-2 flex flex-row justify-between border-b items-center">
                    <Image src="/img/mall-listrik.png" alt="Logo Trimitra" width={160} height={50} />
                    <button onClick={handleClick}>
                        <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M6 18L18 6M6 6l12 12" />
                        </svg>
                    </button>
                </div>
                <ul className={"flex flex-col xl:flex-row " + styles.navbar + " " + styles.navbarMobile}>
                    <li className={`static ${styles.navbarDropdownToggle} ${styles['width-300']}`}>
                        {/* <Link href="/"> */}
                            <div className={[styles.searchContainer+' hidden-desktop']}>
                                <form onSubmit={(e) => searchProduct(e)}>
                                    <div className={styles.search}>
                                        {/* { searchInput } */}
                                            <input onChange={(e) => setSearchInput(e.target.value)} type="text" className="border rounded border-gray" placeholder="Search for name, brand, product category..." />
                                            <button type="submit" className="bg-secondary text-white py-2 font-bold rounded">Search</button>
                                    </div>
                                </form>
                            </div>
                            <a onClick={ isBreakpointMenu ? toggleMenuClick : null} className="flex flex-row items-center py-2 pb-1-mobile xl:py-5 xl:px-4 justify-between">
                                <div className={`flex flex-row items-center ${styles["full-width-mobile"]}`}>
                                    <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 hidden xl:block" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 6h16M4 12h16M4 18h16" />
                                    </svg>
                                    <span className={`xl:mx-2 ${styles["full-width-mobile"]}`}>SHOP DEPARTMENTS</span>
                                    {isBreakpointMenu && menuActive ?
                                        <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 15l7-7 7 7" />
                                        </svg> : isBreakpointMenu && !menuActive ?
                                        <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M19 9l-7 7-7-7" />
                                        </svg> 
                                        :
                                        null
                                    }
                                </div>
                                <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 hidden xl:block" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M19 9l-7 7-7-7" />
                                </svg>
                            </a>
                        {/* </Link> */}
                        {
                            menuActive
                            ?
                            <div className={`xl:shadow-2xl xl:absolute text-left bg-white text-secondary-dark-grey left-0 flex flex-row ${styles.navbarDropdown} ${styles.categoriesContainer}`}>
                                <ul className={`static ${styles["full-width-mobile"]}`} onClick={ !isBreakpointMenu ? handleMenuClick : null}>
                                    {menu.map((data, index) => {
                                        return <Menu onClick={handleMenuClick} menu={menuActive} data={ data } key={ index } />
                                    })}
                                </ul>
                            </div>
                            :
                            null
                        }
                    </li>

                    <li>
                        <Link href="/">
                            <a className="flex flex-row items-center py-2 xl:py-5 xl:px-4">
                                <span>Home</span>
                            </a>
                        </Link>
                    </li>

                    <li className={"static " + styles.navbarDropdownToggle}>
                        <div className={`flex flex-row items-center py-2 xl:py-5 xl:px-4 justify-between`} onClick={handleAboutClick}>
                            <span className="mr-2">About Us</span>
                            
                            <div>
                            {aboutActive ?
                                <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 15l7-7 7 7" />
                                </svg> :
                                <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M19 9l-7 7-7-7" />
                                </svg> 
                            }
                            </div>
                        </div>
                        
                        <div className={`xl:shadow-2xl xl:absolute text-left bg-white text-dark-grey ${aboutActive ? '' : 'hidden'} ` + styles.navbarDropdown}>
                            <ul>
                                <li className="my-4">
                                    <Link href="/about-us/company-profile">
                                        <a className="font-normal">Company Profile</a>
                                    </Link>
                                </li>

                                <li className="my-4">
                                    <Link href="/about-us/visi-misi">
                                        <a className="font-normal">Vision & Mission</a>
                                    </Link>
                                    
                                </li>
                            </ul>
                        </div>
                    </li>

                    <li>
                        <Link href="/project">
                            <a className="flex flex-row items-center py-2 xl:py-5 xl:px-4">
                                <span>Project</span>
                            </a>
                        </Link>
                        
                    </li>

                    <li>
                        <Link href="/request-quotation">
                            <a className="flex flex-row items-center py-2 xl:py-5 xl:px-4">
                                <span>Request For Quotation</span>
                            </a>
                        </Link>
                    </li>

                    <li>
                        <Link href="/tutorial">
                            <a className="flex flex-row items-center py-2 xl:py-5 xl:px-4">
                                <span>Tutorial</span>
                            </a>
                        </Link>
                    </li>

                    <li>
                        <Link href="/contact-us">
                            <a className="flex flex-row items-center py-2 xl:py-5 xl:px-4">
                                <span>Contact Us</span>
                            </a>
                        </Link>
                    </li>
                </ul>
                
            </div>
        </nav>
    )
}

export async function getServerSideProps({ params }) {
   
    const { id } = params;
 
    const [menu] = await Promise.all([
       requestGet("/rest/V1/products/all-categories")
    ]);

    console.log("Product", product)
 
    return {
       props: {
          menu: menu
       },
    };
 }

export default Index;
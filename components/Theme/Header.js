import React, { useContext, useEffect, useState } from 'react'
import Image from 'next/image'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { MenuContext } from '../../contexts/MenuContext';
import useUser from "../../lib/useUser";
import useCart from "../../lib/useCart";
import fetchJson from "../../lib/fetchJson";
import styles from '../../styles/Theme/Header.module.css'
import { moneyFormat } from '../../lib/helper'
import AsyncLocalStorage from '@createnextapp/async-local-storage'

function Header({children}) {
    const { user, mutateUser } = useUser();
    const { cart, mutateCart } = useCart(user);
    const [addToCartState, setAddToCartState] = useState(false)
    const [ searchInput, setSearchInput ] = useState();
    const { isLoggedIn } = user || {};
    const { items_qty: totalItems, subtotal } = cart || {};

    // console.log("Total items", totalItems)

    const { token, setToken } = useContext(MenuContext);
    const router = useRouter();

    const openAccount = () => {
        router.replace('/account');
    };

    const openCart = () => {
        router.replace('/cart');
    };

    const signOut = async () => {
        mutateUser(
            await fetchJson("/api/logout", { method: "POST" }),
            false,
        );
        router.replace('/');
    };

    const searchProduct = (e) => {
        e.preventDefault();
        if(searchInput){
            AsyncLocalStorage.setItem('searchInput',searchInput)
            AsyncLocalStorage.setItem('isSearch','true')
            router.push('/search?page=1')
        }
    }


    const detectAddToCart = async () => {
        let addToCart = await AsyncLocalStorage.getItem('successAddToCart')
        if(addToCart === 'true'){
            // alert(true);
            setAddToCartState(true);
            setTimeout(
                function(){
                    setAddToCartState(false);
                }, 2000
            )
            try{
                await AsyncLocalStorage.setItem('successAddToCart', 'false') 
            } catch(e){
    
            }
        }
    }

    const setDefaultFalse = async () => {
        try{
            await AsyncLocalStorage.setItem('successAddToCart', 'false') 
        } catch(e){

        }
    }

    useEffect(async () => {
        // setDefaultFalse();
        detectAddToCart();
        // console.log(cartState+' AAA')
    }, [cart])
                                                                                                                                                                                                
    return (
        <div className="flex xl:block flex-row items-center m-3 xl:m-0 justify-between">

        <div className="xl:m-5 flex max-width-1136 xl:mx-auto flex-row items-center justify-between flex-1">
            {
                addToCartState
                ?
                <div className={styles.successAddToCart}>
                    <p style={{ color:'white' }}>Successfully added item to cart</p>
                </div>
                :
                null
            }
            <div>
                <Link href="/">
                    <a>
                        <Image src="/img/mall-listrik.png" alt="Logo Trimitra" width={160} height={50} />
                        {/* <Image src="/trimitra-logo.png" alt="Logo Trimitra" width={201} height={40} /> */}
                    </a>
                </Link>
            </div>

            <form onSubmit={(e) => searchProduct(e)}>
                <div className={styles.search}>
                    {/* { searchInput } */}
                        <input onChange={(e) => setSearchInput(e.target.value)} type="text" className="border rounded border-gray" placeholder="Search for name, brand, product category..." />
                        <button type="submit" className="bg-secondary text-white py-2 font-bold rounded">Search</button>
                </div>
            </form>

            <div className="flex items-center">
                <div className="relative h-6 cursor-pointer" onClick={openCart}>
                    <Image src="/cart-icon.png" alt="Logo Trimitra" width={24} height={24} />
                    <div className="bg-secondary text-white w-6 h-6 flex items-center justify-center rounded-full absolute -right-3 -top-2">
                        <span className="font-bold text-sm">{totalItems || '0'}</span>
                    </div>
                </div>

                <div className="flex-col ml-4 hidden xl:flex cursor-pointer" onClick={openCart}>
                    <span className="text-sm">Shopping Cart</span>
                    <span className="font-bold text-sm">{moneyFormat(subtotal || 0)}</span>
                </div>
                <div className="hidden-desktop">
                    <img className={`${styles["img-alliance-header-mobile"]}`} src="/img/se_authorized.png"/>
                </div>

                <div className="ml-6 flex items-center relative hidden xl:flex">
                    <Image className={`${isLoggedIn ? 'cursor-pointer' : ''}`} src="/user-icon.png" alt="Logo Trimitra" width={24} height={24} onClick={openAccount} />
                    <span className={`text-sm font-bold ml-3 mr-5 cursor-pointer ${isLoggedIn ? '' : 'hidden'}`} onClick={openAccount}>
                        {user?.data?.firstname}
                    </span>
                    <Link href="/login">
                        <a className={`text-sm text-secondary font-bold ml-3 mr-5 ${isLoggedIn ? 'hidden' : ''}`}>
                            Sign In
                        </a>
                    </Link>
                    <Link href="/register">
                        <a className={`text-sm ${isLoggedIn ? 'hidden' : ''}`}>
                            Register
                        </a>
                    </Link>
                    <img className={`${styles["img-alliance"]}`} src="/img/se_authorized.png"/>
                </div>
            </div>
        </div>

        { children }

        </div>
    )
}

export default Header;
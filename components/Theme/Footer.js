import React from "react";
import Image from "next/image";
import styles from "../../styles/Theme/Footer.module.css";
import Link from "next/link";
import responsiveStyle from "../../styles/Components/HeaderResponsive.module.css";
import Subscribe from "../Subscribe";

function Footer() {
    return (
        <footer
            className={`${styles.footer} text-white ${responsiveStyle.footerMobile}`}
        >
            <div className="xl:mx-auto max-width-1136 py-10">
                <div>
                    <div className="mb-2 text-center xl:text-left">
                        <Image
                            src="/img/mall-listrik.png"
                            alt="Logo Trimitra"
                            width={160}
                            height={50}
                        />
                    </div>

                    <div className="flex flex-col xl:flex-row">
                        <div className="xl:mr-10">
                            <div className="flex flex-col">
                                    <a href="https://goo.gl/maps/sbrTsQFpMyxZ6jGz6">
                                <div className="flex flex-row justify-center xl:justify-start">
                                    <Image
                                        src="/geo-icon.png"
                                        alt="Trimitra Location"
                                        layout="fixed"
                                        width={20}
                                        height={20}
                                    />
                                    <h3 className="font-bold text-sm ml-2">
                                            PT. DISTRITAMA PERKASA
                                    </h3>
                                </div>
                                        </a>

                                <p className="text-sm text-secondary-dark-grey mt-4 text-center xl:text-left xl:ml-7">
                                Komplek Arcadia Blok G6 No 6 <br/>
                                Jl. Daan Mogot KM 21 <br/>
                                Tangerang 15122 - Indonesia
                                </p>
                            </div>

                            <div className="flex flex-row mt-10 justify-center xl:justify-start">
                                <Image
                                    src="/telepone-icon.png"
                                    alt="Trimitra Contact"
                                    layout="fixed"
                                    width={24}
                                    height={24}
                                />

                                <div className="ml-2">
                                    <div className="grid grid-cols-8 gap-x-2">
                                        <h3 className="font-bold text-sm mb-3 col-span-1">
                                            Ph:
                                        </h3>
                                        <h3 className="font-bold text-sm mb-3 col-span-7">
                                            021 - 29006315
                                        </h3>
                                        <h3 className="font-bold text-sm mb-3 col-span-1">
                                            
                                        </h3>
                                        <h3 className="font-bold text-sm mb-3 col-span-7">
                                            021 - 29006316
                                        </h3>
                                        <h3 className="font-bold text-sm mb-3 col-span-1">
                                            
                                        </h3>
                                        <h3 className="font-bold text-sm mb-3 col-span-7">
                                            021 - 29006317
                                        </h3>
                                    </div>
                                </div>
                            </div>

                            <div className="flex flex-row mt-10 justify-center xl:justify-start">
                                <Image
                                    src="/mail-icon.png"
                                    alt="Trimitra Email"
                                    layout="fixed"
                                    width={24}
                                    height={24}
                                />

                                <div className="ml-2">
                                    <h3 className="font-bold text-sm mb-3">
                                        mall-listrik@distritamaperkasa.com
                                    </h3>
                                </div>
                            </div>
                        </div>

                        <div className="flex flex-col xl:flex-row flex-1 gap-8 justify-center items-center xl:items-start text-center  xl:text-left">
                            <div className="flex-1 mt-5 xl:mt-0 hidden">
                                <div className="ml-2">
                                    <h3 className="font-bold text-sm">
                                        INFORMATION
                                    </h3>

                                    <div className="text-sm text-secondary-dark-grey mt-4">
                                        <Link href="/login">
                                            <a>
                                                <span>Information</span>
                                            </a>
                                        </Link>
                                    </div>
                                </div>
                            </div>

                            <div className="flex-0 justify-center items-center gap-2 xl:mt-0 ">
                                <h3 className="font-bold text-sm text-center">
                                    INFORMATION
                                </h3>
                                <div className="text-sm text-secondary-dark-grey flex flex-col mt-4 hover:text-secondary">
                                    <Link href="/about-us/company-profile">
                                        <a className="mb-3">About Us</a>
                                    </Link>
                                    <Link href="/project">
                                        <a className="mb-3">Project</a>
                                    </Link>
                                    <Link href="/request-quotation">
                                        <a className="mb-3">
                                            Request For Quotation
                                        </a>
                                    </Link>
                                    <Link href="/tutorial">
                                        <a className="mb-3">Tutorial</a>
                                    </Link>
                                    <Link href="/contact-us">
                                        <a className="mb-3">Contact Us</a>
                                    </Link>
                                    <Link href={process.env.NEXT_PUBLIC_LIME_TRACKING}>
                                        <a className="mb-3">Order Tracking</a>
                                    </Link>
                                    {/* <Link href="/"><a className="mb-3">Portfolio</a></Link> */}
                                </div>
                                <h3 className="font-bold text-sm py-4">
                                    <b>Authorized</b> <span>Distributor:</span>
                                </h3>
                                <div className="flex items-center justify-center xl:justify-start">
                                    <img className={`${styles["img-alliance"]} w-20`} src="/img/se_authorized.png"/>
                                </div>
                            </div>
                            {/* <div className="flex-1 w-full xl:w-96">
                                <h3 className="font-bold text-sm text-center uppercase">
                                    Payment Methods
                                </h3>
                                <div className="grid grid-cols-3 gap-y-10 justify-center items-center mt-8 mx-8 xl:mx-0 ">
                                    <div className="relative w-16 md:24 lg:w-24 h-6 md:8 lg:h-9 m-auto">
                                        <Image
                                            src="/img/payment-icon/bca.png"
                                            alt="BCA"
                                            layout="fill"
                                        />
                                    </div>
                                    <div className="relative w-16 md:24 lg:w-24 h-6 md:8 lg:h-9 m-auto">
                                        <Image
                                            src="/img/payment-icon/bni.png"
                                            alt="BNI"
                                            layout="fill"
                                        />
                                    </div>
                                    <div className="relative w-16 md:24 lg:w-24 h-6 md:8 lg:h-9 m-auto">
                                        <Image
                                            src="/img/payment-icon/bri.png"
                                            alt="BRI"
                                            layout="fill"
                                        />
                                    </div>
                                    <div className="relative w-16 md:24 lg:w-24 h-6 md:8 lg:h-9 m-auto">
                                        <Image
                                            src="/img/payment-icon/mandiri.png"
                                            alt="Mandiri"
                                            layout="fill"
                                        />
                                    </div>
                                    <div className="relative w-16 md:24 lg:w-24 h-6 md:8 lg:h-9 m-auto">
                                        <Image
                                            src="/img/payment-icon/permata.png"
                                            alt="Permata"
                                            layout="fill"
                                        />
                                    </div>
                                    <div className="relative w-16 md:24 lg:w-24 h-6 md:8 lg:h-9 m-auto">
                                        <Image
                                            src="/img/payment-icon/kredivo.png"
                                            alt="Kredivo"
                                            layout="fill"
                                        />
                                    </div>
                                    <div className="relative w-12 md:w-14 lg:w-20 h-8 md:h-10 lg:h-12 m-auto">
                                        <Image
                                            src="/img/payment-icon/jcb.png"
                                            alt="JCB"
                                            layout="fill"
                                        />
                                    </div>
                                    <div className="relative w-14 md:w-20 lg:w-24 h-8 md:h-10 lg:h-14 m-auto">
                                        <Image
                                            src="/img/payment-icon/mastercard.png"
                                            alt="Mastercard"
                                            layout="fill"
                                        />
                                    </div>
                                    <div className="relative w-14 md:w-20 lg:w-24 h-8 md:h-10 lg:h-14 m-auto">
                                        <Image
                                            src="/img/payment-icon/visa.png"
                                            alt="Visa"
                                            layout="fill"
                                        />
                                    </div>
                                </div>
                            </div> */}
                            <div className="flex-1 w-full md:w-1/2 xl:w-96">
                                <h3 className="font-bold text-sm text-center uppercase">
                                    Payment Methods
                                </h3>
                                <div className="flex w-full justify-center items-center mt-2 m-auto">
                                    <div className="relative w-72 h-40 m-auto">
                                        <Image
                                            src="/img/payment-icon/payment-methods.png"
                                            alt="Visa"
                                            layout="fill"
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="flex flex-0 w-80 items-center justify-center">
                                <Subscribe></Subscribe>
                            </div>
                        </div>
                    </div>

                    <div className="text-center xl:text-left">
                        <h3 className="font-bold text-sm mb-4 mt-10">
                            JOIN US :
                        </h3>
                        <Link
                            href="https://www.facebook.com/distritama.perkasa.50"
                            passHref={true}
                        >
                            <a target="_blank">
                                <Image
                                    src="/fb-logo.png"
                                    alt="Logo Trimitra FB"
                                    width={32}
                                    height={32}
                                />
                            </a>
                        </Link>
                    </div>
                </div>
            </div>
        </footer>
    );
}

export default Footer;

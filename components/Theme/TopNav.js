import React from 'react'
import responsiveStyle from '../../styles/Components/HeaderResponsive.module.css'
import Link from 'next/link'

function TopNav() {
    return (
        <div className="bg-black">
            <div className="mx-5 xl:mx-auto text-secondary-dark-grey py-1 max-width-1136">
                <span className="text-base mr-8"><Link href="/account">My Account</Link></span>
                <span className={`text-base mr-8 ${responsiveStyle["no-mr"]}`}><Link href="/wishlist">Wishlist</Link></span>
                {/* <span className={`text-base mr-8 ${responsiveStyle["no-mr"]}`}><Link href="/tracking">Track Order</Link></span> */}
            </div>
        </div>
        
    )
}

export default TopNav;
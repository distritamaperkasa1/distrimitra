import React, { useEffect, useState } from "react";
import { ChevronLeft, ChevronRight } from "react-feather";
// import { ChevronLeftIcon, ChevronRightIcon } from "@heroicons/react/solid";

const Pagination = (props) => {
   const { total, current, show } = props;

   const [state, setState] = useState({
      totalPage: 0,
   });

   useEffect(() => {
      let page = [];
      let totalPage = total / show;
      for (let i = 0; i < totalPage; i++) {
         page.push(i + 1);
      }

      setState({ ...state, totalPage: page });
   }, [total]);

   return (
      <div className='bg-white px-4 py-3 flex items-center justify-between border-t border-gray-200 sm:px-6'>
         <div className='flex-1 flex justify-between sm:hidden'>
            <a
               href='#'
               className='relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50'
            >
               Previous
            </a>
            <a
               href='#'
               className='ml-3 relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50'
            >
               Next
            </a>
         </div>
         <div className='hidden sm:flex-1 sm:flex sm:items-center sm:justify-between'>
            <div>
               <p className='text-sm text-gray-700'>
                  Showing <span className='font-medium'>1</span> to{" "}
                  <span className='font-medium'>{show}</span> of{" "}
                  <span className='font-medium'>97</span> results
               </p>
            </div>
            <div>
               <nav
                  className='relative z-0 inline-flex rounded-md shadow-sm -space-x-px'
                  aria-label='Pagination'
               >
                  <a
                     href='#'
                     className='relative inline-flex items-center px-2 py-2 rounded-l-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50'
                  >
                     <span className='sr-only'>Previous</span>
                     <ChevronLeft className='h-5 w-5' aria-hidden='true' />
                  </a>
                  {/* Current: "z-10 bg-indigo-50 border-indigo-500 text-indigo-600", Default: "bg-white border-gray-300 text-gray-500 hover:bg-gray-50" */}
                  {state.totalPage?.length > 0 &&
                     state.totalPage.map((item, key) => {
                        return (
                           <button
                              key={key}
                              className={`z-10 relative inline-flex items-center px-4 py-2 border text-sm font-medium ${
                                 current == item
                                    ? "bg-primary border-primary text-white"
                                    : "border-gray-300 bg-white hover:bg-gray-100"
                              }`}
                           >
                              {item}
                           </button>
                        );
                     })}
                  <a
                     href='#'
                     className='relative inline-flex items-center px-2 py-2 rounded-r-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50'
                  >
                     <span className='sr-only'>Next</span>
                     <ChevronRight className='h-5 w-5' aria-hidden='true' />
                  </a>
               </nav>
            </div>
         </div>
      </div>
   );
};

export default Pagination;

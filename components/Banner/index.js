/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable @next/next/no-img-element */

import React from "react";
import Image from "next/image";
import styles from "../../styles/Components/Banner.module.css";

const Banner = (props) => {
   const { data } = props;

   return (
      <div className="relative lg:mx-3">
         <Image
            src={data.image_url}
            className='rounded'
            width={548}
            height={208}
            objectFit='cover'
         />

         <div className='absolute top-0 right-0 w-full h-full lg:mx-10 lg:p-8'>
            <div className='flex flex-col h-full mx-6 lg:mx-10 lg:my-1 justify-center'>
               {/* <span className='font-bold text-lg lg:text-xl text-white mb-1 lg:mb-6'>
                  {data.name}
                  <span className='block'>{data.title}</span>
               </span>

               <button className='relative bg-secondary text-white text-sm max-w-max font-bold rounded px-5 py-2 lg:px-14 lg:py-3'>
                  Shop now
               </button> */}
            </div>
         </div>
      </div>
   );
};

export default Banner;
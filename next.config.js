const path = require("path");
const withPlugins = require("next-compose-plugins");
const reactSvg = require("next-react-svg");

const nextConfig = {
   reactStrictMode: true,
   swcMinify: true,
   staticPageGenerationTimeout: 6000,
   images: {
      domains: ["trimitra.limecommerce.work", "automatara.limecommerce.work", "distritama.limecommerce.work", "distritamastaging.limecommerce.work", "backend.mall-listrik.com"],
   },
   async rewrites() {
      return [
        {
          source: '/low-voltage-products-systems/mcb/acti-9-ng125.html',
          destination: '/category/408'
        },
      ]
   },
};

module.exports = withPlugins([
   [
      reactSvg,
      {
         include: path.resolve(__dirname, "assets/illustrations"),
         webpack(config, options) {
            return config;
         },
      },
   ],
   nextConfig,
]);

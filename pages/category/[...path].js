/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState, useContext, useCallback } from "react";
import Layout from "../../components/Theme/Layout";
import ReactStars from "react-rating-stars-component";
import { Breadcrumb, Accordion, Checkbox, Product, ProductList } from "../../components";
import Pagination from "next-pagination";
import themePagination from "../../styles/Theme/Pagination.module.css";
import PaginationCustom from "../../components/Pagination/index"
import { requestGet } from "../../lib/axios";
import { IconGrid, IconList } from "../../assets/illustrations";
import { moneyFormat, objectString } from "../../lib/helper";
import { useRouter } from "next/router";
import { MenuContext } from '../../contexts/MenuContext';
import responsiveStyles from '../../styles/Components/CategoryResponsive.module.css'
import { route } from "next/dist/server/router";

const Category = (props) => {
   const { setMenu } = useContext(MenuContext);
   const { category, product, menu, categoryId, reqParams, pathURL, breadcrumbMenu } = props;
   const [filterState, setFilterState] = useState([]);
   const [filterParamsState, setFilterParamsState] = useState({});
   const [categoryState, setCategoryState] = useState();
   const [paginationRangeState, setPaginationRangeState] = useState('');
   const [sortByState, setSortByState] = useState("position");
   const [limitState, setLimitState] = useState(12);
   const [productState, setProductState] = useState();
   const [paginationArrayState, setPaginationArrayState] = useState();
   const [paginationState, setPaginationState] = useState();
   const [pageState, setPageState] = useState(1);
   const [filterStateVisible, setFilterStateVisible] = useState(true);
   const [categoryVisibleState, setCategoryVisibleState] = useState(true);
   const [productVisibleState, setProductVisibleState] = useState(true);
   const router = useRouter();
   const [state, setState] = useState({
      view: "grid",
      pagination:null,
      filter: {
         category: [],
         price: [],
         rating: [],
         other: [],
      },
   });

   const breadcrumb = breadcrumbMenu;

   let paginationRange = product.total_rows / limitState;

   const paginationArray = [];

   const useMediaQuery = (width) => {
    const [targetReached, setTargetReached] = useState(false);
  
    const updateTarget = useCallback((e) => {
      if (e.matches) {
        setTargetReached(true);
      } else {
        setTargetReached(false);
      }
    }, []);
  
    useEffect(() => {
      const media = window.matchMedia(`(max-width: ${width}px)`);
      media.addListener(updateTarget);
  
      // Check on mount (callback is not called until a change occurs)
      if (media.matches) {
        setTargetReached(true);
      }
  
      return () => media.removeListener(updateTarget);
    }, []);
    return targetReached;
  };

  const isBreakpoint = useMediaQuery(768);
  
  const [showMe, setShowMe] = useState(false);
  
  useEffect(() => {
      let arrCategory = [];
      let arrPrice = [];
      let arrRating = [];
      let arrOther = [];
      let breadcrumbItem = [];
      
      console.log(categoryId+' AAA')
      for(var x = 0; x < category.length; x++){
            category[x].visible = true;
       }
    //   paginationRange = product.total_rows / limitState
      console.log(Math.ceil(paginationRange)+' babse')
      setPaginationState(paginationRange)
      for(var i = 0 ; i < Math.ceil(paginationRange); i++){
          paginationArray.push(i+1);
      }
      setPaginationArrayState(paginationArray);
      console.log(paginationArray+' assse')
      var requestParams = { categoryId: parseInt(categoryId), filter: JSON.stringify(filterParamsState), attributeSort: sortByState, dirSort: 'asc', currentPage: pageState, pageSize: limitState }
      console.log(JSON.stringify(requestParams)+' adsdsda')
      console.log(pathURL+' ABAA')
      console.log(JSON.stringify(reqParams)+' adada')
      console.log(JSON.stringify(breadcrumbMenu))
      console.log(JSON.stringify(category))
      setCategoryState(category);
      setSortByState('position')

      console.log(JSON.stringify(arrCategory))

      setState({
         ...state,
         filter: {
            ...state.filter,
            category: arrCategory,
            price: arrPrice,
            rating: arrRating,
            other: arrOther,
         },
      });
   }, [category]);

    useEffect(() => {
        setMenu(menu);
        setProductState(product)
        setFilterState([])
        setFilterParamsState({})
    }, [setMenu, menu, setProductState, product]);

    useEffect(() => {
        if (sortByState !== 'position') {
            setSortByState('position')
        }
    }, [pathURL])

    function toggle(){
        setShowMe(!showMe);
    }

   const onUpdateCheckbox = (filter, index, item) => {
      let items = [];
    //   if(item !== 'undefined'){
    //       alert(JSON.stringify(item)+' AAA')
    //   }
      state.filter[filter].map((value, key) => {
         value.checked = index == key ? value : false;
         items.push(value);
      });

      setState({
         ...state,
         filter: {
            ...state.filter,
            [filter]: items,
         },
      });
    };

    const changeSortBy = async (sortByValue) => {
        setSortByState(sortByValue)

        var requestSortByParams = 
            { 
                categoryId: categoryId, 
                filter: JSON.stringify(filterParamsState), 
                attributeSort: sortByValue,
                dirSort: 'asc',
                currentPage: pageState 
            }
        // alert(JSON.stringify(requestSortByParams))
        const sortByList = await requestGet(`/rest/V1/products/product-by-filter`, requestSortByParams);
        if(sortByList){
            // alert(JSON.stringify(sortByList))
            // alert(categoryId)
            setProductState(sortByList[0])
            setProductVisibleState(false);
            setTimeout(
                function(){
                    setProductVisibleState(true)
                }, 100
            )
        }
    }

    const changeLimitPage = async (limitValue) => {
        // alert(pageState)
        var requestLimitPage = 
            { 
                categoryId: categoryId, 
                filter: JSON.stringify(filterParamsState), 
                attributeSort: sortByState,
                dirSort: 'asc',
                pageSize: limitValue,
                currentPage: pageState
            }
        // alert(JSON.stringify(requestSortByParams))
        const updateLimitPage = await requestGet(`/rest/V1/products/product-by-filter`, requestLimitPage);
        if(updateLimitPage){
            paginationRange = productState.total_rows / limitValue
            console.log(paginationRange+' babse')
            setPaginationState(paginationRange)
            var paginationArr = []
            for(var i = 0 ; i < Math.ceil(paginationRange); i++){
                paginationArr.push(i+1);
            }
            setPaginationArrayState(paginationArr);
            // alert(JSON.stringify(sortByList))
            // alert(categoryId)
            // if(pageState!==1 && pageState >= 3 && ((paginationArrayState[paginationArrayState.length-1])-pageState) >=2 ){
            //     const paginationArrayPointer = [];
            //     for(var i = 0 ; i < Math.ceil(paginationRange); i++){
            //         paginationArrayPointer.push((pageState)+(i-2));
            //     }
            //     setPaginationArrayState(paginationArrayPointer);
            // } else if(pageState === 2 && ((paginationArrayState[paginationArrayState.length-1])-pageState) < 2 ) {
            //     const paginationArrayPointer = [];
            //     for(var i = 0 ; i < Math.ceil(paginationRange); i++){
            //         paginationArrayPointer.push((pageState)+(i-1));
            //     }
            //     setPaginationArrayState(paginationArrayPointer);
            // }
            setLimitState(limitValue)
            setProductState(updateLimitPage[0])
            setProductVisibleState(false);
            setTimeout(
                function(){
                    setProductVisibleState(true)
                    // paginationRange = updateLimitPage[0].total_rows / limitValue;
                    // console.log(Math.ceil(paginationRange)+' babse')
                    // for(var i = 0 ; i < Math.ceil(paginationRange); i++){
                    //     paginationArray.push(i+1);
                    // }
                    // setPaginationArrayState(paginationArray);
                    // console.log(limitValue+' babse limit')
                }, 100
            )
        }
    }

    const controlPagination = (pageValue) => {
        paginationRange = productState.total_rows / limitState;
        setPaginationState(paginationRange)
        console.log(pageValue+' babse')
        if(pageValue!==1 && pageValue >= 3 && (Math.ceil(paginationRange)-pageValue) >=2){
            const paginationArrayPointer = [];
            for(var i = 0 ; i < Math.ceil(paginationRange); i++){
                paginationArrayPointer.push((pageValue)+(i-2));
            }
            setPaginationArrayState(paginationArrayPointer);
        } else if(pageValue === 2 && Math.ceil(paginationRange)!==pageValue){
            const paginationArrayPointer = [];
            for(var i = 0 ; i < Math.ceil(paginationRange); i++){
                paginationArrayPointer.push((pageValue)+(i-1));
            }
            setPaginationArrayState(paginationArrayPointer);
        } else if((Math.ceil(paginationRange)-pageValue) === 1 && pageValue!==1 && pageValue>=5) {
            const paginationArrayPointer = [];
            for(var i = 0 ; i < Math.ceil(paginationRange); i++){
                paginationArrayPointer.push((pageValue)+(i-3));
            }
            setPaginationArrayState(paginationArrayPointer);
        }
    }

    const changeCurrentPage = async (pageValue) => {
        var requestCurrentPage = 
            { 
                categoryId: categoryId, 
                filter: JSON.stringify(filterParamsState), 
                attributeSort: sortByState,
                dirSort: 'asc',
                pageSize: limitState,
                currentPage: pageValue
            }
        // alert(JSON.stringify(requestSortByParams))
        const updateCurrentPage = await requestGet(`/rest/V1/products/product-by-filter`, requestCurrentPage);
        if(updateCurrentPage){
            // alert(JSON.stringify(sortByList))
            // alert(categoryId)
            // alert(paginationArrayPointer);
            controlPagination(pageValue)
            console.log(paginationRange+' babse')
            setPageState(pageValue)
            setProductState(updateCurrentPage[0])
            setProductVisibleState(false);
            setTimeout(
                function(){
                    setProductVisibleState(true)
                }, 100
            )
        }
    }

    const addParamsToCategory = async (attrCode, value, label) => {
        let filterParams = filterState;
        let filterParamsApi = filterParamsState;
        let categoryStateChange = categoryState;
        let filterParamsObj = {};
        filterParamsApi[attrCode] = value;
        filterParamsObj.label = attrCode;
        filterParamsObj.value = label;
        filterParams.push(filterParamsObj)
        for(var j = 0; j < categoryStateChange.length; j++){
            if(categoryStateChange[j].attr_code === attrCode){
                categoryStateChange[j].visible = false
            }
        }
        setCategoryState(categoryStateChange)
        setFilterState(filterParams)
        setFilterParamsState(filterParamsApi)
        setCategoryVisibleState(false);
        setTimeout(
            function(){
                setCategoryVisibleState(true)
            }, 100
        )
        var requestParams = { categoryId: categoryId, filter: JSON.stringify(filterParamsState), attributeSort: sortByState, dirSort: 'asc', pageSize: limitState, currentPage: pageState}
        // filterParams.categoryId = categoryId;
        setShowMe(!showMe)
        const filterProducts = await requestGet('/rest/V1/products/product-by-filter',requestParams)
        if(filterProducts){
            paginationRange = filterProducts[0].total_rows / limitState
            setPaginationState(paginationRange)
            console.log(paginationRange+' babse')
            var paginationArr = []
            for(var i = 0 ; i < Math.ceil(paginationRange); i++){
                paginationArr.push(i+1);
            }
            setPaginationArrayState(paginationArr);
            // controlPagination()
            // alert(JSON.stringify(filterProducts))
            // console.log(JSON.stringify(filterProducts[0].items.reverse()))
            // filterProducts[0].items.reverse()
            // setProductState(filterProducts[0].items.reverse())
            setProductState(filterProducts[0])
            setProductVisibleState(false);
            setTimeout(
                function(){
                    setProductVisibleState(true)
                }, 100
            )
        }   
        // alert(JSON.stringify(requestParams))
        // console.log(JSON.stringify(category))
        // alert(attrCode+' '+value)
    }

    const goToNextPage = async () => {
        setPageState(parseInt(pageState+1))
        var requestParams = { categoryId: parseInt(categoryId), filter: JSON.stringify(filterParamsState), attributeSort: sortByState, dirSort: 'asc', pageSize:limitState, currentPage: parseInt(pageState+1)}
        const [filterProducts] = await Promise.all([requestGet(`/rest/V1/products/product-by-filter`, requestParams)])
        console.log(parseInt(pageState+1)+' babsetate')
        if(filterProducts){
            // alert(JSON.stringify(requestParams))
            setProductState(filterProducts[0])
            // setProductState(filterProducts[0].items.reverse())
            controlPagination(parseInt(pageState+1))
            setProductVisibleState(false);
            setTimeout(
                function(){
                    setProductVisibleState(true)
                }, 100
            )
        }  
        console.log(JSON.stringify(requestParams)+' next')
    }

    const goToPrevPage = async () => {
        setPageState(parseInt(pageState-1))
        var requestParams = { categoryId: parseInt(categoryId), filter: JSON.stringify(filterParamsState), attributeSort: sortByState, dirSort: 'desc', pageSize:limitState, currentPage: parseInt(pageState-1) }
        // const filterProducts = await requestGet(`/rest/V1/products/product-by-filter`, requestParams)
        const [filterProducts] = await Promise.all([requestGet(`/rest/V1/products/product-by-filter`, requestParams)])
        if(filterProducts){
            // alert(JSON.stringify(requestParams))
            setProductState(filterProducts[0])
            controlPagination(parseInt(pageState-1))
            // setProductState(filterProducts[0].items.reverse())
            console.log(JSON.stringify(filterProducts[0])+' resultss')
            setProductVisibleState(false);
            setTimeout(
                function(){
                    setProductVisibleState(true)
                }, 100
            )
        }
        console.log(JSON.stringify(requestParams)+' prev')  
        // alert(JSON.stringify(requestParams))
    }

    const removeFilter = async (attrCode) => {
        let filterSelected = filterState;
        let filterParamsApi = filterParamsState;
        let categoryStateChange = categoryState;
        for(var i = 0; i < filterSelected.length; i++){
            if(filterSelected[i].label === attrCode){
                filterSelected.splice(i, 1)
            }
        }
        for(var i = 0; i < categoryStateChange.length; i++){
            if(categoryStateChange[i].attr_code === attrCode){
                categoryStateChange[i].visible = true
            }
        }
        setFilterState(filterSelected)
        setFilterStateVisible(false)
        setTimeout(
            function(){
                setFilterStateVisible(true)
            }, 100
        )
        setCategoryVisibleState(false)
        setTimeout(
            function(){
                setCategoryVisibleState(true)
            }, 100
        )
        delete filterParamsApi[attrCode];
        // alert(JSON.stringify(filterParamsApi))
        setFilterParamsState(filterParamsApi)
        var requestParams = { categoryId: categoryId, filter: JSON.stringify(filterParamsState), attributeSort: sortByState, dirSort: 'asc' }
        setShowMe(!showMe)
        const filterProducts = await requestGet(`/rest/V1/products/product-by-filter`, requestParams)
        if(filterProducts){
            console.log(JSON.stringify(filterProducts))
            paginationRange = filterProducts[0].total_rows / limitState
            setPaginationState(paginationRange)
            console.log(paginationRange+' babse')
            var paginationArr = []
            for(var i = 0 ; i < Math.ceil(paginationRange); i++){
                paginationArr.push(i+1);
            }
            setPaginationArrayState(paginationArr);
            setProductState(filterProducts[0])
            setProductVisibleState(false);
            setTimeout(
                function(){
                    setProductVisibleState(true)
                }, 100
            )
        }   
        // alert(JSON.stringify(filterState))
    }

   return (
      <Layout title={`${breadcrumbMenu[1].name}`}>
      {/* <Layout title="All category"> */}
         <div className={`mx-auto lg:py-4 max-width-1136 ${responsiveStyles["category-container"]}`}>
            <Breadcrumb responsiveStyles={`${responsiveStyles["product-site-map"]}`} items={breadcrumb} />
            <div className={`flex flex-row flex-1 space-x-8 mt-10 ${responsiveStyles["category-container"]}`}>
                {
                   isBreakpoint
                   ?
                   <button className={`${responsiveStyles["hamburger-menu-button"]}`} onClick={toggle}>
                       {/* <img className={`${responsiveStyles["hamburger-menu"]}`} src="/img/hamburger-menu-icon.png"/> */}
                       {/* <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16"></path> */}
                       <svg className={`h-6 w-6 ${responsiveStyles["hamburger-menu"]}`} xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 6h16M4 12h16M4 18h16"></path></svg>
                   </button>
                    :
                    null
                }
               <div style={{ display: showMe || !isBreakpoint ? "block":"none" }} className={`flex-auto max-w-xs ${responsiveStyles["accordion-category"]}`}>
                  <div>
                      {/* <p>AAAsss</p> */}
                    {
                      filterStateVisible && filterState && filterState.length > 0
                      ?
                    //   <p>{filterState}</p>
                      filterState.map((data, index) => {
                          return(
                            <div className="px-4-mobile relative-mobile" key={index}>
                                <span className="px-3.5 pr-4-mobile text-base text-gray-400">{data.value}</span>
                                {/* <span className="px-1 px-0-mobile text-base text-gray-400">{data.value}</span> */}
                                <a onClick={() => removeFilter(data.label)} className="float-right float-right-filter-menu-mobile">
                                    <span className="font-bold text-base text-gray-400 pr-4 pr-0.5-mobile">x</span>
                                </a>
                            </div>
                          )
                      })
                      :
                      null
                  }
                  </div>
                  {
                      categoryState && categoryState.length > 0 && categoryVisibleState
                      ?
                      categoryState.map((data, index)=> {
                        return(
                            <div key={data.attr_code} className={ data.visible ? null : 'hidden'}>
                                <Accordion title={data.attr_label}>
                                    <div className='flex flex-col'>
                                        <div className='flex-1 flex-row space-y-2'>
                                        {
                                            data.values.map((dataValues, indexValues)=> {
                                                return(
                                                    <a key={dataValues.value} onClick={() => addParamsToCategory(data.attr_code ,dataValues.value, dataValues.display)}>
                                                        <p>
                                                            {dataValues.display}
                                                        </p>
                                                    </a>
                                                )
                                            })
                                        }
                                        {/* {state.filter.category.length > 0 &&
                                            state.filter.category.map((item, key) => {
                                                return (
                                                    <Checkbox
                                                    key={key}
                                                    name='category'
                                                    label={item.name}
                                                    count={item.total}
                                                    item={item}
                                                    onClick={() => alert(item.name)}
                                                    checked={item.checked}
                                                    onChecked={(value, status) =>
                                                        onUpdateCheckbox("category", key)
                                                    }
                                                    />
                                                );
                                            })} */}
                                        </div>
                                    </div>
                                </Accordion>
                            </div>
                        )
                      })
                    :
                    null
                  }
               </div>
               <div className={`flex-1 flex-col ${responsiveStyles["category-grid-container"]}`}>
                  <div className='flex flex-row justify-between'>
                     <div className='inline-flex'>
                        <button
                           onClick={() => setState({ ...state, view: "grid" })}
                           className={`rounded-l border border-grey  text-sm font-medium px-3 py-2 focus:z-10 focus:ring-transparent ${
                              state.view == "grid"
                                 ? "bg-grey hover:bg-grey"
                                 : "bg-white hover:bg-gray-100"
                           }`}
                        >
                           <IconGrid width={18} height={18} />
                        </button>
                        <button
                           onClick={() => setState({ ...state, view: "list" })}
                           className={`border-r border-t border-b rounded-r border-grey bg-white text-sm font-medium px-3 py-2 ${
                              state.view == "list"
                                 ? "bg-grey hover:bg-grey"
                                 : "bg-white hover:bg-gray-100"
                           } focus:z-10 focus:ring-2 focus:ring-transparent`}
                        >
                           <IconList width={18} height={18} />
                        </button>
                     </div>

                     <div className='inline-flex self-end'>
                        <div className='flex-row space-x-3'>
                           <label className={`font-normal text-dark-grey ${responsiveStyles["label-inline-responsive"]}`}>
                              Sort By
                           </label>
                           <select
                              className={`rounded py-1.5 border-grey bg-light-grey ${responsiveStyles["select-inline-responsive"]}`}
                              placeholder=''
                              onChange={(e) => changeSortBy(e.target.value)}
                              defaultValue='position'
                              value={sortByState}
                           >
                                <option value='position'>Position</option>
                                <option value='price'>Price</option>
                                <option value='name'>Product Name</option>
                              
                           </select>
                        </div>
                     </div>
                  </div>
                  <div className='flex-auto mt-10'>
                    {
                        state.view === 'grid'
                        ?
                            <div className={`grid grid-cols-3 gap-x-6 gap-y-0 ${responsiveStyles["category-grid-mobile"]}`}>
                                {productVisibleState && productState && productState.items &&
                                productState.items.length > 0 ?
                                productState.items.filter((data, index) => index < limitState).map((item, key) => {
                                    return <div key={key} className={`${responsiveStyles["product-grid-item-mobile"]}`}><Product data={item} categoryId={categoryId}/></div>;
                                })
                                : productVisibleState && productState &&
                                    productState.length > 0 ?
                                    productState.filter((data, index) => index < limitState).map((item, key) => {
                                        return <div key={key} className={`${responsiveStyles["product-grid-item-mobile"]}`}><Product data={item} categoryId={categoryId}/></div>;
                                    })
                                :
                                <div style={{ minHeight:400 }}></div>
                            }
                            </div>
                        :   state.view !== 'grid' && isBreakpoint
                            ?
                            <div className={`gap-x-6 gap-y-0 ${responsiveStyles["category-grid-mobile"]}`}>
                                {productVisibleState && productState && productState.items &&
                                productState.items.length > 0 ?
                                productState.items.filter((data, index) => index < limitState).map((item, key) => {
                                    return <div className="mb-4" key={key}><ProductList data={item} /></div>;
                                })
                                : productVisibleState && productState &&
                                productState.length > 0 ?
                                productState.filter((data, index) => index < limitState).map((item, key) => {
                                    return <div className="mb-4" key={key}><ProductList data={item} /></div>;
                                })
                                :
                                <div style={{ minHeight:400 }}></div>
                            }
                            </div>
                        :
                        <div className={`grid grid-cols-1 gap-x-6 gap-y-0 ${responsiveStyles["category-grid-mobile"]}`}>
                            {productVisibleState && productState && productState.items &&
                            productState.items.length > 0 ?
                            productState.items.filter((data, index) => index < limitState).map((item, key) => {
                                return <ProductList key={key} data={item} />;
                            })
                            : productVisibleState && productState &&
                            productState.length > 0 ?
                            productState.filter((data, index) => index < limitState).map((item, key) => {
                                return <ProductList key={key} data={item} />;
                            })
                            :
                            <div style={{ minHeight:400 }}></div>
                        }
                        </div>

                    }
                  </div>
                  <div className='mt-10 pagination-container'>
                    {
                        // isBreakpoint
                        // ?
                        // <div className="grid justify-center p-4-mobile">
                        //     <div>
                        //         <button
                        //             onClick={() => goToPrevPage()}
                        //             disabled={pageState <= 1}
                        //         >
                        //             <img style={{ maxWidth:30 }} src="/img/chevron-left.png"/>
                        //         </button>
                        //         <button 
                        //             className="float-right"
                        //             onClick={() => goToNextPage()}
                        //         >
                        //             <img style={{ maxWidth:30 }} src="/img/chevron-right.png"/>
                        //         </button>
                        //     </div>
                        //     <div className="limit-mobile py-4-mobile">
                        //         <form method="GET" action="" className="next-pagination__form pagination-custom-form">
                        //             {/* <input type="hidden" name="page" value="3"/> */}
                        //             <label htmlFor="next-pagination__size" className="next-pagination__label">per page</label>
                        //             <div className="next-pagination__select">
                        //                 <select onChange={(e) => changeLimitPage(e.target.value)} name="size" id="next-pagination__size">
                        //                     <option>12</option>
                        //                     <option>18</option>
                        //                     <option>24</option>
                        //                     <option>36</option>
                        //                     <option>48</option>
                        //                 </select>
                        //                 <span className="next-pagination__select-suffix"><svg className="next-pagination__icon" aria-hidden="true" viewBox="0 0 24 24" width="24" height="24"><path d="M24 24H0V0h24v24z" fill="none" opacity=".87"></path><path fill="currentColor" d="M15.88 9.29L12 13.17 8.12 9.29c-.39-.39-1.02-.39-1.41 0-.39.39-.39 1.02 0 1.41l4.59 4.59c.39.39 1.02.39 1.41 0l4.59-4.59c.39-.39.39-1.02 0-1.41-.39-.38-1.03-.39-1.42 0z"></path></svg></span>
                        //             </div>
                        //             <button className="next-pagination__submit" type="submit">Set page size</button>
                        //         </form>
                        //     </div>
                        // </div>
                        // :
                        // null
                        <div className="flex flex-col md:flex-row items-center gap-2">
                            <div className="flex-0-5">
                                <div className="hidden">
                                    <Pagination
                                        total={product.total_rows}
                                        theme={themePagination}
                                        // sizes={[84, 64, 34, 24, 12]}
                                    />
                                </div>
                                <nav className="next-pagination" aria-label="pagination">
                                <ul className="next-pagination__list">
                                    {
                                        pageState !== 1
                                        ?
                                        <li className="next-pagination__item">
                                            <a onClick={() => goToPrevPage()} className="next-pagination__link" aria-label="Previous page">
                                                <svg className="next-pagination__icon" aria-hidden="true" viewBox="0 0 24 24" width="24" height="24">
                                                    <path d="M0 0h24v24H0V0z" fill="none">
                                                    </path>
                                                    <path fill="currentColor" d="M14.71 6.71c-.39-.39-1.02-.39-1.41 0L8.71 11.3c-.39.39-.39 1.02 0 1.41l4.59 4.59c.39.39 1.02.39 1.41 0 .39-.39.39-1.02 0-1.41L10.83 12l3.88-3.88c.39-.39.38-1.03 0-1.41z">
                                                    </path>
                                                </svg>
                                            </a>
                                        </li>
                                        :
                                        null
                                    }
                                        {
                                            paginationArrayState && paginationArrayState.length > 0
                                            ?
                                            paginationArrayState.filter((data, index) => index < 5).map((item, index) => {
                                                return(
                                                    <li key={index} className="next-pagination__item">
                                                        {
                                                            pageState === item
                                                            ?
                                                            <a onClick={() => changeCurrentPage(item)} className="next-pagination__link next-pagination__link--disabled next-pagination__link--current" aria-label="Page 1">
                                                                {item}
                                                            </a>
                                                            :
                                                            <a onClick={() => changeCurrentPage(item)} className="next-pagination__link" aria-label="Page 1">
                                                                {item}
                                                            </a>
                                                        }
                                                    </li>
                                                )
                                            })
                                            :
                                            null
                                        }
                                        {/* <li class="Pagination_next-pagination__item__A08yS"><a class="Pagination_next-pagination__link__3zpQL" aria-label="Page 2">2</a></li>
                                        <li class="Pagination_next-pagination__item__A08yS"><a class="Pagination_next-pagination__link__3zpQL" aria-label="Page 3">3</a></li>
                                        <li class="Pagination_next-pagination__item__A08yS"><a class="Pagination_next-pagination__link__3zpQL" aria-label="Page 4, current page">4</a></li> */}
                                        {/* Pagination_next-pagination__link--disabled__1E0wg */}
                                        <li className="next-pagination__item">
                                            {
                                                paginationArrayState && pageState !== Math.ceil(paginationState) 
                                                ?
                                                <a onClick={() => goToNextPage()} className="next-pagination__link" aria-label="No next page available">
                                                    <svg className="next-pagination__icon" aria-hidden="true" viewBox="0 0 24 24" width="24" height="24">
                                                        <path d="M0 0h24v24H0V0z" fill="none"></path>
                                                        <path fill="currentColor" d="M9.29 6.71c-.39.39-.39 1.02 0 1.41L13.17 12l-3.88 3.88c-.39.39-.39 1.02 0 1.41.39.39 1.02.39 1.41 0l4.59-4.59c.39-.39.39-1.02 0-1.41L10.7 6.7c-.38-.38-1.02-.38-1.41.01z"></path>
                                                    </svg>
                                                    {/* {
                                                        paginationState
                                                        ?
                                                        <p>{Math.ceil(paginationState)}</p>
                                                        :
                                                        null
                                                    } */}
                                                </a>
                                                : pageState === Math.ceil(paginationState) ?
                                                null
                                                :
                                                null
                                            }
                                        </li>
                                </ul>
                                </nav>
                                {/* <PaginationCustom/> */}
                                <div className="p-4-mobile hidden">
                                    <button
                                        onClick={() => goToPrevPage()}
                                        disabled={pageState <= 1}
                                    >
                                        <img style={{ maxWidth:30 }} src="/img/chevron-left.png"/>
                                    </button>
                                    <button 
                                        className="float-right"
                                        onClick={() => goToNextPage()}
                                    >
                                        <img style={{ maxWidth:30 }} src="/img/chevron-right.png"/>
                                    </button>
                                </div>
                            </div>
                            <div className="flex-0-5">
                                <form method="GET" action="" className="next-pagination__form pagination-custom-form">
                                {/* <input type="hidden" name="page" value="3"/> */}
                                    <label htmlFor="next-pagination__size" className="next-pagination__label">per page</label>
                                    <div className="next-pagination__select__2acpV">
                                        <select onChange={(e) => changeLimitPage(e.target.value)} name="size" id="next-pagination__size">
                                            <option>12</option>
                                            <option>18</option>
                                            <option>24</option>
                                            <option>36</option>
                                            <option>48</option>
                                        </select>
                                        <span className="next-pagination__select-suffix"><svg className="next-pagination__icon" aria-hidden="true" viewBox="0 0 24 24" width="24" height="24"><path d="M24 24H0V0h24v24z" fill="none" opacity=".87"></path><path fill="currentColor" d="M15.88 9.29L12 13.17 8.12 9.29c-.39-.39-1.02-.39-1.41 0-.39.39-.39 1.02 0 1.41l4.59 4.59c.39.39 1.02.39 1.41 0l4.59-4.59c.39-.39.39-1.02 0-1.41-.39-.38-1.03-.39-1.42 0z"></path></svg></span>
                                    </div>
                                    <button className="next-pagination__submit" type="submit">Set page size</button>
                                </form>
                            {/* <p className="float-right">AAA</p> */}
                            </div>
                    </div>
                    }
                  </div>
               </div>
            </div>
         </div>
      </Layout>
   );
};

export async function getStaticPaths() {
   const menus = await getFirst100Categories();
   const BASE_API_URL = process.env.NEXT_PUBLIC_API_ENDPOINT;
   const condition = BASE_API_URL+"\/";
   const regexBaseUrl = new RegExp(condition, "gi");
//    const paths = menus.map((menu) => ({
//       params: { path: menu.url_path.replace(regexBaseUrl, '').split('/') },
//    }))
   const paths = menus.map((menu) => ({ params: { path: menu.url_path.replace(BASE_API_URL, '').split('/') },}))

   return {
      paths,
      fallback: 'blocking'
   }
}

async function getFirst100Categories () {
   const getParentMenus = await requestGet('/rest/V1/products/all-categories');
   const subParentMenu = getParentMenus.flatMap((menu) => (menu.children));

   const childrenMenus = subParentMenu.flatMap((menu) => (menu.children));
   
   const top50Menu = childrenMenus.slice(0, 100)

   return top50Menu;
}

export async function getStaticProps({ query, params }) {
   console.log("query: ", params)
   const { page, size } = query || {};
   const { path } = params || {};
   const pathURL = path.toString().replace(/,/g, '/');

   console.log('pathURL',pathURL)

   const getCategory = await requestGet(`/rest/V1/urlrewrite/get-id?requestPath=${pathURL}`)
   console.log('result :',getCategory)
   const catId = getCategory[0]?.category_id;

   let reqParams = {
    //   pageSize: size || 12,
      currentPage: page || 1,
   };

   if (catId) {
      reqParams.categoryId = parseInt(catId) || "";
   }

   console.log("Req params", reqParams)
   console.log(reqParams)
//    alert(JSON.stringify(reqParams))


   const [category, product, menu, breadcrumbItems] = await Promise.all([
      requestGet("/rest/V1/products/sidebar-categories-filter?categoryId="+catId),
      requestGet(`/rest/V1/products-list`, reqParams).catch((error) => {
        console.error("Error fetching products:", error);
        return null;
      }),
      requestGet("/rest/V1/products/all-categories"),
      requestGet('/rest/V1/products/breadcrumbs?categoryId='+catId)
   ])

   for(var x = 0; x < category.length; x++){
    //    if(category[x].attr_code !== 'price'){
           category[x].visible = true;
    //     } else {
    //        category[x].visible = false;
    //    }
   }

   return {
      props: {
         category: category,
         product: product == null ? [] : product[0],
         menu: menu,
         categoryId: catId,
         reqParams: reqParams,
         pathURL: pathURL,
         breadcrumbMenu: breadcrumbItems 
      },
      revalidate: 60,
   };
}

export default Category;
import React, { useEffect, useContext, useState } from 'react'
import Layout from "../components/Theme/Layout";
import Link from 'next/link'
import styles from '../styles/Pages/Login.module.css';
import { requestGet, requestPost } from "../lib/axios";
import { MenuContext } from '../contexts/MenuContext';
import { useRouter } from 'next/router';
import LoaderBounce from '../components/Loader/Bounce';

const Register = (props) => {
    const router = useRouter();
    const { setMenu } = useContext(MenuContext);
    const { menuProps } = props;
    const [firstName, setFirstName] = useState("");
    const [lastName, setlastName] = useState("");
    const [email, setEmail] = useState("");
    const [phone, setPhone] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPass, setConfirmPass] = useState("");
    const [passConfirmed, setPassConfirmed] = useState(true);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState("");
    const [showPassword, setShowPassword] = useState(false);
    const [showConfirm, setShowConfirm] = useState(false);
    const [formValid, setFormValid] = useState(true);


    useEffect(() => {
        setMenu(menuProps);
    }, [setMenu, menuProps]);

    // useEffect(() => {
    //     if (error != "") {
    //         setTimeout(() => {
    //             setError("")
    //         }, 3000)
    //     }
    // }, [error])

    const handleSubmit = async (e) => {
        e.preventDefault();

        if (firstName == "" || lastName == "" || email == "" || phone == "" || password == "" || confirmPass == "") { 
            setError("Please fill all the fields");
            setFormValid(false);
        } else {
            setFormValid(true);
            
            if (password === confirmPass){
                setPassConfirmed(true);
                let customerData = {
                    email: email,
                    firstname: firstName,
                    lastname: lastName,
                    custom_attributes: [
                        {
                            attribute_code: "phone",
                            value: phone
                        }
                    ]
                }
                let body = {
                    customer: customerData,
                    password: password,
                }

                setLoading(true);

                try {
                    const res = await requestPost('/rest/V1/customers', body, '');
                    setLoading(false);
                    router.replace('/login?referer=register');
                } catch (error) {
                    let errorArgument = error.response.data.message;
                    error.response.data.parameters?.forEach((element, index) => {
                        const regex = new RegExp('%'+(parseInt(index)+1),"g");
                        errorArgument = errorArgument.replace(regex, element)
                    });
                    setError(errorArgument);
                    setLoading(false);
                }
                
            } else {
                setPassConfirmed(false);
            }
        }
    }
    return (
        <Layout title='Register'>
            <div className={`mx-auto mt-10 mb-16 ${styles['login-container']}`}>
                <h1 className="font-bold text-xl text-center">Create Account</h1>

                <form className="mx-5 xl:mx-auto mt-14" onSubmit={handleSubmit}>
                    <p className="text-center font-bold text-sm xl:text-left xl:text-lg">Personal Information</p>
                    <div className="mt-2 xl:mt-3">
                        <label className="block text-sm font-bold">First Name *</label>
                        <input className={`w-full rounded mt-1 border-gray-400 ${!formValid && firstName == "" ? "ring-2 ring-red-400" : "" }`} type="text" placeholder="First Name" 
                            value={firstName} 
                            onChange={(e) => setFirstName(e.target.value)}
                        />
                    </div>
                    <div className="mt-2">
                        <label className="block text-sm font-bold">Last Name *</label>
                        <input className={`w-full rounded mt-1 border-gray-400 ${!formValid && lastName == "" ? "ring-2 ring-red-400" : "" }`} type="text" placeholder="Last Name" 
                            value={lastName} 
                            onChange={(e) => setlastName(e.target.value)}
                        />
                    </div>

                    <div className="mt-12">
                        <label className="block text-sm font-bold">Email *</label>
                        <input className={`w-full rounded mt-1 border-gray-400 ${!formValid && email == "" ? "ring-2 ring-red-400" : "" }`} type="text" placeholder="Email" 
                            value={email} 
                            onChange={(e) => setEmail(e.target.value)} 
                        />
                    </div>

                    <div className="mt-2">
                        <label className="block text-sm font-bold">Phone *</label>
                        <input className={`w-full rounded mt-1 border-gray-400 ${!formValid && phone == "" ? "ring-2 ring-red-400" : "" }`} type="number" placeholder="Phone" 
                            value={phone} 
                            onChange={(e) => setPhone(e.target.value)} 
                        />
                    </div>

                    <div className="mt-2">
                        <label className="block text-sm font-bold">Password *</label>
                        <div className="relative">
                            <input className={`w-full rounded mt-1 border-gray-400 ${!formValid && password == "" ? "ring-2 ring-red-400" : "" }`} placeholder="Password" 
                                type={showPassword ? "text" : "password"}
                                value={password} 
                                onChange={(e) => setPassword(e.target.value)}
                            />
                                <div className="absolute inset-y-0 right-0 pr-3 mt-1 mr-3 flex items-center text-lg text-gray-600">
                                <i
                                    className={`fa fa-eye ${
                                        showPassword ? "hidden" : "block"
                                    }`}
                                    onClick={() => setShowPassword(!showPassword)}
                                    aria-hidden="true"
                                ></i>
                                <i
                                    className={`fa fa-eye-slash ${
                                        !showPassword ? "hidden" : "block"
                                    }`}
                                    onClick={() => setShowPassword(!showPassword)}
                                    aria-hidden="true"
                                ></i>
                            </div>
                        </div>
                    </div>

                    <div className="mt-2">
                        <label className="block text-sm font-bold">Confirm Password *</label>
                        <div className="relative">
                            <input className={`w-full rounded mt-1 border-gray-400 ${!formValid && confirmPass == "" ? "ring-2 ring-red-400" : "" }`} placeholder="Confirm Password" 
                                type={showConfirm ? "text" : "password"}
                                value={confirmPass}
                                onChange={(e) => setConfirmPass(e.target.value)}
                            />
                                <div className="absolute inset-y-0 right-0 pr-3 mt-1 mr-3 flex items-center text-lg text-gray-600">
                                    <i
                                        className={`fa fa-eye ${
                                            showConfirm ? "hidden" : "block"
                                        }`}
                                        onClick={() => setShowConfirm(!showConfirm)}
                                        aria-hidden="true"
                                    ></i>
                                    <i
                                        className={`fa fa-eye-slash ${
                                            !showConfirm ? "hidden" : "block"
                                        }`}
                                        onClick={() => setShowConfirm(!showConfirm)}
                                        aria-hidden="true"
                                    ></i>
                                </div>
                        </div>
                    </div>
                    <div className={`mt-2 text-red-600 font-bold flex flex-row items-center text-sm ${passConfirmed ? 'hidden' : ''}`}>
                        <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 mr-1" viewBox="0 0 20 20" fill="currentColor">
                            <path fillRule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z" clipRule="evenodd" />
                        </svg>
                        Password confirmation and Password must match.
                    </div>

                    <div className="w-full mt-14">
                        <div className="mx-4 lg:mx-6">
                            <button className="bg-secondary text-white text-lg font-bold rounded py-2 w-full disabled:opacity-50 disabled:cursor-auto" type="submit" disabled={loading}>
                                <div className={`flex flex-row items-center justify-center my-3 ${!loading ? 'hidden' : ''}`}>
                                    <LoaderBounce />
                                </div>
                                <span className={`${loading ? 'hidden' : ''}`}>Create Account</span>
                            </button>
                            <div className={`mt-2 text-red-600 font-bold flex flex-row items-center justify-center ${!error ? 'hidden' : ''}`}>
                                <div className="">
                                    <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 mr-1" viewBox="0 0 20 20" fill="currentColor">
                                        <path fillRule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z" clipRule="evenodd" />
                                    </svg>
                                </div>
                                
                                {error}
                            </div>
                        </div>
                        
                        <span className="block text-base text-dark-grey text-center mt-8">
                            {`Already Have an Account? `}
                            <Link href="/login">
                                <a className="text-secondary font-bold">
                                    Sign In
                                </a>
                            </Link>
                            
                        </span>
                    </div>
                    
                </form>
            </div>
        </Layout>
    )
}

export default Register;

export async function getStaticProps() {
    let resp = {};

    try {
        const res = await requestGet("/rest/V1/products/all-categories")
        resp.menuProps = res;
    } catch (error) {
        console.log("error in register page", error)
    }

    return {
        props: resp,
        revalidate: 10
    };
}
import React, { useState } from "react";
import Link from "next/link";
import Layout from "../components/Theme/Layout";
import { requestPut } from "../lib/axios";
import LoaderBounce from "../components/Loader/Bounce";

const ForgotPassword = (props) => {
    const [loading, setLoading] = useState(false);
    const [email, setEmail] = useState("");
    const [error, setError] = useState("");
    const [success, setSuccess] = useState(false);

    const handleSubmit = async (e) => {
        e.preventDefault();
        const param = {
            email: email,
            template: "email_reset",
        };

        setLoading(true);

        try {
            const res = await requestPut(
                "/rest/default/V1/customers/password",
                param
            );
            setLoading(false);
            setSuccess(true);
            setError("");
        } catch (error) {
            console.log("error logging in", error);
            setError(error.message);
            setLoading(false);
        }
    };

    return (
        <Layout title="Forgot Password">
            <div className="container flex justify-center mx-auto min-h-75vh">
                <div className="flex flex-col self-center py-12 h-full">
                    <div className="text-lg lg:text-xl text-center font-extrabold pb-12">
                        Forgot Your Password?
                    </div>
                    <div
                        className={`${
                            !error ? "hidden" : "block"
                        } w-full py-4 mx-auto my-4 rounded rounded-md bg-red-400 font-bold text-white text-center`}
                    >
                        {`We can't find your account, please enter a valid email address.`}
                    </div>
                    <form
                        className={`${
                            success ? "hidden" : "block"
                        } mx-5 xl:mx-auto`}
                        onSubmit={handleSubmit}
                    >
                        <div className="mx-4 p-12 border rounded-lg flex flex-col justify-center items-center gap-4">
                            <label
                                htmlFor="emailinput"
                                className="block w-full text-md md:text-lg"
                            >
                                {`Enter your email and we'll send you a link to
                                reset your password.`}
                            </label>
                            <input
                                type="email"
                                name="email-input"
                                id="email-input"
                                className="mt-4 block w-full px-4 py-2 border rounded"
                                placeholder="Enter your e-mail here"
                                value={email}
                                onChange={(e) => setEmail(e.target.value)}
                            />
                            <button
                                type="submit"
                                className="mt-4 py-2 block w-3/4 font-semibold text-white text-lg bg-secondary rounded-sm"
                            >
                                <div
                                    className={`flex flex-row items-center justify-center my-3 ${
                                        !loading ? "hidden" : ""
                                    }`}
                                >
                                    <LoaderBounce />
                                </div>
                                <span className={`${loading ? "hidden" : ""}`}>
                                    Send Link
                                </span>
                            </button>
                        </div>
                    </form>
                    <div
                        className={`${
                            success ? "block" : "hidden"
                        } flex flex-col gap-8 justify-center mx-6`}
                    >
                        <div className="text-md font-medium">
                            <span>
                                {`We've sent reset link to your email, please check
                            your email.`}
                            </span>
                        </div>
                        <Link href="/login">
                            <a className="w-full bg-secondary mx-auto py-2 text-center text-white text-lg font-semibold">
                                <span>Go to Login</span>
                            </a>
                        </Link>
                    </div>
                </div>
            </div>
        </Layout>
    );
};

export default ForgotPassword;

import React, { useEffect, useContext, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { MenuContext } from "../contexts/MenuContext";
import Layout from "../components/Theme/Layout";
import LoaderBounce from "../components/Loader/Bounce";
import { requestGet, requestPost } from "../lib/axios";
import useUser from "../lib/useUser";
import fetchJson from "../lib/fetchJson";
import { IconCheck, IconFailed } from "../assets/illustrations";
import styles from "../styles/Pages/Login.module.css";

const Login = (props) => {
    const { mutateUser } = useUser({
        redirectTo: "/",
        redirectIfFound: true,
    });

    const { query, replace } = useRouter();

    const { setMenu, menu } = useContext(MenuContext);
    const { menuProps } = props;

    const [ user, setUser ] = useState("");
    const [ password, setPassword ] = useState("");
    const [ remember, setRemember ] = useState(false);
    const [ loading, setLoading ] = useState(false);
    const [ error, setError ] = useState("");
    const [ notif, setNotif ] = useState("");
    const [showPassword, setShowPassword] = useState(false);

    const handleSubmit = async (e) => {
        e.preventDefault();
        // console.log(`User: ${user}, Pass: ${password}`);
        const param = {
            username: user,
            password: password,
            remember: remember
        }

        setLoading(true);

        try {
            mutateUser(
                await fetchJson("/api/login", {
                    method: "POST",
                    headers: { "Content-Type": "application/json" },
                    body: JSON.stringify(param),
                })
            );
            setLoading(false);
        } catch (error) {
            if(error.data.message === 'Invalid login or password.'){
                setError('Invalid email or pasword.');
            } else if(error.data.message === "This account isn't confirmed. Verify and try again.") {
                setNotif({
                    type: "error",
                    message:
                        "This account isn't confirmed. Please check your email for the confirmation link or <a class='underline' href='/account/resend'>click here</a> for a new link.",
                });
            } else {
                setError(error.data.message)
            }
            setLoading(false);
        }
    };

    useEffect(() => {
        if (query.referer === "register") {
            setNotif({
                type: "success",
                message:
                    "You must confirm your account. Please check your email for the confirmation link or <a class='underline' href='/account/resend'>click here</a> for a new link.",
            });

            replace("/login");
        } else if (query.referer === "resend") {
            setNotif({
                type: "success",
                message: "Please check your email for confirmation key.",
            });

            replace("/login");
        } else if (query.referer === "confirmed") {
            setNotif({
                type: "success",
                message:
                    "Account confirmed, you can now sign in using your account",
            });

            replace("/login");
        } else if (query.referer === "active") {
            setNotif({
                type: "success",
                message: "Account already active",
            });

            replace("/login");
        }

        if (menu.length < 1) {
            setMenu(menuProps);
        }
    }, [query, menu]);

    return (
        <Layout title="Login">
            <div className={`mx-auto mt-10 mb-16 ${styles["login-container"]}`}>
                <h1 className="font-bold text-xl text-center">
                    Customer Login
                </h1>

                {/* Notif */}
                <div className={` ${notif.type === "success" ? "border-l-4 p-4 my-4 bg-green-100 border-green-500 text-green-700" : notif === "" ? "" : "border-l-4 p-4 my-4 bg-red-100 border-red-500 text-red-700"}`} role="alert">
                    <div
                        className={` flex items-center justify-center ${
                            notif === ""
                                ? "hidden"
                                : notif.type === "success"
                                ? "text-green-600"
                                : "text-red-600"
                        }`}
                    >
                        <div className="flex-1 flex-grow-0">
                                {notif.type === "success" && <IconCheck />}
                                {notif.type !== "success" && <IconFailed />}
                        </div>

                        <p
                            className="pl-2"
                            dangerouslySetInnerHTML={{ __html: notif.message }}
                            ></p>
                    </div>
                </div>

                <div
                                className={`mt-2 text-red-600 font-bold bg-red-100 py-2 rounded-br border-l-4 border-red-600 ${
                                    !error ? "hidden" : ""
                                }`}
                            >
                                <p className="text-center">{error}</p>
                            </div>

                <form className="mx-5 xl:mx-auto" onSubmit={handleSubmit}>
                    <div className="mt-5">
                        <label className="block text-sm font-bold">Email</label>
                        <input 
                            className="w-full rounded mt-1 border-gray-400" 
                            type="email" 
                            placeholder="Email" 
                            value={user} 
                            onChange={(e) => setUser(e.target.value)} 
                            required
                        />
                    </div>

                    <div className="mt-2">
                        <label className="block text-sm font-bold">
                            Password
                        </label>
                        <div className="relative">
                            <input
                                type={showPassword ? "text" : "password"}
                                placeholder="Password"
                                className="w-full rounded mt-1 border-gray-400"
                                value={password}
                                onChange={(e) => setPassword(e.target.value)}
                            />
                            <div className="absolute inset-y-0 right-0 pr-3 mr-3 mt-1 flex items-center text-lg text-gray-600">
                                <i
                                    className={`fa fa-eye ${
                                        showPassword ? "hidden" : "block"
                                    }`}
                                    onClick={() =>
                                        setShowPassword(!showPassword)
                                    }
                                    aria-hidden="true"
                                ></i>
                                <i
                                    className={`fa fa-eye-slash ${
                                        !showPassword ? "hidden" : "block"
                                    }`}
                                    onClick={() =>
                                        setShowPassword(!showPassword)
                                    }
                                    aria-hidden="true"
                                ></i>
                            </div>
                        </div>
                    </div>

                    <div className="mt-2 flex items-center">
                        <input 
                            type="checkbox" 
                            className="mr-1" 
                            name="remember"
                            checked={remember}
                            onChange={() => setRemember(!remember)}
                        />
                        <label className="text-base">Remember me</label>
                    </div>

                    <Link href="/forgotpassword">
                        <a className="text-base underline">
                            <span className="block mt-2 text-dark-grey">
                                Forgot Password ?
                            </span>
                        </a>
                    </Link>

                    <div className="w-full mt-14">
                        <div className="mx-4 lg:mx-6 text-center">
                            <button
                                className="bg-secondary text-white text-lg font-bold rounded py-2 w-full disabled:opacity-50 disabled:cursor-not-allowed"
                                type="submit"
                                disabled={loading}
                            >
                                <div
                                    className={`flex flex-row items-center justify-center my-3 ${
                                        !loading ? "hidden" : ""
                                    }`}
                                >
                                    <LoaderBounce />
                                </div>
                                <span className={`${loading ? "hidden" : ""}`}>
                                    Sign In
                                </span>
                            </button>
                        </div>

                        <span className="block text-base text-dark-grey text-center mt-8">
                            {`If you don’t have an account, `}
                            <Link href="/register">
                                <a className="text-secondary font-bold">
                                    Register Now
                                </a>
                            </Link>
                        </span>
                    </div>
                </form>
            </div>
        </Layout>
    );
};

export default Login;

export async function getStaticProps() {
    let resp = {};

    try {
        const res = await requestGet("/rest/V1/products/all-categories");
        resp.menuProps = res;
    } catch (error) {
        console.log("error in login page", error);
    }

    return {
        props: resp,
        revalidate: 10,
    };
}

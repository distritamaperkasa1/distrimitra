import React, { useEffect, useState } from "react";
import { requestGet } from "../../lib/axios";
import Layout from "../../components/Theme/Layout";
import Spinner from "../../components/Loader/Spinner";

const CompanyProfile = () => {
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(true);
    const BASE_API_URL = process.env.NEXT_PUBLIC_API_ENDPOINT;

    function createMarkup() {
        return { __html: data.content };
    }

    useEffect(() => {
        const fetchData = async () => {
            const content = await requestGet("/rest/all/V1/cmsPage/6");
            setData(content);
            setLoading(false);
        };
        fetchData();
    }, []);

    return (
        <Layout title="Company Profile">
            <div className="flex flex-col items-center gap-8 max-width-1136 my-12 mx-8 lg:mx-auto px-4 min-h-75vh">
                {loading ? (
                    <Spinner />
                ) : (
                    <>
                        <div>
                            <h1 className="text-2xl font-bold text-secondary uppercase tracking-wide text-center">
                                {data.title}
                            </h1>
                        </div>
                        <div
                            className="w-full"
                            dangerouslySetInnerHTML={createMarkup()}
                        />
                    </>
                )}
            </div>
        </Layout>
    );
};

export default CompanyProfile;

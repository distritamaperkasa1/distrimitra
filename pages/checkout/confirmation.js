import React from 'react'
import { useRouter } from 'next/router'
import Layout from '../../components/Theme/Layout'

const Confirmation = () => {
    const router = useRouter();
    return (
        <Layout title="Order Confirmation">
            <div className="custom-container mx-auto my-10 p-4-mobile justify-between">
                <h1 className="text-3xl text-normal text-dark-grey mb-5 text-2xl-mobile">Thank you for your purchase!</h1>
                <span className="block">We&#39;ll email you an order confirmation with details and tracking info.</span>

                <button className="py-3 px-6 bg-secondary text-base text-white mt-5 rounded" onClick={() => router.push('/')}>Continue Shopping</button>
            </div>
        </Layout>
    )
}

export default Confirmation

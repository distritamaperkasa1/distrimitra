import React, { useState, useEffect, useContext } from 'react'
import Image from 'next/image'
import { useRouter } from 'next/router'
import Layout from '../../components/Theme/Layout'
import LoaderBounce from '../../components/Loader/Bounce'
import { MenuContext } from '../../contexts/MenuContext'
import { requestGet, requestPost, requestPut } from '../../lib/axios'
import useRegion from '../../lib/useRegion'
import useUser from '../../lib/useUser'
import useCart from '../../lib/useCart'
import useCustomer from '../../lib/useCustomer'
import { moneyFormat, formatAddress } from '../../lib/helper'
import styles from '../../styles/Pages/Shipping.module.css'
import responsiveStyles from '../../styles/Components/CheckoutResponsive.module.css'

const Shipping = (props) => {
    const { user } = useUser({ redirectTo: "/login" });
    const { customer, loadingCustomer, mutateCustomer } = useCustomer();
    const router = useRouter();
    const { cart } = useCart(user);
    const { regions, loadingRegions } = useRegion();
    const { menu } = props;
    const { setMenu, setCheckout } = useContext(MenuContext);
    const [ showItems, setShowItems ] = useState(true);
    const initialAddress = {
        firstname: "",
        lastname: "",
        company: "",
        region: "",
        city: "",
        district: "",
        postal: "",
        street: "",
        phone: ""
    }
    const [ address, setAddress ] = useState(initialAddress);
    const [ loading, setLoading ] = useState(false);
    const [ error, setError ] = useState('');
    const [ searchCity, setSearchCity ] = useState("");
    const [ cityList, setCityList ] = useState([]);
    const [ shippingAddress, setShippingAddress ] = useState("");
    const [ addMode, setAddMode ] = useState(false);
    const [errorPhone, setErrorPhone] = useState("");

    useEffect(() => {
        setMenu(menu);
    }, [setMenu, menu]);

    useEffect(() => {
        if (error != '') {
            setTimeout(() => {
                setError('')
            }, 3000);
        }
    }, [error])

    const RegionsList = () => {
        let regionResult = [];
        regions?.region.map((reg, index) => {
            regionResult.push(
                <option 
                    value={`${reg.id}&${reg.code}&${reg.name}`} 
                    // value={reg.id}
                    key={"region-"+reg.id}
                >
                    {reg.name}
                </option>
            )
        })

        return regionResult
    }
    
    const CityList = () => {
        let cityResult = [];
        cityList?.map((city, index) => {
            cityResult.push(
                <li 
                    // value={city.label} 
                    key={"city-"+city.label} 
                    className="hover:bg-gray-100 p-2"
                    onClick={() => handleSelectCity(city)}
                >
                    {city.label}
                </li>
            )
        })
        
        return cityResult
    }

    const handleSelectCity = (city) => {
        const { label, region_id, zip } = city;
        let region = "";
        for (var i = 0; i < regions?.region.length; i++) {
            const reg = regions?.region[i];
            if (reg.id == region_id){
                region = `${reg.id}&${reg.code}&${reg.name}`
                break;
            }
        }

        const newState = {
            ...address,
            city: label,
            region: region,
            postal: zip
        }
        setAddress(newState);

        setCityList([]);
        setSearchCity(label);
    }

    const CartItems = () => {
        let cartItemsResult = [];
        cart?.items.map((cartItem, index) => {
            cartItemsResult.push(<CartItemResponsive data={cartItem} key={"items-"+cartItem.name} />)
        })

        return cartItemsResult
    }

    const CartItemResponsive = ({data}) => {
        const subTotalItem = data.price * data.qty;
        return (
            <div className={`text-base border-t flex py-4 items-center ${responsiveStyles["item-cart-container"]}`}>
                <span>
                    <Image className={`flex-1 ${responsiveStyles["responsive-img-cart"]}`} src={data.image_url} alt={data.name} layout="fixed" width={72} height={72} />
                </span>
                <div className={`ml-1 ${responsiveStyles["item-name-container"]}`}>
                    <p className={`mb-1 ${responsiveStyles["responsive-item-name"]}`}>{data.name}</p>
                    <span className="block mb-1">Qty: {data.qty}</span>
                    <span className="block font-bold mb-1">{moneyFormat(subTotalItem)}</span>
                </div>
            </div>
        )
    }

    const handleAddressChange = (props) => {
        const { name, value } = props;

        if (name == "phone"){
            phoneCheck(value)
        }

        const newState = {
            ...address,
            [name]: value
        }
        setAddress(newState);
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        const { street, city, region, postcode, firstname, lastname, company, telephone } = shippingAddress;
        
        const param = {
            address: {
                street: [
                    street[0]
                ],
                city: city,
                region_id: region.region_id,
                region: region.region,
                country_id: regions.countryId,
                postcode: postcode,
                firstname: firstname,
                lastname: lastname,
                company: company,
                telephone: telephone,
                save_in_address_book: 1
            },
            region: region
        }

        setLoading(true);
        try {
            const getShipping = await requestPost('/rest/default/V1/carts/mine/estimate-shipping-methods', param, user.token);
            const checkoutData = {
                ...param,
                shipping: getShipping
            }
            setCheckout(checkoutData);

            router.push('/checkout/shipping')
        } catch (error) {
            setError(error.message);
        }
        
        setLoading(false);
    }

    const handleSaveAddress = async (e) => {
        e.preventDefault();
        const { street, city, region: regionState, postal, firstname, lastname, company, phone, save } = address;

        if (city === '') {
            setError("Please select a city from the list shown")
        } else {
            setLoading(true);
            
            const splitRegion = regionState.split('&');
            const region =  {
                "region_code": splitRegion[1],
                "region": splitRegion[2],
                "region_id": parseInt(splitRegion[0])
            }

            const addAddress = {
                "customer_id": user.data.id,
                "region": region,
                "region_id": region.region_id,
                "country_id": regions.countryId,
                "street": [
                    street
                ],
                "telephone": 0+phone,
                "postcode": postal,
                "city": city,
                "firstname": firstname,
                "lastname": lastname,
                "company": company,
                "default_shipping": true
            }

            const body = {"customer": {...customer, addresses: customer.addresses.concat([addAddress])}}
            
            if(phone.length >= 8 && phone.length < 13){
                try {
                    const updateResp = await requestPut('/rest/default/V1/customers/me', null, body, user.token);
                    mutateCustomer(updateResp);
                } catch (error) {
                    setError(error.message);
                }
                setShippingAddress("");
                setAddMode(false);
                setAddress(initialAddress);
                setSearchCity("");
                setLoading(false);
            } else {
                // setError('Your phone number must be started with 08xxxxxxx');
                phone.length <= 8 ? 
                setError("Phone number must be at least 8 characters") 
                : setError("Phone number must be less than 13 characters");
                
                setLoading(false);
            }

            setLoading(false);
        }
    }

    useEffect(() => {
        const handleSearchCity = async (city) => {
            let cityList = [];
            if (city.length > 3){
                cityList = await requestGet(`/rest/V1/customer/get-city?search=${city}`);
            }
    
            setCityList(cityList)
        }

        const timeOutId = setTimeout(async () => await handleSearchCity(searchCity), 300);
        return () => clearTimeout(timeOutId);
    }, [searchCity]);

    const CustomerAddress = () => {
        let sorted = [];
        customer?.addresses?.forEach(element => {
            if (element.default_shipping){
                sorted.unshift(element);

                if (shippingAddress == "") {
                    setShippingAddress(element);
                }
            } else {
                sorted.push(element);
            }
        });

        let customerAddress = [];
        sorted.map((address, index) => {
            customerAddress.push(
                <div 
                    className={`w-full xl:w-1/2 p-4 text-base relative ${shippingAddress?.id != address.id ? 'border border-white' : 'border border-secondary' }`}
                    key={'customer-address-' + index}
                >
                    <svg 
                        xmlns="http://www.w3.org/2000/svg" 
                        className={`h-6 w-6 absolute top-0 right-0 text-secondary ${shippingAddress?.id != address.id ? 'hidden' : '' }`} 
                        fill="none" 
                        viewBox="0 0 24 24" 
                        stroke="currentColor"
                    >
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M5 13l4 4L19 7" />
                    </svg>
                    {formatAddress(address)}
                    {shippingAddress?.id != address.id && <button className="text-base bg-gray-100 px-4 py-2 mt-4" onClick={() => setShippingAddress(address)}>Ship Here</button>}
                </div>
            )
        })

        if (customerAddress.length < 1 && !loadingCustomer) {
            return <p>Your address book is empty</p>
        }
        return customerAddress;
    }

    function phoneCheck(value) {
        value.length < 8 ? 
                setErrorPhone("Phone number must be at least 8 characters") 
                : value.length >= 13 ? 
                    setErrorPhone("Phone number must be less than 13 characters") 
                    : setErrorPhone("");
    }

    return (
        <Layout title="Checkout Shipping">
            <div className="custom-container mx-auto xl:flex xl:flex-row-reverse my-10 justify-between">
                <div className={styles.summaryContainer+' '+responsiveStyles.summaryContainer+' flex-1'}>
                    <div className={ styles.summaryContent+' '+responsiveStyles.summaryContent }>
                        <h2 className="text-lg font-semibold text-dark-grey">Order Summary</h2>
                        <button className="mt-5 flex justify-between w-full mb-3" onClick={() => setShowItems(!showItems)}>
                            <span className="text-base text-dark-grey">{cart?.items_qty} Item in Cart</span>
                            {showItems ?
                                <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 ml-10" viewBox="0 0 20 20" fill="currentColor">
                                    <path fillRule="evenodd" d="M14.707 12.707a1 1 0 01-1.414 0L10 9.414l-3.293 3.293a1 1 0 01-1.414-1.414l4-4a1 1 0 011.414 0l4 4a1 1 0 010 1.414z" clipRule="evenodd" />
                                </svg> : 
                                <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 ml-10" viewBox="0 0 20 20" fill="currentColor">
                                    <path fillRule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clipRule="evenodd" />
                                </svg>
                            }
                        </button>
                        { showItems && 
                            <div>
                                <CartItems />
                            </div>
                        }
                    </div>
                </div>
                
                <div className="flex-auto xl:mr-10 text-dark-grey p-5 xl:p-0">
                    <h1 className="text-lg font-semibold pb-6 mb-5 border-b">Shipping Address</h1>
                    {!addMode ? 
                        <>
                            <div className="flex flex-col xl:flex-row flex-wrap">
                                <CustomerAddress />
                            </div>
                            <button className="text-base bg-gray-100 px-4 py-2 mt-4 block" onClick={() => setAddMode(true)}>+ New Address</button>

                            <div className={`mt-5 text-red-600 font-bold flex flex-row items-center ${!error ? 'invisible' : ''}`}>
                                <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 mr-1" viewBox="0 0 20 20" fill="currentColor">
                                    <path fillRule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z" clipRule="evenodd" />
                                </svg>
                                {error}
                            </div>

                            <button 
                                className="text-lg bg-secondary text-white rounded py-2 px-20 mt-5 mb-20 w-full xl:w-auto disabled:opacity-50 disabled:cursor-not-allowed" 
                                disabled={loading}
                                onClick={(e) => handleSubmit(e)}
                            >
                                <div className={`flex flex-row items-center justify-center my-3 ${!loading ? 'hidden' : ''}`}>
                                    <LoaderBounce />
                                </div>
                                {loading ? '' : 'Next'}
                            </button>
                        </>
                    :
                        <form onSubmit={(e) => handleSaveAddress(e)}>
                            <div className="mb-2">
                                <span className="block text-base mb-1">First Name *</span>
                                <input 
                                    type="text" 
                                    className="w-full text-base rounded" 
                                    name="firstname" 
                                    required 
                                    onChange={(e) => handleAddressChange(e.target)} 
                                    value={address.firstname}
                                />
                            </div>

                            <div className="mb-2">
                                <span className="block text-base mb-1">Last Name *</span>
                                <input 
                                    type="text" 
                                    className="w-full text-base rounded" 
                                    name="lastname" 
                                    required 
                                    onChange={(e) => handleAddressChange(e.target)}
                                    value={address.lastname}
                                />
                            </div>

                            <div className="mb-2">
                                <span className="block text-base mb-1">Company</span>
                                <input 
                                    type="text" 
                                    className="w-full text-base rounded" 
                                    name="company" 
                                    onChange={(e) => handleAddressChange(e.target)} 
                                    value={address.company}
                                />
                            </div>

                            <div className="mb-2 relative">
                                <label className="block text-base mb-1">City *</label>
                                <input 
                                    className="w-full text-base rounded"
                                    type="text" 
                                    name="city"
                                    placeholder="Search your city..."
                                    value={searchCity}
                                    onChange={(e) => setSearchCity(e.target.value)}
                                    required
                                    autoComplete="none"
                                />
                                <ul className={`list-reset text-base my-1 rounded shadow w-full bg-white absolute h-64 overflow-auto ${cityList.length > 0 ? '' : 'hidden'}`}>
                                    <CityList />
                                </ul>
                            </div>

                            <div className="mb-2">
                                <span className="block text-base mb-1">State/Province * </span>
                                <select 
                                    className="w-full text-base rounded" 
                                    name="region" 
                                    required
                                    onChange={(e) => handleAddressChange(e.target)}
                                    value={address.region} 
                                >
                                    <option value="">Please select a region, state or province.</option>
                                    <RegionsList />
                                </select>
                            </div>

                            <div className="mb-2">
                                <span className="block text-base mb-1">Zip/Postal Code *</span>
                                <input 
                                    type="text" 
                                    className="w-full text-base rounded" 
                                    name="postal" 
                                    required 
                                    onChange={(e) => handleAddressChange(e.target)} 
                                    value={address.postal} 
                                />
                            </div>

                            <div className="mb-2">
                                <span className="block text-base mb-1">Street Address *</span>
                                <textarea 
                                    className="w-full text-base rounded" 
                                    name="street" 
                                    required 
                                    onChange={(e) => handleAddressChange(e.target)}
                                    value={address.street} 
                                />
                            </div>

                            <div className="mt-2">
                                <label className="block text-sm font-bold">Phone *</label>
                                <small className="block mb-1 text-gray-400">* Please fill the number start with 8xxxxxxx</small>
                                <div className="flex items-center">
                                    
                                    <input className="flex-0 w-16 text-center rounded mt-1 border-gray-400 bg-gray-200 text-base " type="text" placeholder="Phone" disabled
                                        value="+62"
                                    />
                                    <p className="text-2xl font-bold text-center px-2">-</p>
                                    <input 
                                        className="w-full rounded mt-1 border-gray-400 text-base" 
                                        style={{ appearance : "textfield" }} 
                                        type="number" 
                                        placeholder="Phone" 
                                        reuired
                                        name="phone" 
                                        onInput={(e) => { e.target.value = (e.target.value.substring(0,1) === '0' ? e.target.value.slice(1) : e.target.value); }}
                                        onChange={(e) => handleAddressChange(e.target)} 
                                        value={address.phone} 
                                        // onChange={(e) => {setPhone(e.target.value);}}
                                    />
                                </div>
                                    <span className="text-sm text-red-700">{errorPhone}</span>
                            </div>

                            <div className={`mt-2 text-red-600 font-bold flex flex-row items-center ${!error ? 'invisible' : ''}`}>
                                <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 mr-1" viewBox="0 0 20 20" fill="currentColor">
                                    <path fillRule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z" clipRule="evenodd" />
                                </svg>
                                {error}
                            </div>

                            <div className="flex justify-end">
                                <button
                                    className="text-lg bg-gray-100 text-black mr-2 rounded py-2 px-20 mt-10 mb-20 w-full xl:w-auto disabled:opacity-50 disabled:cursor-not-allowed" 
                                    disabled={loading}
                                    onClick={() => setAddMode(false)}
                                >
                                    Cancel
                                </button>
                                <button 
                                    type="submit" 
                                    className="text-lg bg-secondary text-white rounded py-2 px-20 mt-10 mb-20 w-full xl:w-auto disabled:opacity-50 disabled:cursor-not-allowed" 
                                    disabled={loading}
                                >
                                    <div className={`flex flex-row items-center justify-center my-3 ${!loading ? 'hidden' : ''}`}>
                                        <LoaderBounce />
                                    </div>
                                    {loading ? '' : 'Ship Here'}
                                </button>
                            </div>
                        </form>
                    }
                </div>
            </div>
        </Layout>
    )
}

export async function getStaticProps() {
    let resp = {};

    try {
        const [menuResp] = await Promise.allSettled([
            requestGet('/rest/V1/products/all-categories'),
        ]);
    
        const menu = menuResp.status = "fulfilled" && menuResp.value;
        resp.menu = menu;
    } catch (error) {
        console.log("error in register page", error)
    }

    return {
        props: resp,
        revalidate: 10
    };
}


export default Shipping

import React, { useState, useEffect, useContext } from 'react'
import { useRouter } from 'next/router'
import Layout from '../../components/Theme/Layout'
import LoaderBounce from '../../components/Loader/Bounce'
import { MenuContext } from '../../contexts/MenuContext'
import { requestGet, requestPost } from '../../lib/axios'
import useUser from '../../lib/useUser'
import { isEmptyObj, moneyFormat } from '../../lib/helper'

const ShippingMethod = (props) => {
    const { user } = useUser({ redirectTo: "/login" });
    const router = useRouter();
    const { menu } = props;
    const { setMenu, 
        checkout,
        setCheckout
    } = useContext(MenuContext);
    const [shipment, setShipment] = useState("");
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState("")

    useEffect(() => {
        setMenu(menu);
    }, [setMenu, menu]);

    useEffect(() => {
        if (error !== "") {
            setTimeout(() => {
                setError("");
            }, 4000)
        }
    }, [error])

    const MethodList = () => {
        let methods = [];
        {!isEmptyObj(checkout) && checkout.shipping.map((method, index) => {
            methods.push(
                <div 
                    className="flex flex-row items-center cursor-pointer" 
                    key={method.method_code}
                    onClick={() => handleShipmentChange(`${method.method_code}-${method.carrier_code}`)}
                >
                    <input 
                        type="radio" 
                        name="shipment" 
                        value={`${method.method_code}-${method.carrier_code}`}
                        defaultChecked={shipment === `${method.method_code}-${method.carrier_code}`}
                    />
                    <div className="ml-2 py-3 border-b w-full">
                        <h2> {method.method_title}</h2>
                        <span>{moneyFormat(method.amount)}</span>
                    </div>
                </div>
            )
        })}

        return methods;
    }

    const handleShipmentChange = (shipment) => {
        setShipment(shipment);
        setError("");
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        const { address, region } = checkout;
        const { country_id, region_id, region: regionName, street, company, telephone, postcode, city, firstname, lastname} = address;
        const { data, token } = user;
        const { id: customerId } = data;
        const addressParam = {
            countryId: country_id,
            regionId: region_id,
            regionCode: region.region_code,
            region: regionName,
            customerId: customerId,
            street: street,
            company: company,
            telephone: telephone,
            fax: null,
            postcode: postcode,
            city: city,
            firstname: firstname,
            lastname: lastname,
            middlename: null,
            prefix: null,
            suffix: null,
            vatId: null,
            customAttributes: []
        }

        const shipping = shipment.split('-');
        const param = { 
            addressInformation: {
                shipping_address: addressParam,
                billing_address: addressParam,
                shipping_method_code: shipping[0],
                shipping_carrier_code: shipping[1],
                extension_attributes: {}
            }
        }

        setLoading(true);
        try {
            const getBillingOption = await requestPost('/rest/default/V1/carts/mine/shipping-information', param, token)
            const checkoutData = {
                ...checkout,
                ...param,
                paymentMethod: getBillingOption.payment_methods
            }

            router.push('/checkout/payment')
            setCheckout(checkoutData);
        } catch (error) {
            setError(error.response?.data?.message || error.message);
        }
        setLoading(false);
    }

    return (
        <Layout title="Checkout Shipping">
            <div className="custom-container mx-auto my-10 justify-between">
                <form onSubmit={(e) => handleSubmit(e)}>
                    <div className="mt-10 p-5 xl:p-0">
                        <h1 className="text-lg font-semibold text-dark-grey">Shipping</h1>
                        <div className="xl:flex">
                            <span className="text-base font-bold block text-dark-grey pt-3">Shipping option</span>
                            <div className="xl:ml-10 flex-1 rounded text-base">
                                <MethodList />

                                <div className={`mt-2 text-red-600 font-bold flex flex-row items-center ${!error ? 'hidden' : ''}`}>
                                    <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 mr-1" viewBox="0 0 20 20" fill="currentColor">
                                        <path fillRule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z" clipRule="evenodd" />
                                    </svg>
                                    {error}
                                </div>

                                <button 
                                    type="submit" 
                                    className="text-lg bg-secondary text-white rounded py-2 px-20 mt-5 mb-20 w-full xl:w-auto mx-auto disabled:opacity-50 disabled:cursor-not-allowed" 
                                    disabled={loading}
                                >
                                    <div className={`flex flex-row items-center justify-center my-3 ${!loading ? 'hidden' : ''}`}>
                                        <LoaderBounce />
                                    </div>
                                    {loading ? '' : 'Next'}
                                </button>
                                
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </Layout>
    )
}

export async function getStaticProps() {
    let resp = {};

    try {
        const [menuResp] = await Promise.allSettled([
            requestGet('/rest/V1/products/all-categories'),
        ]);
    
        const menu = menuResp.status = "fulfilled" && menuResp.value;
        resp.menu = menu;
    } catch (error) {
        console.log("error in register page", error)
    }

    return {
        props: resp,
        revalidate: 10
    };
}

export default ShippingMethod

import React, { useState, useEffect, useContext } from 'react'
import { useRouter } from 'next/router'
import Layout from '../../components/Theme/Layout'
import LoaderBounce from '../../components/Loader/Bounce'
import Spinner from '../../components/Loader/Spinner'
import { MenuContext } from '../../contexts/MenuContext'
import { requestGet, requestPost, requestPut, requestDelete } from '../../lib/axios'
import fetchJson from '../../lib/fetchJson'
import useUser from '../../lib/useUser'
import useCart from '../../lib/useCart'
import { isEmptyObj, moneyFormat } from '../../lib/helper'
import styles from '../../styles/Pages/Cart.module.css'

const Payment = (props) => {
    const { user, mutateUser } = useUser({ redirectTo: "/login" });
    const { cart, mutateCart } = useCart(user);
    const router = useRouter();
    const { menu } = props;
    const { 
        menu: menuData,
        setMenu, 
        checkout,
        setCheckout
    } = useContext(MenuContext);
    
    const [payment, setPayment] = useState("");
    const [loading, setLoading] = useState(false);
    const [loadingSubmit, setLoadingSubmit] = useState(false);
    const [error, setError] = useState("");
    const [ storeCredit, setStoreCredit ] = useState({
        balance: 0,
        used: false
    })

    const getStoreCredit = async () => {
        let resp = {
            ...storeCredit
        };

        try {
            const [balanceResp, storeCreditUse] = await Promise.all([
                requestGet("/rest/V1/carts/mine/aw-get-customer-store-credit", null, user.token),
                requestGet("/rest/V1/carts/mine/aw-store-credit?cartId="+cart.items[0].quote_id, null, user.token)
            ]);

            resp.balance = balanceResp.customer_store_credit_balance;
            resp.used = (storeCreditUse === '1');
        } catch (error) {
            console.log("Error get store credit", error.message)
        }
        
        setStoreCredit(resp);
    }

    useEffect(() => {
        getStoreCredit();

        if (menuData.length < 1) {
            setMenu(menu);
        }

        if (error !== "") {
            setTimeout(() => {
                setError("")
            }, 3000)
        }
    }, [menu, error]);

    const MethodList = () => {
        if (loading) return <Spinner />

        let methods = [];
        {!isEmptyObj(checkout) && checkout.paymentMethod.filter((method) => {
                if (method.code == "m2invoice") {
                    return false; // skip
                }

                if (method.code == "cchosted"){
                    return false;
                }

                if (method.code == "ovo"){
                    return false;
                }
        
                return true;
            }).map((method, index) => {
            methods.push(
                <div 
                    className="flex flex-row items-center cursor-pointer" 
                    key={method.code}
                    onClick={() => handleChange(`${method.code}`)}
                >
                    <input 
                        type="radio" 
                        name="payment" 
                        value={method.code}
                        defaultChecked={payment === method.code}
                    />
                    <div className="ml-2 py-3 border-b w-full">
                        {
                            method.title === 'Bank Transfer - BCA'
                            ?
                            <div style={{ display:'inline-flex', verticalAlign:'middle' }}>
                                <span><img style={{ width:70 }} src="/img/Bca.png"/></span>
                                <h2 style={{ padding:'0 10px' }}>{method.title}</h2>
                            </div>
                            :
                            method.title === 'Bank Transfer - BNI'
                            ?
                            <div style={{ display:'inline-flex', verticalAlign:'middle' }}>
                                <span><img style={{ width:70 }} src="/img/bni.png"/></span>
                                <h2 style={{ padding:'0 10px' }}>{method.title}</h2>
                            </div>
                            :
                            method.title === 'Bank Transfer - BRI'
                            ?
                            <div style={{ display:'inline-flex', verticalAlign:'middle' }}>
                                <span><img style={{ width:70 }} src="/img/bri.png"/></span>
                                <h2 style={{ padding:'0 10px' }}>{method.title}</h2>
                            </div>
                            :
                            method.title === 'Bank Transfer - Mandiri'
                            ?
                            <div style={{ display:'inline-flex', verticalAlign:'middle' }}>
                                <span><img style={{ width:70 }} src="/img/mandiri.png"/></span>
                                <h2 style={{ padding:'0 10px' }}>{method.title}</h2>
                            </div>
                            :
                            method.title === 'Bank Transfer - Permata'
                            ?
                            <div style={{ display:'inline-flex', verticalAlign:'middle' }}>
                                <span><img style={{ width:70 }} src="/img/permata.png"/></span>
                                <h2 style={{ padding:'0 10px' }}>{method.title}</h2>
                            </div>
                            :
                            method.title === 'Credit and Debit Cards'
                            ?
                            <div style={{ display:'inline-flex', verticalAlign:'middle' }}>
                                <span><img style={{ width:70 }} src="/img/credit-debit.png"/></span>
                                <h2 style={{ padding:'0 10px' }}>{method.title}</h2>
                            </div>
                            :
                            // method.title === 'OVO'
                            // ?
                            // <div style={{ display:'inline-flex', verticalAlign:'middle' }}>
                            //     <span><img style={{ width:30 }} src="/img/ovo.png"/></span>
                            //     <h2 style={{ padding:'0 10px', lineHeight:'30px' }}>{method.title}</h2>
                            // </div>
                            // :
                           <div>
                                <h2> {method.title}</h2>
                                {
                                    (method.instructions != null && payment == method.code) && (
                                       <div className="w-full flex gap-3 items-center">
                                            <div style={{ display:'inline-flex', verticalAlign:'middle' }}>
                                            <span><img style={{ width:70 }} src="/img/Bca.png"/></span>
                                        </div>
                                            <div
                                                    dangerouslySetInnerHTML={{
                                                    __html: method.instructions?.replaceAll("\n", "<br>"),
                                           
                                                    }}
                                                    className="text-sm text-gray-600 font-semibold text-[16px] text-clavis-gray-modern-700 px-1 py-4 leading-5"
                                                  ></div>
                                       </div>
                                    )
                                }
                           </div>
                        }
                    </div>
                </div>
            )
        })}

        return methods;
    }

    const handleChange = (payment) => {
        setPayment(payment);
        setError("");
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        const { token } = user;
        const { addressInformation } = checkout;

        const param = { 
            "paymentMethod": {
                "method": payment
              },
            "billing_address": addressInformation.billing_address
        }

        setLoadingSubmit(true);
        try {
            if (payment == "checkmo") {
                const paymentResp = await requestPost('/rest/default/V1/carts/mine/payment-information', param, token);
                mutateCart();
                const checkoutData = {};
                setCheckout(checkoutData);
                router.push('/checkout/confirmation');
            } else if (payment === "free") {
                const paymentResp = await requestPut('/rest/V1/carts/mine/order', null, param, token);
                mutateCart();
                const checkoutData = {};
                setCheckout(checkoutData);
                router.push('/checkout/confirmation');
            } else {
                const paymentResp = await requestPut('/rest/V1/carts/mine/order/invoice', null, param, token);
                console.log('paymentResp', paymentResp);
                mutateCart();
                const checkoutData = {};
                setCheckout(checkoutData);
                if(paymentResp[0].invoice_url == null) 
                    router.push('/checkout/confirmation');
                else 
                    window.open(paymentResp[0].invoice_url,"_self");
            }
        } catch (error) {
            setError(error?.response?.data?.message || error.message);
        }
        setLoadingSubmit(false);
    }

    const handleUseCredit = async () => {
        try {
            setLoading(true);
            const resp = await requestPut("/rest/V1/carts/mine/apply-aw-store-credit?cartId="+cart.items[0].quote_id, null, null, user.token);
            mutateCart();
            getStoreCredit();
            await refreshPayment();
            setLoading(false);
        } catch (error) {
            const errorMsg = error?.response?.data?.message || error.message;
            setError(errorMsg);
        }
    }

    const handleCancelCredit = async () => {
        try {
            setLoading(true);
            const resp = await requestDelete("/rest/V1/carts/mine/remove-aw-store-credit?cartId="+cart.items[0].quote_id, null, user.token);
            mutateCart();
            getStoreCredit();
            await refreshPayment();
            setLoading(false);
        } catch (error) {
            const errorMsg = error?.response?.data?.message || error.message;
            setError(errorMsg);
        }
    }

    const refreshPayment = async () => {
        try {
            const { token } = user;
            const param = { 
                addressInformation: checkout.addressInformation
            }
            const getBillingOption = await requestPost('/rest/default/V1/carts/mine/shipping-information', param, token)
            const checkoutData = {
                ...checkout,
                ...param,
                paymentMethod: getBillingOption.payment_methods
            }

            setCheckout(checkoutData)
        } catch (error) {
            const errorMsg = error?.response?.data?.message || error.message;
            setError(errorMsg);
        }
    }

    return (
        <Layout title="Checkout Payment">
            <div className="custom-container mx-auto my-10 justify-between">
                <form onSubmit={(e) => handleSubmit(e)}>
                    <div className="mt-10 p-5 xl:p-0">
                        <h1 className="text-lg font-semibold mb-5 text-dark-grey">Payment option</h1>
                        <div className="flex-1 rounded text-base">
                            <div className='flex flex-col xl:flex-row'>
                                <div className='flex-1 xl:mr-7'>
                                    <MethodList />
                                </div>
                                
                                <div className={`flex-1 w-full xl:w-max mt-2 xl:mt-0 ${styles.summaryContainer}`}>
                                    <h2 className="text-base xl:text-lg text-gray-500 py-2 border-b">Summary</h2>
                                    <div className="flex justify-between pt-3">
                                        <h3 className="text-sm xl:text-base">Subtotal</h3>
                                        <span className="text-sm xl:text-base">{moneyFormat(cart?.subtotal || 0)}</span>
                                    </div>
                                    <div className="flex justify-between">
                                        <h3 className="text-sm xl:text-base">Discount</h3>
                                        <span className="text-sm xl:text-base">{moneyFormat(cart?.discount_amount || 0)}</span>
                                    </div>
                                    <div className="flex justify-between">
                                        <h3 className="text-sm xl:text-base">Shipping</h3>
                                        <span className="text-sm xl:text-base">{moneyFormat(cart?.shipping_amount || 0)}</span>
                                    </div>
                                    <div className={`flex justify-between pb-3 border-b ${cart?.extension_attributes?.aw_store_credit_amount < 0 ? '' : 'hidden'}`}>
                                        <h3 className="text-sm xl:text-base">Store Credit</h3>
                                        <span className={`text-sm xl:text-base ${cart?.extension_attributes?.aw_store_credit_amount < 0 ? 'text-green-700' : ''}`}>{moneyFormat(cart?.extension_attributes?.aw_store_credit_amount || 0)}</span>
                                    </div>
                                    <div className="flex justify-between my-4">
                                        <h3 className="text-lg xl:text-lg font-bold">Order Total</h3>
                                        <span className="text-lg xl:text-lg font-bold">{moneyFormat(cart?.grand_total || 0)}</span>
                                    </div>
                                </div>
                            </div>

                            <div>
                                <span className="flex items-center text-base text-dark-grey w-max mt-4">
                                    Apply Store Credit
                                </span>
                                <div className={`mt-2 text-base`}>
                                    <span className="text-base text-black block">You have <b>{moneyFormat(storeCredit.balance)}</b> on your Store Credit account</span>
                                    <div className={storeCredit.balance === 0 ? "hidden" : ""}>
                                        {
                                            storeCredit.used ?
                                            <button 
                                                className="text-base py-2 px-4 rounded bg-primary text-white mt-2 disabled:opacity-50 disabled:cursor-auto" 
                                                onClick={handleCancelCredit}
                                                type="button"
                                                disabled={loading}
                                            >
                                                Cancel Store Credit
                                            </button>
                                            :
                                            <button 
                                                className="text-base py-2 px-4 rounded bg-accent text-white mt-2 disabled:opacity-50 disabled:cursor-auto" 
                                                onClick={handleUseCredit}
                                                type="button"
                                                disabled={loading}
                                            >
                                                Use Store Credit
                                            </button>
                                        }
                                    </div>
                                </div>
                            </div>

                            <div className={`mt-3 text-red-600 font-bold flex flex-row items-center ${!error ? 'hidden' : ''}`}>
                                <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 mr-1" viewBox="0 0 20 20" fill="currentColor">
                                    <path fillRule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z" clipRule="evenodd" />
                                </svg>
                                {error}
                            </div>

                            <button 
                                type="submit" 
                                className="text-lg bg-secondary text-white rounded py-2 px-20 mt-5 mb-10 w-full xl:w-auto mx-auto disabled:opacity-50 disabled:cursor-not-allowed" 
                                disabled={loadingSubmit}
                            >
                                <div className={`flex flex-row items-center justify-center my-3 ${!loadingSubmit ? 'hidden' : ''}`}>
                                    <LoaderBounce />
                                </div>
                                {loadingSubmit ? '' : 'Place Order'}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </Layout>
    )
}

export async function getStaticProps() {
    let resp = {};

    try {
        const [menuResp] = await Promise.allSettled([
            requestGet('/rest/V1/products/all-categories'),
        ]);
    
        const menu = menuResp.status = "fulfilled" && menuResp.value;
        resp.menu = menu;
    } catch (error) {
        console.log("error in register page", error)
    }

    return {
        props: resp,
        revalidate: 10
    };
}

export default Payment

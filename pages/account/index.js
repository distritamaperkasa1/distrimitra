import React, { useEffect, useContext, useState } from 'react'
import { MenuContext } from '../../contexts/MenuContext';
import { requestGet } from "../../lib/axios";
import useUser from "../../lib/useUser";
import Layout from "../../components/Theme/Layout";
import AccountDetail from "../../components/Account/Index";
import AccountOrder from "../../components/Account/Order";
import AddressBook from "../../components/Account/Address";
import Wishlist from "../../components/Wishlist";
import StoreCredit from "../../components/StoreCredit/Index"
import responsiveStyles from '../../styles/Components/AccountResponsive.module.css';

const Account = (props) => {
    const { user } = useUser({ redirectTo: "/login" });
    // const { user } = useUser();

    const { setMenu } = useContext(MenuContext);
    const { menuProps } = props;
    const sidebarMenu = [
        {
            title: "My Account",
            content: <AccountDetail />
        },
        {
            title: "My Order",
            content: <AccountOrder />
        },
        {
            title: "Address Book",
            content: <AddressBook />
        },
        {
            title: "Wishlist",
            content: <Wishlist/>
        },
        {
            title: "Store Credit & Refund",
            content: <StoreCredit />
        }
    ]
    const [sidebarIndex, setSidebarIndex] = useState(0);
    useEffect(() => {
        setMenu(menuProps);
    }, [setMenu, menuProps]);

    return (
        <Layout title='Account'>
            <div className="relative">
            <h1 className="hidden xl:block text-center text-lg mt-8 font-bold">{sidebarMenu[sidebarIndex].title}</h1>
            <div className={`mx-4 mt-5 text-secondary-dark-grey mb-10 custom-container xl:mx-auto xl:flex`}>
                {/* <div className="border-b border-secondary-dark-grey pb-4 mb-5 pl-4 xl:hidden">
                    <h1 className="font-bold mb-2">John Doe</h1>
                    <p>johndoe@mail.com</p>
                </div> */}

                <div className={`xl:pl-4 font-bold xl:pl-0 xl:text-sm ${responsiveStyles["scroll-sidebar"]}`}>
                    {sidebarMenu.map((sidebar, index) => {
                        return (
                            <div 
                                className={`mb-4 flex justify-between items-center cursor-pointer border-l-2 ${responsiveStyles["sidebar-item"]} ${sidebarIndex == index ? 'border-primary text-dark-grey' : 'border-white' } pl-3`} 
                                key={'side'+index}
                                onClick={() => setSidebarIndex(index)}
                            >
                                <h2>{sidebar.title}</h2>
                                {/* <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 xl:hidden" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M9 5l7 7-7 7" />
                                </svg> */}
                            </div>
                        )
                    })}
                </div>
                
                <div className="flex-1 xl:ml-8">
                    {sidebarMenu[sidebarIndex].content}
                </div>
            </div>
            </div>
        </Layout>
    )
}

export default Account;

export async function getStaticProps() {
    let resp = {};

    try {
        const res = await requestGet("/rest/V1/products/all-categories")
        resp.menuProps = res;
    } catch (error) {
        console.log("error in account page", error)
    }

    return {
        props: resp,
        revalidate: 10
    };
}
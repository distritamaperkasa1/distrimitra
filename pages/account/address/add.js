import React, { useState, useEffect, useContext } from 'react'
import Image from 'next/image'
import { useRouter } from 'next/router'
import Layout from '../../../components/Theme/Layout'
import LoaderBounce from '../../../components/Loader/Bounce'
import { MenuContext } from '../../../contexts/MenuContext'
import { requestGet, requestPut } from '../../../lib/axios'
import useRegion from '../../../lib/useRegion'
import useUser from '../../../lib/useUser'
import useCustomer from '../../../lib/useCustomer'
import { moneyFormat } from '../../../lib/helper'
import styles from '../../../styles/Pages/Shipping.module.css'

const Shipping = (props) => {
    const { user } = useUser({ redirectTo: "/login" });
    const router = useRouter();
    console.log("User", user)
    const { regions, loadingRegions } = useRegion();
    const { customer, loadingCustomer } = useCustomer();
    const { menu } = props;
    const { setMenu, setCheckout } = useContext(MenuContext);
    const [address, setAddress] = useState({
        firstname: "",
        lastname: "",
        company: "",
        region: "",
        city: "",
        district: "",
        postal: "",
        street: "",
        phone: "",
        defaultBilling: false,
        defaultShipping: false
    });
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState('');
    const [searchCity, setSearchCity] = useState("");
    const [cityList, setCityList] = useState([]);
    const [errorPhone, setErrorPhone] = useState("");

    useEffect(() => {
        setMenu(menu);
    }, [setMenu, menu]);

    const RegionsList = () => {
        console.log("Region", regions)
        let regionResult = [];
        regions?.region.map((reg, index) => {
            regionResult.push(
                <option 
                    value={`${reg.id}&${reg.code}&${reg.name}`} 
                    // value={reg.id}
                    key={"region-"+reg.id}
                >
                    {reg.name}
                </option>
            )
        })

        return regionResult
    }
    
    const CityList = () => {
        let cityResult = [];
        cityList?.map((city, index) => {
            cityResult.push(
                <li 
                    // value={city.label} 
                    key={"city-"+city.label} 
                    className="hover:bg-gray-100 p-2"
                    onClick={() => handleSelectCity(city)}
                >
                    {city.label}
                </li>
            )
        })
        
        return cityResult
    }

    const handleSelectCity = (city) => {
        console.log("selected city", city)
        const { label, region_id, zip } = city;
        let region = "";
        for (var i = 0; i < regions?.region.length; i++) {
            const reg = regions?.region[i];
            if (reg.id == region_id){
                region = `${reg.id}&${reg.code}&${reg.name}`
                break;
            }
        }

        const newState = {
            ...address,
            city: label,
            region: region,
            postal: zip
        }
        setAddress(newState);

        setCityList([]);
        setSearchCity(label);
    }

    const handleAddressChange = (props) => {
        const { name, value } = props;

        let updatedValue = value;
        if (name == "defaultBilling" || name == "defaultShipping") {
            updatedValue = !address[name]
        }

        if (name == "phone"){
            phoneCheck(value)
        }

        const newState = {
            ...address,
            [name]: updatedValue
        }
        setAddress(newState);
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        console.log("Address", address)
        const { street, city, region, postal, firstname, lastname, company, phone, defaultBilling, defaultShipping } = address;
        const splitRegion = region.split('&');
        const newAddress = {
            // "id": defaultAddress.id,
            "customer_id": user.data.id,
            "region": {
                "region_code": splitRegion[1],
                "region": splitRegion[2],
                "region_id": parseInt(splitRegion[0])
            },
            "region_id": parseInt(splitRegion[0]),
            "country_id": regions.countryId,
            "street": [
                street
            ],
            "telephone": 0+phone,
            "postcode": postal,
            "city": city,
            "firstname": firstname,
            "lastname": lastname,
            "company": company,
            "default_billing": defaultBilling,
            "default_shipping": defaultShipping,
        }
        
        console.log("newAddress", newAddress)

        const addressList = [...customer.addresses, newAddress];

        const body = {"customer": {...customer, addresses: addressList}}

        setLoading(true);
        if(phone.length >= 8 && phone.length < 13){
            try {
                const updateResp = await requestPut('/rest/default/V1/customers/me', null, body, user.token);
                router.push("/account")

            } catch (error) {
                setError(error.message);
            }
        } else {
            // setError('Your phone number must be started with 08xxxxxxx');
            phone.length <= 8 ? 
            setError("Phone number must be at least 8 characters") 
            : setError("Phone number must be less than 13 characters");
                
        }
        
        setLoading(false);
    }

    useEffect(() => {
        const handleSearchCity = async (city) => {
            let cityList = [];
            if (city.length > 3){
                cityList = await requestGet(`/rest/V1/customer/get-city?search=${city}`);
            }
    
            setCityList(cityList)
            console.log("search city", city)
        }

        const timeOutId = setTimeout(async () => await handleSearchCity(searchCity), 300);
        return () => clearTimeout(timeOutId);
    }, [searchCity]);

    function phoneCheck(value) {
        value.length < 8 ? 
                setErrorPhone("Phone number must be at least 8 characters") 
                : value.length >= 13 ? 
                    setErrorPhone("Phone number must be less than 13 characters") 
                    : setErrorPhone("");
    }

    return (
        <Layout title="Add New Address">
            <div className="max-width-1136 mx-auto xl:flex xl:flex-row-reverse my-10 justify-between">
                <div className="flex-auto xl:mr-20 text-dark-grey p-5 xl:p-0">
                    <h1 className="text-lg font-semibold pb-6 mb-5 border-b">Add New Address</h1>
                    <form onSubmit={(e) => handleSubmit(e)}>
                        <div className="mb-2">
                            <span className="block text-base mb-1">First Name *</span>
                            <input 
                                type="text" 
                                className="w-full text-base rounded" 
                                name="firstname" 
                                required 
                                onChange={(e) => handleAddressChange(e.target)} 
                                value={address.firstname}
                            />
                        </div>

                        <div className="mb-2">
                            <span className="block text-base mb-1">Last Name *</span>
                            <input 
                                type="text" 
                                className="w-full text-base rounded" 
                                name="lastname" 
                                required 
                                onChange={(e) => handleAddressChange(e.target)}
                                value={address.lastname}
                            />
                        </div>

                        <div className="mb-2">
                            <span className="block text-base mb-1">Company</span>
                            <input 
                                type="text" 
                                className="w-full text-base rounded" 
                                name="company" 
                                onChange={(e) => handleAddressChange(e.target)} 
                                value={address.company}
                            />
                        </div>

                        <div className="mb-2 relative">
                            <label className="block text-base mb-1">City *</label>
                            <input 
                                className="w-full text-base rounded"
                                type="text" 
                                name="city"
                                placeholder="Search your city..."
                                value={searchCity}
                                onChange={(e) => setSearchCity(e.target.value)}
                                required
                                autoComplete="none"
                            />
                            <ul className={`list-reset text-base my-1 rounded shadow w-full bg-white absolute h-64 overflow-auto ${cityList.length > 0 ? '' : 'hidden'}`}>
                                <CityList />
                            </ul>
                        </div>

                        <div className="mb-2">
                            <span className="block text-base mb-1">State/Province * </span>
                            <select 
                                className="w-full text-base rounded" 
                                name="region" 
                                required
                                onChange={(e) => handleAddressChange(e.target)}
                                value={address.region} 
                            >
                                <option value="">Please select a region, state or province.</option>
                                <RegionsList />
                            </select>
                        </div>

                        <div className="mb-2">
                            <span className="block text-base mb-1">Zip/Postal Code *</span>
                            <input 
                                type="text" 
                                className="w-full text-base rounded" 
                                name="postal" 
                                required 
                                onChange={(e) => handleAddressChange(e.target)} 
                                value={address.postal} 
                            />
                        </div>

                        <div className="mb-2">
                            <span className="block text-base mb-1">Street Address *</span>
                            <textarea 
                                className="w-full text-base rounded" 
                                name="street" 
                                required 
                                onChange={(e) => handleAddressChange(e.target)}
                                value={address.street} 
                            />
                        </div>

                        <div className="mt-2">
                            <label className="block text-sm font-bold">Phone *</label>
                            <small className="block mb-1 text-gray-400">* Please fill the number start with 8xxxxxxx</small>
                            <div className="flex items-center">
                                
                                <input className="flex-0 w-16 text-center rounded mt-1 border-gray-400 bg-gray-200 text-base " type="text" placeholder="Phone" disabled
                                    value="+62"
                                />
                                <p className="text-2xl font-bold text-center px-2">-</p>
                                <input 
                                    className="w-full rounded mt-1 border-gray-400 text-base" 
                                    style={{ appearance : "textfield" }} 
                                    type="number" 
                                    placeholder="Phone" 
                                    reuired
                                    name="phone" 
                                    onInput={(e) => { e.target.value = (e.target.value.substring(0,1) === '0' ? e.target.value.slice(1) : e.target.value); }}
                                    onChange={(e) => handleAddressChange(e.target)} 
                                    value={address.phone} 
                                    // onChange={(e) => {setPhone(e.target.value);}}
                                />
                            </div>
                                <span className="text-sm text-red-700">{errorPhone}</span>
                        </div>

                        <div className="mb-2 flex items-center text-base hidden">
                            <input 
                                type="checkbox" 
                                name="defaultBilling"
                                className="mr-1"
                                defaultChecked={address.defaultBilling} 
                                onChange={(e) => handleAddressChange(e.target)}
                            />
                            <label>Default Billing Address</label>
                        </div>

                        <div className="mb-2 flex items-center text-base">
                            <input 
                                type="checkbox"
                                name="defaultShipping"
                                className="mr-1" 
                                defaultChecked={address.defaultShipping} 
                                onChange={(e) => handleAddressChange(e.target)}
                            />
                            <label>Default Shipping Address</label>
                        </div>

                        <button 
                            type="submit" 
                            className="text-lg bg-secondary text-white rounded py-2 px-20 mt-10 mb-20 w-full xl:w-auto disabled:opacity-50 disabled:cursor-not-allowed" 
                            disabled={loading}
                        >
                            <div className={`flex flex-row items-center justify-center my-3 ${!loading ? 'hidden' : ''}`}>
                                <LoaderBounce />
                            </div>
                            {loading ? '' : 'Save'}
                        </button>
                        <div className={`mt-2 text-red-600 font-bold flex flex-row items-center justify-center ${!error ? 'hidden' : ''}`}>
                            <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 mr-1" viewBox="0 0 20 20" fill="currentColor">
                                <path fillRule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z" clipRule="evenodd" />
                            </svg>
                            {error}
                        </div>

                    </form>
                </div>
            </div>
        </Layout>
    )
}

export async function getStaticProps() {
    let resp = {};

    try {
        const [menuResp] = await Promise.allSettled([
            requestGet('/rest/V1/products/all-categories'),
        ]);
    
        const menu = menuResp.status = "fulfilled" && menuResp.value;
        resp.menu = menu;
    } catch (error) {
        console.log("error in register page", error)
    }

    return {
        props: resp,
        revalidate: 10
    };
}


export default Shipping

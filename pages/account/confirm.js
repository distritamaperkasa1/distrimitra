import React, { useContext, useEffect, useState } from 'react'
import { useRouter } from "next/router";
import Spinner from '../../components/Loader/Spinner';
import { requestPut, requestGet } from '../../lib/axios';
import { MenuContext } from '../../contexts/MenuContext'
import Layout from "../../components/Theme/Layout";

const Confirm = () => {
    const { query, push } = useRouter();
    const { setMenu, menu } = useContext(MenuContext);
    const [ confirmed, setConfirmed ] = useState(false);
    const [ error, setError ] = useState("");

    // Confirming account
    const doConfirm = async () => {
        console.log("do confirmm", query);
        try {
            const body = {
                token : query.token,
                email : query.email
            }

            const confirmResp = await requestPut("/rest/V1/customer/confirmation", null, body, null);

            if (confirmResp === "Verification Success") {
                setConfirmed(true);
                push("/login?referer=confirmed");
            }
        } catch (error) {
            const errorMsg = error.response.data.message;

            if (errorMsg === "The account is already active.") {
                push("/login?referer=active");
            }
        }
    }

    useEffect(() => {
        if (menu.length < 1) {
            setMenu(menu);
        }

        if (Object.keys(query).length !== 0) {
            doConfirm();
        }
    }, [menu, query]);

    return (
        <Layout title="Account Confirmation">
            <div className="custom-container mx-auto p-10">
                {confirmed ? 
                    <div className="flex flex-col items-center text-secondary">
                        <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M5 13l4 4L19 7" />
                        </svg>
                        <p className="mt-2">Account confirmed</p>
                    </div>
                :
                    <div className="flex flex-col items-center">
                        <Spinner /> 
                        <p className="mt-4">Confirming your account</p>
                    </div>
                }
            </div>
        </Layout>
    )
}

export async function getStaticProps() {
    let resp = {};
 
    try {
        const [categories] = await Promise.all([
            requestGet("/rest/V1/products/all-categories")
        ]);
    
        resp = {
            menuProps: categories
        }

    } catch (error) {
        console.log("Error in confirm page", error);
    }
 
    return {
       props: resp
    };
}

export default Confirm

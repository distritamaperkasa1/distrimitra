import React, { useContext, useEffect, useState } from 'react'
import { useRouter } from "next/router";
import { requestGet } from '../../lib/axios';
import { MenuContext } from '../../contexts/MenuContext'
import Layout from "../../components/Theme/Layout";

const Resend = () => {
    const router = useRouter();
    const { setMenu, menu } = useContext(MenuContext);
    const [ email, setEmail ] = useState("");
    const [ loading, setLoading ] = useState(false);
    const [ error, setError ] = useState("");

    useEffect(() => {
        if (menu.length < 1) {
            setMenu(menu);
        }
    }, [menu]);

    const handleSubmit = async (e) => {
        e.preventDefault();
        setLoading(true);
        
        try {
            const resendResp = await requestGet(`/rest/V1/customer/resend-confirmation?email=${email}`);
            router.push("/login?referer=resend")
        } catch (error) {
            setError(error.response.data.message)
        }

        setLoading(false);
    }

    return (
        <Layout title='Account'>
            <div className="custom-container mx-auto p-10 xl:px-0">
                <h1 className="text-xl font-bold">Send Email Confirmation Link</h1>
                <form className="mt-2" onSubmit={handleSubmit}>
                    <span className="text-base">Please enter your email below and we will send you the confirmation link.</span>
                    <label className="block text-base font-bold">Email *</label>
                    <input 
                        className="w-full xl:w-1/2 rounded mt-1 border-gray-400"
                        name="email"
                        type="email" 
                        placeholder="Email" 
                        value={email} 
                        onChange={(e) => setEmail(e.target.value)}
                        required
                    />

                    <div className={`mt-2 text-base text-red-600 font-semibold ${!error ? 'hidden' : ''}`}>
                        <p className="text-left">
                            {error}
                        </p>
                    </div>

                    <button 
                        className="block mt-5 py-2 px-4 rounded bg-secondary text-white disabled:opacity-50 disabled:cursor-auto" 
                        type="submit" 
                        disabled={loading}
                    >
                        Send Email
                    </button>
                </form>
            </div>
        </Layout>
    )
}

export async function getStaticProps() {
    let resp = {};
 
    try {
        const [categories] = await Promise.all([
            requestGet("/rest/V1/products/all-categories")
        ]);
    
        resp = {
            menuProps: categories
        }

    } catch (error) {
        console.log("Error in confirm page", error);
    }
 
    return {
       props: resp
    };
}

export default Resend

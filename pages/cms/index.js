import React, { useEffect, useContext, useState, useCallback } from "react";
import { requestGet } from "../../lib/axios";
import Layout from "../../components/Theme/Layout";
import { MenuContext } from "../../contexts/MenuContext";

const Cms = (props) => {
    const { cms, menu } = props;
    const [html, setHtml] = useState();
    const { setMenu } = useContext(MenuContext);
    console.log(JSON.stringify(menu));
    useEffect(() => {
        // setMenu(menu)
        console.log(JSON.stringify(cms))
        setHtml(cms)
     })
    
     useEffect(() => {
        setMenu(menu); 
     }, [setMenu, menu])

    return(
    <Layout title="CMS">
        <div style={{ minHeight:400 }} dangerouslySetInnerHTML={{ __html: cms?.items[0].content }}>
        </div>
    </Layout>  
    )
}

export default Cms;

export async function getStaticProps() {
    let resp = {};
 
    try {
        const [menu, cms] = await Promise.all([
            requestGet("/rest/V1/products/all-categories"),
            requestGet("/rest/V1/cmsPage/search/", 
                {
                    "searchCriteria[sortOrders][0][field]": 'identifier',
                    "searchCriteria[sortOrders][0][direction]": 'asc',
                    "searchCriteria[filter_groups][0][filters][0][field]": 'identifier',
                    "searchCriteria[filter_groups][0][filters][0][value]": 'home',
                    "identifier": 'home',
                }, 
                '1343s4ho6ld05matrhwxmvxbefcmb3an'
            ),
        ]);
        resp = {
            cms: cms,
            menu: menu,
         }
    } catch (error) {
        console.log("error in cms page", error);
    }
    return {
        props: resp,
        revalidate: 10,
    };
 
 }

// export async function getStaticPaths() {
//     const product = await requestGet("/rest/default/V1/products/", {
//        "searchCriteria[currentPage]": 0,
//        "searchCriteria[pageSize]": 300,
//     });
 
//     const paths = product.items.map((item) => ({
//        params: { id: item.sku },
//     }));
 
//     return {
//        paths: paths,
//        fallback: 'blocking', // for new pages, do SSR and show page only when html is generated completely
//     };
//  }

// export async function getStaticProps() {
//     const cms = await requestGet("/rest/V1/cmsPage/search/", {
//         "searchCriteria[sortOrders][0][field]": 'identifier',
//         "searchCriteria[sortOrders][0][direction]": 'asc',
//         "searchCriteria[filter_groups][0][filters][0][field]": 'identifier',
//         "searchCriteria[filter_groups][0][filters][0][value]": 'home',
//         "identifier": 'home',
//     });
//     return {
//        props: {
//           cms: 'cms',
//        },
//     };
//  }
import React, { useState, useEffect } from "react";
import Image from "next/image";
import { useRouter } from "next/router";
import {
    detailDateFormat,
    formatAddress,
    moneyFormat,
} from "../../../lib/helper";
import { requestGet } from "../../../lib/axios";
import Spinner from "../../../components/Loader/Spinner";
import useOrder from "../../../lib/useOrder";
import useUser from "../../../lib/useUser";
import moment from "moment";

const Invoice = () => {
    const router = useRouter();
    const { id } = router.query;
    const { user } = useUser();
    const { order } = useOrder();

    const [loading, setLoading] = useState(true);
    const [orderDetails, setOrderDetails] = useState(null);
    const [invoiceDetails, setInvoiceDetails] = useState(null);

    useEffect(() => {
        if (order) {
            const itemDetails = order.items.find(
                (item) => item.entity_id == id
            );
            setOrderDetails(itemDetails);
            const invoice = async () => {
                const res = await requestGet(
                    `/rest/V1/orders/invoicedata/${id}`,
                    null,
                    user?.token
                );
                setInvoiceDetails(res[0]);
                setTimeout(() => {
                    window.print();
                }, 2000);
                setLoading(false);
            };

            invoice();
        }
    }, [order]);

    return (
        <div className="flex flex-col max-w-screen-xl justify-center p-4 m-auto gap-4 capitalize">
            <div>
                <Image
                    src="/img/mall-listrik.png"
                    alt="Logo Trimitra"
                    width={160}
                    height={50}
                />
            </div>
            {loading ? (
                <Spinner />
            ) : (
                <>
                    <table className="border border-collapse border-black">
                        <thead>
                            <td className="bg-gray-500 text-white p-3 leading-8">
                                <tr>Invoice # {invoiceDetails.increment_id}</tr>
                                <tr>Order # {orderDetails.increment_id}</tr>
                                <tr>
                                    Order date :{" "}
                                    {moment(orderDetails.created_at).format(
                                        "MMM DD, YYYY"
                                    )}
                                </tr>
                            </td>
                        </thead>
                        <thead>
                            <td className="bg-gray-200 p-3 border border-black">
                                <tr className="text-lg font-bold">Ship to:</tr>
                            </td>
                        </thead>
                        <tbody>
                            <td className="p-3 border border-black leading-8">
                                <tr>
                                    {formatAddress(
                                        orderDetails.extension_attributes
                                            .shipping_assignments[0].shipping
                                            .address
                                    )}
                                </tr>
                            </td>
                        </tbody>
                    </table>

                    <table className="border border-collapse border-black">
                        <thead>
                            <td className="bg-gray-200 p-3 border border-black">
                                <tr className="text-lg font-bold">
                                    payment method :
                                </tr>
                            </td>
                            <td className="bg-gray-200 p-3 border border-black">
                                <tr className="text-lg font-bold">
                                    shipping method:
                                </tr>
                            </td>
                        </thead>
                        <tbody className="align-text-top leading-10">
                            <td className="p-3">
                                <tr>
                                    <p>
                                        {
                                            orderDetails.payment
                                                .additional_information[0]
                                        }
                                    </p>
                                </tr>
                            </td>
                            <td className="p-3 w-1/2">
                                <tr>
                                    <p>{orderDetails.shipping_description}</p>
                                </tr>
                                <tr>
                                    <p>
                                        (Total Shipping Charge{" "}
                                        {moneyFormat(
                                            orderDetails.shipping_amount
                                        )}
                                        )
                                    </p>
                                </tr>
                            </td>
                        </tbody>
                    </table>

                    <table className="w-full">
                        <thead className="table-fixed border border-black bg-gray-200 leading-10">
                            <tr>
                                <th className="text-left pl-3">products</th>
                                <th>SKU</th>
                                <th className="text-center">price</th>
                                <th>qty</th>
                                <th className="text-right pr-3">subtotal</th>
                            </tr>
                        </thead>
                        <tbody id="items-section leading-10">
                            {orderDetails.items.map((item, index) => (
                                <tr key={index} className="align-top">
                                    <td className="text-left w-1/3 px-2 text-lg">
                                        {item.name}
                                    </td>
                                    <td className="text-center">{item.sku}</td>
                                    <td className="text-center text-lg font-bold text-black">
                                        {moneyFormat(item.price)}
                                    </td>
                                    <td className="text-center">
                                        {item.qty_ordered}
                                    </td>
                                    <td className="text-right text-lg font-bold text-black px-2">
                                        {moneyFormat(
                                            item.qty_ordered * item.price
                                        )}
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                        <br className="leading-10" />
                        <tbody className="font-bold text-right">
                            <tr>
                                <td colSpan="4">subtotal</td>
                                <td className="text-lg">
                                    {moneyFormat(orderDetails.subtotal)}
                                </td>
                            </tr>
                            <tr>
                                <td colSpan="4">shipping & handling</td>
                                <td className="text-lg">
                                    {moneyFormat(orderDetails.shipping_amount)}
                                </td>
                            </tr>
                            <tr>
                                <td colSpan="4">grand total</td>
                                <td className="text-lg">
                                    {moneyFormat(orderDetails.grand_total)}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </>
            )}
        </div>
    );
};

export default Invoice;

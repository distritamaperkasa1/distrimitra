import React, { useState, useEffect } from "react";
import Image from "next/image";
import { useRouter } from "next/router";
import {
    detailDateFormat,
    formatAddress,
    moneyFormat,
} from "../../../lib/helper";
import Spinner from "../../../components/Loader/Spinner";
import useOrder from "../../../lib/useOrder";
import moment from "moment";

const Invoice = ({ props }) => {
    const router = useRouter();
    const { id } = router.query;

    const { order } = useOrder();
    const [loading, setLoading] = useState(true);
    const [orderDetails, setOrderDetails] = useState(null);

    useEffect(() => {
        if (order) {
            const itemDetails = order.items.find(
                (item) => item.entity_id == id
            );
            setLoading(false);
            setOrderDetails(itemDetails);
            setTimeout(() => {
                window.print();
            }, 2000);
        }
    }, [order]);

    return (
        <div className="flex flex-col max-w-screen-xl justify-center p-4 m-auto gap-8">
            <div>
                <Image
                    src="/img/mall-listrik.png"
                    alt="Logo Trimitra"
                    width={160}
                    height={50}
                />
            </div>
            {loading ? (
                <Spinner />
            ) : (
                <>
                    <div className="flex items-center gap-8">
                        <h1 className="text-3xl font-normal">
                            Order # {orderDetails.increment_id}
                        </h1>
                        <div className="ring-2 ring-gray-300 px-3 py-1 rounded uppercase">
                            {orderDetails.status}
                        </div>
                    </div>
                    <div className="text-lg">
                        {moment(orderDetails.created_at).format("DD MMMM YYYY")}
                    </div>
                    <div className="border border-gray-300 p-8">
                        <table className="w-full">
                            <thead className="table-fixed border-b leading-10 text-sm">
                                <tr>
                                    <th className="text-left w-1/2 px-2 ">
                                        Product Name
                                    </th>
                                    <th className="text-left">SKU</th>
                                    <th className="text-right">Price</th>
                                    <th className="text-right">Qty</th>
                                    <th className="text-right px-2">
                                        Subtotal
                                    </th>
                                </tr>
                            </thead>
                            <tbody id="items-section leading-10">
                                {orderDetails.items.map((item, index) => (
                                    <tr key={index} className="align-top">
                                        <td className="text-left w-3/5 px-2 text-lg">
                                            {item.name}
                                        </td>
                                        <td className="text-left text-sm">
                                            {item.sku}
                                        </td>
                                        <td className="text-right text-lg font-bold text-gray-500">
                                            {moneyFormat(item.price)}
                                        </td>
                                        <td className="text-right text-sm">
                                            <div className="flex-col">
                                                <div className="capitalize">
                                                    qty ordered :{" "}
                                                    {item.qty_ordered}
                                                </div>
                                                {item.qty_shipped && (
                                                    <div className="capitalize">
                                                        qty shipped :
                                                        {item.qty_shipped}
                                                    </div>
                                                )}
                                            </div>
                                        </td>
                                        <td className="text-right text-lg font-bold text-gray-500 px-2">
                                            {moneyFormat(
                                                item.qty_ordered * item.price
                                            )}
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                            <tbody
                                id="total-section"
                                className="bg-gray-200 leading-10 border-t border-b border-gray-300"
                            >
                                <tr>
                                    <td className="text-right" colSpan="4">
                                        Subtotal
                                    </td>
                                    <td className="text-right">
                                        {moneyFormat(orderDetails.subtotal)}
                                    </td>
                                </tr>

                                {orderDetails.shipping_amount != 0 ? (
                                    <tr>
                                        <td className="text-right" colSpan="4">
                                            Shipping & Handling
                                        </td>
                                        <td className="text-right">
                                            {moneyFormat(
                                                orderDetails.shipping_amount
                                            )}
                                        </td>
                                    </tr>
                                ) : null}

                                <tr>
                                    <td className="text-right" colSpan="4">
                                        Tax
                                    </td>
                                    <td className="text-right">
                                        {moneyFormat(orderDetails.tax_amount)}
                                    </td>
                                </tr>

                                <tr>
                                    <td
                                        className="text-right font-bold"
                                        colSpan="4"
                                    >
                                        Grand Total
                                    </td>
                                    <td className="text-right font-bold">
                                        {moneyFormat(orderDetails.grand_total)}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div className="flex flex-col gap-4">
                        <div className="text-xl font-thin py-2 border-b">
                            <h2>Order Information</h2>
                        </div>
                        <div className="grid grid-cols-4 gap-y-4">
                            <div className=" font-bold">Shipping Address</div>
                            <div className=" font-bold">Shipping Method</div>
                            <div className=" font-bold">Billing Address</div>
                            <div className=" font-bold">Payment Methode</div>
                            {formatAddress(
                                orderDetails.extension_attributes
                                    .shipping_assignments[0].shipping.address
                            )}
                            <p>{orderDetails.shipping_description}</p>
                            {formatAddress(orderDetails.billing_address)}
                            <p>
                                {orderDetails.payment.additional_information[0]}
                            </p>
                        </div>
                    </div>
                </>
            )}
        </div>
    );
};

export default Invoice;

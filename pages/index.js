/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable @next/next/no-img-element */

import React, { useEffect, useContext, useState, useCallback } from "react";
import { Carousel, Product } from "../components";
import ReactStars from "react-rating-stars-component";
import Banner from "../components/Banner";
import Layout from "../components/Theme/Layout";
import { requestGet } from "../lib/axios";
import { MenuContext } from "../contexts/MenuContext";
import {
   CarouselProvider,
   Slider,
   Slide,
   ButtonBack,
   ButtonNext,
   DotGroup,
   Image as ImageCarousel
} from "pure-react-carousel";
import styles from "../styles/Pages/Home.module.css";
import "pure-react-carousel/dist/react-carousel.es.css";
import Image from "next/image";
import Link from "next/link";
import router from "next/router";

const Home = (props) => {
   const { setMenu } = useContext(MenuContext);
   const { product, menuProps, banner } = props;

   const checkStatus = (banner) => banner.status === "1";

   const topBanner = banner.top_banners || false;
   const secondBanner = banner.second_banners || false;
   const thirdBanner = banner.third_banners[0] || false;
   const promoProduct = banner.promo_products || [];
//    const promoProduct = promoProductJson || [];
   const newProduct = banner.new_products || [];
   const popularProduct = banner.populer_products || [];
   const recomProduct = banner.recomendation_products || [];
   const useMediaQuery = (width) => {
    const [targetReached, setTargetReached] = useState(false);

    const updateTarget = useCallback((e) => {
      if (e.matches) {
        setTargetReached(true);
      } else {
        setTargetReached(false);
      }
    }, []);
  
    useEffect(() => {
      const media = window.matchMedia(`(max-width: ${width}px)`);
      media.addListener(updateTarget);
    //   console.log(JSON.stringify(promoProduct))
  
      // Check on mount (callback is not called until a change occurs)
      if (media.matches) {
        setTargetReached(true);
      }
  
      return () => media.removeListener(updateTarget);
    }, []);
    return targetReached;
  };
   const isBreakpoint = useMediaQuery(768);

   useEffect(() => {
      setMenu(menuProps);
      console.log(JSON.stringify(banner.review))
   }, [setMenu, menuProps]);

   console.log(banner);

   return (
      <Layout title='Home'>
         <div
            className={`flex items-center relative h-full ${styles["slide-container"]} mx-auto`}
         >
             <CarouselProvider
                naturalSlideWidth={0}
                naturalSlideHeight={0}
                isIntrinsicHeight={true}
                totalSlides={topBanner.filter(checkStatus).length}
                className="w-full h-full"
                isPlaying={true}
                visibleSlides={1}
                infinite={true}
                interval={5000}
                dragEnabled={true}
            >
                <Slider className="h-full">
                    {topBanner.map((banner, index) => {
                        if (banner.status === "1") {
                            return (
                                <Slide index={index} key={"top-banner-"+index} className="h-full">
                                    <Link href={banner.banner_url || "/"} >
                                        <a>
                                            <ImageCarousel 
                                                src={banner.image_url}
                                                hasMasterSpinner={true}
                                                // isBgImage={true}
                                                style={{ objectFit: "fill" }}
                                            />
                                        </a>
                                    </Link>
                                </Slide>
                            )
                        }
                    })}
                </Slider>
                
                <ButtonBack className='cursor-pointer focus:outline-none hidden xl:block absolute left-5 bottom-0 h-full'>
                    <div className="rounded-full bg-gray-100 p-3 opacity-60">
                        <svg
                            xmlns='http://www.w3.org/2000/svg'
                            className='h-6 w-6 cursor-pointer'
                            fill='none'
                            viewBox='0 0 24 24'
                            stroke='#5A5F62'
                        >
                            <path
                                strokeLinecap='round'
                                strokeLinejoin='round'
                                strokeWidth={2}
                                d='M15 19l-7-7 7-7'
                            />
                        </svg>
                    </div>
                </ButtonBack>
                <ButtonNext className='cursor-pointer focus:outline-none hidden xl:block absolute right-5 bottom-0 h-full'>
                    <div className="rounded-full bg-gray-100 p-3 opacity-60">
                        <svg
                            xmlns='http://www.w3.org/2000/svg'
                            className='h-6 w-6 cursor-pointer'
                            fill='none'
                            viewBox='0 0 24 24'
                            stroke='#5A5F62'
                        >
                            <path
                                strokeLinecap='round'
                                strokeLinejoin='round'
                                strokeWidth={2}
                                d='M9 5l7 7-7 7'
                            />
                        </svg>
                    </div>
                </ButtonNext>
                {/* <div className='absolute left-0 top-0 w-full h-full'>
                    <div className='flex max-width-1136 lg:mx-auto w-full h-full lg:mx-5 items-center p-3'>
                        <div className='flex flex-col'>
                            <span className='font-bold text-3xl text-white font-shadow'>
                                Automatara
                            </span>
                            <span className='text-white font-normal text-lg max-w-2xl mt-3 font-shadow'>
                                Automatara.com is a market place for electrical and industrial automation products from SCHNEIDER ELECTRIC. We provide original product with fast delivery and competitive price.
                            </span>
                            
                        </div>
                    </div>
                </div> */}
            </CarouselProvider>
         </div>
         <div className='max-width-1136 mx-auto lg:my-12'>
            {secondBanner && (
               <div className='flex flex-col items-center lg:flex-row m-4'>
                  {secondBanner.map((data, index) => {
                     return data.status === "1" ? (
                        <Banner data={data} key={index} />
                     ) : (
                        ""
                     );
                  })}
               </div>
            )}

            {promoProduct.length > 0 && (
                <div className={styles.carouselContainer}>
                {
                    isBreakpoint
                    ?
                    <CarouselProvider
                        naturalSlideWidth={216}
                        naturalSlideHeight={410}
                        totalSlides={promoProduct.length}
                        isIntrinsicHeight={true}
                        infinite={true}
                        dragStep={true}
                        visibleSlides={2}
                        orientation='horizontal'
                        className='m-4'
                        step={2}
                    >
                        <div className='flex flex-row items-center justify-between'>
                            <div className='font-bold text-xl my-6'>
                                Promo This Week
                            </div>

                            <div className='flex flex-row'>
                                <ButtonBack className='cursor-pointer focus:outline-none'>
                                <svg
                                    xmlns='http://www.w3.org/2000/svg'
                                    className='h-6 w-6 cursor-pointer'
                                    fill='none'
                                    viewBox='0 0 24 24'
                                    stroke='#5A5F62'
                                >
                                    <path
                                        strokeLinecap='round'
                                        strokeLinejoin='round'
                                        strokeWidth={2}
                                        d='M15 19l-7-7 7-7'
                                    />
                                </svg>
                                </ButtonBack>

                                <ButtonNext className='cursor-pointer focus:outline-none'>
                                <svg
                                    xmlns='http://www.w3.org/2000/svg'
                                    className='h-6 w-6 cursor-pointer'
                                    fill='none'
                                    viewBox='0 0 24 24'
                                    stroke='#5A5F62'
                                >
                                    <path
                                        strokeLinecap='round'
                                        strokeLinejoin='round'
                                        strokeWidth={2}
                                        d='M9 5l7 7-7 7'
                                    />
                                </svg>
                                </ButtonNext>
                            </div>
                        </div>

                        <Slider classNameTray='gap-6 md:gap-4 sm-gap-2'>
                            {promoProduct.map((data, index) => {
                                return (
                                <Slide index={index} key={index}>
                                    <Product data={data} categoryId={data.category_id}  />
                                </Slide>
                                );
                            })}
                        </Slider>
                    </CarouselProvider>
                    :
                    <CarouselProvider
                        naturalSlideWidth={216}
                        naturalSlideHeight={410}
                        totalSlides={promoProduct.length}
                        isIntrinsicHeight={true}
                        infinite={true}
                        dragStep={false}
                        visibleSlides={5}
                        orientation='horizontal'
                        className=''
                        step={5}
                    >
                        <div className='flex flex-row items-center justify-between'>
                            <div className='font-bold text-xl my-6'>
                                Promo This Week
                            </div>

                            <div className='flex flex-row'>
                                <ButtonBack className='cursor-pointer focus:outline-none'>
                                <svg
                                    xmlns='http://www.w3.org/2000/svg'
                                    className='h-6 w-6 cursor-pointer'
                                    fill='none'
                                    viewBox='0 0 24 24'
                                    stroke='#5A5F62'
                                >
                                    <path
                                        strokeLinecap='round'
                                        strokeLinejoin='round'
                                        strokeWidth={2}
                                        d='M15 19l-7-7 7-7'
                                    />
                                </svg>
                                </ButtonBack>

                                <ButtonNext className='cursor-pointer focus:outline-none'>
                                <svg
                                    xmlns='http://www.w3.org/2000/svg'
                                    className='h-6 w-6 cursor-pointer'
                                    fill='none'
                                    viewBox='0 0 24 24'
                                    stroke='#5A5F62'
                                >
                                    <path
                                        strokeLinecap='round'
                                        strokeLinejoin='round'
                                        strokeWidth={2}
                                        d='M9 5l7 7-7 7'
                                    />
                                </svg>
                                </ButtonNext>
                            </div>
                        </div>

                        <Slider classNameTray='gap-6 md:gap-4 sm-gap-2'>
                            {promoProduct.map((data, index) => {
                                return (
                                <Slide index={index} key={index}>
                                    <Product data={data} categoryId={data.category_id} />
                                </Slide>
                                );
                            })}
                        </Slider>
                    </CarouselProvider>
                }
                </div>
            )}

            {newProduct.length > 0 && (
                <div className={styles.carouselContainer}>
                {
                    isBreakpoint
                    ?
                    <CarouselProvider
                        naturalSlideWidth={0}
                        naturalSlideHeight={0}
                        totalSlides={newProduct.length}
                        isIntrinsicHeight={true}
                        infinite={true}
                        dragStep={true}
                        visibleSlides={2}
                        orientation='horizontal'
                        className='m-4'
                        step={2}
                    >
                        <div className='flex flex-row items-center justify-between'>
                            <div className='font-bold text-xl my-6'>New Product</div>

                            <div className='flex flex-row'>
                                <ButtonBack className='cursor-pointer focus:outline-none'>
                                <svg
                                    xmlns='http://www.w3.org/2000/svg'
                                    className='h-6 w-6 cursor-pointer'
                                    fill='none'
                                    viewBox='0 0 24 24'
                                    stroke='#5A5F62'
                                >
                                    <path
                                        strokeLinecap='round'
                                        strokeLinejoin='round'
                                        strokeWidth={2}
                                        d='M15 19l-7-7 7-7'
                                    />
                                </svg>
                                </ButtonBack>

                                <ButtonNext className='cursor-pointer focus:outline-none'>
                                <svg
                                    xmlns='http://www.w3.org/2000/svg'
                                    className='h-6 w-6 cursor-pointer'
                                    fill='none'
                                    viewBox='0 0 24 24'
                                    stroke='#5A5F62'
                                >
                                    <path
                                        strokeLinecap='round'
                                        strokeLinejoin='round'
                                        strokeWidth={2}
                                        d='M9 5l7 7-7 7'
                                    />
                                </svg>
                                </ButtonNext>
                            </div>
                        </div>

                        <Slider classNameTray='gap-6 md:gap-4 sm-gap-2'>
                            {newProduct.map((data, index) => {
                                return (
                                <Slide index={index} key={index}>
                                    <Product data={data} categoryId={data.category_id} />
                                </Slide>
                                );
                            })}
                        </Slider>
                    </CarouselProvider>
                    :
                    <CarouselProvider
                        naturalSlideWidth={0}
                        naturalSlideHeight={0}
                        totalSlides={newProduct.length}
                        isIntrinsicHeight={true}
                        infinite={true}
                        dragStep={false}
                        visibleSlides={5}
                        orientation='horizontal'
                        className=''
                        step={5}
                    >
                        <div className='flex flex-row items-center justify-between'>
                            <div className='font-bold text-xl my-6'>New Product</div>

                            <div className='flex flex-row'>
                                <ButtonBack className='cursor-pointer focus:outline-none'>
                                <svg
                                    xmlns='http://www.w3.org/2000/svg'
                                    className='h-6 w-6 cursor-pointer'
                                    fill='none'
                                    viewBox='0 0 24 24'
                                    stroke='#5A5F62'
                                >
                                    <path
                                        strokeLinecap='round'
                                        strokeLinejoin='round'
                                        strokeWidth={2}
                                        d='M15 19l-7-7 7-7'
                                    />
                                </svg>
                                </ButtonBack>

                                <ButtonNext className='cursor-pointer focus:outline-none'>
                                <svg
                                    xmlns='http://www.w3.org/2000/svg'
                                    className='h-6 w-6 cursor-pointer'
                                    fill='none'
                                    viewBox='0 0 24 24'
                                    stroke='#5A5F62'
                                >
                                    <path
                                        strokeLinecap='round'
                                        strokeLinejoin='round'
                                        strokeWidth={2}
                                        d='M9 5l7 7-7 7'
                                    />
                                </svg>
                                </ButtonNext>
                            </div>
                        </div>

                        <Slider classNameTray='gap-6 md:gap-4 sm-gap-2'>
                            {newProduct.map((data, index) => {
                                return (
                                <Slide index={index} key={index}>
                                    <Product data={data} categoryId={data.category_id} />
                                </Slide>
                                );
                            })}
                        </Slider>
                    </CarouselProvider>
                }
               </div>
            )}

            {(thirdBanner && thirdBanner.status == '1') && (           
                <div className='relative mx-4'>
                    <Link href={thirdBanner.banner_url}>
                        <a>
                        <div className='absolute w-full h-full lg:max-w-screen-2xl max-h-56 flex flex-row items-center px-4 lg:px-10 lg:py-8 justify-between z-10'>
                            <div className='flex flex-col justify-center h-full'>
                                <span className='font-bold text-lg xl:text-2xl text-white'>
                                    {thirdBanner.name} <br />
                                    {thirdBanner.title}
                                </span>

                                {/* <a href={thirdBanner.banner_url} className='bg-secondary text-sm text-white xl:text-md py-2 xl:py-3 max-w-max font-bold rounded px-6 xl:px-14 mt-4 xl:mt-4'>
                                    Shop now
                                </a> */}
                            </div>
                        </div>
                        <div className={`w-full ${styles.singleBanner}`}>
                            <Image
                                src={thirdBanner.image_url}
                                className='rounded w-full h-full max-h-56 object-cover'
                                layout='fill'
                            />
                        </div>
                        </a>
                    </Link>
                </div>
            )}

            {popularProduct.length > 0 && (
                <div className={styles.carouselContainer}>
                {
                    isBreakpoint
                    ?
                    <CarouselProvider
                        naturalSlideWidth={0}
                        naturalSlideHeight={0}
                        totalSlides={popularProduct.length}
                        isIntrinsicHeight={true}
                        infinite={true}
                        dragStep={true}
                        visibleSlides={2}
                        orientation='horizontal'
                        className='m-4 mt-12'
                        step={2}
                    >
                        <div className='flex flex-row items-center justify-between'>
                            <div className='font-bold text-xl my-6'>
                                Popular Product
                            </div>

                            <div className='flex flex-row'>
                                <ButtonBack className='cursor-pointer focus:outline-none'>
                                <svg
                                    xmlns='http://www.w3.org/2000/svg'
                                    className='h-6 w-6 cursor-pointer'
                                    fill='none'
                                    viewBox='0 0 24 24'
                                    stroke='#5A5F62'
                                >
                                    <path
                                        strokeLinecap='round'
                                        strokeLinejoin='round'
                                        strokeWidth={2}
                                        d='M15 19l-7-7 7-7'
                                    />
                                </svg>
                                </ButtonBack>

                                <ButtonNext className='cursor-pointer focus:outline-none'>
                                <svg
                                    xmlns='http://www.w3.org/2000/svg'
                                    className='h-6 w-6 cursor-pointer'
                                    fill='none'
                                    viewBox='0 0 24 24'
                                    stroke='#5A5F62'
                                >
                                    <path
                                        strokeLinecap='round'
                                        strokeLinejoin='round'
                                        strokeWidth={2}
                                        d='M9 5l7 7-7 7'
                                    />
                                </svg>
                                </ButtonNext>
                            </div>
                        </div>

                        <Slider classNameTray='gap-6 md:gap-4 sm-gap-2'>
                            {popularProduct.map((data, index) => {
                                return (
                                <Slide index={index} key={index}>
                                    <Product data={data} categoryId={data.category_id} />
                                </Slide>
                                );
                            })}
                        </Slider>
                    </CarouselProvider>
                    :
                    <CarouselProvider
                        naturalSlideWidth={0}
                        naturalSlideHeight={0}
                        totalSlides={popularProduct.length}
                        isIntrinsicHeight={true}
                        infinite={true}
                        dragStep={false}
                        visibleSlides={5}
                        orientation='horizontal'
                        className='mt-12'
                        step={5}
                    >
                        <div className='flex flex-row items-center justify-between'>
                            <div className='font-bold text-xl my-6'>
                                Popular Product
                            </div>

                            <div className='flex flex-row'>
                                <ButtonBack className='cursor-pointer focus:outline-none'>
                                <svg
                                    xmlns='http://www.w3.org/2000/svg'
                                    className='h-6 w-6 cursor-pointer'
                                    fill='none'
                                    viewBox='0 0 24 24'
                                    stroke='#5A5F62'
                                >
                                    <path
                                        strokeLinecap='round'
                                        strokeLinejoin='round'
                                        strokeWidth={2}
                                        d='M15 19l-7-7 7-7'
                                    />
                                </svg>
                                </ButtonBack>

                                <ButtonNext className='cursor-pointer focus:outline-none'>
                                <svg
                                    xmlns='http://www.w3.org/2000/svg'
                                    className='h-6 w-6 cursor-pointer'
                                    fill='none'
                                    viewBox='0 0 24 24'
                                    stroke='#5A5F62'
                                >
                                    <path
                                        strokeLinecap='round'
                                        strokeLinejoin='round'
                                        strokeWidth={2}
                                        d='M9 5l7 7-7 7'
                                    />
                                </svg>
                                </ButtonNext>
                            </div>
                        </div>

                        <Slider classNameTray='gap-6 md:gap-4 sm-gap-2'>
                            {popularProduct.map((data, index) => {
                                return (
                                <Slide index={index} key={index}>
                                    <Product data={data} categoryId={data.category_id} />
                                </Slide>
                                );
                            })}
                        </Slider>
                    </CarouselProvider>
                }
               </div>
            )}

            {recomProduct.length > 0 && (
                <div className={styles.carouselContainer}>
                {
                    isBreakpoint
                    ?   
                    <CarouselProvider
                        naturalSlideWidth={0}
                        naturalSlideHeight={0}
                        totalSlides={recomProduct.length}
                        isIntrinsicHeight={true}
                        infinite={true}
                        dragStep={true}
                        visibleSlides={2}
                        orientation='horizontal'
                        className='m-4'
                        step={2}
                    >
                        <div className='flex flex-row items-center justify-between'>
                            <div className='font-bold text-xl my-6'>
                                Recommendation
                            </div>

                            <div className='flex flex-row'>
                                <ButtonBack className='cursor-pointer focus:outline-none'>
                                <svg
                                    xmlns='http://www.w3.org/2000/svg'
                                    className='h-6 w-6 cursor-pointer'
                                    fill='none'
                                    viewBox='0 0 24 24'
                                    stroke='#5A5F62'
                                >
                                    <path
                                        strokeLinecap='round'
                                        strokeLinejoin='round'
                                        strokeWidth={2}
                                        d='M15 19l-7-7 7-7'
                                    />
                                </svg>
                                </ButtonBack>

                                <ButtonNext className='cursor-pointer focus:outline-none'>
                                <svg
                                    xmlns='http://www.w3.org/2000/svg'
                                    className='h-6 w-6 cursor-pointer'
                                    fill='none'
                                    viewBox='0 0 24 24'
                                    stroke='#5A5F62'
                                >
                                    <path
                                        strokeLinecap='round'
                                        strokeLinejoin='round'
                                        strokeWidth={2}
                                        d='M9 5l7 7-7 7'
                                    />
                                </svg>
                                </ButtonNext>
                            </div>
                        </div>

                        <Slider classNameTray='gap-6 md:gap-4 sm-gap-2'>
                            {recomProduct.map((data, index) => {
                                return (
                                <Slide index={index} key={index}>
                                    <Product data={data} categoryId={data.category_id} />
                                </Slide>
                                );
                            })}
                        </Slider>
                    </CarouselProvider>
                    :
                    <CarouselProvider
                        naturalSlideWidth={0}
                        naturalSlideHeight={0}
                        totalSlides={recomProduct.length}
                        isIntrinsicHeight={true}
                        infinite={true}
                        dragStep={false}
                        visibleSlides={5}
                        orientation='horizontal'
                        className=''
                        step={5}
                    >
                        <div className='flex flex-row items-center justify-between'>
                            <div className='font-bold text-xl my-6'>
                                Recommendation
                            </div>

                            <div className='flex flex-row'>
                                <ButtonBack className='cursor-pointer focus:outline-none'>
                                <svg
                                    xmlns='http://www.w3.org/2000/svg'
                                    className='h-6 w-6 cursor-pointer'
                                    fill='none'
                                    viewBox='0 0 24 24'
                                    stroke='#5A5F62'
                                >
                                    <path
                                        strokeLinecap='round'
                                        strokeLinejoin='round'
                                        strokeWidth={2}
                                        d='M15 19l-7-7 7-7'
                                    />
                                </svg>
                                </ButtonBack>

                                <ButtonNext className='cursor-pointer focus:outline-none'>
                                <svg
                                    xmlns='http://www.w3.org/2000/svg'
                                    className='h-6 w-6 cursor-pointer'
                                    fill='none'
                                    viewBox='0 0 24 24'
                                    stroke='#5A5F62'
                                >
                                    <path
                                        strokeLinecap='round'
                                        strokeLinejoin='round'
                                        strokeWidth={2}
                                        d='M9 5l7 7-7 7'
                                    />
                                </svg>
                                </ButtonNext>
                            </div>
                        </div>

                        <Slider classNameTray='gap-6 md:gap-4 sm-gap-2'>
                            {recomProduct.map((data, index) => {
                                return (
                                <Slide index={index} key={index}>
                                    <Product data={data} categoryId={data.category_id} />
                                </Slide>
                                );
                            })}
                        </Slider>
                    </CarouselProvider>
                }
               </div>
            )}
            <div className="mt-10-mobile mb-20">
                <div>
                    <h1 className="font-bold text-xl mb-2 px-4-mobile">We Are Trusted By</h1>
                </div>
                {
                    isBreakpoint
                    ?
                    <CarouselProvider
                        naturalSlideWidth={0}
                        naturalSlideHeight={0}
                        // totalSlides={recomProduct.length}
                        isIntrinsicHeight={true}
                        infinite={true}
                        dragStep={false}
                        visibleSlides={2}
                        orientation='horizontal'
                        className='carousel-container-custom-logo'
                    >
                        <Slider classNameTray='gap-10 md:gap-4 sm-gap-10'>
                        {
                            banner.logo && banner.logo.length > 0
                            ?
                            banner.logo.map((data, index) => {
                                return(
                                        <Slide key={index} style={{ position:'relative', height:150 }}>
                                            <div className="img-logo-child" style={{ position:'relative', height:150 }}>
                                                {/* <p>{JSON.stringify(data)}</p> */}
                                                {/* <h1 className="font-bold text-xl my-3">{data.name}</h1> */}
                                                    <img className={`${styles["review-logo-img"]}`}src={data.logo} alt={data.name}/>
                                                {/* <p className="text-base text-gray-500">{data.detail}</p> */}
                                            </div>
                                        </Slide>
                                )
                            })
                            :
                            null
                        }
                        </Slider>
                    </CarouselProvider>
                    :
                    <div className="grid grid-cols-4 gap-x-6 gap-y-0">
                        {
                            banner.logo && banner.logo.length > 0
                            ?
                            banner.logo.map((data, index) => {
                                return(
                                    <div style={{ position:'relative', height:150 }} key={index}>
                                        {/* <p>{JSON.stringify(data)}</p> */}
                                        {/* <h1 className="font-bold text-xl my-3">{data.name}</h1> */}
                                            <img className={`${styles["review-logo-img"]}`} src={data.logo} alt={data.name}/>
                                        {/* <p className="text-base text-gray-500">{data.detail}</p> */}
                                    </div>
                                )
                            })
                            :
                            null
                        }
                    </div>

                }
                <div className="grid grid-cols-3 grid-cols-1-mobile px-4-mobile gap-x-6 gap-y-0">
                    {
                        banner.review && banner.review.length > 0 && !isBreakpoint
                        ?
                        banner.review.map((data, index) => {
                            return(
                                <div key={index}>
                                    {/* <p>{JSON.stringify(data)}</p> */}
                                    <h1 className="font-bold text-xl my-3">{data.name}</h1>
                                    <div className="review-star-wrapper-homepage">
                                        <ReactStars
                                            classNames='flex-1'
                                            count={5}

                                            // onChange={ratingChanged}
                                            size={20}
                                            value={parseInt(data.score/20)}
                                            activeColor='#F2994A'
                                            edit={false}
                                            isHalf={true}
                                        />
                                    </div>
                                    <p className="text-base text-gray-500">{data.detail}</p>
                                </div>
                            )
                        })
                        : banner.review && banner.review.length > 0 && isBreakpoint
                        ?
                        <div className="flex" style={{ overflowX:'auto', paddingBottom:30 }}>
                        {
                            banner.review.map((data, index) => {
                                // filter((data, index) => index < 1)
                                return(
                                    <div style={{ minWidth:300 }} key={index}>
                                        {/* <p>{JSON.stringify(data)}</p> */}
                                        <h1 className="font-bold text-xl my-3">{data.name}</h1>
                                        <div className="review-star-wrapper-homepage">
                                            <ReactStars
                                                classNames='flex-1'
                                                count={5}

                                                // onChange={ratingChanged}
                                                size={20}
                                                value={parseInt(data.score/20)}
                                                activeColor='#F2994A'
                                                edit={false}
                                                isHalf={true}
                                            />
                                        </div>
                                        <p className="text-base text-gray-500">{data.detail}</p>
                                    </div>
                                )
                            })
                        }
                        </div>
                        :
                        null
                    }
                </div>
                <div className="grid grid-cols-1-mobile grid-cols-5 px-4-mobile gap-x-3 gap-y-0 mt-16 mb-8">
                    <div className="flex mb-4">
                        <div style={{ width:'25%', minWidth:60 }}>
                            <svg style={{ position: 'relative', left: '50%',transform: 'translateX(-50%) translateY(20%)' }} width="60" height="20" viewBox="0 0 30 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M10.3937 14.1294C10.3679 14.1294 10.3421 14.1294 10.3163 14.1294L0.690072 12.5279C0.431996 12.4762 0.251343 12.2696 0.251343 12.0113V2.37633C0.251343 2.22134 0.328766 2.09219 0.431996 1.98886C0.535227 1.88554 0.690072 1.85971 0.844918 1.88554L10.4712 3.48706C10.7292 3.53872 10.9099 3.74537 10.9099 4.00368V13.6386C10.9099 13.7936 10.8325 13.9228 10.7292 14.0261C10.626 14.0778 10.5228 14.1294 10.3937 14.1294ZM1.28365 11.5722L9.87758 12.9929V4.41697L1.28365 2.99627V11.5722Z" fill="#AFAFAF"/>
                                <path d="M10.3936 14.1296C10.2646 14.1296 10.1613 14.0779 10.0581 14.0004C9.92906 13.8971 9.87744 13.7679 9.87744 13.613V3.978C9.87744 3.71969 10.0581 3.51305 10.2904 3.46138L19.9166 1.57572C20.0714 1.54989 20.2263 1.57572 20.3553 1.67905C20.4844 1.78237 20.536 1.91153 20.536 2.06651V5.32122C20.536 5.60536 20.3037 5.83783 20.0198 5.83783C19.736 5.83783 19.5037 5.60536 19.5037 5.32122V2.71229L10.9097 4.3913V12.9672L14.368 12.2956C14.6519 12.2439 14.9099 12.4247 14.9615 12.7089C15.0132 12.993 14.8325 13.2513 14.5486 13.303L10.4968 14.1037C10.471 14.1296 10.4194 14.1296 10.3936 14.1296Z" fill="#AFAFAF"/>
                                <path d="M10.3937 4.49459C10.3679 4.49459 10.3421 4.49459 10.3163 4.49459L0.690072 2.89307C0.431996 2.84141 0.251343 2.63476 0.251343 2.40228C0.251343 2.14397 0.431996 1.93732 0.664265 1.88566L10.2905 0C10.3421 0 10.4195 0 10.4712 0L20.0974 1.57569C20.3555 1.62735 20.5361 1.834 20.5361 2.06648C20.5361 2.32479 20.3555 2.53144 20.1232 2.5831L10.497 4.46876C10.4712 4.49459 10.4195 4.49459 10.3937 4.49459ZM3.68376 2.35062L10.3679 3.46135L17.0779 2.14397L10.3937 1.03324L3.68376 2.35062Z" fill="#AFAFAF"/>
                                <path d="M7.29677 9.11835C7.19354 9.11835 7.09031 9.09252 6.98708 9.01503L4.94828 7.51683L3.29659 8.31759C3.14174 8.39509 2.96109 8.39509 2.80624 8.29176C2.6514 8.18844 2.57397 8.03345 2.57397 7.85264V2.76393C2.57397 2.47979 2.80624 2.24731 3.09013 2.24731C3.37401 2.24731 3.60628 2.47979 3.60628 2.76393V7.02604L4.79343 6.45776C4.97408 6.38027 5.18054 6.4061 5.30958 6.50943L6.78062 7.59433V3.64219C6.78062 3.35805 7.01289 3.12557 7.29677 3.12557C7.58065 3.12557 7.81292 3.35805 7.81292 3.64219V8.5759C7.81292 8.78255 7.70969 8.93754 7.52904 9.04086C7.47742 9.09252 7.37419 9.11835 7.29677 9.11835Z" fill="#AFAFAF"/>
                                <path d="M20.9233 14.6461C20.7942 14.6461 20.691 14.5945 20.5878 14.517C20.4845 14.4136 20.4071 14.2845 20.4071 14.1295V7.67176C20.4071 7.41345 20.5878 7.20681 20.8458 7.15514L27.2977 6.09607C27.4526 6.07024 27.6074 6.12191 27.7107 6.1994C27.8139 6.30272 27.8913 6.43188 27.8913 6.58686V13.0446C27.8913 13.3029 27.7107 13.5096 27.4526 13.5612L21.0007 14.6203C20.9749 14.6461 20.9491 14.6461 20.9233 14.6461ZM21.4394 8.11089V13.5096L26.859 12.6055V7.20681L21.4394 8.11089Z" fill="#AFAFAF"/>
                                <path d="M20.9234 14.6462C20.8976 14.6462 20.846 14.6462 20.8201 14.6462L14.3682 13.3805C14.136 13.3288 13.9553 13.1222 13.9553 12.8639V6.40611C13.9553 6.25113 14.0327 6.09614 14.136 6.01865C14.265 5.91532 14.4199 5.88949 14.5747 5.91532L21.0266 7.18104C21.2589 7.2327 21.4395 7.43935 21.4395 7.69766V14.1554C21.4395 14.3104 21.3621 14.4654 21.2589 14.5429C21.1556 14.5945 21.0266 14.6462 20.9234 14.6462ZM14.9876 12.4247L20.4072 13.4838V8.08512L14.9876 7.02605V12.4247Z" fill="#AFAFAF"/>
                                <path d="M20.9234 8.18864C20.8976 8.18864 20.846 8.18864 20.8201 8.18864L14.3682 6.92292C14.1102 6.87126 13.9553 6.66461 13.9553 6.4063C13.9553 6.14799 14.136 5.94135 14.3941 5.91552L20.846 4.85645C20.8976 4.85645 20.975 4.85645 21.0266 4.85645L27.4785 6.09633C27.7366 6.14799 27.8914 6.35464 27.8914 6.61295C27.8914 6.87126 27.7108 7.07791 27.4527 7.10374L21.0008 8.16281C20.975 8.18864 20.9492 8.18864 20.9234 8.18864ZM17.3877 6.43213L20.9234 7.12957L24.459 6.53546L20.9234 5.83802L17.3877 6.43213Z" fill="#AFAFAF"/>
                                <path d="M22.9878 11.2623C22.9104 11.2623 22.833 11.2365 22.7556 11.2107C22.5749 11.1332 22.4717 10.9524 22.4717 10.7457V7.43934C22.4717 7.1552 22.7039 6.92273 22.9878 6.92273C23.2717 6.92273 23.504 7.1552 23.504 7.43934V9.7383L24.2266 9.22168C24.3814 9.11836 24.5879 9.09253 24.7428 9.17002L25.2847 9.42833V6.84523C25.2847 6.56109 25.517 6.32861 25.8009 6.32861C26.0847 6.32861 26.317 6.56109 26.317 6.84523V10.2549C26.317 10.4357 26.2138 10.5907 26.0847 10.694C25.9299 10.7974 25.7492 10.7974 25.5944 10.7199L24.5879 10.2291L23.2975 11.159C23.2201 11.2365 23.0911 11.2623 22.9878 11.2623Z" fill="#AFAFAF"/>
                            </svg>
                        </div>
                        <div>
                            <h2 className="font-bold text-base" style={{ lineHeight:'inherit' }}>COMPLETE PRODUCTS</h2>
                            <p className="text-11-px">More than 3.000+ Products</p>
                        </div>
                    </div>
                    <div className="flex mb-4">
                        <div style={{ width:'25%', minWidth:60 }}>
                            <svg style={{ position: 'relative', left: '50%',transform: 'translateX(-50%) translateY(20%)' }} width="60" height="30" viewBox="0 0 19 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M9.72704 15.1373C6.50115 15.1373 3.88818 12.5219 3.88818 9.29312C3.88818 6.06431 6.50115 3.44897 9.72704 3.44897C12.9529 3.44897 15.5659 6.06431 15.5659 9.29312C15.5659 12.5219 12.9529 15.1373 9.72704 15.1373ZM9.72704 4.7405C7.21085 4.7405 5.17854 6.77465 5.17854 9.29312C5.17854 11.8116 7.21085 13.8457 9.72704 13.8457C12.2432 13.8457 14.2755 11.8116 14.2755 9.29312C14.2755 6.77465 12.2432 4.7405 9.72704 4.7405Z" fill="#AFAFAF"/>
                                <path d="M11.8561 17.7203C11.5335 17.7203 11.2109 17.6557 10.9206 17.5265L9.88827 17.0745C9.6302 16.9776 9.37213 16.9453 9.11406 17.0422L8.04952 17.4297C6.92046 17.8494 5.69462 17.3328 5.17848 16.2673L4.6946 15.2341C4.56556 15.0081 4.37201 14.8143 4.11394 14.7175L3.04939 14.33C1.92033 13.9103 1.30742 12.7479 1.59775 11.5855L1.88808 10.4877C1.95259 10.2294 1.92033 9.97112 1.7913 9.7451L1.21064 8.77646C0.629979 7.74324 0.88805 6.45172 1.85582 5.74138L2.79132 5.09562C3.01714 4.93418 3.14617 4.70816 3.21069 4.44986L3.40424 3.31978C3.5978 2.15741 4.66234 1.31792 5.85592 1.41478L6.98498 1.51164C7.24305 1.54393 7.50112 1.44707 7.72693 1.28563L8.59792 0.543003C9.50117 -0.231911 10.8238 -0.167334 11.6948 0.672155L12.5012 1.47936C12.6948 1.67309 12.9529 1.76995 13.2109 1.76995H14.34C15.5336 1.76995 16.5013 2.67402 16.6304 3.83639L16.7272 4.96647C16.7594 5.22477 16.8885 5.48308 17.082 5.64452L17.953 6.38714C18.8562 7.16206 19.0498 8.45358 18.3724 9.45451L17.7272 10.3909C17.5659 10.6169 17.5336 10.8752 17.5659 11.1335L17.7594 12.2636C17.953 13.4259 17.2756 14.556 16.1142 14.8789L15.0174 15.1695C14.7594 15.2341 14.5336 15.3955 14.4045 15.6538L13.8561 16.6225C13.4368 17.3005 12.6625 17.7203 11.8561 17.7203ZM9.46891 15.6538C9.7915 15.6538 10.1141 15.7184 10.4044 15.8475L11.4367 16.2996C11.9206 16.5256 12.469 16.3319 12.7271 15.8798L13.2755 14.9112C13.5658 14.3946 14.0497 14.0071 14.6626 13.8457L15.7594 13.5551C16.2755 13.4259 16.5659 12.9093 16.4691 12.3927L16.2755 11.2626C16.1788 10.6815 16.3078 10.068 16.6304 9.55138L17.2756 8.61502C17.5659 8.19528 17.5014 7.61409 17.082 7.25892L16.211 6.5163C15.7594 6.12884 15.4691 5.57994 15.4045 4.96647L15.3078 3.83639C15.2432 3.31978 14.8239 2.93232 14.3077 2.93232H13.1787C12.5658 2.93232 12.0174 2.7063 11.5657 2.28656L10.7593 1.47936C10.3722 1.12419 9.7915 1.0919 9.40439 1.41478L8.5334 2.15741C8.08178 2.54486 7.46886 2.73859 6.8882 2.7063L5.75914 2.60944C5.243 2.57715 4.79137 2.93232 4.6946 3.44893L4.50104 4.57901C4.40427 5.1602 4.08168 5.70909 3.56554 6.06426L2.63003 6.71002C2.21066 7.00062 2.08163 7.5818 2.3397 8.03384L2.92036 9.00248C3.21069 9.51909 3.30747 10.1326 3.14617 10.7137L2.85584 11.8115C2.72681 12.3281 2.98488 12.8448 3.50102 13.0062L4.56556 13.3937C5.14622 13.5874 5.59785 14.0071 5.85592 14.556L6.3398 15.5892C6.56561 16.0736 7.11401 16.2673 7.5979 16.1059L8.66244 15.7184C8.92051 15.7184 9.21084 15.6538 9.46891 15.6538Z" fill="#AFAFAF"/>
                                <path d="M4.33976 22.5632C4.21072 22.5632 4.08169 22.5309 3.98491 22.4341C3.82362 22.3049 3.69458 22.1112 3.69458 21.9175V14.1038C3.69458 13.7486 3.98491 13.458 4.33976 13.458C4.69461 13.458 4.98494 13.7486 4.98494 14.1038V20.9811L9.46892 19.2699C9.63021 19.2053 9.79151 19.2053 9.9528 19.2699L14.2755 20.9811V14.5235C14.2755 14.1683 14.5658 13.8777 14.9207 13.8777C15.2755 13.8777 15.5658 14.1683 15.5658 14.5235V21.9498C15.5658 22.1758 15.4691 22.3695 15.2755 22.4987C15.1142 22.6278 14.8884 22.6278 14.6626 22.5632L9.72699 20.5291L4.59783 22.4987C4.50105 22.531 4.43653 22.5632 4.33976 22.5632Z" fill="#AFAFAF"/>
                                <path d="M9.17865 11.3271C9.01735 11.3271 8.85606 11.2625 8.72702 11.1334L7.24311 9.64811C6.98504 9.3898 6.98504 9.00235 7.24311 8.74404C7.50119 8.48574 7.88829 8.48574 8.14636 8.74404L9.17865 9.77726L11.5013 7.45252C11.7594 7.19421 12.1465 7.19421 12.4045 7.45252C12.6626 7.71082 12.6626 8.09828 12.4045 8.35658L9.63027 11.1334C9.5335 11.2625 9.33994 11.3271 9.17865 11.3271Z" fill="#AFAFAF"/>
                            </svg>
                        </div>
                        <div>
                            <h2 className="font-bold text-base" style={{ lineHeight:'inherit' }}>WARRANTY</h2>
                            <p className="text-11-px">Official Warranty 12 months</p>
                        </div>
                    </div>
                    <div className="flex mb-4">
                        <div style={{ width:'25%', minWidth:60 }}>
                            <svg style={{ position: 'relative', left: '50%',transform: 'translateX(-50%) translateY(20%)' }} width="50" height="30" viewBox="0 0 25 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M16.3273 15.2219H2.78384C1.44693 15.2219 0.342529 14.1165 0.342529 12.7784V3.20793C0.342529 1.86981 1.44693 0.764404 2.78384 0.764404H18.0711C19.408 0.764404 20.5124 1.86981 20.5124 3.20793V5.36055C20.5124 5.68054 20.2509 5.94234 19.9312 5.94234C19.6115 5.94234 19.3499 5.68054 19.3499 5.36055V3.20793C19.3499 2.50978 18.7686 1.92799 18.0711 1.92799H2.78384C2.08632 1.92799 1.50506 2.50978 1.50506 3.20793V12.7784C1.50506 13.4765 2.08632 14.0583 2.78384 14.0583H16.2983C16.618 14.0583 16.8795 14.3201 16.8795 14.6401C16.8795 14.9601 16.647 15.2219 16.3273 15.2219Z" fill="#AFAFAF"/>
                                <path d="M19.6987 5.56421H0.923795C0.604099 5.56421 0.342529 5.30241 0.342529 4.98242C0.342529 4.66244 0.604099 4.40063 0.923795 4.40063H19.6987C20.0184 4.40063 20.2799 4.66244 20.2799 4.98242C20.2799 5.30241 20.0184 5.56421 19.6987 5.56421Z" fill="#AFAFAF"/>
                                <path d="M16.9376 7.10597H0.952847C0.633151 7.10597 0.371582 6.84416 0.371582 6.52418C0.371582 6.20419 0.633151 5.94238 0.952847 5.94238H16.9376C17.2573 5.94238 17.5189 6.20419 17.5189 6.52418C17.5189 6.84416 17.2573 7.10597 16.9376 7.10597Z" fill="#AFAFAF"/>
                                <path d="M19.0011 16.6474C18.9139 16.6474 18.8558 16.6474 18.7686 16.6183L17.5479 16.1238C15.3682 15.222 13.9441 13.1275 13.9441 10.7713V6.90238C13.9441 6.72784 14.0022 6.58239 14.1475 6.46603C14.2928 6.34967 14.4382 6.29149 14.5835 6.32058C16.7051 6.52421 18.5651 4.98247 18.5942 4.98247C18.7977 4.80793 19.1174 4.80793 19.3499 4.98247C19.3789 5.01156 21.239 6.52421 23.3606 6.32058C23.535 6.32058 23.6803 6.34967 23.7965 6.46603C23.9128 6.58239 24 6.72784 24 6.90238V10.7713C24 13.1275 22.5759 15.2511 20.3961 16.1238L19.1755 16.6183C19.1464 16.6183 19.0883 16.6474 19.0011 16.6474ZM15.1357 7.45508V10.7131C15.1357 12.6039 16.2692 14.2911 18.0129 14.9893L19.0011 15.3965L19.9892 14.9893C21.733 14.262 22.8665 12.6039 22.8665 10.7131V7.45508C21.1227 7.42599 19.6696 6.5533 19.0011 6.08787C18.3326 6.5533 16.8795 7.42599 15.1357 7.45508Z" fill="#AFAFAF"/>
                                <path d="M18.3325 12.953C18.1872 12.953 18.0419 12.8949 17.9257 12.7785L16.4144 11.2658C16.1819 11.0331 16.1819 10.6841 16.4144 10.4513C16.6469 10.2186 16.9956 10.2186 17.2281 10.4513L18.3325 11.5567L20.7448 9.14231C20.9773 8.90959 21.3261 8.90959 21.5586 9.14231C21.7911 9.37503 21.7911 9.7241 21.5586 9.95682L18.7394 12.7785C18.6522 12.8949 18.5069 12.953 18.3325 12.953Z" fill="#AFAFAF"/>
                            </svg>
                        </div>
                        <div>
                            <h2 className="font-bold text-base" style={{ lineHeight:'inherit' }}>PAYMENT</h2>
                            <p className="text-11-px">Safe and Reliable</p>
                        </div>
                    </div>
                    <div className="flex mb-4">
                        <div style={{ width:'25%', minWidth:60 }}>
                            <svg style={{ position: 'relative', left: '50%',transform: 'translateX(-50%) translateY(20%)' }} width="55" height="30" viewBox="0 0 30 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M7.47807 21.4082C6.17353 21.4082 5.08643 20.3473 5.08643 19.0144C5.08643 17.6815 6.14636 16.6206 7.47807 16.6206C8.80977 16.6206 9.86971 17.6815 9.86971 19.0144C9.86971 20.3473 8.7826 21.4082 7.47807 21.4082ZM7.47807 17.7087C6.77144 17.7087 6.17353 18.2799 6.17353 19.0144C6.17353 19.7217 6.74427 20.3201 7.47807 20.3201C8.18469 20.3201 8.7826 19.7489 8.7826 19.0144C8.7826 18.3071 8.18469 17.7087 7.47807 17.7087Z" fill="#AFAFAF"/>
                                <path d="M16.9904 21.4083C15.7946 21.4083 14.8162 20.429 14.8162 19.2321C14.8162 18.0352 15.7946 17.0559 16.9904 17.0559C18.1862 17.0559 19.1646 18.0352 19.1646 19.2321C19.1646 20.429 18.1862 21.4083 16.9904 21.4083ZM16.9904 18.1712C16.3925 18.1712 15.9033 18.6608 15.9033 19.2593C15.9033 19.8577 16.3925 20.3474 16.9904 20.3474C17.5883 20.3474 18.0775 19.8577 18.0775 19.2593C18.0503 18.6336 17.5883 18.1712 16.9904 18.1712Z" fill="#AFAFAF"/>
                                <path d="M16.0391 16.2127H7.45092C7.20632 16.2127 6.9889 16.0495 6.93454 15.8046L4.2983 4.8149H1.11851C0.819551 4.8149 0.574951 4.57008 0.574951 4.27085C0.574951 3.97163 0.819551 3.72681 1.11851 3.72681H4.73314C4.97774 3.72681 5.19517 3.89002 5.24952 4.13484L7.88576 15.1246H15.6586L16.4467 13.166C16.5555 12.894 16.8816 12.758 17.1534 12.8668C17.4251 12.9756 17.561 13.302 17.4523 13.574L16.5011 15.8862C16.4739 16.0767 16.2565 16.2127 16.0391 16.2127Z" fill="#AFAFAF"/>
                                <path d="M13.3757 8.21528H5.84751C5.54855 8.21528 5.30396 7.97046 5.30396 7.67124C5.30396 7.37202 5.54855 7.1272 5.84751 7.1272H13.4029C13.7019 7.1272 13.9465 7.37202 13.9465 7.67124C13.9465 7.97046 13.6747 8.21528 13.3757 8.21528Z" fill="#AFAFAF"/>
                                <path d="M18.3491 14.0908C15.3052 14.0908 12.832 11.6154 12.832 8.5687C12.832 5.52204 15.3052 3.04663 18.3491 3.04663C21.393 3.04663 23.8662 5.52204 23.8662 8.5687C23.8662 11.6154 21.3658 14.0908 18.3491 14.0908ZM18.3491 4.16192C15.9031 4.16192 13.9191 6.14769 13.9191 8.5959C13.9191 11.0441 15.9031 13.0299 18.3491 13.0299C20.7951 13.0299 22.7791 11.0441 22.7791 8.5959C22.7791 6.14769 20.7679 4.16192 18.3491 4.16192Z" fill="#AFAFAF"/>
                                <path d="M20.1158 2.85624H16.6371C16.3381 2.85624 16.0935 2.61142 16.0935 2.3122V0.544046C16.0935 0.244821 16.3381 0 16.6371 0H20.1158C20.4148 0 20.6594 0.244821 20.6594 0.544046V2.3122C20.6594 2.61142 20.4148 2.85624 20.1158 2.85624ZM17.1806 1.76815H19.5723V1.08809H17.1806V1.76815Z" fill="#AFAFAF"/>
                                <path d="M17.398 4.16211C17.0991 4.16211 16.8545 3.91729 16.8545 3.61806V2.31236C16.8545 2.01313 17.0991 1.76831 17.398 1.76831C17.697 1.76831 17.9416 2.01313 17.9416 2.31236V3.61806C17.9416 3.91729 17.7242 4.16211 17.398 4.16211Z" fill="#AFAFAF"/>
                                <path d="M19.3275 4.16211C19.0285 4.16211 18.7839 3.91729 18.7839 3.61806V2.31236C18.7839 2.01313 19.0285 1.76831 19.3275 1.76831C19.6264 1.76831 19.871 2.01313 19.871 2.31236V3.61806C19.871 3.91729 19.6264 4.16211 19.3275 4.16211Z" fill="#AFAFAF"/>
                                <path d="M18.3492 9.11288C18.2677 9.11288 18.1862 9.08567 18.1318 9.05847C17.9416 8.97686 17.8057 8.78645 17.8057 8.56883L17.8328 5.49497C17.8328 5.19575 18.0774 4.95093 18.3764 4.95093C18.6754 4.95093 18.92 5.19575 18.92 5.49497L18.8928 7.34473L19.7353 6.61026C19.9527 6.41985 20.306 6.44705 20.4963 6.66467C20.6865 6.88229 20.6593 7.23591 20.4419 7.42633L18.6754 8.94966C18.5938 9.08567 18.4851 9.11288 18.3492 9.11288Z" fill="#AFAFAF"/>
                            </svg>
                        </div>
                        <div>
                            <h2 className="font-bold text-base" style={{ lineHeight:'inherit' }}>FAST AND EFFICIENT</h2>
                            <p className="text-11-px">Fast Response, Shopping +/-5 Minutes</p>
                        </div>
                    </div>
                    <div className="flex mb-4">
                        <div style={{ width:'25%', minWidth:60 }}>
                            <svg style={{ position: 'relative', left: '50%',transform: 'translateX(-50%) translateY(20%)' }} width="65" height="30" viewBox="0 0 35 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M3.97403 21.4412C3.86455 21.4412 3.75506 21.4047 3.64558 21.3681C3.39013 21.222 3.20766 20.9298 3.28065 20.6376L3.60909 18.2268C1.96687 17.7155 0.799072 16.2179 0.799072 14.4281V8.98558C0.799072 6.79397 2.58726 5.00415 4.77689 5.00415H13.4989C15.6885 5.00415 17.4767 6.79397 17.4767 8.98558V14.4281C17.4767 16.6197 15.6885 18.4095 13.4989 18.4095H7.69639L4.48494 21.2951C4.33896 21.3681 4.1565 21.4412 3.97403 21.4412ZM4.74039 6.4287C3.35363 6.4287 2.22233 7.56103 2.22233 8.94905V14.3915C2.22233 15.67 3.17116 16.7658 4.44845 16.8753C4.63091 16.9119 4.81338 16.9849 4.95936 17.1676C5.06884 17.3137 5.14183 17.5328 5.10533 17.7155L4.95936 18.8478L6.89353 17.0945C7.0395 16.9849 7.18547 16.9119 7.36794 16.9119H13.4624C14.8492 16.9119 15.9805 15.7795 15.9805 14.3915V8.94905C15.9805 7.56103 14.8492 6.4287 13.4624 6.4287H4.74039Z" fill="#AFAFAF"/>
                                <path d="M23.3885 16.5101C23.2061 16.5101 23.0236 16.437 22.9141 16.3274L19.7027 13.4418H16.7467C16.3453 13.4418 16.0168 13.1131 16.0168 12.7113C16.0168 12.3095 16.3453 11.9808 16.7467 11.9808H19.9581C20.1406 11.9808 20.3231 12.0538 20.4326 12.1634L22.3667 13.9167L22.2207 12.7844C22.1843 12.6017 22.2572 12.3826 22.3667 12.2365C22.4762 12.0903 22.6587 11.9808 22.8776 11.9442C24.1549 11.7981 25.1038 10.7389 25.1038 9.46042V3.98141C25.1038 2.5934 23.9724 1.46107 22.5857 1.46107H13.9002C12.5134 1.46107 11.3821 2.5934 11.3821 3.98141V5.25985C11.3821 5.66164 11.0537 5.99038 10.6522 5.99038C10.2508 5.99038 9.92236 5.66164 9.92236 5.25985V3.98141C9.92236 1.78981 11.7106 0 13.9002 0H22.6222C24.8118 0 26.6 1.78981 26.6 3.98141V9.46042C26.6 11.2502 25.4322 12.7478 23.79 13.2592L24.1184 15.67C24.1549 15.9622 24.0089 16.2544 23.7535 16.4005C23.644 16.4736 23.498 16.5101 23.3885 16.5101Z" fill="#AFAFAF"/>
                                <path d="M9.41153 14.3914H7.8058C7.40437 14.3914 7.07593 14.0627 7.07593 13.6609C7.07593 13.2591 7.40437 12.9304 7.8058 12.9304H9.41153C9.5575 12.9304 9.66698 12.8208 9.66698 12.6747C9.66698 12.5286 9.5575 12.419 9.41153 12.419H8.86412C7.87879 12.419 7.07593 11.6154 7.07593 10.6292C7.07593 9.64295 7.87879 8.83936 8.86412 8.83936H10.4334C10.8348 8.83936 11.1632 9.1681 11.1632 9.56989C11.1632 9.97169 10.8348 10.3004 10.4334 10.3004H8.86412C8.68165 10.3004 8.53568 10.4465 8.53568 10.6292C8.53568 10.8118 8.68165 10.9579 8.86412 10.9579H9.41153C10.3604 10.9579 11.1267 11.725 11.1267 12.6747C11.1267 13.6244 10.3969 14.3914 9.41153 14.3914Z" fill="#AFAFAF"/>
                                <path d="M9.11952 10.264C8.71809 10.264 8.38965 9.93522 8.38965 9.53342V7.88972C8.38965 7.48792 8.71809 7.15918 9.11952 7.15918C9.52095 7.15918 9.8494 7.48792 9.8494 7.88972V9.53342C9.8494 9.93522 9.52095 10.264 9.11952 10.264Z" fill="#AFAFAF"/>
                                <path d="M9.11952 16.0349C8.71809 16.0349 8.38965 15.7062 8.38965 15.3044V13.6607C8.38965 13.2589 8.71809 12.9302 9.11952 12.9302C9.52095 12.9302 9.8494 13.2589 9.8494 13.6607V15.3044C9.8494 15.7062 9.52095 16.0349 9.11952 16.0349Z" fill="#AFAFAF"/>
                            </svg>
                        </div>
                        <div>
                            <h2 className="font-bold text-base" style={{ lineHeight:'inherit' }}>COMPETITIVE PRICE</h2>
                            <p className="text-11-px">Variety Promos and Offers (Price includes 10% VAT)</p>
                        </div>
                    </div>
                </div>
            </div>
         </div>
      </Layout>
   );
};

export default Home;

export async function getStaticProps() {
   let resp = {};

   try {
      const [product, banner, categories] = await Promise.all([
         requestGet("/rest/default/V1/products/", {
            "searchCriteria[currentPage]": 0,
            "searchCriteria[pageSize]": 10,
         }),
         requestGet("/rest/V1/home/all"),
         requestGet("/rest/V1/products/all-categories")
      ]);

      resp = {
         product: product,
         banner: banner[0],
         menuProps: categories
      }
      console.log(JSON.stringify(banner[0]))
   } catch (error) {
      console.log("error in index page", error);
   }

   return {
      props: resp,
      revalidate: 10,
   };
}

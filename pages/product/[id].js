import React, { useEffect, useContext, useState } from "react";
import router, { useRouter } from "next/router";
import Image from "next/image";
import Layout from "../../components/Theme/Layout";
import { Breadcrumb, Tab } from "../../components";
import { MenuContext } from "../../contexts/MenuContext";
import { requestGet, requestPost, requestPut } from "../../lib/axios";
import { moneyFormat, dateFormat } from "../../lib/helper";
import fetchJson from "../../lib/fetchJson";
import useUser from "../../lib/useUser";
import useCart from "../../lib/useCart";
import responsiveStyles from "../../styles/Components/DetailProductResponsive.module.css";
import styles from "../../styles/Components/DetailProduct.module.css";
import Link from "next/link";
import AsyncLocalStorage from "@createnextapp/async-local-storage";

import {
    ButtonBack,
    ButtonNext,
    CarouselProvider,
    Slide,
    Slider,
} from "pure-react-carousel";
import ReactStars from "react-rating-stars-component";
import { IconWishlist } from "../../assets/illustrations";

const DetailProduct = (props) => {
    const { setMenu } = useContext(MenuContext);
    const { product, menu } = props;
    const { user, mutateUser } = useUser();
    const { cart, mutateCart } = useCart(user);
    const [cartState, setCartState] = useState([]);
    const router = useRouter();
    const [loading, setLoading] = useState(false);
    const [totalItemsAsync, setTotalItemsAsync] = useState(0);
    const [form, setForm] = useState({});
    const [formResp, setFormResp] = useState("");
    const [breadcrumb, setBreadcrumb] = useState([]);

    useEffect(() => {
        setTimeout(() => {
            setFormResp({});
        }, 5000);
    }, [formResp]);

    useEffect(() => {
        const getBreadcrump = async () => {
            const categoryId = router.query.category_id;
            try {
                if (categoryId != 0) {
                    const res = await requestGet(
                        "/rest/V1/products/breadcrumbs?categoryId=" + categoryId
                    );
                    res.map((element, index) => {
                        index > 0
                            ? (element.path = "/category/" + element.path)
                            : element.path;
                    });
                    setBreadcrumb(res);
                } else if (categoryId == 0) {
                    setBreadcrumb([
                        {
                            name: "Home",
                            path: "/",
                        },
                        {
                            name: product.name.slice(0, 20) + "...",
                        },
                    ]);
                }
            } catch (e) {
                console.log(e);
            }
        };

        if (breadcrumb.length === 0) {
            getBreadcrump();
        }
    });

    const readCurrentQtyCart = async () => {
        let currentQtyCart;
        try {
            currentQtyCart = await AsyncLocalStorage.getItem("totalItems");
            //  console.log(currentQtyCart+' asdsa')
            setTotalItemsAsync(currentQtyCart);
        } catch (e) {}
    };

    console.log(
        JSON.stringify(product.tab_more_information.product_attributes)
    );

    useEffect(() => {
        setMenu(menu);
        setCartState(cart?.items);
        readCurrentQtyCart();
    }, [setMenu, menu, cart, setCartState]);

    const addToCartAsync = async (e) => {
        // e.preventDefault();
        // alert('ABCDEF')
        setLoading(true);
        let updatedQtyCart;
        try {
            var updateQtyCart = (
                parseInt(totalItemsAsync) + parseInt(state.qty)
            ).toString();
            // setTotalItemsAsync(updateQtyCart)
            // console.log(updateQtyCart)
            await AsyncLocalStorage.setItem("totalItems", updateQtyCart);
        } catch (e) {
            // error
        }
        updatedQtyCart = await AsyncLocalStorage.getItem("totalItems");
        console.log(updatedQtyCart + " updated");
        setTotalItemsAsync(updatedQtyCart);
        setTimeout(function () {
            setLoading(false);
        }, 800);
        addToCartNew(e);
        // setTimeout(
        //     function(){
        //         readCurrentQtyCart();
        //     }, 1000
        // )
    };

    //    useEffect(async () => {
    //     const product = await requestGet("/rest/default/V1/products/", {
    //         "searchCriteria[currentPage]": 0,
    //         "searchCriteria[pageSize]": 3000,
    //      });

    //      const paths = product.items.map((item) => ({
    //          params: { id: item.sku },
    //         }));
    //     for(var i = 0; i < paths.length; i++){
    //             if(paths[i].params.id === 'E31KTU' ||
    //             paths[i].params.id === 'A9N18481' ||
    //             paths[i].params.id === 'DOMF01325' ||
    //             paths[i].params.id === 'LE1M35M721' ||
    //             paths[i].params.id === 'XB4BW34B5' ||
    //             paths[i].params.id === 'XB5FVG5'){
    //                 delete paths[i]
    //             }
    //     }

    //         console.log(JSON.stringify(paths))
    //     })

    const [state, setState] = useState({
        slideActive: product.image_url,
        qty: 1,
        tabActive: 0,
        //   tabs: [
        //      { title: "Detail", active: true },
        //      { title: "Information", active: false },
        //      { title: "Review", active: false },
        //   ],
        tabs: [
            { title: "Characteristics", active: true },
            { title: "Documentation", active: false },
            { title: "Review", active: false },
        ],
    });

    const addToCartNew = async (e) => {
        e.preventDefault();
        if (!user.isLoggedIn) {
            router.push("/login");
        } else {
            // setLoading(true)
            // setDisableCart(true)
            const quoteId = await fetchJson("/api/cart/quote", {
                method: "POST",
                headers: { "Content-Type": "application/json" },
            });

            const param = {
                cartItem: {
                    sku: product.sku,
                    qty: state.qty,
                    quote_id: quoteId,
                },
            };

            // console.log(JSON.stringify(param));
            // console.log(JSON.stringify(product));

            const resp = fetchJson("/api/cart/item", {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify(param),
            }).catch((error) => {
                console.log(error);
                setDisableCart(true);
            });

            let cState = cartState;

            if (cState && cState.length > 0) {
                for (var i = 0; i < cState.length; i++) {
                    if (cState[i].quote_id === quoteId) {
                        const body = {
                            cartItem: {
                                sku: cState[i].sku,
                                qty: parseInt(cState[i].qty + state.qty),
                                quote_id: quoteId,
                            },
                        };
                        promise.push(
                            requestPut(
                                `/rest/default/V1/carts/mine/items/${cState[i].product_id}`,
                                null,
                                body,
                                user.token
                            )
                        );
                    }
                }
            }
            // setDisableCart(false)

            //    let promise = [];

            setTimeout(function () {
                mutateUser(resp);
                mutateCart();
            }, 800);

            //    mutateUser(resp)

            //    router.push('/')

            //   if(resp){
            //       setLoading(false)
            //   }

            console.log("resp add to cart", resp);
        }
    };

    const addToCart = async () => {
        if (!user.isLoggedIn) {
            router.push("/login");
        } else {
            setLoading(true);
            const quoteId = await fetchJson("/api/cart/quote", {
                method: "POST",
                headers: { "Content-Type": "application/json" },
            });

            const param = {
                cartItem: {
                    sku: product.sku,
                    qty: state.qty,
                    quote_id: quoteId,
                },
            };

            console.log(JSON.stringify(param));

            const resp = fetchJson("/api/cart/item", {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify(param),
            });

            setTimeout(function () {
                mutateUser(resp);
                mutateCart();
                setLoading(false);
            }, 500);
            await AsyncLocalStorage.setItem("successAddToCart", "true");
            // setTimeout(
            //     async function(){
            //         try{
            //             await AsyncLocalStorage.setItem('successAddToCart','false')
            //         } catch(e){

            //         }
            //     }, 1500
            // )
            // console.log("resp add to cart", resp);
        }
    };

    const addWishlist = async () => {
        if (!user.isLoggedIn) {
            router.push("/login");
        } else {
            const addwishList = await requestPost(
                "/rest/V1/wishlist/add/" + product.product_id,
                {
                    customerId: user.data.id,
                },
                user.token
            );

            if (addwishList) {
                alert("Wishlist berhasil ditambahkan");
            } else {
                alert("Wishlist gagal ditambahkan");
            }
            // console.log(JSON.stringify(product.product_id))
            // console.log(JSON.stringify(user.data.id));
        }
    };

    const onChangeTab = (index) => {
        let tabs = [];
        state.tabs.map((item, key) => {
            let tab = {};
            tab.title = item.title;
            tab.active = key == index;

            tabs.push(tab);
        });

        setState({ ...state, tabs: tabs, tabActive: index });
    };

    const handleRatingChange = (newRating) => {
        // console.log("rating", newRating);
        handleFormChange({
            name: "rating",
            value: newRating,
        });
    };

    const handleFormChange = ({ name, value }) => {
        setForm({
            ...form,
            [name]: value,
        });
    };

    const handleSubmitReview = async (e) => {
        e.preventDefault();
        setLoading(true);

        const param = {
            review: {
                title: form.title,
                detail: form.detail,
                nickname: form.nickname,
                ratings: [
                    {
                        rating_name: "Rating",
                        value: form.rating,
                    },
                ],
                review_entity: "product",
                review_status: 2,
                entity_pk_value: product.product_id,
            },
        };

        try {
            const resp = await fetchJson("/api/review", {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify(param),
            });

            // console.log("formResponse", resp);
            setForm({});
            setFormResp(resp);
        } catch (error) {
            setFormResp(error);
        }

        setLoading(false);
    };

    return (
        <Layout title={`${product.brand} - ${product.name}`}>
            <div
                className={`max-w-screen-xl max-width-1100 mx-auto lg:py-4 ${styles["product-detail-container"]}`}
            >
                <Breadcrumb
                    responsiveStyles={`${responsiveStyles["product-site-map"]} uppercase`}
                    items={breadcrumb}
                />

                <div
                    className={`flex ${responsiveStyles["image-slider"]} flex-row mt-10 md:mt-20 bottom-1 border-gray-200 space-x-20`}
                >
                    <div className="flex-initial flex-col">
                        <Image
                            alt={state.slideActive}
                            src={
                                state.slideActive
                                    ? state.slideActive
                                    : "/img/example-product.png"
                            }
                            objectFit="contain"
                            width={361.74}
                            height={361.74}
                        />

                        <CarouselProvider
                            naturalSlideWidth={216}
                            naturalSlideHeight={410}
                            totalSlides={product.media_gallery.length}
                            isIntrinsicHeight={true}
                            infinite={true}
                            dragStep={false}
                            visibleSlides={4}
                            orientation="horizontal"
                        >
                            <div className="flex flex-row items-center">
                                <ButtonBack className="cursor-pointer focus:outline-none">
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        className="h-8 w-8 cursor-pointer"
                                        fill="none"
                                        viewBox="0 0 24 24"
                                        stroke="#5A5F62"
                                    >
                                        <path
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                            strokeWidth={2}
                                            d="M15 19l-7-7 7-7"
                                        />
                                    </svg>
                                </ButtonBack>

                                <Slider
                                    classNameTray="gap-6 md:gap-4 sm-gap-2 border rounded border-gray-100 p-2 my-8"
                                    classNameTrayWrap="px-2 py-8"
                                >
                                    {product.media_gallery.map((item, key) => {
                                        return (
                                            <Slide key={key}>
                                                <Image
                                                    alt={item}
                                                    src={
                                                        item
                                                            ? item
                                                            : "/img/example-product.png"
                                                    }
                                                    className="cursor-pointer"
                                                    objectFit="cover"
                                                    width={340}
                                                    height={340}
                                                />
                                            </Slide>
                                        );
                                    })}
                                </Slider>

                                <ButtonNext className="cursor-pointer focus:outline-none">
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        className="h-8 w-8 cursor-pointer"
                                        fill="none"
                                        viewBox="0 0 24 24"
                                        stroke="#5A5F62"
                                    >
                                        <path
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                            strokeWidth={2}
                                            d="M9 5l7 7-7 7"
                                        />
                                    </svg>
                                </ButtonNext>
                            </div>
                        </CarouselProvider>
                    </div>
                    <div
                        className={`flex-1 flex-col ${responsiveStyles["detail-container-mobile"]}`}
                    >
                        <div className="text-xl text-black font-bold">
                            {product.name}
                        </div>
                        <div className="flex flex-row flex-wrap gap-2 mt-2">
                            <div className="text-md text-gray-500 py-2.5">
                                {product.sku}
                            </div>
                        </div>
                        <div
                            style={{ paddingBottom: 15 }}
                            className={`flex flex-row items-center react-star-mobile ${responsiveStyles["react-star-mobile"]}`}
                        >
                            <ReactStars
                                classNames={`flex-initial ${responsiveStyles["flex-initial-mobile"]}`}
                                count={5}
                                // onChange={ratingChanged}
                                size={24}
                                value={
                                    parseInt(
                                        product?.rating_review_percentage
                                    ) / 20 || 0
                                }
                                activeColor="#F2994A"
                                edit={false}
                                isHalf={true}
                            />

                            <div
                                className={`flex-auto font-normal text-gray-600 text-xs px-10 ${responsiveStyles["rating-value-label-mobile"]}`}
                            >
                                <div className="text-lg">
                                    <span className="text-lg font-semibold text-gray-800">
                                        {/* 0.0 */}
                                    </span>{" "}
                                    ({product?.rating_review_count || 0}{" "}
                                    Reviews)
                                </div>
                            </div>
                        </div>
                        <div className="text-md text-black-500 py-2.5">
                            <p className="mb-2 font-bold">Description:</p>
                            {product.description}
                        </div>
                        <div>
                            {product.custom_is_preorder_active &&
                            product.custom_is_preorder_duration &&
                            product.custom_is_preorder_active !== "0" ? (
                                <p className="text-lg text-gray-600 font-bold mb-2">
                                    Pre order time :{" "}
                                    {product.custom_is_preorder_duration} days
                                </p>
                            ) : null}
                        </div>
                        <div className="flex flex-row flex-wrap gap-4">
                            {product.regular_price == product.final_price ? (
                                <div className="text-2xl font-semibold text-gray-800 mt-5.5">
                                    {moneyFormat(product.final_price)}
                                </div>
                            ) : (
                                <div className="flex flex-col">
                                    <div className="text-lg font-semibold line-through text-gray-600 mt-5.5">
                                        {moneyFormat(product.regular_price)}
                                    </div>
                                    <div className="text-2xl font-semibold text-red-500 mt-5.5">
                                        {moneyFormat(product.final_price)}
                                    </div>
                                </div>
                            )}

                            <div className="flex self-end lg:mx-4">
                                {(!product.custom_is_preorder_active &&
                                    product.qty > 0) ||
                                (product.custom_is_preorder_active === "0" &&
                                    product.qty > 0) ? (
                                    <span>
                                        <img
                                            style={{
                                                objectFit: "contain",
                                                maxWidth: 15,
                                                display: "inline",
                                                height: 15,
                                            }}
                                            src="/img/in-stock.png"
                                        />
                                        <span
                                            style={{ verticalAlign: "middle" }}
                                            className="text-lg text-gray-600 font-bold px-2"
                                        >
                                            In Stock
                                        </span>
                                    </span>
                                ) : (product.custom_is_preorder_active &&
                                      product.qty > 0) ||
                                  (product.custom_is_preorder_active !== "0" &&
                                      product.qty > 0) ? (
                                    <span className="text-lg text-gray-600 font-bold">
                                        Pre Order
                                    </span>
                                ) : (
                                    <span>Out of Stock</span>
                                )}
                            </div>
                        </div>

                        <div className="flex flex-row flex-wrap gap-2 items-center space-x-4 qty-container mt-8">
                            <div className="flex-none">
                                <button
                                    onClick={() =>
                                        setState({
                                            ...state,
                                            qty: state.qty - 1,
                                        })
                                    }
                                    className="bg-gray-100 p-4 z-0"
                                >
                                    -
                                </button>
                                <input
                                    type="number"
                                    className="relative py-4 bg-gray-100 z-10 outline-none text-md w-16 text-center max-w-sm font-normal text-gray-600 font-semibold"
                                    style={{
                                        WebkitAppearance: "none",
                                        MozAppearance: "textfield",
                                        border: "none",
                                        zIndex: 10,
                                        display: "inline-block",
                                    }}
                                    min="1"
                                    value={state.qty}
                                    onChange={(e) => {
                                        let value = e.target.value;

                                        if (parseInt(value) <= 0) {
                                            value = 1;
                                        }

                                        setState({ ...state, qty: value });
                                    }}
                                />
                                <button
                                    onClick={() =>
                                        setState({
                                            ...state,
                                            qty: parseInt(state.qty) + 1,
                                        })
                                    }
                                    className="bg-gray-100 p-4 z-0"
                                >
                                    +
                                </button>
                            </div>

                            <div>
                                <button
                                    className={
                                        `bg-secondary text-white font-semibold py-4 px-12 rounded disabled:opacity-50 disabled:cursor-default ` +
                                        (loading
                                            ? `${styles["cursor-disabled"]}`
                                            : "")
                                    }
                                    disabled={product.qty < 1 || loading}
                                    onClick={(e) => addToCart(e)}
                                >
                                    Add to cart
                                </button>
                            </div>
                        </div>

                        <p
                            className={`text-red-600 text-base font-bold ${
                                product.qty > 1 ? "hidden" : ""
                            }`}
                        >
                            *Out of Stock
                        </p>
                        <div className="mt-4">
                            <button
                                onClick={() => addWishlist()}
                                className="text-base text-dark-grey py-2 px-4 rounded border border-grey flex flex-row space-x-3 items-center"
                            >
                                <IconWishlist width={16} height={16} />
                                <span>Add to Wishlist</span>
                            </button>
                        </div>
                    </div>
                </div>

                <div className="flex-auto lg:mt-12">
                    <Tab
                        paddingStyles={`${responsiveStyles["detail-description"]}`}
                        responsiveStyles={`${responsiveStyles["flex-tab"]} ${responsiveStyles["justify-between"]}`}
                        tabs={state.tabs}
                        onChange={(index) => onChangeTab(index)}
                    >
                        {state.tabActive == 0 && (
                            <table style={{ width: "100%" }}>
                                <tr>
                                    <th
                                        className="p-1.5"
                                        colSpan={2}
                                        style={{ textAlign: "left" }}
                                    >
                                        Main
                                    </th>
                                    {/* <th>Contact</th> */}
                                </tr>
                                {product.tab_more_information.product_attributes
                                    ? product.tab_more_information.product_attributes.map(
                                          (data, index) => {
                                              return (
                                                  <tr key={"detail-" + index}>
                                                      <td
                                                          className="text-base"
                                                          style={{
                                                              padding: "0.5em",
                                                              width: "20vw",
                                                          }}
                                                      >
                                                          <p
                                                              className="text-gray-700"
                                                              style={{
                                                                  lineHeight:
                                                                      "1.25em",
                                                                  fontWeight:
                                                                      "bold",
                                                              }}
                                                          >
                                                              {data.name}
                                                          </p>
                                                      </td>{" "}
                                                      {/* backgroundColor:'#d3d3d3' */}
                                                      <td
                                                          className="text-base"
                                                          style={{
                                                              padding: "0.5em",
                                                              verticalAlign:
                                                                  "top",
                                                              lineHeight:
                                                                  "1.25em",
                                                          }}
                                                      >
                                                          {data.value}
                                                      </td>
                                                  </tr>
                                              );
                                          }
                                      )
                                    : null}
                            </table>
                            //  <p>
                            //     Perfect for class, work or the gym, the Wayfarer
                            //     Messenger Bag is packed with pockets. The dual-buckle
                            //     flap closure reveals an organizational panel, and the
                            //     roomy main compartment has spaces for your laptop and a
                            //     change of clothes. An adjustable shoulder strap and
                            //     easy-grip handle promise easy carrying.
                            //  </p>
                        )}
                        {state.tabActive === 1 && (
                            <div style={{ minHeight: 200 }}>
                                {product.tab_document_product
                                    ? product.tab_document_product.map(
                                          (data, index) => {
                                              return (
                                                  <div
                                                      className="flex mb-1.5 max-content"
                                                      key={"document-" + index}
                                                  >
                                                      <span>
                                                          <img src="/img/pdf-icon.png" />
                                                      </span>
                                                      <span>
                                                          <Link
                                                              href={data.value}
                                                          >
                                                              <a
                                                                  className="underline text-blue-600"
                                                                  target="_blank"
                                                              >
                                                                  <p
                                                                      className="text-base"
                                                                      style={{
                                                                          lineHeight:
                                                                              "32px",
                                                                          padding:
                                                                              "0 0.5em",
                                                                      }}
                                                                  >
                                                                      {
                                                                          data.name
                                                                      }
                                                                  </p>
                                                              </a>
                                                          </Link>
                                                      </span>
                                                      <span
                                                          className="text-base"
                                                          style={{
                                                              lineHeight:
                                                                  "32px",
                                                          }}
                                                      >
                                                          PDF
                                                      </span>
                                                  </div>
                                              );
                                          }
                                      )
                                    : null}
                            </div>
                        )}

                        {state.tabActive === 2 && (
                            <div className="px-1 xl:px-2">
                                <h1 className="text-lg">Customer Reviews</h1>

                                <div>
                                    {product.tab_review ? (
                                        product.tab_review.map(
                                            (data, index) => {
                                                return (
                                                    <div
                                                        key={"review-" + index}
                                                        className="pb-4 mb-2 border-b"
                                                    >
                                                        <h3 className="text-lg font-semibold text-black">
                                                            {data.title}
                                                        </h3>
                                                        <div className="">
                                                            <div className="flex items-center">
                                                                <span className="text-base mr-2">
                                                                    {
                                                                        data.rating_code
                                                                    }
                                                                </span>
                                                                <ReactStars
                                                                    classNames={`flex-initial ${responsiveStyles["flex-initial-mobile"]}`}
                                                                    count={5}
                                                                    // onChange={ratingChanged}
                                                                    size={24}
                                                                    value={
                                                                        parseInt(
                                                                            data.rating_percent
                                                                        ) /
                                                                            20 ||
                                                                        0
                                                                    }
                                                                    activeColor="#F2994A"
                                                                    edit={false}
                                                                    isHalf={true}
                                                                />
                                                            </div>
                                                            <div className="text-base">
                                                                <p>
                                                                    {
                                                                        data.detail
                                                                    }
                                                                </p>
                                                                <span>
                                                                    Review by{" "}
                                                                    {
                                                                        data.nickname
                                                                    }{" "}
                                                                    {dateFormat(
                                                                        data.created_at
                                                                    )}
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                );
                                            }
                                        )
                                    ) : (
                                        <div></div>
                                    )}
                                </div>

                                <div className="mt-4 text-base xl:w-1/2">
                                    <span>You&#39;re reviewing:</span>
                                    <h1 className="text-lg font-semibold text-black">
                                        {product.name}
                                    </h1>

                                    <form onSubmit={handleSubmitReview}>
                                        <div className="mb-2">
                                            <label className="block pb-1">
                                                Your Rating *
                                            </label>
                                            <ReactStars
                                                classNames={`flex-initial ${responsiveStyles["flex-initial-mobile"]}`}
                                                count={5}
                                                onChange={handleRatingChange}
                                                size={24}
                                                value={form.rating || 0}
                                                activeColor="#F2994A"
                                            />
                                        </div>

                                        <div className="mb-2">
                                            <label className="block pb-1">
                                                Nickname *
                                            </label>
                                            <input
                                                type="text"
                                                name="nickname"
                                                className="w-full"
                                                onChange={(e) =>
                                                    handleFormChange({
                                                        name: e.target.name,
                                                        value: e.target.value,
                                                    })
                                                }
                                                value={form.nickname || ""}
                                            />
                                        </div>

                                        <div className="mb-2">
                                            <label className="block pb-1">
                                                Summary *
                                            </label>
                                            <input
                                                type="text"
                                                name="title"
                                                className="w-full"
                                                onChange={(e) =>
                                                    handleFormChange({
                                                        name: e.target.name,
                                                        value: e.target.value,
                                                    })
                                                }
                                                value={form.title || ""}
                                            />
                                        </div>

                                        <div className="mb-2">
                                            <label className="block pb-1">
                                                Review *
                                            </label>
                                            <textarea
                                                name="detail"
                                                className="w-full"
                                                onChange={(e) =>
                                                    handleFormChange({
                                                        name: e.target.name,
                                                        value: e.target.value,
                                                    })
                                                }
                                                value={form.detail || ""}
                                            />
                                        </div>

                                        <button
                                            className="py-2 px-4 rounded bg-primary text-white disabled:opacity-50 disabled:cursor-auto"
                                            disabled={loading}
                                        >
                                            Submit Review
                                        </button>

                                        <div
                                            className={`mt-2 ${
                                                formResp.error
                                                    ? "text-red-600"
                                                    : "text-green-600"
                                            } font-bold flex flex-row items-center justify-center ${
                                                formResp === "" ? "hidden" : ""
                                            }`}
                                        >
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                className={`h-5 w-5 mr-1 ${
                                                    formResp.error
                                                        ? ""
                                                        : "hidden"
                                                }`}
                                                viewBox="0 0 20 20"
                                                fill="currentColor"
                                            >
                                                <path
                                                    fillRule="evenodd"
                                                    d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z"
                                                    clipRule="evenodd"
                                                />
                                            </svg>
                                            <p>{formResp.message}</p>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        )}
                    </Tab>
                </div>
            </div>
        </Layout>
    );
};

// export async function getServerSideProps({ params }) {

//    const { id } = params;

//    const [product, menu] = await Promise.all([
//       requestPost("/rest/V1/products/detail-by-sku", {
//          sku: id,
//       }),
//       requestGet("/rest/V1/products/all-categories")
//    ]);

//    console.log("Product", product)

//    return {
//       props: {
//          product: product[0],
//          menu: menu
//       },
//    };
// }
export async function getStaticPaths() {
    const product = await requestGet("/rest/default/V1/products/", {
        "searchCriteria[currentPage]": 0,
        "searchCriteria[pageSize]": 300,
    });

    const paths = product.items.map((item) => ({
        params: { id: item.sku },
    }));

    // for(var i = 0; i < paths.length; i++){
    //     if(paths[i].params.id === 'E31KTU' ||
    //     paths[i].params.id === 'A9N18481' ||
    //     paths[i].params.id === 'DOMF01325' ||
    //     paths[i].params.id === 'LE1M35M721' ||
    //     paths[i].params.id === 'XB4BW34B5' ||
    //     paths[i].params.id === 'XB5FVG5'){
    //         delete paths[i]
    //     }
    // }

    return {
        paths: paths,
        fallback: "blocking", // for new pages, do SSR and show page only when html is generated completely
        //   fallback: false, // for new pages, do SSR and show page only when html is generated completely
    };
}

export async function getStaticProps({ params }) {
    const { id } = params;

    try {
        const res = await requestPost("/rest/V1/products/detail-by-sku", {
            sku: id,
        });

        const menu = await requestGet("/rest/V1/products/all-categories", {});

        return {
            props: {
                product: res[0],
                menu: menu,
            },
            //  },
            revalidate: 60,
        };
    } catch (error) {
        console.log("error in detail page", error);
    }
}

export default DetailProduct;

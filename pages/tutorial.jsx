import React, { useState, useEffect } from "react";
import ReactPlayer from "react-player/lazy";
import { requestGet } from "../lib/axios";
import Layout from "../components/Theme/Layout";

const Tutorial = () => {
    const [video, setVideo] = useState();
    const [gridNumber, setGridNumber] = useState();

    const fetchData = async () => {
        try {
            const res = await requestGet("/rest/V1/default/tutorial");
            setVideo(res[0].data);
            setGridNumber(res[0].config.gridColumn);
        } catch (error) {
            console.log("error in tutorial page", error);
        }
    };

    const gridOption =
        gridNumber == 1
            ? "lg:grid-cols-1"
            : gridNumber == 2
            ? "lg:grid-cols-2"
            : gridNumber == 3
            ? "lg:grid-cols-3"
            : gridNumber == 4
            ? "lg:grid-cols-4"
            : "";

    const gridHeight =
        gridNumber == 1
            ? ""
            : gridNumber == 2
            ? "lg:h-96"
            : gridNumber == 3
            ? "lg:h-52"
            : gridNumber == 4
            ? "lg:h-40"
            : "";

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <Layout title="Tutorial">
            <div className="max-w-screen-xl mx-auto">
                <h1 className="w-full text-xl md:text-2xl font-bold text-center p-8">
                    Tutorial
                </h1>
                <div
                    className={`grid grid-cols-1 ${gridOption} gap-2 p-8 mx-8`}
                >
                    {video
                        ? video.map((item, index) => {
                              return (
                                  <div className="w-full" key={index}>
                                      <div className={`w-full sm:h-96 ${gridHeight} bg-gray-200`}>
                                          <ReactPlayer
                                              width={"100%"}
                                              height={"100%"}
                                              controls={true}
                                              url={item.link}
                                          />
                                      </div>
                                      <div className="w-full p-4 text-center font-bold">
                                          {item.title}
                                      </div>
                                  </div>
                              );
                          })
                        : ""}
                </div>
            </div>
        </Layout>
    );
};

export default Tutorial;

import React, { useState, useContext, useEffect, useCallback } from 'react'
import {
    CarouselProvider,
    Slider,
    Slide,
    ButtonBack,
    ButtonNext,
} from 'pure-react-carousel'
import Image from "next/image"
import Layout from '../components/Theme/Layout'
import { requestGet } from '../lib/axios'
import { MenuContext } from '../contexts/MenuContext'
import { IconBriefcase } from '../assets/illustrations/index'
import styles from "../styles/Pages/Home.module.css"
import "pure-react-carousel/dist/react-carousel.es.css";

const Portfolio = (props) => {
    const { menuProps } = props;
    const { setMenu } = useContext(MenuContext);
    const useMediaQuery = (width) => {
        const [targetReached, setTargetReached] = useState(false);
      
        const updateTarget = useCallback((e) => {
          if (e.matches) {
            setTargetReached(true);
          } else {
            setTargetReached(false);
          }
        }, []);
      
        useEffect(() => {
          const media = window.matchMedia(`(max-width: ${width}px)`);
          media.addEventListener("change", updateTarget);
      
          // Check on mount (callback is not called until a change occurs)
          if (media.matches) {
            setTargetReached(true);
          }
      
          return () => media.removeEventListener("change", updateTarget);
        }, []);
        return targetReached;
    };
    const isBreakpoint = useMediaQuery(768);

    const portfolioItem = [
        {
            id: 1,
            company: "Cinema XXI",
            description: "Remote Control - Monitoring For Theater",
        },
        {
            id: 2,
            company: "PLTA Batang Agam, Padang",
            description: "Automatic Remote Trash Rack System",
        },
        {
            id: 3,
            company: "PT. Cipta Kawan Teknik Abadi",
            description: "Water Treatment",
        },
        {
            id: 4,
            company: "PT. Hitachi Metal",
            description: "Power Monitoring Expert 2",
        },
        {
            id: 5,
            company: "PT. Inovasi Panel",
            description: "Panel Distribusi",
        },
        {
            id: 6,
            company: "PT. Sharpindo",
            description: "DOL Starter 2",
        },
        {
            id: 7,
            company: "PT. Surya Toto",
            description: "Panel Pompa",
        },
        {
            id: 8,
            company: "PT. Tekno Fluida",
            description: "Oil Pump",
        },
    ]

    const portfolioImage = [
        {
            id: 1,
            name: "Cinema XXI - Remote Control - Monitoring For Theater",
            img: "https://trimitra.limecommerce.work/media/mageplaza/bannerslider/banner/image/c/i/cinema_xxi_-_remote_control___monitoring_for_theater.png"
        },
        {
            id: 2,
            name: "PT. Cipta Kawan Teknik Abadi - Water Treatment",
            img: "https://trimitra.limecommerce.work/media/mageplaza/bannerslider/banner/image/p/t/pt._cipta_kawan_teknik_abadi_-_water_treatment.jpg"
        },
        {
            id: 3,
            name: "PT. Inovasi Panel - Panel Distribusi",
            img: "https://trimitra.limecommerce.work/media/mageplaza/bannerslider/banner/image/p/t/pt._inovasi_panel_-_panel_distribusi.png"
        },
        {
            id: 4,
            name: "PT. Surya Toto - Panel Pompa",
            img: "https://trimitra.limecommerce.work/media/mageplaza/bannerslider/banner/image/p/t/pt._surya_toto_-_panel_pompa.png"
        },
        {
            id: 5,
            name: "PLTA Batang Agam, Padang - Automatic Remote Trash Rack System",
            img: "https://trimitra.limecommerce.work/media/mageplaza/bannerslider/banner/image/p/l/plta_batang_agam_padang_-_automatic_remote_trash_rack_system.png"
        },
        {
            id: 6,
            name: "PT. Hitachi Metal - Power Monitoring Expert 2",
            img: "https://trimitra.limecommerce.work/media/mageplaza/bannerslider/banner/image/p/t/pt._hitachi_metal_-_power_monitoring_expert_2.png"
        },
        {
            id: 7,
            name: "PT. Sharpindo - DOL Starter 2",
            img: "https://trimitra.limecommerce.work/media/mageplaza/bannerslider/banner/image/p/t/pt._sharpindo_-_dol_starter_2.png"
        },
        {
            id: 8,
            name: "PT. Tekno Fluida - Oil Pump",
            img: "https://trimitra.limecommerce.work/media/mageplaza/bannerslider/banner/image/p/t/pt._tekno_fluida_-_oil_pump.png"
        },
    ];

    useEffect(() => {
        setMenu(menuProps);
    }, [menuProps]);

    return (
        <Layout title='Portfolio'>
            <div className="custom-container mx-auto text-base p-10">
                <h1 className="font-semibold text-xl text-center mb-6">Portfolio</h1>
                <div className="flex flex-col xl:flex-row flex-wrap mb-10">
                    {portfolioItem.map((item, index) =>{
                        return (
                        <div className="flex items-center xl:w-1/2 my-2" key={"portfolio-"+index}>
                            <IconBriefcase />
                            <div className="ml-2">
                                <h3 className="font-semibold">{ item.company }</h3>
                                <span>{ item.description }</span>
                            </div>
                        </div>
                        )
                    })}
                    
                </div>

                <div className="mb-10">
                    {portfolioImage.length > 0 && (
                        <div className={styles.carouselContainer}>
                        {
                            <CarouselProvider
                                naturalSlideWidth={isBreakpoint ? 0 : 233}
                                naturalSlideHeight={isBreakpoint ? 0 : 378}
                                totalSlides={portfolioImage.length}
                                isIntrinsicHeight={true}
                                infinite={true}
                                dragStep={true}
                                visibleSlides={isBreakpoint ? 2 : 5}
                                orientation='horizontal'
                                className=''
                            >
                                <div className='flex flex-row items-center justify-end mb-2'>
                                    <div className='flex flex-row'>
                                        <ButtonBack className='cursor-pointer focus:outline-none'>
                                        <svg
                                            xmlns='http://www.w3.org/2000/svg'
                                            className='h-6 w-6 cursor-pointer'
                                            fill='none'
                                            viewBox='0 0 24 24'
                                            stroke='#5A5F62'
                                        >
                                            <path
                                                strokeLinecap='round'
                                                strokeLinejoin='round'
                                                strokeWidth={2}
                                                d='M15 19l-7-7 7-7'
                                            />
                                        </svg>
                                        </ButtonBack>

                                        <ButtonNext className='cursor-pointer focus:outline-none'>
                                        <svg
                                            xmlns='http://www.w3.org/2000/svg'
                                            className='h-6 w-6 cursor-pointer'
                                            fill='none'
                                            viewBox='0 0 24 24'
                                            stroke='#5A5F62'
                                        >
                                            <path
                                                strokeLinecap='round'
                                                strokeLinejoin='round'
                                                strokeWidth={2}
                                                d='M9 5l7 7-7 7'
                                            />
                                        </svg>
                                        </ButtonNext>
                                    </div>
                                </div>

                                <Slider classNameTray='gap-6 md:gap-4 sm-gap-2'>
                                    {portfolioImage.map((data, index) => {
                                        return (
                                        <Slide index={index} key={index}>
                                            <div className="shadow bg-white rounded h-full">
                                                <Image 
                                                    src={ data.img }
                                                    objectFit="cover"
                                                    height={310}
                                                    width={233}
                                                />
                                                <h3 className="p-2 text-base">{ data.name }</h3>
                                            </div>
                                        </Slide>
                                        );
                                    })}
                                </Slider>
                            </CarouselProvider>
                        }
                        </div>
                    )}
                </div>
            </div>
            <div className="bg-primary opacity-10 w-full absolute z-0" style={{ height: 238, zIndex: -99, transform: `translate(${0}px, -${278}px)`}}>
            </div>
        </Layout>
    )
}

export async function getStaticProps() {
    let resp = {};
 
    try {
        const [categories] = await Promise.all([
            requestGet("/rest/V1/products/all-categories")
        ]);
    
        resp = {
            menuProps: categories
        }

    } catch (error) {
        console.log("error in index page", error);
    }
 
    return {
       props: resp,
       revalidate: 10,
    };
}
 

export default Portfolio

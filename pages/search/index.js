import React, { useEffect, useContext, useState, useCallback } from "react";
import { requestDelete, requestGet } from "../../lib/axios";
import { useRouter } from 'next/router'
import Layout from "../../components/Theme/Layout";
import { Carousel, Product } from "../../components";
import { MenuContext } from "../../contexts/MenuContext";
import useUser from "../../lib/useUser";
import { moneyFormat } from "../../lib/helper";
// import styles from "../../styles/Pages/Wishlist.module.css"
import Link from "next/link"
import AsyncLocalStorage from '@createnextapp/async-local-storage'
import Pagination from "next-pagination";
import themePagination from "../../styles/Theme/Pagination.module.css";
import { route } from "next/dist/server/router";

const Search = (props) => {
    const [searchResults, setSearchResults] = useState();
    const { setMenu } = useContext(MenuContext);
    const [totalCount, setTotalCount] = useState();
    const [searchLoaded, setSearchLoaded] = useState(false);
    const [searchPaginationLoaded, setSearchPaginationLoaded] = useState(true);
    const [hideSearchResults, setHideSearchResults] = useState(false);
    const [newSearch, setNewSearch] = useState(false);
    const [menuLoaded, setMenuLoaded] = useState(false);
    const [currentPage, setCurrentPage] = useState(1);
    const [paginationState, setPaginationState] = useState();
    const [renderPagination, setRenderPagination] = useState(true);
    const [paginationArrayState, setPaginationArrayState] = useState();
    const router = useRouter()
    let paginationRange;
    const paginationArray = [];
    var { page } = router.query
    const [pageState, setPageState] = useState(page);
    // console.log(page+' page')
    const getSearchResults = async () => {
        let searchResultsData
        try{
            searchResultsData = await AsyncLocalStorage.getItem('searchResults')
        } catch(e){

        }
        setSearchResults(JSON.parse(searchResultsData).items)
        setTotalCount(JSON.parse(searchResultsData).total_count)
        setSearchLoaded(true);
        console.log(searchResults)
        console.log(totalCount+' dsa')
    }
    
    const goToNextPage = async () => {
        page = parseInt(page)+1;  
        // setCurrentPage(page)
        // setSearchResults();
        await searchProductPagination(page);
        // setSearchPaginationLoaded(false);
        setHideSearchResults(true);
        // searchProduct();
        setTimeout(
            function(){
                setSearchPaginationLoaded(true);
            }, 1000
        )
        setTimeout(
            function(){
                setHideSearchResults(false);
            }, 1000
        )
        router.push('/search?page='+page)
        setTimeout(
            function(){
                router.push('/search?page='+page)
            }, 1000
        )
    }
    // const goToNextPage = () => {
    //     page = parseInt(page)+1;  
    //     // setCurrentPage(page)
    //     // setSearchResults();
    //     setNewSearch(true);
    //     setSearchLoaded(false);
    //     // searchProduct();
    //     // controlPagination(page, totalCount)
    //     setTimeout(
    //         function(){
    //             setNewSearch(false);
    //         }, 1000
    //     )
    //     router.push('/search?page='+page)
    //     setTimeout(
    //         function(){
    //             router.push('/search?page='+page)
    //         }, 1000
    //     )
    // }
        
        const goToPageNumber = async (pageNumber) => {  
            // setCurrentPage(page)
            // alert(JSON.stringify(pageNumber))
            // setSearchResults();
            // setNewSearch(true);
            await searchProductPagination(pageNumber);
            // setSearchPaginationLoaded(false);
            setHideSearchResults(true);
            // searchProduct();
            setTimeout(
                function(){
                    setSearchPaginationLoaded(true);
                }, 1000
            )
            setTimeout(
                function(){
                    setHideSearchResults(false);
                }, 1000
            )
            router.push('/search?page='+pageNumber)
            setTimeout(
                function(){
                    router.push('/search?page='+pageNumber)
                }, 1000
            )
            // router.reload()
            // controlPagination(pageNumber, totalCount)
        }
        
        const goToPrevPage = async () => {
            page = parseInt(page)-1;  
            await searchProductPagination(page);
            // setSearchPaginationLoaded(false);
            setHideSearchResults(true);
            // searchProduct();
            setTimeout(
                function(){
                    setSearchPaginationLoaded(true);
                }, 1000
            )
            setTimeout(
                function(){
                    setHideSearchResults(false);
                }, 1000
            )
            router.push('/search?page='+page)
            setTimeout(
                function(){
                    router.push('/search?page='+page)
                }, 1000
            )
        }
        // const goToPrevPage = () => {
        //     page = parseInt(page)-1;  
        //         // setCurrentPage(page)
        //         // setSearchResults();
        //     setNewSearch(true);
        //     setSearchLoaded(false);
        //     setTimeout(
        //         function(){
        //             setNewSearch(false);
        //         }, 1000
        //     )
        //     // searchProduct();
        //     // controlPagination(page, totalCount)
        //     router.push('/search?page='+page)
        //     setTimeout(
        //         function(){
        //             router.push('/search?page='+page)
        //         }, 1000
        //     )
        // }

    const controlPagination = (pageValue, totalCount) => {
        paginationRange = totalCount / 10;
        // setRenderPagination(false);
        // var pageValue = pageState;
        setPageState(pageValue)
        setPaginationState(paginationRange)
        console.log(pageValue+' babse')
        console.log(Math.ceil(paginationRange)+' paginationRange')
        console.log(pageValue+' babse')
        if(pageValue && pageValue === 3){
            console.log(pageValue+' pageValuee')
        }
        if(pageValue && pageValue!==1 && pageValue >= 3 && (Math.ceil(paginationRange)-pageValue) >=2){
            const paginationArrayPointer = [];
            for(var i = 0 ; i < Math.ceil(paginationRange); i++){
                paginationArrayPointer.push((pageValue)+(i-2));
            }
            console.log(JSON.stringify(paginationArrayState)+' paginationArrayState a')
            setPaginationArrayState(paginationArrayPointer);
            setRenderPagination(true);
            setTimeout(
                function(){
                    console.log(JSON.stringify(paginationArrayState)+' paginationArrayState a')
                }, 2000
            )
        } else if(pageValue && pageValue === 2 && Math.ceil(paginationRange)!==pageValue){
            const paginationArrayPointer = [];
            for(var i = 0 ; i < Math.ceil(paginationRange); i++){
                paginationArrayPointer.push((parseInt(pageValue))+(i-1));
            }
            setPaginationArrayState(paginationArrayPointer);
            setRenderPagination(true);
            setTimeout(
                function(){
                    console.log(JSON.stringify(paginationArrayState)+' paginationArrayState b')
                }, 2000
            )
        } else if(pageValue && (Math.ceil(paginationRange)-pageValue) === 1 && pageValue!==1 && pageValue>=5) {
            const paginationArrayPointer = [];
            for(var i = 0 ; i < Math.ceil(paginationRange); i++){
                paginationArrayPointer.push((pageValue)+(i-3));
            }
            setPaginationArrayState(paginationArrayPointer);
            setRenderPagination(true);
            setTimeout(
                function(){
                    console.log(JSON.stringify(paginationArrayState)+' paginationArrayState c')
                }, 2000
            )
            // console.log(JSON.stringify(paginationArrayState)+' paginationArrayState c')
        } else if(Math.ceil(paginationRange) === pageValue && pageValue!==1 && pageValue>=5){
            const paginationArrayPointer = [];
            for(var i = 0 ; i < Math.ceil(paginationRange); i++){
                paginationArrayPointer.push((pageValue)+(i-4));
            }
            setPaginationArrayState(paginationArrayPointer);
            setRenderPagination(true);
            setTimeout(
                function(){
                    console.log(JSON.stringify(paginationArrayState)+' paginationArrayState d')
                }, 2000
            )
        }
    }

    const detectNewSearch = async () => {
        const newSearch = await AsyncLocalStorage.getItem('isSearch')
        if(newSearch==='true'){
            setPaginationState()
            searchProduct();
            // setTimeout(
            //     function(){
            //         setSearchLoaded(true);
            //     }, 1000
            // )
            controlPagination(parseInt(page), totalCount)
            AsyncLocalStorage.setItem('isSearch','false')
        }
    }

    const searchProduct = async () => {
        // setSearchLoaded(false);
        let searchInputData
        try{
            searchInputData = await AsyncLocalStorage.getItem('searchInput')
        } catch(e) {
            
        }
        
        // else {
            //     setCurrentPage(1)
            // }
            // // alert(currentPage)
        let searchResults;
        if(page){
            // setCurrentPage(page)
            searchResults = await requestGet('/rest//V1/products-search',{
                'searchCriteria[filter_groups][0][filters][0][field]':'name',
                'searchCriteria[filter_groups][0][filters][0][value]':`%`+searchInputData+`%`,
                'searchCriteria[filter_groups][0][filters][0][condition_type]':'like',
                'searchCriteria[filter_groups][1][filters][0][field]':'status',
                'searchCriteria[filter_groups][1][filters][0][value]':1,
                'searchCriteria[filter_groups][1][filters][0][condition_type]':'eq',
                'searchCriteria[pageSize]':10,
                'searchCriteria[currentPage]':page
            })
        } else {
            searchResults = await requestGet('/rest//V1/products-search',{
                'searchCriteria[filter_groups][0][filters][0][field]':'name',
                'searchCriteria[filter_groups][0][filters][0][value]':`%`+searchInputData+`%`,
                'searchCriteria[filter_groups][0][filters][0][condition_type]':'like',
                'searchCriteria[filter_groups][1][filters][0][field]':'status',
                'searchCriteria[filter_groups][1][filters][0][value]':1,
                'searchCriteria[filter_groups][1][filters][0][condition_type]':'eq',
                'searchCriteria[pageSize]':10,
                'searchCriteria[currentPage]':1
            })
        }
        if(searchResults.items.length > 0){
            // setSearchLoaded(false);
            console.log(JSON.stringify(searchResults)+' searchResults')
            // console.log(JSON.stringify(searchResults))
            // AsyncLocalStorage.setItem('searchResults',JSON.stringify(searchResults))
            // router.push('/search')
            // setSearchResults();
            if(searchResults.items.length>0){
                setTimeout(
                    function(){
                        controlPagination(parseInt(page), searchResults.total_count) 
                        setSearchResults(searchResults.items)
                    }, 800
                )
            }
            setSearchLoaded(true)
            setTotalCount(searchResults.total_count)
            if(!paginationState){
                let paginationRange = searchResults.total_count / 10;
                setPaginationState(paginationRange)
                for(var i = 0 ; i < Math.ceil(paginationRange); i++){
                    paginationArray.push(i+1);
                }
                setPaginationArrayState(paginationArray);
            }
            // if(searchResults.total_count && searchResults.items.length>0){
            // }
            // setSearchLoaded(false);
            // console.log(Math.ceil(paginationRange)+' babse')
            // setPaginationState(paginationRange)
            // for(var i = 0 ; i < Math.ceil(paginationRange); i++){
            //     paginationArray.push(i+1);
            // }
            // setPaginationArrayState(paginationArray);
            // console.log(JSON.stringify(paginationArrayState)+' paginationArrayState')
            // console.log(paginationArray+' assse')
            // setPageState(page)
        }
    }

    const searchProductPagination = async (page) => {
        // setSearchLoaded(false);
        let searchInputData
        try{
            searchInputData = await AsyncLocalStorage.getItem('searchInput')
        } catch(e) {
            
        }
        
        // else {
            //     setCurrentPage(1)
            // }
            // // alert(currentPage)
        let searchResults;
        // alert(page)
        if(page){
            // setCurrentPage(page)
            searchResults = await requestGet('/rest//V1/products-search',{
                'searchCriteria[filter_groups][0][filters][0][field]':'name',
                'searchCriteria[filter_groups][0][filters][0][value]':`%`+searchInputData+`%`,
                'searchCriteria[filter_groups][0][filters][0][condition_type]':'like',
                'searchCriteria[filter_groups][1][filters][0][field]':'status',
                'searchCriteria[filter_groups][1][filters][0][value]':1,
                'searchCriteria[filter_groups][1][filters][0][condition_type]':'eq',
                'searchCriteria[pageSize]':10,
                'searchCriteria[currentPage]':page
            })
        } else {
            searchResults = await requestGet('/rest//V1/products-search',{
                'searchCriteria[filter_groups][0][filters][0][field]':'name',
                'searchCriteria[filter_groups][0][filters][0][value]':`%`+searchInputData+`%`,
                'searchCriteria[filter_groups][0][filters][0][condition_type]':'like',
                'searchCriteria[filter_groups][1][filters][0][field]':'status',
                'searchCriteria[filter_groups][1][filters][0][value]':1,
                'searchCriteria[filter_groups][1][filters][0][condition_type]':'eq',
                'searchCriteria[pageSize]':10,
                'searchCriteria[currentPage]':1
            })
        }
        setSearchResults();
        if(searchResults.items.length > 0){
            // setSearchLoaded(false);
            console.log(JSON.stringify(searchResults)+' searchResults')
            // console.log(JSON.stringify(searchResults))
            // AsyncLocalStorage.setItem('searchResults',JSON.stringify(searchResults))
            // router.push('/search')
            // setSearchResults();
            setTimeout(
                function(){
                    controlPagination(parseInt(page), searchResults.total_count) 
                    // setSearchResults()
                    setSearchResults(searchResults.items)
                    // setSearchPaginationLoaded(true)
                }, 1500
            )
            setTotalCount(searchResults.total_count)
            if(!paginationState){
                let paginationRange = searchResults.total_count / 10;
                setPaginationState(paginationRange)
                for(var i = 0 ; i < Math.ceil(paginationRange); i++){
                    paginationArray.push(i+1);
                }
                setPaginationArrayState(paginationArray);
            }
        }
    }

    const setMenuProps = async () => {
        const menuProps = await requestGet("/rest/V1/products/all-categories");
        if(menuProps){
            setMenu(menuProps)
            setMenuLoaded(true)
        }
        console.log('menuprops')
    }

    useEffect(() => {
        detectNewSearch();
        if(!menuLoaded){
            setMenuProps();
        }
        if(!searchLoaded){
            console.log(currentPage+' currentpage')
            setPaginationState();
            searchProduct();
            // controlPagination(parseInt(page), totalCount)
            // controlPagination(currentPage, totalCount)
            // setRenderPagination(false)
            // setTimeout(
            //     function(){
            //         controlPagination(2, 3.3)
            //     }, 1000
            // )
            // getSearchResults();
        }
        if(!searchPaginationLoaded){
            console.log(currentPage+' currentpage')
            setPaginationState();
            searchProductPagination();
            // controlPagination(parseInt(page), totalCount)
        }
    }); 
    return(
        <Layout title="Search Product">
            <div className="max-width-1100 mx-auto lg:py-4">
                {/* <p>{JSON.stringify(paginationArrayState.slice(0,5))}</p> */}
            {
                !hideSearchResults
                ?
                <div style={{ minHeight:400 }} className="grid grid-cols-4 grid-cols-2-mobile gap-x-6 gap-y-0 p-4-mobile">
                    {
                        searchResults
                        ?
                        // <p>{JSON.stringify(searchResults)}</p>
                        searchResults.map((data, index) => {
                            return(
                            <div style={{ marginBottom:'1em', minHeight:400 }} key={data.product_id}>
                                {
                                    searchResults.length > 0
                                    ?
                                        <Product data={data}/>
                                    :
                                    // <p>No results</p>
                                    null
                                }
                                {/* <p>{JSON.stringify(data)}</p>
                                <p>{data.product_id}</p> */}
                            </div>
                            )
                        })
                        :
                        // <p>No results</p>
                        null
                    }
                </div>
                :
                <div style={{ minHeight:400 }}>
                    <p>Please wait...</p>
                </div>
            }
            {
                !hideSearchResults
                ?
                <div className="flex-0-5">
                    <div className="hidden">
                        <Pagination
                            total={totalCount}
                            theme={themePagination}
                            // sizes={[84, 64, 34, 24, 12]}
                        />
                    </div>
                    <nav className="next-pagination p-1em-mobile" aria-label="pagination">
                    <ul className="next-pagination__list">
                        {
                            parseInt(page) !== 1
                            ?
                            <li className="next-pagination__item">
                                <a onClick={() => goToPrevPage()} className="next-pagination__link" aria-label="Previous page">
                                    <svg className="next-pagination__icon" aria-hidden="true" viewBox="0 0 24 24" width="24" height="24">
                                        <path d="M0 0h24v24H0V0z" fill="none">
                                        </path>
                                        <path fill="currentColor" d="M14.71 6.71c-.39-.39-1.02-.39-1.41 0L8.71 11.3c-.39.39-.39 1.02 0 1.41l4.59 4.59c.39.39 1.02.39 1.41 0 .39-.39.39-1.02 0-1.41L10.83 12l3.88-3.88c.39-.39.38-1.03 0-1.41z">
                                        </path>
                                    </svg>
                                </a>
                            </li>
                            :
                            null
                        }
                            {
                                paginationArrayState && paginationArrayState.length > 0
                                ?
                                paginationArrayState.filter((data, index) => index < 5).map((item, index) => {
                                    return(
                                        <li key={index} className="next-pagination__item">
                                            {
                                                parseInt(page) === item || page === item
                                                ?
                                                    <a className="next-pagination__link next-pagination__link--disabled next-pagination__link--current" aria-label="Page 1">
                                                        {item}
                                                    </a>
                                                :
                                                // <Link href={'/search?page='+item}>
                                                    <a onClick={() => goToPageNumber(item)} className="next-pagination__link" aria-label="Page 1">
                                                        {item}
                                                    </a>
                                                // </Link>
                                            }
                                        </li>
                                    )
                                })
                                :
                                null
                            }
                            {/* <li class="Pagination_next-pagination__item__A08yS"><a class="Pagination_next-pagination__link__3zpQL" aria-label="Page 2">2</a></li>
                            <li class="Pagination_next-pagination__item__A08yS"><a class="Pagination_next-pagination__link__3zpQL" aria-label="Page 3">3</a></li>
                            <li class="Pagination_next-pagination__item__A08yS"><a class="Pagination_next-pagination__link__3zpQL" aria-label="Page 4, current page">4</a></li> */}
                            {/* Pagination_next-pagination__link--disabled__1E0wg */}
                            <li className="next-pagination__item">
                                {
                                    paginationArrayState && pageState !== Math.ceil(paginationState) 
                                    ?
                                    <a onClick={() => goToNextPage()} className="next-pagination__link" aria-label="No next page available">
                                        <svg className="next-pagination__icon" aria-hidden="true" viewBox="0 0 24 24" width="24" height="24">
                                            <path d="M0 0h24v24H0V0z" fill="none"></path>
                                            <path fill="currentColor" d="M9.29 6.71c-.39.39-.39 1.02 0 1.41L13.17 12l-3.88 3.88c-.39.39-.39 1.02 0 1.41.39.39 1.02.39 1.41 0l4.59-4.59c.39-.39.39-1.02 0-1.41L10.7 6.7c-.38-.38-1.02-.38-1.41.01z"></path>
                                        </svg>
                                        {/* {
                                            paginationState
                                            ?
                                            <p>{Math.ceil(paginationState)}</p>
                                            :
                                            null
                                        } */}
                                    </a>
                                    : page === Math.ceil(paginationState) ?
                                    null
                                    :
                                    null
                                }
                            </li>
                    </ul>
                    </nav>
                    {/* <PaginationCustom/> */}
                    <div className="p-4-mobile hidden">
                        <button
                            onClick={() => goToPrevPage()}
                            disabled={pageState <= 1}
                        >
                            <img style={{ maxWidth:30 }} src="/img/chevron-left.png"/>
                        </button>
                        <button 
                            className="float-right"
                            onClick={() => goToNextPage()}
                        >
                            <img style={{ maxWidth:30 }} src="/img/chevron-right.png"/>
                        </button>
                    </div>
                </div>
                :
                null
            }
            {/* <div className="p-4-mobile">
                <button
                className="inline-flex"
                onClick={() => goToPrevPage()}
                disabled={page <= 1}
                >
                    <img style={{ maxWidth:30 }} src="/img/chevron-left.png"/>
                    <span style={{ lineHeight:'30px' }}>Prev</span> 
                </button>
                <button className="inline-flex float-right" onClick={() => goToNextPage()}>
                    <span style={{ lineHeight:'30px' }}>Next</span> 
                    <img style={{ maxWidth:30 }} src="/img/chevron-right.png"/>
                </button>
            </div> */}
            {/* {
                totalCount > 0
                ?
                <Pagination
                    total={totalCount}
                    theme={themePagination}
                    sizes={[84, 64, 34, 24, 10]}
                    onClick={() => alert('aaa')}
                />
                :
                null
            } */}
            </div>
        </Layout>
    )
}

export default Search;

// export async function getStaticProps() {
//     let resp = {};
 
//     try {
//        const [categories] = await Promise.all([
//           requestGet("/rest/V1/products/all-categories")
//        ]);
 
//        resp = {
//           menuProps: categories
//        }
//     } catch (error) {
//        console.log("error in search page", error);
//     }
 
//     return {
//        props: resp,
//        revalidate: 10,
//     };
//  }
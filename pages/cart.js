import React, { useState, useEffect, useContext, useCallback, createRef, useRef } from 'react'
import Image from "next/image";
import { useRouter } from "next/router";
import Layout from '../components/Theme/Layout'
import useUser from "../lib/useUser";
import useCart from "../lib/useCart";
import { requestGet, requestDelete, requestPut } from '../lib/axios'
import { moneyFormat } from '../lib/helper'
import { MenuContext } from '../contexts/MenuContext'
import { IconDelete } from "../assets/illustrations";
import styles from '../styles/Pages/Cart.module.css'
import Link from 'next/link'
import responsiveStyles from "../styles/Components/CartResponsive.module.css";
import AsyncLocalStorage from '@createnextapp/async-local-storage';

const Cart = (props) => {
    const useFocus = () => {
        const htmlElRef = useRef(null)
        const setFocus = () => {htmlElRef.current &&  htmlElRef.current.focus()}
    
        return [ htmlElRef, setFocus ] 
    }
    const [inputRef, setInputFocus] = useFocus();
    const { user } = useUser({ redirectTo: "/login" });
    // const { user } = useUser();
    const { cart, loadingCart, mutateCart } = useCart(user);
    const [width, setWidth] = React.useState(0);
    console.log("Cart", cart)
    const { menu } = props;
    const { setMenu } = useContext(MenuContext);
    const router = useRouter();

    const [ currentIndex, setCurrentIndex ] = useState(0);
    const [ cartState, setCartState ] = useState([]);
    const [ updatedQty, setUpdatedQty ] = useState([]);
    const [ error, setError ] = useState(false);
    const [ deleteItem, setDeleteItem ] = useState("");
    const [ loadingUpdate, setLoadingUpdate ] = useState(false);
    const input_ref = createRef();
    const { items_qty: totalItems, subtotal } = cart || {};
    const [ voucher, setVoucher ] = useState("");
    const [ voucherStatus, setVoucherStatus ] = useState(false);
    const [ loadingVoucher, setLoadingVoucher ] = useState(false);

    console.log("Total items on cart", totalItems);

    useEffect(() => {
        setMenu(menu);
        console.log(window.innerWidth)
        setWidth(window.innerWidth);
    }, [setMenu, menu]);

    useEffect(() => {
        setCartState(cart?.items)
        detectQty();
        detectSubtotal();

        if (cart?.coupon_code) {
            setVoucher(cart.coupon_code);
        } else {
            setVoucher("");
        }
        
    }, [cart, setCartState])

    useEffect(() => {
        if (error) {
            setTimeout(() => {
                setError(false);
            }, 3000)
        }
    }, [error])

    const ItemsCart = () => {
        let itemsRow = [];
        cartState?.map((item, index) => {
            {
                isBreakpoint
                ?
                itemsRow.push(<ItemRowResponsive data={[item, index]} key={index} />)
                :
                itemsRow.push(<ItemRow data={item} key={index} />)
            }
        })

        return itemsRow
    }

    const detectQty = async () => {
        if(cart && cart.items_qty>0){
            setTimeout(
                async function(){
                    // alert(JSON.stringify(cart.items_qty))
                    await AsyncLocalStorage.setItem('totalItems', totalItems)
                }, 1000
            )
        } else {
            await AsyncLocalStorage.setItem('totalItems', '0')
        }
    }

    const detectSubtotal = async () => {
        if(cart && cart.subtotal>0){
            setTimeout(
                async function(){
                    // alert(JSON.stringify(cart.items_qty))
                    await AsyncLocalStorage.setItem('subtotal', subtotal)
                }, 1000
            )
        } else {
            await AsyncLocalStorage.setItem('subtotal', '0')
        }
    }

    const useMediaQuery = (width) => {
        const [targetReached, setTargetReached] = useState(false);
      
        const updateTarget = useCallback((e) => {
          if (e.matches) {
            setTargetReached(true);
          } else {
            setTargetReached(false);
          }
        }, []);
      
        useEffect(() => {
          const media = window.matchMedia(`(max-width: ${width}px)`);
          media.addListener(updateTarget);
      
          // Check on mount (callback is not called until a change occurs)
          if (media.matches) {
            setTargetReached(true);
          }
      
          return () => media.removeListener(updateTarget);
        }, []);
        return targetReached;
    };

    const isBreakpoint = useMediaQuery(768);

    const ItemRow = ({data}) => {
        console.log("Props", data)
        const subTotalItem = data.price * data.qty;
        return (
            <tr>
                <td className="xl:p-3 xl:pl-0 text-left flex items-center">
                    <Link href={`/product/${data.sku}`}>
                        <a>
                            <Image className="flex-1" src={data.image_url} alt="Trimitra Location" layout="fixed" width={72} height={72} /> 
                        </a>
                    </Link>
                    <p title={data.name} className={`flex-1 pl-2`}>{data.name}</p>
                </td>
                <td className="xl:p-3">{moneyFormat(data.price)}</td>
                <td className="xl:p-3">
                    <div className="flex items-center">
                        <button 
                            className="bg-gray-200 h-6 w-6 rounded disabled:opacity-50 disabled:cursor-auto cursor-pointer"
                            onClick={() => handleQtyChange(data, data.qty - 1)} 
                            disabled={data.qty <= 1}
                        >
                            <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 p-1" viewBox="0 0 20 20" fill="currentColor">
                                <path fillRule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clipRule="evenodd" />
                            </svg>
                        </button>

                        <input 
                            type="text" 
                            className="w-full text-center text-base h-7 rounded mx-1" 
                            value={data.qty}
                            onChange={(e) => handleQtyChange(data, e.target.value)}
                            required
                        />

                        <button 
                            className="bg-gray-200 h-6 w-6 rounded cursor-pointer" 
                            onClick={() => handleQtyChange(data, data.qty + 1)}
                        >
                            <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 p-1" viewBox="0 0 20 20" fill="currentColor">
                                <path fillRule="evenodd" d="M10 3a1 1 0 011 1v5h5a1 1 0 110 2h-5v5a1 1 0 11-2 0v-5H4a1 1 0 110-2h5V4a1 1 0 011-1z" clipRule="evenodd" />
                            </svg>
                        </button>
                    </div>
                </td>
                <td className="xl:p-3">{moneyFormat(subTotalItem)}</td>
                <td>
                    <button 
                        className="disabled:cursor-auto"
                        onClick={() => setDeleteItem(data.item_id)}
                    >
                        <IconDelete width={24} height={24} /> 
                    </button>
                </td>
            </tr>
        )
    }

    const ItemRowResponsive = ({data, key}) => {
        useEffect(() => {
            // setInputFocus();
            if(updatedQty.length > 0){
                const thisInput = document.getElementById(`qtyInput${currentIndex}`);
                // console.log(data[1])
                // alert(thisInput.value())
                if(thisInput!==null){
                    thisInput.focus();
                    // alert('BBB')
                }
            }
        }, [currentIndex])
        console.log("Props", data[0])
        const subTotalItem = data[0].price * data[0].qty;
        return (
            // <p>{JSON.stringify(data)}</p>
            <tr>
                <td className="xl:p-4 xl:pl-0 text-left flex items-center">
                    {/* <input className="mr-4" type="checkbox" /> */}
                    <Link href={`/product/${data[0].sku}`}>
                        <a>
                            <Image className={`flex-1 ${responsiveStyles["img-cart-items"]}`} src={data[0].image_url} alt="Trimitra Location" layout="fixed" width={72} height={72} /> 
                        </a>
                    </Link>
                    <div style={{ width:'100%' }} className="">
                        <p title={data[0].name} className={`flex-1 pl-2 ${responsiveStyles["item-name"]}`}>{data[0].name}</p>
                        <p className="pl-2" style={{ width:'100%' }}>{moneyFormat(data[0].price)}</p>
                        <div className="flex mt-2 ml-2 justify-between">
                            <div className="flex">
                                <button 
                                    className={`bg-gray-200 h-6 w-6 rounded disabled:opacity-50 disabled:cursor-auto cursor-pointer ${responsiveStyles["height-28"]}`}
                                    onClick={() => handleQtyChange(data[0], data[0].qty - 1)} 
                                    disabled={data[0].qty <= 1}
                                >
                                    <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 p-1" viewBox="0 0 20 20" fill="currentColor">
                                        <path fillRule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clipRule="evenodd" />
                                    </svg>
                                </button>
                                    {/* <p style={{ float:'right' }}>{JSON.stringify(key)}</p> */}
                                    <input 
                                        type="text"
                                        name={`qty-${data[1]}`} 
                                        id={`qtyInput${data[1]}`}
                                        className={`input-qty w-full text-center text-base h-7 rounded mx-1 ${responsiveStyles["max-width-50"]}`}
                                        value={data[0].qty}
                                        // ref={input_ref}
                                        ref={inputRef}
                                        min={1}
                                        // ref={}
                                        onChange={(e) => handleQtyChange(data[0], e.target.value, data[1], e)}
                                        // onChange={(e) => console.log(e.target.value)}
                                        required
                                    />
                                <button 
                                    className={`bg-gray-200 h-6 w-6 rounded cursor-pointer ${responsiveStyles["height-28"]}`}
                                    onClick={() => handleQtyChange(data[0], data[0].qty + 1)}
                                >
                                    <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 p-1" viewBox="0 0 20 20" fill="currentColor">
                                        <path fillRule="evenodd" d="M10 3a1 1 0 011 1v5h5a1 1 0 110 2h-5v5a1 1 0 11-2 0v-5H4a1 1 0 110-2h5V4a1 1 0 011-1z" clipRule="evenodd" />
                                    </svg>
                                </button>
                            </div>
                            <button 
                                className="disabled:cursor-auto"
                                onClick={() => setDeleteItem(data[0].item_id)}
                            >
                                <IconDelete width={24} height={24} /> 
                            </button>
                        </div>
                    </div>
                </td>
                {/* <td className="xl:p-4">{moneyFormat(data.price)}</td>
                <td className="xl:p-4">{data.qty}</td>
                <td className="xl:p-4l">{moneyFormat(subTotalItem)}</td> */}
            </tr>
        )
    }

    const handleQtyChange = (item, updateQty, index, e) => {
        console.log(e);
        console.log("updateQty", updateQty)
        let state = [...cartState];
        const itemIndex = checkArrayValue(state, item.item_id);
        console.log("selected items", itemIndex)
        state[itemIndex].qty = updateQty;
        console.log("updated state", state)
        // setInputFocus(true);
        // alert('AAA')
        // const thisInput = document.querySelector(
        //     `input[name=qty-${parseInt(index)}]`
        // );
        setCurrentIndex(index);
        // input_ref.focus();
        
        setCartState(state);

        let updateQtyState = [...updatedQty];
        const updatedItemIndex = checkArrayValue(updateQtyState, item.item_id);
        if (updatedItemIndex !== null) {
            updateQtyState[updatedItemIndex].qty = updateQty;
            console.log("index updated qty", updatedItemIndex)
        } else {
            console.log("index updated qty not found")
            updateQtyState.push(state[itemIndex]);
        }

        setUpdatedQty(updateQtyState);
        

        // setTimeout(
        //     function(){
        //         handleUpdateItems();
        //     }, 2000
        // )
    }

    const checkArrayValue = (array, value) => {
        for (let index = 0; index < array.length; index++) {
            console.log("items to check",array[index].item_id, value)
            if (array[index].item_id == value){
                return index
            }
        }

        return null
    }

    const handleDeleteItems = async () => {
        try {
            const res = await requestDelete(`/rest/default/V1/carts/mine/items/${deleteItem}`, null, user.token);
            setDeleteItem("");
            mutateCart();
            // await AsyncLocalStorage.setItem('isCleared', 'true')
            // alert('Clear')
        } catch (error) {
            setDeleteItem("");
            setError(error.message)
        }
    }

    const handleUpdateItems = async () => {
        setLoadingUpdate(true);
        let items = [];
        updatedQty.forEach(item => {
            const body = {
                "item_id": item.item_id,
                "sku": item.sku,
                "qty": item.qty,
                "quote_id": item.quote_id
            };

            items.push(body);
        });

        const body = {
            cartItem: items
        }
        console.log("body update", body)
        try {
            const res = await requestPut(`/rest/default/V1/carts/mine/items/update`, null, body, user.token);
            console.log("update res", res)
            setUpdatedQty([]);
            mutateCart();
        } catch (error) {
            setError(error.message);
            mutateCart();
        }
        setLoadingUpdate(false);
    }

    const useVoucher = async () => {
        setLoadingVoucher(true);
        if (voucherStatus?.timeout){
            clearTimeout(voucherStatus.timeout);
        }
        
        try {
            const res = await requestPut(`/rest/default/V1/carts/mine/coupons/${voucher}`, null, null, user.token);
            const timeout = setTimeout(() => {
                setVoucherStatus(false);
            }, 4000);
            setVoucherStatus({ error: false, message: "Your coupon was succesfully applied.", timeout: timeout });
            mutateCart();
        } catch (error) {
            const timeout = setTimeout(() => {
                setVoucherStatus(false);
            }, 4000);
            const errMsg = error?.response?.data?.message || "Something went wrong. Please try again."
            setVoucherStatus({ error: true, message: errMsg, timeout: timeout });
        }
        setLoadingVoucher(false);
    }

    const removeVoucher = async () => {
        setLoadingVoucher(true);
        if (voucherStatus?.timeout){
            clearTimeout(voucherStatus.timeout);
        }

        try {
            const res = await requestDelete(`/rest/default/V1/carts/mine/coupons`, null, user.token);
            const timeout = setTimeout(() => {
                setVoucherStatus(false);
            }, 4000);
            setVoucherStatus({ error: false, message: "Your coupon was succesfully removed.", timeout: timeout });
            mutateCart();
        } catch (error) {
            const timeout = setTimeout(() => {
                setVoucherStatus(false);
            }, 4000);
            const errMsg = error?.response?.data?.message || "Something went wrong. Please try again."
            setVoucherStatus({ error: true, message: errMsg, timeout: timeout });
        }
        setLoadingVoucher(false);
    }

    return (
        <Layout title="My Cart">
            <div className="max-width-1136 mx-auto p-5 pt-2 xl:pt-5 mb-40">
                <h1 className={`text-lg font-bold xl:font-normal xl:text-xl text-center xl:text-left text-dark-grey xl:mb-5 ${responsiveStyles["shopping-cart-title"]}`}>Shopping Cart</h1>
                <div className={`flex ${responsiveStyles["cart-container"]}`}>
                    <div className={`flex-1 xl:mr-7 ${styles["cart-items-container"]} mb-4 md:mb-0`}>
                        <form>
                            <table className={`table-fixed w-full text-center text-dark-grey rounded text-base`}>
                                <thead>
                                    <tr className="border-b">
                                        <th className={`xl:p-1 w-5/12 ${responsiveStyles["hidden-xs"]}`}>Item</th>
                                        <th className={`xl:p-1 w-2/12 ${responsiveStyles["hidden-xs"]}`}>Price</th>
                                        <th className={`xl:p-1 w-2/12 ${responsiveStyles["hidden-xs"]}`}>Qty</th>
                                        <th className={`xl:p-1 w-2/12 ${responsiveStyles["hidden-xs"]}`}>Subtotal</th>
                                        <th className={`xl:p-1 w-1/12 ${responsiveStyles["hidden-xs"]}`}></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <ItemsCart />
                                </tbody>
                            </table>

                            <div className={`mt-2 text-red-600 font-bold flex flex-row items-center justify-center ${!error ? 'hidden' : ''}`}>
                                <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 mr-1" viewBox="0 0 20 20" fill="currentColor">
                                    <path fillRule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z" clipRule="evenodd" />
                                </svg>
                                {error}
                            </div>
                        </form>
                    </div>

                    <div className={ `${styles.summaryContainer} ${responsiveStyles.summaryContainer} ${styles.summaryContainerFlex}` }>
                        <h2 className="text-base xl:text-lg text-gray-500 py-2 border-b">Summary</h2>
                        <div className="flex justify-between pt-3">
                            <h3 className="text-sm xl:text-base">Subtotal</h3>
                            <span className="text-sm xl:text-base">{moneyFormat(cart?.subtotal || 0)}</span>
                        </div>
                        <div className="flex justify-between">
                            <h3 className="text-sm xl:text-base">Discount</h3>
                            <span className="text-sm xl:text-base">{moneyFormat(cart?.discount_amount || 0)}</span>
                        </div>
                        <div className={`flex justify-between pb-3 border-b  ${cart?.extension_attributes?.aw_store_credit_amount < 0 ? '' : 'hidden'}`}>
                            <h3 className="text-sm xl:text-base">Store Credit</h3>
                            <span className={`text-sm xl:text-base ${cart?.extension_attributes?.aw_store_credit_amount < 0 ? 'text-green-700' : ''}`}>{moneyFormat(cart?.extension_attributes?.aw_store_credit_amount || 0)}</span>
                        </div>
                        <div className="flex justify-between mt-4 mb-2">
                            <h3 className="text-lg xl:text-lg font-bold">Order Total</h3>
                            <span className="text-lg xl:text-lg font-bold">{moneyFormat(cart?.grand_total || 0)}</span>
                        </div>
                        <button 
                            className={`mr-1 py-2 px-2 bg-secondary mt-2 mb-6 rounded w-full text-white ${updatedQty.length > 0 ? '' : 'invisible'} disabled:opacity-50 disabled:cursor-auto`}
                            disabled={loadingUpdate}
                            type="submit"
                            onClick={handleUpdateItems}
                        >
                            Update Cart
                        </button>
                        <div className={`grid grid-flow-col items-center text-base w-full relative mb-1`}>
                            <input 
                                type="text"
                                name="voucher"
                                placeholder="Enter your discount code" 
                                className="text-base py-2 rounded border-gray-200"
                                value= {voucher }
                                onChange={(e) => setVoucher(e.target.value)}
                                disabled={ cart?.coupon_code }
                            />
                            <button 
                                className={`bg-gray-200 py-2 px-3 h-max rounded text-black font-bold absolute right-0 ${responsiveStyles["button-apply-discount"]} disabled:opacity-50 disabled:cursor-auto`}
                                disabled={ voucher === "" || loadingVoucher}
                                onClick={ cart?.coupon_code ? removeVoucher : useVoucher }
                            >
                                { cart?.coupon_code ? "Remove Coupon" : "Apply" }
                            </button>
                        </div>

                        {
                            voucherStatus &&
                            <p className={`text-base mb-2 text-center ${voucherStatus.error ? 'text-red-600': 'text-green-600'}`}>
                                <span className='inline-block align-middle'>
                                {
                                    voucherStatus.error ? 
                                    <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                        <path fillRule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z" clipRule="evenodd" />
                                    </svg> :
                                    <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                        <path fillRule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clipRule="evenodd" />
                                    </svg>
                                }
                                </span>
                                { voucherStatus.message }
                            </p>
                        }
                        
                        <button 
                            className={`rounded bg-secondary text-white font-bold w-full py-3 mb-4 mt-1 ${styles["proceed-checkout-button"]} disabled:opacity-50 disabled:cursor-auto`} 
                            onClick={() => router.push("/checkout")}
                            disabled={updatedQty.length > 0}
                        >
                            Proceed to Checkout
                        </button>
                    </div>
                </div>
                
            </div>
            <div 
                className={`min-w-screen h-screen animated fadeIn faster fixed left-0 top-0 flex justify-center items-center inset-0 z-50 outline-none focus:outline-none bg-no-repeat bg-center bg-cover ${deleteItem == "" ? 'hidden' : ''}`}
                id="modal-id"
            >
                <div className="absolute bg-black opacity-80 inset-0 z-0"></div>
                <div className="w-full  max-w-lg p-5 relative mx-auto my-auto rounded-xl shadow-lg  bg-white ">
                    <div className="">
                        <div className="text-center p-5 flex-auto justify-center">
                                <h2 className="text-xl font-bold py-4 ">Are you sure?</h2>
                                <p className="text-sm text-gray-500 px-8">Do you really want to delete this item?</p>    
                        </div>
                    
                        <div className="p-3  mt-2 text-center space-x-4 md:block">
                            <button 
                                className="mb-2 md:mb-0 bg-white px-5 py-2 text-sm shadow-sm font-medium tracking-wider border text-gray-600 rounded-full hover:shadow-lg hover:bg-gray-100"
                                onClick={() => setDeleteItem("")}
                            >
                                Cancel
                            </button>
                            <button 
                                className="mb-2 md:mb-0 bg-red-500 border border-red-500 px-5 py-2 text-sm shadow-sm font-medium tracking-wider text-white rounded-full hover:shadow-lg hover:bg-red-600"
                                onClick={handleDeleteItems}
                            >
                                Delete
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    )
}

export async function getStaticProps() {
    let resp = {};

    try {
        const res = await requestGet("/rest/V1/products/all-categories")
        resp.menu = res;
    } catch (error) {
        console.log("error in register page", error)
    }

    return {
        props: resp,
        revalidate: 10
    };
}

export default Cart

import React, { useState, useEffect } from "react";
import Link from "next/link";
import Layout from "../components/Theme/Layout";
import { requestPost } from "../lib/axios";
import LoaderBounce from "../components/Loader/Bounce";
import { useRouter } from "next/router";

const ResetPassword = (props) => {
    const { query } = useRouter();

    const [loading, setLoading] = useState(false);
    const [newpassword, setNewpassword] = useState("");
    const [confirmation, setConfirmation] = useState("");
    const [error, setError] = useState("");
    const [success, setSuccess] = useState(false);
    const [validate, setValidate] = useState(false);
    const [confirmed, setConfirmed] = useState(false);
    const [showPassword, setShowPassword] = useState(false);
    const [showConfirm, setShowConfirm] = useState(false);

    useEffect(() => {
        CheckPassword(newpassword);
        ConfirmPassword(newpassword, confirmation);
    }, [newpassword, confirmation]);

    const CheckPassword = (inputtxt) => {
        var passw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,32}$/;
        if (inputtxt.match(passw)) {
            setValidate(true);
        } else {
            setValidate(false);
        }
    };

    const ConfirmPassword = (newPassword, confirmation) => {
        if (newPassword === confirmation) {
            setConfirmed(true);
        } else {
            setConfirmed(false);
        }
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        let param = {
            email: query.email,
            newPassword: newpassword,
            resetToken: query.token,
        };

        if (confirmed && newpassword.length > 8 && validate) {
            setLoading(true);
            try {
                const res = await requestPost(
                    "/rest/default/V1/customers/resetPassword",
                    param
                );
                setLoading(false);
                setSuccess(true);
                setError("");
            } catch (error) {
                console.log("error logging in", error);
                setError({ server: error.message });
                setLoading(false);
            }
        } else {
            setLoading(false);
        }
    };

    return (
        <Layout title="Reset Password">
            <div className="container flex justify-center mx-auto min-h-75vh">
                <div className="flex flex-col self-center py-12 h-full w-full">
                    <div className="w-full lg:w-1/2 lg:mx-auto text-lg md:text-xl text-center font-extrabold pb-12">
                        Reset Password
                    </div>
                    <form
                        className={`${
                            success ? "hidden" : "block"
                        } px-5 mx-auto w-full lg:w-1/2`}
                        onSubmit={handleSubmit}
                    >
                        <div className="mx-auto p-12 border rounded-lg flex flex-col justify-center items-center gap-2">
                            <label
                                htmlFor="new-password"
                                className="block w-full text-lg"
                            >
                                New Password
                            </label>
                            <div className="relative w-full">
                                <input
                                    type={showPassword ? "text" : "password"}
                                    name="new-password"
                                    id="new-password"
                                    className="block w-full px-4 border rounded"
                                    placeholder="New Password"
                                    value={newpassword}
                                    onChange={(e) =>
                                        setNewpassword(e.target.value)
                                    }
                                />
                                <div className="absolute inset-y-0 right-0 pr-3 mt-1 mr-3 flex items-center text-lg text-gray-600">
                                    <i
                                        className={`fa fa-eye ${
                                            showPassword ? "hidden" : "block"
                                        }`}
                                        onClick={() =>
                                            setShowPassword(!showPassword)
                                        }
                                        aria-hidden="true"
                                    ></i>
                                    <i
                                        className={`fa fa-eye-slash ${
                                            !showPassword ? "hidden" : "block"
                                        }`}
                                        onClick={() =>
                                            setShowPassword(!showPassword)
                                        }
                                        aria-hidden="true"
                                    ></i>
                                </div>
                            </div>
                            <label
                                htmlFor="new-password"
                                className="block w-full text-lg"
                            >
                                Confirmation Password
                            </label>
                            <div className="relative w-full">
                                <input
                                    type={!showConfirm ? "password" : "text"}
                                    name="new-password"
                                    id="new-password"
                                    className="block w-full px-4 border rounded"
                                    placeholder="Confirmation Password"
                                    value={confirmation}
                                    onChange={(e) =>
                                        setConfirmation(e.target.value)
                                    }
                                />
                                    <div className="absolute inset-y-0 right-0 pr-3 mt-1 mr-3 flex items-center text-lg text-gray-600">
                                            <i
                                                className={`fa fa-eye ${
                                                    showConfirm ? "hidden" : "block"
                                                }`}
                                                onClick={() => setShowConfirm(!showConfirm)}
                                                aria-hidden="true"
                                            ></i>
                                            <i
                                                className={`fa fa-eye-slash ${
                                                    !showConfirm ? "hidden" : "block"
                                                }`}
                                                onClick={() => setShowConfirm(!showConfirm)}
                                                aria-hidden="true"
                                            ></i>
                                        </div>
                            </div>
                            <div
                                name="errors"
                                className="flex flex-col items-start"
                            >
                                <div
                                    className={`mt-2 text-red-600 font-bold flex flex-row items-center justify-center ${
                                        validate ? "hidden" : ""
                                    }`}
                                >
                                    <div>
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            className="h-5 w-5 mr-1"
                                            viewBox="0 0 20 20"
                                            fill="currentColor"
                                        >
                                            <path
                                                fillRule="evenodd"
                                                d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z"
                                                clipRule="evenodd"
                                            />
                                        </svg>
                                    </div>
                                    <div>
                                        Password must contain at least one number ,
                                        one uppercase and lowercase letter, and at
                                        least 8 characters
                                    </div>
                                </div>
                                <div
                                    className={`mt-2 text-red-600 font-bold flex flex-row items-center justify-center ${
                                        confirmed ? "hidden" : ""
                                    }`}
                                >
                                    <div>
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            className="h-5 w-5 mr-1"
                                            viewBox="0 0 20 20"
                                            fill="currentColor"
                                        >
                                            <path
                                                fillRule="evenodd"
                                                d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z"
                                                clipRule="evenodd"
                                            />
                                        </svg>
                                    </div>
                                    <div>
                                        New password and confirmation password must match.
                                    </div>
                                </div>
                                <div
                                    className={`mt-2 text-red-600 font-bold flex flex-row items-center justify-center ${
                                        !error ? "hidden" : ""
                                    }`}
                                >
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        className="h-5 w-5 mr-1"
                                        viewBox="0 0 20 20"
                                        fill="currentColor"
                                    >
                                        <path
                                            fillRule="evenodd"
                                            d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z"
                                            clipRule="evenodd"
                                        />
                                    </svg>
                                    {error}
                                </div>
                            </div>

                            <button
                                type="submit"
                                className="mt-4 py-2 block w-3/4 font-semibold text-white text-lg bg-secondary rounded-sm"
                            >
                                <div
                                    className={`flex flex-row items-center justify-center my-3 ${
                                        !loading ? "hidden" : ""
                                    }`}
                                >
                                    <LoaderBounce />
                                </div>
                                <span className={`${loading ? "hidden" : ""}`}>
                                    Reset Password
                                </span>
                            </button>
                        </div>
                    </form>
                    <div
                        className={`${
                            success ? "grid" : "hidden"
                        } flex flex-col gap-8 justify-center`}
                    >
                        <div className="text-lg text-center font-medium">
                            <span>
                                {`Your password has been successfully reset .`}
                            </span>
                        </div>
                        <Link href="/login">
                            <a className="w-full bg-secondary mx-auto py-2 text-center text-white text-lg font-semibold">
                                <span>Go to Login</span>
                            </a>
                        </Link>
                    </div>
                </div>
            </div>
        </Layout>
    );
};

export default ResetPassword;

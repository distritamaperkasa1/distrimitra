import React, { useEffect, useContext, useState } from "react";
import Layout from "../components/Theme/Layout";
import Link from "next/link";
import { requestGet, requestPost } from "../lib/axios";
import { MenuContext } from "../contexts/MenuContext";
import styles from "../styles/Pages/Contact.module.css";
import Image from "next/image";

const Contact = (props) => {
    const { setMenu } = useContext(MenuContext);
    const [name, setName] = useState();
    const [email, setEmail] = useState();
    const [phoneNumber, setPhoneNumber] = useState();
    const [comment, setComment] = useState();
    const [loading, setLoading] = useState(false);
    const { menuProps } = props;
    const sendContactUs = async (e) => {
        e.preventDefault();
        if (
            name &&
            email &&
            email.match(
                /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            ) &&
            phoneNumber &&
            comment
        ) {
            let sendFormContact;
            try {
                sendFormContact = await requestPost(
                    "/rest/V1/contactus?" +
                        "contactForm[name]=" +
                        name +
                        "&contactForm[email]=" +
                        email +
                        "&contactForm[telephone]=" +
                        phoneNumber +
                        "&contactForm[comment]=" +
                        comment
                );
            } catch (e) {
                alert(e);
            }
            if (sendFormContact) {
                // alert(JSON.stringify(sendFormContact))
                alert(sendFormContact.message);
                // if(sendFormContact.message !== 'The email address is invalid. Verify the email address and try again.'){
                if (!sendFormContact.message.match(/try again/g)) {
                    setName("");
                    setEmail("");
                    setPhoneNumber("");
                    setComment("");
                }
            }
        } else {
            if (
                email &&
                !email.match(
                    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                )
            ) {
                alert("Wrong email format, please try again.");
            } else if (!name || !email || !phoneNumber || !comment) {
                alert("Please fill all required fields");
            }
        }
    };

    const changeName = (e) => {
        setName(e.target.value);
    };

    const changeEmail = (e) => {
        setEmail(e.target.value);
    };

    const changePhoneNumber = (e) => {
        setPhoneNumber(e.target.value);
    };
    const changeComment = (e) => {
        setComment(e.target.value);
    };

    useEffect(() => {
        setMenu(menuProps);
    }, [setMenu, menuProps]);
    return (
        <Layout title="Contact Us">
            <div className={`mx-auto mt-10`}>
                <h3 className="font-bold text-xl text-center">Contact Us</h3>

                <div className={styles["inner-container"]}>
                    <form
                        onSubmit={(e) => sendContactUs(e)}
                        className="absolute left-0 top-0 w-full p-6 shadow-xl rounded z-10 bg-white xl:flex xl:flex-row-reverse xl:justify-end"
                    >
                        <div className="flex-1">
                            <div className="xl:flex">
                                <div className="mt-2 xl:mt-10 xl:flex-1 xl:mr-20">
                                    {/* <p>{name}</p> */}
                                    <label className="block text-sm font-bold">
                                        Your Name *
                                    </label>
                                    <input
                                        value={name}
                                        onChange={(e) => changeName(e)}
                                        className="w-full rounded mt-1 border-gray-400"
                                        type="text"
                                        placeholder="Your Name *"
                                    ></input>
                                </div>

                                <div className="mt-4 xl:mt-10 xl:flex-1">
                                    {/* <p>{email}</p> */}
                                    <label className="block text-sm font-bold">
                                        Your Email *
                                    </label>
                                    <input
                                        value={email}
                                        onChange={(e) => changeEmail(e)}
                                        className="w-full rounded mt-1 border-gray-400"
                                        type="text"
                                        placeholder="Your Email *"
                                    ></input>
                                </div>
                            </div>

                            <div className="mt-4">
                                {/* <p>{phoneNumber}</p> */}
                                <label className="block text-sm font-bold">
                                    Phone Number *
                                </label>
                                <input
                                    value={phoneNumber}
                                    onChange={(e) => changePhoneNumber(e)}
                                    type="tel"
                                    className="w-full rounded mt-1 border-gray-400"
                                    placeholder=""
                                ></input>
                            </div>

                            <div className="mt-4">
                                {/* <p>{comment}</p> */}
                                <label className="block text-sm font-bold">
                                    Your Message *
                                </label>
                                <textarea
                                    value={comment}
                                    onChange={(e) => changeComment(e)}
                                    className={`w-full rounded mt-1 border-gray-400 ${styles["inner-container--message"]}`}
                                ></textarea>
                            </div>

                            <div className="w-full mt-10 xl:mb-4">
                                <button
                                    className="bg-secondary text-white text-lg font-bold rounded py-2 w-full disabled:opacity-50 disabled:cursor-auto"
                                    disabled={loading}
                                >
                                    Submit
                                </button>
                            </div>
                        </div>

                        <div className="mt-12 mx-1 xl:ml-16 xl:mr-32">
                            <div className="xl:mr-10">
                                <div className="flex flex-col">
                                    <a href="https://goo.gl/maps/H3YBEu3xg95WhTwh7">
                                        <div className="flex flex-row justify-center xl:justify-start">
                                            <Image
                                                src="/geo-icon.png"
                                                alt="Trimitra Location"
                                                layout="fixed"
                                                width={20}
                                                height={20}
                                            />
                                            <h3 className="font-bold text-sm ml-2">
                                                PT. DISTRITAMA PERKASA
                                            </h3>
                                        </div>
                                    </a>

                                    <p className="text-sm text-secondary-dark-grey mt-4 text-center xl:text-left xl:ml-7">
                                        Komplek Arcadia Blok G6 No 6 <br />
                                        Jl. Daan Mogot KM 21 <br />
                                        Tangerang 15122 - Indonesia
                                    </p>
                                </div>

                                <div className="flex flex-row mt-10 justify-center xl:justify-start">
                                    <Image
                                        src="/telepone-icon.png"
                                        alt="Trimitra Contact"
                                        layout="fixed"
                                        width={24}
                                        height={24}
                                    />

                                    <div className="ml-2">
                                        <div className="grid grid-cols-8 gap-x-2">
                                            <h3 className="font-bold text-sm mb-3 col-span-1">
                                                Ph:
                                            </h3>
                                            <h3 className="font-bold text-sm mb-3 col-span-7">
                                                021 - 29006315
                                            </h3>
                                            <h3 className="font-bold text-sm mb-3 col-span-1"></h3>
                                            <h3 className="font-bold text-sm mb-3 col-span-7">
                                                021 - 29006316
                                            </h3>
                                            <h3 className="font-bold text-sm mb-3 col-span-1"></h3>
                                            <h3 className="font-bold text-sm mb-3 col-span-7">
                                                021 - 29006317
                                            </h3>
                                        </div>
                                    </div>
                                </div>

                                <div className="flex flex-row mt-10 justify-center xl:justify-start">
                                    <Image
                                        src="/mail-icon.png"
                                        alt="Trimitra Email"
                                        layout="fixed"
                                        width={24}
                                        height={24}
                                    />

                                    <div className="ml-2">
                                        <h3 className="font-bold text-sm mb-3">
                                            mall-listrik@distritamaperkasa.com
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            <div className="text-center xl:text-left">
                                <h3 className="font-bold text-sm mb-4 mt-10">
                                    JOIN US :
                                </h3>
                                <Link
                                    href="https://www.facebook.com/distritama.perkasa.50"
                                    passHref={true}
                                >
                                    <a target="_blank">
                                        <Image
                                            src="/fb-logo.png"
                                            alt="Logo Trimitra FB"
                                            width={32}
                                            height={32}
                                        />
                                    </a>
                                </Link>
                            </div>
                        </div>
                    </form>
                </div>

                <div className={styles["maps-container"]}>
                    <Image
                        src="/distritama-map.png"
                        alt="Map Distritama"
                        width={1440}
                        height={700}
                        layout="fill"
                        className="object-cover"
                    />
                </div>
            </div>
        </Layout>
    );
};

export default Contact;

export async function getStaticProps() {
    let resp = {};

    try {
        const res = await requestGet("/rest/V1/products/all-categories");
        resp.menuProps = res;
    } catch (error) {
        console.log("error in register page", error);
    }

    return {
        props: resp,
        revalidate: 10,
    };
}

import React from "react";
import Link from "next/link";
import Layout from "../components/Theme/Layout";
import styles from "../styles/Pages/Custom404.module.css";

const Custom404Page = (props) => {
    return (
        <Layout title="Ops page was not found!">
            <div
                className={`container flex flex-col text-center justify-center items-center mx-auto min-h-75vh ${styles.notfound}`}
            >
                <h1>404</h1>
                <h2>Oops! Nothing was found</h2>
                <p className="max-w-2xl">
                    The page you are looking for might have been removed had its
                    name changed or is temporarily unavailable.{" "}
                    <Link href="/">Return to homepage</Link>
                </p>
            </div>
        </Layout>
    );
};

export default Custom404Page;

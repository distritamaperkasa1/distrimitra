import React, { useState, useContext, useEffect, useCallback } from 'react'
import {
    CarouselProvider,
    Slider,
    Slide,
    ButtonBack,
    ButtonNext,
} from 'pure-react-carousel'
import Image from "next/image"
import Layout from '../components/Theme/Layout'
import { requestGet } from '../lib/axios'
import { MenuContext } from '../contexts/MenuContext'
import { IconBriefcase } from '../assets/illustrations/index'
import styles from "../styles/Pages/Home.module.css"
import "pure-react-carousel/dist/react-carousel.es.css";

const Project = (props) => {
    const { menuProps, gallery, list } = props;
    const { setMenu } = useContext(MenuContext);
    const useMediaQuery = (width) => {
        const [targetReached, setTargetReached] = useState(false);
      
        const updateTarget = useCallback((e) => {
          if (e.matches) {
            setTargetReached(true);
          } else {
            setTargetReached(false);
          }
        }, []);
      
        useEffect(() => {
          const media = window.matchMedia(`(max-width: ${width}px)`);
          media.addEventListener("change", updateTarget);
      
          // Check on mount (callback is not called until a change occurs)
          if (media.matches) {
            setTargetReached(true);
          }
      
          return () => media.removeEventListener("change", updateTarget);
        }, []);
        return targetReached;
    };
    const isBreakpoint = useMediaQuery(768);

    useEffect(() => {
        setMenu(menuProps);
    }, [menuProps]);

    return (
        <Layout title='Project'>
            <div className="custom-container mx-auto text-base p-10">
                <h1 className="font-semibold text-xl text-center mb-6">Project</h1>
                <div className="flex flex-col xl:flex-row flex-wrap mb-10">
                    {list?.map((item, index) => {
                        return (
                        <div className="flex items-center xl:w-1/2 my-2" key={"portfolio-"+index}>
                            <IconBriefcase />
                            <div className="ml-2">
                                <h3 className="font-semibold">{ item.name }</h3>
                                <span>{ item.description }</span>
                            </div>
                        </div>
                        )
                    })}
                    
                </div>

                <div className="mb-10">
                    {gallery?.length > 0 && (
                        <div className={styles.carouselContainer}>
                        {
                            <CarouselProvider
                                naturalSlideWidth={isBreakpoint ? 0 : 233}
                                naturalSlideHeight={isBreakpoint ? 0 : 378}
                                totalSlides={gallery.length}
                                isIntrinsicHeight={true}
                                infinite={true}
                                dragStep={true}
                                visibleSlides={isBreakpoint ? 2 : 5}
                                orientation='horizontal'
                                className=''
                                step={isBreakpoint ? 2 : 5}
                            >
                                <div className='flex flex-row items-center justify-end mb-2'>
                                    <div className='flex flex-row'>
                                        <ButtonBack className='cursor-pointer focus:outline-none'>
                                        <svg
                                            xmlns='http://www.w3.org/2000/svg'
                                            className='h-6 w-6 cursor-pointer'
                                            fill='none'
                                            viewBox='0 0 24 24'
                                            stroke='#5A5F62'
                                        >
                                            <path
                                                strokeLinecap='round'
                                                strokeLinejoin='round'
                                                strokeWidth={2}
                                                d='M15 19l-7-7 7-7'
                                            />
                                        </svg>
                                        </ButtonBack>

                                        <ButtonNext className='cursor-pointer focus:outline-none'>
                                        <svg
                                            xmlns='http://www.w3.org/2000/svg'
                                            className='h-6 w-6 cursor-pointer'
                                            fill='none'
                                            viewBox='0 0 24 24'
                                            stroke='#5A5F62'
                                        >
                                            <path
                                                strokeLinecap='round'
                                                strokeLinejoin='round'
                                                strokeWidth={2}
                                                d='M9 5l7 7-7 7'
                                            />
                                        </svg>
                                        </ButtonNext>
                                    </div>
                                </div>

                                <Slider classNameTray='gap-6 md:gap-4 sm-gap-2'>
                                    {gallery.map((data, index) => {
                                        return (
                                        <Slide index={index} key={index}>
                                            <div className="shadow bg-white rounded h-full">
                                                <Image 
                                                    src={ data.image_url }
                                                    objectFit="cover"
                                                    height={310}
                                                    width={233}
                                                />
                                                <h3 className="p-2 text-base">{ data.name }</h3>
                                            </div>
                                        </Slide>
                                        );
                                    })}
                                </Slider>
                            </CarouselProvider>
                        }
                        </div>
                    )}
                </div>
            </div>
            <div className="bg-primary opacity-10 w-full absolute z-0" style={{ height: 238, zIndex: -99, transform: `translate(${0}px, -${278}px)`}}>
            </div>
        </Layout>
    )
}

export async function getStaticProps() {
    let resp = {};
 
    try {
        const [categories, portfolio] = await Promise.all([
            requestGet("/rest/V1/products/all-categories"),
            requestGet("/rest/default/V1/portofolio")
        ]);
    
        resp = {
            menuProps: categories,
            gallery: portfolio[0].gallery,
            list: portfolio[0].list
        }

    } catch (error) {
        console.log("error in index page", error);
    }
 
    return {
       props: resp,
       revalidate: 10,
    };
}
 

export default Project

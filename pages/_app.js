
import Router from "next/router";
import nProgress from "nprogress";
import { MenuProvider } from "../contexts/MenuContext";
import "../styles/nprogress.css";
import "../styles/globals.css";
import { SWRConfig } from "swr";
import fetch from "../lib/fetchJson";

Router.events.on("routeChangeStart", nProgress.start);
Router.events.on("routeChangeError", nProgress.done);
Router.events.on("routeChangeComplete", nProgress.done);

function MyApp({ Component, pageProps }) {
   return (
      <SWRConfig
         value={{
         fetcher: fetch,
         onError: (err) => {
            console.error(err);
         },
         }}
      >
         <MenuProvider>
            <Component {...pageProps} />
         </MenuProvider>
      </SWRConfig>
   );
}

export default MyApp;

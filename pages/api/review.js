import { requestPost } from "../../lib/axios";
import withSession from "../../lib/session";

export default withSession(async (req, res) => {
  const param = await req.body;
  const userSession = req.session.get("user");

  try {
    const submitReview = await requestPost('/rest/V1/reviews', param, userSession.token || "");
    const formResponse = {
        error: false,
        message: "Thank you for your review!"
    }
    res.json(formResponse);
  } catch (error) {
    console.error(error)
    const formResponse = {
        error: true,
        message: error.message || "Sorry, there is something wrong"
    }
    const { response: fetchResponse } = error;
    res.status(fetchResponse?.status || 500).json(formResponse);
  }
});
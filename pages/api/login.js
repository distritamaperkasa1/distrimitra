import { requestGet, requestPost } from "../../lib/axios";
import withSession from "../../lib/session";
import moment from "moment";

export default withSession(async (req, res) => {
  const param = await req.body;
  try {
    const token = await requestPost('/rest/V1/integration/customer/token', param, '');
    console.log("token", token);
    const [accountData] = await Promise.allSettled([
      requestGet('/rest/default/V1/customers/me', null, token),
    ]);

    const accountValue = accountData.status = "fulfilled" && accountData.value;
    delete accountValue.addresses;

    const remember = param.remember;
    let expiredTime = moment().add(3, 'hours');
    console.log("Expired", !remember && expiredTime)
    console.log("date now", moment())
    const user = { 
      isLoggedIn: true, 
      token: token, 
      data: accountValue,
      expiredTime: !remember && expiredTime
    };
    
    console.log("user: ", user);
    req.session.set("user", user);
    await req.session.save();
    res.json(user);
  } catch (error) {
    const { response: fetchResponse } = error;
    console.error(fetchResponse)
    res.status(fetchResponse?.status || 500).json(fetchResponse.data);
  }
});
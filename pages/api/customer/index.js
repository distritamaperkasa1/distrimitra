import { requestGet } from "../../../lib/axios";
import withSession from "../../../lib/session";

export default withSession(async (req, res) => {
  const userSession = req.session.get("user");

  try {
    const address = await requestGet('/rest/default/V1/customers/me', null, userSession.token);
    console.log("address: ", address)
    res.json(address);
  } catch (error) {
    console.error(error)
    const { response: fetchResponse } = error;
    res.status(fetchResponse?.status || 500).json(error.message);
  }
});
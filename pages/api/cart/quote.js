import { requestPost } from "../../../lib/axios";
import withSession from "../../../lib/session";

export default withSession(async (req, res) => {
  const userSession = req.session.get("user");

  try {
    const quote = await requestPost('/rest/default/V1/carts/mine', null, userSession.token);
    res.json(quote);
  } catch (error) {
    console.error(error)
    const { response: fetchResponse } = error;
    res.status(fetchResponse?.status || 500).json(error.message);
  }
});
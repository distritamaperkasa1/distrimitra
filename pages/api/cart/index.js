import { requestGet } from "../../../lib/axios";
import withSession from "../../../lib/session";

export default withSession(async (req, res) => {
  const userSession = req.session.get("user");

  try {
    const cart = await requestGet('/rest/V1/carts/mine/all', null, userSession.token);
    console.log("cart: ", cart)
    res.json(cart);
  } catch (error) {
    console.error(error)
    const { response: fetchResponse } = error;
    if (fetchResponse?.status === 404) {
      res.json({})
    } else {
      res.status(fetchResponse?.status || 500).json(error.message);
    }
  }
});
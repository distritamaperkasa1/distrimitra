import { requestGet, requestPost } from "../../../lib/axios";
import withSession from "../../../lib/session";
import fetchJson from "../../../lib/fetchJson";

export default withSession(async (req, res) => {
  const param = await req.body;
  const userSession = req.session.get("user");

  try {
    const addItem = await requestPost('/rest/default/V1/carts/mine/items', param, userSession.token);
    const user = { 
      ...userSession,
    };
    console.log("user: ", user);
    req.session.set("user", user);
    await req.session.save();
    res.json(user);
  } catch (error) {
    console.error(error)
    const { response: fetchResponse } = error;
    res.status(fetchResponse?.status || 500).json(error.message);
  }
});
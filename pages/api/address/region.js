import { requestGet } from "../../../lib/axios";

export default async function handler(req, res) {
    try {
        const getRegions = await requestGet('/rest/default/V1/directory/countries')
        console.log("Get region", getRegions)
        const resp = {
            countryId: getRegions[0].id,
            region: getRegions[0].available_regions
        }
        res.json(resp);
    } catch (error) {
        const { response: fetchResponse } = error;
        res.status(fetchResponse?.status || 500).json(error.message);
    }
}
import { requestGet } from "../../../lib/axios";

export default async function handler(req, res) {
    const param = req.query;
    const { regionId } = param;
    try {
        const getRegions = await requestGet(`/rest/V1/customer/get-city?search=&regionId=${regionId}`)
        res.json(getRegions);
    } catch (error) {
        const { response: fetchResponse } = error;
        res.status(fetchResponse?.status || 500).json(error.message);
    }
}
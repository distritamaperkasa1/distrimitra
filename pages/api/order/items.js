import { requestGet } from "../../../lib/axios";
import withSession from "../../../lib/session";

export default withSession(async (req, res) => {
    const userSession = req.session.get("user");
    const param = await req.query;
    try {
        const order = await requestGet(`/rest/V1/orders/itemorder/${param.orderId}`, null, userSession.token);
        console.log("order item: ", order)
        res.json(order);
    } catch (error) {
        res.status(fetchResponse?.status || 500).json(error.message);
    }
});
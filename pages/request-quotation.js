import React, { useEffect, useContext, useState } from 'react'
import Layout from "../components/Theme/Layout";
import { requestGet, requestPost } from "../lib/axios";
import { MenuContext } from '../contexts/MenuContext';
import styles from '../styles/Pages/RequestQuotation.module.css';

const Contact = (props) => {
    const { setMenu } = useContext(MenuContext);
    const { menuProps } = props;
    const [name, setName] = useState();
    const [email, setEmail] = useState();
    const [phoneNumber, setPhoneNumber] = useState();
    const [company, setCompany] = useState();
    const [description, setDescription] = useState();
    const [other, setOther] = useState();
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        setMenu(menuProps);
    }, [setMenu, menuProps])

    const sendQuotation = async (e) => {
        e.preventDefault();
        let sendQuotationForm
        if(name && email && email.match(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        ) && phoneNumber && company && description){
            try{
                sendQuotationForm = 
                await requestPost('/rest/V1/quotation?'+
                    'quotationForm[name]='+name+
                    '&quotationForm[email]='+email+
                    '&quotationForm[telephone]='+phoneNumber+
                    '&quotationForm[company]='+company+
                    '&quotationForm[description]='+description+
                    '&quotationForm[other]='+other
                )
            } catch(e){
                alert(e)
            }
            if(sendQuotationForm){
                // alert(JSON.stringify(sendFormContact))
                alert(sendQuotationForm.message)
                if(!sendQuotationForm.message.match(/try again/g)){
                    setName('')
                    setEmail('')
                    setPhoneNumber('')
                    setCompany('')
                    setDescription('')
                    setOther('')
                }
            }
        } else {
            // fix email match
            if(email && !email.match(
                /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                )){
                    alert('Wrong email format, please try again.')
            } 
            if(!name || !email || !phoneNumber || !company || !description) {
                alert('Please fill all required fields')
            }
        }
    }

    return (
        <Layout title='Request for Quotation'>
            <div className={`mx-auto mt-10`}>
                <h3 className="font-bold text-xl text-center">Request for Quotation</h3>

                <div className={styles['inner-container']}>
                    <form onSubmit={(e) => sendQuotation(e)} className="absolute left-0 top-0 w-full p-6 shadow-xl rounded z-10 bg-white xl:pt-0 xl:px-36">
                        <div className="flex-1">
                            <div className="xl:flex">
                                <div className="mt-2 xl:mt-10 xl:flex-1 xl:mr-20">
                                    <label className="block text-sm font-bold">Your Name *</label>
                                    <input value={name} onChange={(e) => setName(e.target.value)} className="w-full rounded mt-1 border-gray-400" type="text" placeholder="Your Name *"></input>
                                </div>

                                <div className="mt-4 xl:mt-10 xl:flex-1">
                                    <label className="block text-sm font-bold">Your Email *</label>
                                    <input value={email} onChange={(e) => setEmail(e.target.value)} className="w-full rounded mt-1 border-gray-400" type="text" placeholder="Your Email *"></input>
                                </div>
                            </div>

                            <div className="mt-4">
                                <label className="block text-sm font-bold">Phone Number *</label>
                                <input value={phoneNumber} onChange={(e) => setPhoneNumber(e.target.value)} className="w-full rounded mt-1 border-gray-400" type="text" placeholder=""></input>
                            </div>

                            <div className="mt-4">
                                <label className="block text-sm font-bold">Company *</label>
                                <input value={company} onChange={(e) => setCompany(e.target.value)} className="w-full rounded mt-1 border-gray-400" type="text" placeholder=""></input>
                            </div>

                            <div className="mt-4">
                                <label className="block text-sm font-bold">Description *</label>
                                <textarea value={description} onChange={(e) => setDescription(e.target.value)} className={`w-full rounded mt-1 border-gray-400 ${styles['inner-container--desc']}`}></textarea>
                            </div>

                            <div className="mt-4">
                                {/* fix other */}
                                <label className="block text-sm font-bold">Other</label>
                                <input value={other} onChange={(e) => setOther(e.target.value)} className="w-full rounded mt-1 border-gray-400" type="text" placeholder=""></input>
                            </div>
                        
                            <div className={`w-full mt-10 xl:mb-4 mb-6 ${styles['submit-btn']}`}>
                                <button 
                                    className="bg-secondary text-white text-lg font-bold rounded py-2 w-full disabled:opacity-50 disabled:cursor-auto"
                                    disabled={loading}
                                >
                                    Submit Request
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

                <div className={styles['colored-container']}>
                </div>
                
            </div>
        </Layout>
    )
}

export default Contact;

export async function getStaticProps() {
    let resp = {};

    try {
        const res = await requestGet("/rest/V1/products/all-categories")
        resp.menuProps = res;
    } catch (error) {
        console.log("error in request quotation page", error)
    }

    return {
        props: resp,
        revalidate: 10
    };
}
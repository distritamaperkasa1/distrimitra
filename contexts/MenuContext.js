import { createContext, useState } from 'react';

const MenuContext = createContext();

const MenuProvider = ({ children }) => {
    const [menu, setMenu] = useState([]);
    const [token, setToken] = useState("");
    const [account, setAccount] = useState({});
    const [checkout, setCheckout] = useState({});

    return (
        <MenuContext.Provider
            value={{
                menu,
                setMenu,
                token,
                setToken,
                account,
                setAccount,
                checkout,
                setCheckout
            }}
        >
            {children}
        </MenuContext.Provider>
    );
};

export { MenuProvider, MenuContext };
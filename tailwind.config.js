const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
    content: [
        "./pages/**/*.{js,ts,jsx,tsx}",
        "./components/**/*.{js,ts,jsx,tsx}",  
    ],
    theme: {
        fontFamily: {
            sans: ["Open Sans", ...defaultTheme.fontFamily.sans],
        },
        fontSize: {
            xs: ["10px", "16px"],
            sm: ["12px", "18px"],
            base: ["14px", "24px"],
            lg: ["16px", "32px"],
            xl: ["24px", "32px"],
            "2xl": ["32px", "40px"],
            "3xl": ["40px", "39px"],
        },
        minHeight: {
            0: "0",
            "1/4": "25%",
            "1/2": "50%",
            "3/4": "75%",
            full: "100%",
            "25vh": "25vh",
            "50vh": "50vh",
            "75vh": "75vh",
            "100vh": "100vh",
        },
        extend: {
            screens: {
                "1xl": "1440px",
            },
            colors: {
                primary: "var(--primary)",
                "dark-blue": "var(--dark-blue)",
                secondary: "var(--secondary)",
                "secoondary-dark": "var(--secoondary-dark)",
                accent: "var(--accent)",
                black: "var(--black)",
                "dark-grey": "var(--dark-grey)",
                "dark-grey-2": "var(--dark-grey-2)",
                "secondary-dark-grey": "var(--secondary-dark-grey)",
                grey: "var(--grey)",
                "light-grey": "var(--light-grey)",
                "gray-2": "var(--gray-2)",
            },
            animation: {
                bounce200: "bounce 1s infinite 200ms",
                bounce400: "bounce 1s infinite 400ms",
            },
            gridTemplateColumns: { 
                1: "repeat(1, minmax(0, 1fr))" ,
                2: "repeat(2, minmax(0, 1fr))" ,
                3: "repeat(3, minmax(0, 1fr))" ,
                4: "repeat(4, minmax(0, 1fr))" ,
                5: "repeat(5, minmax(0, 1fr))" ,
                6: "repeat(6, minmax(0, 1fr))" ,
                7: "repeat(7, minmax(0, 1fr))" ,
                8: "repeat(8, minmax(0, 1fr))" ,
                9: "repeat(9, minmax(0, 1fr))" ,
                10: "repeat(10, minmax(0, 1fr))" ,
                11: "repeat(11, minmax(0, 1fr))" ,
                12: "repeat(12, minmax(0, 1fr))" ,
            },
        },
    },
    variants: {
        extend: {
            textDecoration: ["focus-visible"],
            opacity: ["disabled"],
            cursor: ["disabled"],
        },
    },
    plugins: [require("@tailwindcss/forms")],
};

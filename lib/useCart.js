import useSWR from "swr";

export default function useCart(user) {
  const { data: cart, mutate: mutateCart, isValidating } = useSWR(user?.isLoggedIn && `/api/cart`);

  const loadingCart = cart === undefined;

  return { cart, loadingCart, mutateCart };
}
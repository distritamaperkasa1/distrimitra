import useSWR from "swr";
import useUser from "./useUser";

export default function useOrder() {
    const { user } = useUser({ redirectTo: "/login" });
    const { data: customer, mutate: mutateCustomer, isValidating } = useSWR(user?.isLoggedIn && `/api/customer`);

    const loadingCustomer = customer === undefined;

    return { customer, loadingCustomer, mutateCustomer };
}
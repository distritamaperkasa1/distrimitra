import useSWR from "swr";

export default function useRegions() {
  const { data: regions } = useSWR(`/api/address/region`);

  const loadingRegions = regions === undefined;

  return { regions, loadingRegions };
}
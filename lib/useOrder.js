import useSWR from "swr";
import useUser from "./useUser";

export default function useOrder() {
    const { user } = useUser({ redirectTo: "/login" });
    const { data: order, mutate: mutateOrder, isValidating } = useSWR(user?.isLoggedIn && `/api/order?pageSize=10&currentPage=1`);

    const loadingOrder = order === undefined;

    return { order, loadingOrder, mutateOrder };
}
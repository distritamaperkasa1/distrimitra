import useSWR from "swr";
import { ListObjectGroup } from "../lib/helper"

export default function useCity(search) {
  const { data: cities } = useSWR(`/api/address/city?search=${search.length > 3 ? search : ''}`);

  const loadingCities = cities === undefined;

  // const cities = !loadingCities ? ListObjectGroup({ listObject: citiesRes, key: 'city_id' }) : {};

  return { cities: cities, loadingCities };
}
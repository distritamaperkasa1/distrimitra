import React from "react";
import moment from "moment";

export const objectString = (data, convert) => {
   if (!convert) {
      let newdata = [];
      for (const key in data) {
         if (typeof data[key] === "object") {
            for (const kd in data[key]) {
               if (typeof data[key][kd] === "object") {
                  for (const kdd in data[key][kd]) {
                     newdata.push(
                        encodeURIComponent(key) +
                           "[" +
                           encodeURIComponent(kd) +
                           "]" +
                           "[" +
                           encodeURIComponent(kdd) +
                           "]=" +
                           encodeURIComponent(data[key][kd][kdd])
                     );
                  }
               } else {
                  newdata.push(
                     encodeURIComponent(key) +
                        "[" +
                        encodeURIComponent(kd) +
                        "]=" +
                        encodeURIComponent(data[key][kd])
                  );
               }
            }
         } else {
            newdata.push(
               encodeURIComponent(key) + "=" + encodeURIComponent(data[key])
            );
         }
      }
      return "?" + newdata.join("&");
   } else {
      if (data) {
         data = data.replace("?", "");
         if (data) {
            data = data.split("&");
            let newdata = [];
            for (const key in data) {
               let dk = data[key].split("=");
               let dkk = [];
               dkk[decodeURIComponent(dk[0])] = decodeURIComponent(dk[1]);
               newdata.push(dkk);
            }

            return newdata;
         } else {
            return false;
         }
      }
   }
};

export const isEmptyObj = (obj) => {
   for (var prop in obj) {
      if (obj.hasOwnProperty(prop)) {
         return false;
      }
   }

   return JSON.stringify(obj) === JSON.stringify({});
};

export const moneyFormat = (price) => {
   return (price * 1).toLocaleString('in-ID', { 
      style: 'currency', 
      currency: 'IDR', 
      maximumFractionDigits: 0, 
      minimumFractionDigits: 0 
   });
};

export const ListObjectGroup = ({listObject, key}) => {
   return listObject.reduce((prev, x) => {
      (prev[x[key]] = prev[x[key]] || []).push(x);
      return prev;
   }, {});
}

export const dateFormat = (date) => {
   const d = new Date(date);
   return `${d.getDate()}/${d.getMonth()}/${d.getFullYear()}`;
}

export const detailDateFormat = (date) => {
   const d = new Date(date);
   const fullMonth = d.toLocaleString('default', { month: 'long' });
   return `${d.getDate()} ${fullMonth}, ${d.getFullYear()}`;
}

export const searchAddress = (listAddress, id) => {
   if (!listAddress || !id) {
      return false;
   }
   
   let addressFound = "";
   listAddress.forEach((address, index) => {
      if (address.id == id) {
         addressFound = {...address, index};
      }
   });

   return addressFound
}

export const searchDefaultAddress = (listAddress, defaultId) => {
   let defaultContent = <p>You have not set a default address</p>;

   if (!listAddress || !defaultId) {
      return defaultContent;
   }
   
   let defaultAddress = "";
   listAddress.forEach(address => {
       if (address.id == defaultId) {
           defaultAddress = address;
       }
   });
   return formatAddress(defaultAddress)
}

export const formatAddress = (address) => {
   const { firstname, lastname, street, city, region, postcode, telephone } = address;
   return (
       <p>
           {`${firstname} ${lastname}`} <br/>
           {street.map((street, index) => {
               return (
                   <React.Fragment key={index}>
                       <span>{street}</span> <br/>
                   </React.Fragment>
               )
           })}
           <span>{`${city}, `}</span><br/>
           <span>{`${region.region || region}, ${postcode}`}</span><br/>
           <span>{telephone}</span>
       </p>
   )
}

export const isDateExpired = (date) => {
   return moment().isAfter(date);
}